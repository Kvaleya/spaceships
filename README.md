## Build

### Server

Nejdřív je potřeba aspoň jednou spustit server, aby se vygeneroval `KSP.Spaceships/ClientInterface/server_comm.proto`. Server se pustí prostě:

```
cd KSP.Spaceships
dotnet run
```

### Frontend

```
cd KSP.Spaceships/frontend
npm install
npm run build
```

Pokud se vám nechce buildit frontend lokálně, někdo z přítomných vám ho nakopíruje.

### Example client (C#)

```
cd KSP.Spaceships.ClientExample
dotnet run
```

### Example client (Typescript)

```
cd TypescriptClient
yarn install
yarn build
```

## Protokol

Viz example client pro příklad použití. Všechny typy jsou popsané v `KSP.Spaceships/ClientInterface/DataClasses.cs` pro C#, případně v `KSP.Spaceships/frontend/src/protocol/types.ts` pro typescript.
