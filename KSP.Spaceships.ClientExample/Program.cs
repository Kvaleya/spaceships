﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Numerics;

namespace KSP.Spaceships.ClientExample
{
	class WebSocketHelper : IDisposable
	{
		// Server má několik websocket api, liší se jen tím,
		// v jakém formátu posílají/přijímají data.
		// Pro nás jsou relevantní tyto:
		// - /ws: server posílá i přijímá json
		// - /ws_hybrid: server posílá gzipovaný json, od klienta očekává json bez komprese
		// Pokud je to možné, používejte hybrid, aby se šetřila propustnost sítě.
		// Odpovědi serveru jsou při běžící hře poměrně velké.
		const bool UseHybrid = false;

		public ClientWebSocket WebSocket;
		byte[] _bufferRecieve = new byte[1 << 24];
		bool _closed = false;

		public WebSocketHelper()
		{
		}

		public async Task ConnectAsync(string url)
		{
			WebSocket = new ClientWebSocket();
			await WebSocket.ConnectAsync(new Uri(url + (UseHybrid ? "ws_hybrid" : "ws")), CancellationToken.None);
			//Console.WriteLine("Připojeno");
		}

		public async Task<ServerResponse?> ReceiveAsync()
		{
			if (WebSocket == null || WebSocket.State != WebSocketState.Open || _closed)
				return null;

			ValueWebSocketReceiveResult lastResult = new();
			var index = 0;
			var done = false;
			while (!done)
			{
				if (index >= _bufferRecieve.Length)
				{
					var newBuffer = new byte[_bufferRecieve.Length * 2];
					Array.Copy(_bufferRecieve, newBuffer, _bufferRecieve.Length);
					_bufferRecieve = newBuffer;
				}
				lastResult = await WebSocket.ReceiveAsync(_bufferRecieve.AsMemory(startIndex: index), CancellationToken.None);
				index += lastResult.Count;
				done = lastResult.EndOfMessage;
			}
			var totalBytes = index;

			if (lastResult.MessageType == WebSocketMessageType.Close)
			{
				await WebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, null, CancellationToken.None);
				_closed = true;
				return null;
			}
			else
			{
				string msg = string.Empty;

				if(UseHybrid)
				{
					// Expect gzipped json
					using (var ms = new MemoryStream(_bufferRecieve, 0, totalBytes))
					using (var decompressed = new MemoryStream())
					using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress))
					{
						gzip.CopyTo(decompressed);
						msg = System.Text.Encoding.UTF8.GetString(decompressed.ToArray());
					}
				}
				else
				{
					// Expect json
					msg = System.Text.Encoding.UTF8.GetString(_bufferRecieve, 0, totalBytes);
				}

				var state = JsonSerializer.Deserialize<ServerResponse>(msg);
				return state;
			}
		}

		int _sends = 0;

		public async Task SendAsync(ClientRequest request)
		{
			//Console.WriteLine($"Posílám ({_sends})");
			_sends++;
			var data = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(request));
			await WebSocket.SendAsync(data, WebSocketMessageType.Text, true, CancellationToken.None);
		}

		private void DisposeWs()
		{
			if(WebSocket != null)
			{
				WebSocket.Dispose();
				WebSocket = null;
			}
		}

		public void Dispose()
		{
			DisposeWs();
		}
	}

	class Program
	{
		const string ServerUrl = "ws://localhost:5000/api/";
		const string UserIdFile = "userid.txt";
		const string PlayerName = "SuperHroch";
		const string ShipName = "Hipporion";

		static string GetUserId()
		{
			if(File.Exists(UserIdFile))
			{
				return File.ReadAllText(UserIdFile);
			}
			return "";
		}

		static void StoreUserId(string userid)
		{
			File.WriteAllText(UserIdFile, userid);
		}

		static async Task Main(string[] args)
		{
			using (var ws = new WebSocketHelper())
			{
				try
				{
					await ws.ConnectAsync(ServerUrl);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.ToString());
					return;
				}

				Console.WriteLine("Connected!");

				// Nejdřív je potřeba pořídit si validni userId.
				// UserId si budeme pamatovat v souboru.
				// Pak můžeme hrát za stejného hráče i mezi různými běhy programu.
				string userId = GetUserId();
				Console.WriteLine($"Current user id: {userId}");

				// To se dělá tak, že serveru prostě něco pošleme s (ne)vyplněným userId,
				// a příští request nám vrátí, jaké userId máme používat
				// (typicky vrátí co samé co jsme mu poslali).
				await ws.SendAsync(new ClientRequest()
				{
					nameChange = PlayerName,
					userId = userId,
				});

				var response = await ws.ReceiveAsync();

				userId = response.playerGameState.playerUserId; // Toto je naše nové userId
				StoreUserId(userId); // Přijaté userId si zapíšeme pro příště
				Console.WriteLine($"Recieved user id: {userId}");

				// Poté si vyrobíme vlastní loď.
				// Pokud už jsme v lodi kýženého jména (z předchozího běhu programu), server nás v ní nechá a nic se nestane.
				await ws.SendAsync(new ClientRequest()
				{
					userId = userId,
					shipAssignment = new RequestShipAssignment()
					{
						requestedShipName = ShipName,
					}
				});

				bool firstTick = true;

				// Nyní počkáme než začne hra.
				while(true)
				{
					// Normálně nám server odpoví okamžitě na libovolný request.
					// Ale pokud běží hra, tak odpověď od serveru dostaneme jen na každý tick,
					// Jinak by přesně tento pattern nekonečné smyčky na klientovi co odpoví na každý request
					// celou hru zahltil.
					response = await ws.ReceiveAsync();
					if(response == null)
					{
						Console.WriteLine("Server ukončil spojení.");
						break;
					}

					if(response.mainGameState == null)
					{
						// Dokud nám server nepošle hlavní stav hry, čekáme
						continue;
					}

					// Nyní běží hra!
					var currentPos = response.mainGameState.shipState.position;
					var finishPos = response.mainGameState.raceFinishPosition;

					if(response.mainGameState.raceFinishRadius < 0)
					{
						// Pokud nejsou závody, letíme doprostřed
						finishPos = new Vec2()
						{
							x = response.worldSize.x / 2,
							y = response.worldSize.y / 2,
						};
					}

					// Spočítáme vektor k cíli
					double toFinishX = finishPos.x - currentPos.x;
					double toFinishY = finishPos.y - currentPos.y;

					// Normalizujeme
					double length = Math.Sqrt(toFinishX * toFinishX + toFinishY * toFinishY);
					toFinishX /= length;
					toFinishY /= length;

					// Vynásobíme maximální rychlostí lodi
					var maxSpeed = response.mainGameState.systems.engines.maxVelocity;
					toFinishX *= maxSpeed;
					toFinishY *= maxSpeed;


					var request = new ClientRequest()
					{
						userId = userId,
						shipControl = new RequestShipControl()
						{
							systemState = new RequestSystemState(),
							powerState = new RequestPowerState(),
						}
					};

					// Motory na 100% směrem k cíli!
					request = request with
					{
						// Pro účely praktického C# bota bude možná lepší si ty struktury ze serveru
						// vykrást a zbavit se jejich immutability, tady jenom překáží...
						shipControl = request.shipControl with
						{
							// Motory mají by-default věškerou nepřiřazenou energii,
							// jelikož jiný systém nepotřebujeme, tak s přiřazováním energie nic neděláme.
							systemState = request.shipControl.systemState with
							{
								engines = new SystemEnginesCommand()
								{
									// U motorů se neovládá cílené zrychlení, ale cílená rychlost.
									// Motory na tuto rychlost zrychlí automaticky.
									// Viz: https://gitlab.com/Kvaleya/spaceships/-/blob/zavody/KSP.Spaceships/Game/ShipSystems.cs#L315
									// Také se hodí vědět, že celá simulace běží v diskrétních krocích a žádné hodnoty neintegruje,
									// takže zrychlení se počítá v_new = v_old + a * deltaT, pozice p_new = p_old + v * deltaT, atd.
									desiredVelocity = new Vec2()
									{
										x = toFinishX,
										y = toFinishY,
									},
									enabled = true,
								}
							}
						}
					};

					// Příklad ovládání zbraní, pokud někdy bude relevantní...
					// Nevylučuju, že na detekci gamemódu (jestli je závod nebo ne) ještě nepřidám rozumné api
					if (response.mainGameState.raceFinishRadius < 0)
					{
						request = request with
						{
							shipControl = request.shipControl with
							{
								powerState = new RequestPowerState()
								{
									weaponLaser = new SystemPowerCommand()
									{
										targetPower = response.mainGameState.systems.weaponLaser?.powerState?.powerStates?.Last() ?? 0,
									}
								}
							}
						};

						// Jakmile je zbraň nabitá, vystřelíme.
						if(response.mainGameState.systems.weaponLaser.chargeState > 0.9999)
						{
							request = request with
							{
								shipControl = request.shipControl with
								{
									systemState = request.shipControl.systemState with
									{
										weaponLaser = new SystemWeaponFireCommand()
										{
											// Vystřelíme do pravého dolního rohu světa
											targetPosition = response.worldSize,
										}
									}
								}
							};
						}
					}

					// Následující demonstruje kde číst další užitečná data o mapě.
					// Vypíšeme do konzole nějaké věci o mapě. Abychom konzoli nespamovali pořád, uděláme to jen první tick.
					if (firstTick)
					{
						// Další užitečné data
						// Simulace tickne každých tickIntervalSeconds sekund, typická hodnota je 0.1 (tedy 10 Hz)
						var tickIntervalSeconds = response.mainGameState.tickIntervalSeconds;
						// Simulace každý tick simuluje (by default) tickIntervalSeconds herního času, ale tato hodnota se může
						// vynásobit timeScale. Typická hodnota timeScale je 1, ale např. při hodnotě 2
						// by byla deltaT každého ticku 2x větší, a hra by běžela 2x rychleji
						// (ale simulace by stále tickala při 10 hz reálného času).
						var tickDeltaTofGameTime = response.mainGameState.timeScale * tickIntervalSeconds;

						// Loď ví o světě kolem díky sensorům. Má 3 typy sensorů (viz manuál pro detaily).
						// Pro účely závodu nám stačí používat "kamery".
						// "signatures" je seznam věcí co sensor vidí.
						var visibleEntities = response.mainGameState.systems.sensorCameras.signatures;

						Console.WriteLine($"Simulace běží na {1 / tickIntervalSeconds} Hz, jeden tick počítá deltaT = {tickDeltaTofGameTime} sekund.");

						int stillAsteroids = 0;
						int movingAsteroids = 0;

						foreach (var s in visibleEntities)
						{
							if(s.description == "Race Finish")
							{
								// Cíl závodu je entita, která každou příchozí loď zničí a vypíše její pořadí.
								// Vy ji můžete vesele ignorovat, protože pozici cíle dostáváte i v mainGameState,
								// viz výše, kde se používá pro spočítání směru letu lodi.
								// Zde tuto entitu jen potřebujeme oddělit od ostatních popsaných entit.
							}
							else if(!string.IsNullOrEmpty(s.description))
							{
								// Ve hře (záměrně) nejdou od sebe snadno rozlišit typy entit.
								// Lodě lze pro účely závodu detekovat tak, že mají neprázdné description.
								Console.WriteLine($"Nalezena cizí loď: {s.description}");
							}
							else
							{
								var velocityVec = new Vector2((float)s.velocity.x, (float)s.velocity.y);

								// Každý signature v sobě má také uloženo:
								// s.radius
								// s.position

								if (velocityVec.LengthSquared() < 0.00001f)
								{
									// V mapě je nějaký počet statických asteroidů, jsou (aspoň 2x) větší než loď a mají nulovou rychlost.
									// Tyto asteroidy se nikdy nehýbou.
									stillAsteroids++;
								}
								else
								{
									movingAsteroids++;
								}
							}
						}

						Console.WriteLine($"Nalezeno {stillAsteroids} stálých a {movingAsteroids} pohyblivých asteroidů.");

						firstTick = false;
					}

					// Odešleme
					await ws.SendAsync(request);
				}
			}
		}
	}
}