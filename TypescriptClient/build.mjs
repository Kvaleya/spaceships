#!/usr/bin/env node

import esbuild from "esbuild";
import esbuildSvelte from "esbuild-svelte";
import sveltePreprocess from "svelte-preprocess";

const debug = true
const serve = true
const servePort = 8012

esbuild.build({
		entryPoints: ["bot.ts"],
		mainFields: ["svelte", "browser", "module", "main"],
		bundle: true,
		sourcemap: true,
		treeShaking: true,
		outdir: "./static/build",
		target: ['es2022'],
		watch: serve,
		minify: !debug,
		plugins: [
			esbuildSvelte({
				preprocess: sveltePreprocess(),
			}),
		],
	})
	.catch(() => process.exit(1));


if (serve) {
	esbuild.serve({ servedir: "static", port: servePort }, { }).then((server) => {
		console.log(`Debug server running on http://localhost:${servePort}`)
	})
}
