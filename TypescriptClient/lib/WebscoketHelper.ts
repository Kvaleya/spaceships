import * as pako from 'pako';
import { Extension } from 'typescript';
import type { ClientRequest, ServerResponse } from '../../KSP.Spaceships/frontend/src/protocol/types';

export class WebsocketHelper
{
    private _ws: WebSocket;
    private _recieveQueue: ServerResponse[] = [];
    private _resolveOnRecieveQueue: ((value: ServerResponse) => void)[] = [];
    private _closed: boolean = false;

    get closed(): boolean
    {
        return this._closed;
    }

    public async connect(url: string): Promise<void>
    {
        return new Promise((resolve, reject) => {
            this.connectWs(url, (data: Blob) => data.arrayBuffer().then(buf => this.onRecieve(buf)), (ws: WebSocket) => {
                this._ws = ws;
                resolve();
            });
        });
    }

    public close()
    {
        if(!this._closed)
        {
            this._ws.close();
        }
    }

    public send(request: ClientRequest)
    {
        if(this._closed)
        {
            throw new Error("Websocket is closed.");
        }
        this._ws.send(JSON.stringify(request));
    }

    public async recieve(): Promise<ServerResponse>
    {
        return new Promise((resolve, reject) => {
            if(this._closed)
            {
                reject("Websocket is closed.");
            }

            if(this._recieveQueue.length > 0)
            {
                resolve(this._recieveQueue.shift());
            }
            else
            {
                this._resolveOnRecieveQueue.push(resolve);
            }
        });
    }

    private connectWs(target: string, ondata: Function, onconnected: Function = undefined)
	{
		// https://stackoverflow.com/questions/22431751/websocket-how-to-automatically-reconnect-after-it-dies
		let ws = new WebSocket(target);
		ws.onopen = () => {
			//console.log("WS: opened")
			if(onconnected)
				onconnected(ws);
		};
		ws.onmessage = (event) => {
			ondata(event.data);
		};
		ws.onerror = (error) => {
			console.error("WS: error: ", error, "Closing");
			this._closed = true;
            ws.close();
		};
		ws.onclose = (event) => {
            this._closed = true;
            //console.log("WS: closed.");
			// console.log("WS: closed. Attempting to reconnect...");
			// setTimeout(() => {
			// 	this.connectWs(target, ondata, onconnected);
			// }, 1000);
		};
	}

	private onRecieve(buf: ArrayBuffer)
	{
        const bytes = new Uint8Array(buf, 0, buf.byteLength);
		const uncompressed = pako.inflate(bytes);
        const response = JSON.parse(new TextDecoder().decode(uncompressed)) as ServerResponse;

        if(this._resolveOnRecieveQueue.length > 0)
        {
            const firstResolve = this._resolveOnRecieveQueue.shift();
            firstResolve(response);
        }
        else
        {
            this._recieveQueue.push(response);
        }
	}
}
