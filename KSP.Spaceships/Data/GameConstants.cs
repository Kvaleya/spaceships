using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	public class GameConstants
	{
		// TODO: put all game related numeric stuff here

		//
		// Game administration
		//

		public string PrivilegePassword { get; set; } = "";//"lanius"; // TODO: better password

		/// <summary>
		/// In real time seconds.
		/// </summary>
		public double MaxGameTime { get; set; } = 600;

		public int MaxNameChars { get; set; } = 32;
		public int MaxChatChars { get; set; } = 1000;

		public double RaceFinishRadius { get; set; } = 15.0;

		/// <summary>
		/// In seconds.
		/// </summary>
		public double PointerLifetime = 10;

		/// <summary>
		/// Takes effect for new players only.
		/// For debugging only.
		/// </summary>
		public bool EveryoneIsPrivileged { get; set; } = true; // TODO: false for real games

		public bool AllowPartiallyMannedShips { get; set; } = true;
		public bool AllowUnmannedShips { get; set; } = false;

		public bool AllowAnyoneToUsePrivilegedShips { get; set; } = true;

		/// <summary>
		/// This option stops multiple radars from sending duplicate signatures.
		/// It caused some NullReferenceExceptions during main play.
		/// This property makes it possible to disable it in-game.
		/// </summary>
		public bool RadarCompaction { get; set; } = true;

		public double GetTimeDeltaPerTick()
		{
			return TimeScale * TickIntervalSeconds;
		}

		//
		// Simulation controls
		//

		public double TickIntervalSeconds { get; set; } = 0.1;
		public double TimeScale { get; set; } = 1.0;

		//
		// World settings
		//

		public int WorldSizeXY { get; set; } = 300;
		public double WorldAsteroidRingRadiusScale { get; set; } = 0.9;
		public double WorldAsteroidMassFromRadiusPow { get; set; } = 3.0;
		public double WorldAsteroidHealthFromMassScale { get; set; } = 0.2;
		public int WorldAsteroidSeedOffset { get; set; } = 0;
		public AsteroidType[] WorldAsteroidTypes { get; set; } = new AsteroidType[]
		{
			new AsteroidType() // Immovable
			{
				Density = 15e-4,
				RadiusMin = 5.0,
				RadiusMax = 20.0,
				VelocityScale = 0.0,
				RandomSeed = 9, // 6, 9 are goodish seeds for defense
				IsImmovable = true,
				OutOfRingPow = 0.3,
				NormalizedRingScale = 1.0,
			},
			new AsteroidType() // Small
			{
				Density = 10e-4,
				RadiusMin = 0.25,
				RadiusMax = 0.75,
				VelocityScale = 1.5,
				RandomSeed = 1001,
				IsImmovable = false,
			},
			new AsteroidType() // Medium
			{
				Density = 5e-4,
				RadiusMin = 1.5,
				RadiusMax = 2.5,
				VelocityScale = 1.5,
				RandomSeed = 2001,
				IsImmovable = false,
			},
			new AsteroidType() // Ship-sized
			{
				Density = 8e-4,
				RadiusMin = 0.85,
				RadiusMax = 1.15,
				VelocityScale = 1.5,
				RandomSeed = 3001,
				IsImmovable = false,
			},
		};

		public double ExplosionDebrisDensity { get; set; } = 0.08;
		public double ExplosionDebrisVelocity { get; set; } = 8.0;
		public double ExplosionDebrisIntensity { get; set; } = 1.0;
		public double ExplosionDebrisIntensityLoss { get; set; } = 0.45;
		public double ExplosionDebrisRadius { get; set; } = 0.5;

		public double ImpactDebrisVelocity { get; set; } = 10.0;
		public double ImpactDebrisVelocityRandom { get; set; } = 4.0;
		public double ImpactDebrisIntensity { get; set; } = 1.0;
		public double ImpactDebrisIntensityLoss { get; set; } = 0.95;
		public double ImpactDebrisRadius { get; set; } = 0.5;

		public double ShadowBanRequestProb { get; set; } = 0.1;
		public double ShadowBanTickProb { get; set; } = 0.1;

		//
		// Spawn settings
		//

		public double SpawnRingRadiusScale { get; set; } = 0.995;
		public double SpawnBaseShipSpacing { get; set; } = 1.0 / 180.0 * Math.PI;

		//
		// Defense gamemode settings
		//

		public int DefenseStationCount { get; set; } = 3;

		/// <summary>
		/// How much closer to defenders the stations will be, between 0..1
		/// </summary>
		public double DefenseStationPlacementBias { get; set; } = 0.35;

		/// <summary>
		/// Stations will be randomly places along a diagonal line (-1,1 .. 1,-1) with max spread of this value (1.0 = along the entire diagonal, 0.2 = along 20% of it).
		/// </summary>
		public double DefenseStationPlacementDistance { get; set; } = 0.4;

		/// <summary>
		/// How much closer to the center will defenders spawn (0.0 = center, 1.0 = base spawn circle)
		/// </summary>
		public double DefenseDefenderSpawnDistanceMultiplier { get; set; } = 1.0;

		public ShipConsts Ship { get; set; } = new ShipConsts();
		public ShipConsts Station { get; set; } = ShipConsts.GetStationConsts();
		public ShipConsts RaceShip { get; set; } = ShipConsts.GetRaceShipConsts();

		public double ShipFartIntensity { get; set; } = 1.0;
		public double ShipFartDecay { get; set; } = 0.6;
		public double ShipFartRandomVelocity { get; set; } = 0.5;
		public double ShipFartVelocityScale { get; set; } = 1.5;
		public double ShipFartRadius { get; set; } = 0.2;

		//
		// Role -> systems mapping
		//

		public string[][] RoleSystems { get; set; } =
		{
			new string[]
			{
				nameof(ShipEnt.SystemSensorCamera),
				//nameof(ShipEnt.SystemSensorInfrared), // Obsolete
				nameof(ShipEnt.SystemSensorRadar),
				//nameof(ShipEnt.SystemSensorRadarSmall), // No need for small radar, we have big radar!
				nameof(ShipEnt.SystemSensorProbe),

				nameof(ShipEnt.SystemWeaponLaser),
				nameof(ShipEnt.SystemWeaponPlasma),
				nameof(ShipEnt.SystemWeaponMissiles),
				nameof(ShipEnt.SystemWeaponMines),
				nameof(ShipEnt.SystemWeaponProbes),

				nameof(ShipEnt.SystemRemoteMissiles),

				nameof(ShipEnt.SystemShields),
				nameof(ShipEnt.SystemStealth),
				nameof(ShipEnt.SystemAutoRepair),
				nameof(ShipEnt.SystemEngines),
			},
			new string[] // TODO
			{
				nameof(ShipEnt.SystemSensorCamera),
				//nameof(ShipEnt.SystemSensorInfrared),
				//nameof(ShipEnt.SystemSensorRadar),
				nameof(ShipEnt.SystemSensorRadarSmall),
				nameof(ShipEnt.SystemSensorProbe),

				//nameof(ShipEnt.SystemWeaponLaser),
				//nameof(ShipEnt.SystemWeaponPlasma),
				//nameof(ShipEnt.SystemWeaponMissiles),
				//nameof(ShipEnt.SystemWeaponMines),
				//nameof(ShipEnt.SystemWeaponProbes),

				//nameof(ShipEnt.SystemRemoteMissiles),

				nameof(ShipEnt.SystemShields),
				//nameof(ShipEnt.SystemStealth),
				//nameof(ShipEnt.SystemAutoRepair),
				nameof(ShipEnt.SystemEngines),
			},
			new string[] // TODO
			{
				//nameof(ShipEnt.SystemSensorCamera),
				//nameof(ShipEnt.SystemSensorInfrared),
				//nameof(ShipEnt.SystemSensorRadar),
				nameof(ShipEnt.SystemSensorRadarSmall),
				nameof(ShipEnt.SystemSensorProbe),

				nameof(ShipEnt.SystemWeaponLaser),
				nameof(ShipEnt.SystemWeaponPlasma),
				//nameof(ShipEnt.SystemWeaponMissiles),
				nameof(ShipEnt.SystemWeaponMines),
				nameof(ShipEnt.SystemWeaponProbes),

				//nameof(ShipEnt.SystemRemoteMissiles),

				//nameof(ShipEnt.SystemShields),
				//nameof(ShipEnt.SystemStealth),
				//nameof(ShipEnt.SystemAutoRepair),
				//nameof(ShipEnt.SystemEngines),

			},
			new string[] // TODO
			{
				//nameof(ShipEnt.SystemSensorCamera),
				//nameof(ShipEnt.SystemSensorInfrared),
				nameof(ShipEnt.SystemSensorRadar),
				//nameof(ShipEnt.SystemSensorRadarSmall),
				nameof(ShipEnt.SystemSensorProbe),

				//nameof(ShipEnt.SystemWeaponLaser),
				//nameof(ShipEnt.SystemWeaponPlasma),
				nameof(ShipEnt.SystemWeaponMissiles),
				//nameof(ShipEnt.SystemWeaponMines),
				//nameof(ShipEnt.SystemWeaponProbes),

				nameof(ShipEnt.SystemRemoteMissiles),

				//nameof(ShipEnt.SystemShields),
				nameof(ShipEnt.SystemStealth),
				nameof(ShipEnt.SystemAutoRepair),
				//nameof(ShipEnt.SystemEngines),
			},
			new string[] // Two man A
			{
				nameof(ShipEnt.SystemSensorCamera),
				nameof(ShipEnt.SystemSensorRadar),
				//nameof(ShipEnt.SystemSensorRadarSmall),
				//nameof(ShipEnt.SystemSensorProbe),

				//nameof(ShipEnt.SystemWeaponLaser),
				//nameof(ShipEnt.SystemWeaponPlasma),
				//nameof(ShipEnt.SystemWeaponMissiles),
				//nameof(ShipEnt.SystemWeaponMines),
				//nameof(ShipEnt.SystemWeaponProbes),

				//nameof(ShipEnt.SystemRemoteMissiles),

				nameof(ShipEnt.SystemShields),
				nameof(ShipEnt.SystemStealth),
				nameof(ShipEnt.SystemAutoRepair),
				nameof(ShipEnt.SystemEngines),
			},
			new string[] // Two man B
			{
				nameof(ShipEnt.SystemSensorCamera),
				nameof(ShipEnt.SystemSensorRadar),
				//nameof(ShipEnt.SystemSensorRadarSmall),
				nameof(ShipEnt.SystemSensorProbe),

				nameof(ShipEnt.SystemWeaponLaser),
				nameof(ShipEnt.SystemWeaponPlasma),
				nameof(ShipEnt.SystemWeaponMissiles),
				nameof(ShipEnt.SystemWeaponMines),
				nameof(ShipEnt.SystemWeaponProbes),

				nameof(ShipEnt.SystemRemoteMissiles),

				//nameof(ShipEnt.SystemShields),
				//nameof(ShipEnt.SystemStealth),
				//nameof(ShipEnt.SystemAutoRepair),
				//nameof(ShipEnt.SystemEngines),
			},
		};

		//
		// Names and other pretty strings
		//

		public string[] InicialCommonShips { get; set; } =
		{
			"Akka", // TODO: better names. Replace with references to whatever you wish.
			"Bane",
			"Chiron",
			"Deimos",
			"Etna",

			"Feros",
			"Gamma",
			"Hosk",
			"Ibri", // Akka..Ibri are from Planetside. But replace at will.
			"Juktas",

			"Kios",
			"Logain",
			"Methana",
			"Nysa",
			"Orome",

			"Paros",
			"Qualian", // Enderal
			"Rysn", // Stormlight
			"Soli",
			"Tuon",

			"Unn",
			"Venli",
			"Wyrn",
			"Xelas",
			"Yavanna",

			"Zote", // HK
		};

		public string[] InicialPrivilegedShips { get; set; } =
		{
			"Annihilator", // TODO: better names
			"Compilator",
			"Taurovenator",
			"Decimator",
			"Exteminator",

			"Prokrastinator",
			"Obfuscator",
			"Iterator",
			"Desalinator",
			"Enumerator",

			"Irrigator",
			"Baconator",
		};

		public string[] InicialTwoManShips { get; set; } =
		{
			"Ship00",
			"Ship01",
			"Ship02",
			"Ship03",
			"Ship04",
			"Ship05",
			"Ship06",
			"Ship07",
			"Ship08",
			"Ship09",
		};

		public GameStrings Strings { get; set; } = GameStrings.CzechStrings();
	}

	//
	// Ship stuff
	//

	public class ShipConsts
	{
		//
		// Ship properties
		//

		public bool IsStation { get; set; } = false;

		public bool AllowWeapons { get; set; } = true;

		public double ShipRadius { get; set; } = 1;
		public double ShipMass { get; set; } = 1;

		public int ShipReactorPowerUnits { get; set; } = 25;
		public double ShipMaxHealth { get; set; } = 1;
		public double ShipStartingHealth { get; set; } = 0.6;

		public double ShipExplosionInnerDamage { get; set; } = 0.26;
		public double ShipExplosionInnerRadius { get; set; } = 8.0;
		public double ShipExplosionOuterRadius { get; set; } = 16.0;

		// In real time seconds
		public double ShipSpawnImmortalityTime { get; set; } = 10.0;

		// Sensors
		public double SensorRadiusCameras { get; set; } = 30;
		public double SensorRadiusInfrared { get; set; } = 0; // Disabled
		public double SensorRadiusRadar { get; set; } = 170;
		public double SensorRadiusRadarSmall { get; set; } = 100;
		public double SensorRadiusProbe { get; set; } = 60;
		public double SensorRadiusMissile { get; set; } = 10; // TODO: missile sensor?

		//
		// Weapons
		//

		// Laser
		public int SystemWeaponLaserInitialPower = 0;
		public int[] SystemWeaponsPowerStatesLaser { get; set; } = { 0, 1, 2, 3, 4 }; // Allow a low-power mode
		public double SystemWeaponChargeSpeedLaser { get; set; } = 1.0 / 2.5; // Fast charge, low damage

		public double SystemWeaponLaserAutoFireRange { get; set; } = -1.0;
		public double SystemWeaponLaserAimAssistRange { get; set; } = 6.0;

		public double SystemWeaponLaserDamage { get; set; } = 0.13;
		// Cosmetic laser effect
		public double SystemWeaponLaserDebrisSpacing { get; set; } = 2.1;
		public double SystemWeaponLaserDebrisIntensity { get; set; } = 1.0;
		public double SystemWeaponLaserDebrisIntensityLoss { get; set; } = 0.2;
		public double SystemWeaponLaserDebrisRadius { get; set; } = 0.3;
		public double SystemWeaponLaserDebrisVelocity { get; set; } = 0.3;
		public double SystemWeaponLaserDebrisDirectionalVelocity { get; set; } = 1.5;
		public int SystemWeaponLaserImpactDebrisCount { get; set; } = 5;

		// Plasma
		public int[] SystemWeaponsPowerStatesPlasma { get; set; } = { 0, 1, 2, 3, 4 };
		public double SystemWeaponChargeSpeedPlasma { get; set; } = 1.0 / 5; // Slowish charge, huge damage if all shots hit

		public double SystemWeaponPlasmaProjectileRadius { get; set; } = 0.3;
		public double SystemWeaponPlasmaFireHeat { get; set; } = 1.0;
		public double SystemWeaponPlasmaFireDamage { get; set; } = 0.04;
		public int SystemWeaponPlasmaFireCount { get; set; } = 19;
		public double SystemWeaponPlasmaFireSpread { get; set; } = 13 / 180.0 * Math.PI;
		public double SystemWeaponPlasmaFireVelocity { get; set; } = 70.0;

		public int SystemWeaponPlasmaImpactDebrisCount { get; set; } = 2;

		// Missiles
		public int[] SystemWeaponsPowerStatesMissiles { get; set; } = { 0, 1, 2, 3, 4 };
		public double SystemWeaponChargeSpeedMissiles { get; set; } = 1.0 / 10;

		public double SystemWeaponMissilesExplosionInnerDamage { get; set; } = 0.45; // High damage
		public double SystemWeaponMissilesExplosionInnerRadius { get; set; } = 10.0; // High explosion radius
		public double SystemWeaponMissilesExplosionOuterRadius { get; set; } = 16.0;
		public double SystemWeaponMissilesExplodeDetectionRange { get; set; } = 3.0; // Small auto-boom for missiles
		public double SystemWeaponMissilesRadius { get; set; } = 0.4;
		public double SystemWeaponMissilesMass { get; set; } = 0.25;
		public double SystemWeaponMissilesHealth { get; set; } = 0.01; // Missiles get one-shot by anything
		public double SystemWeaponMissilesStartVelocity { get; set; } = 8.0;
		public double SystemWeaponMissilesAcceleration { get; set; } = 30.0;
		public double SystemWeaponMissilesMaxVelocity { get; set; } = 24.0;
		public double SystemWeaponMissilesHeatIntensity { get; set; } = 0.75;

		public bool SystemWeaponMissilesRequiredPowerToControl { get; set; } = false;

		// Mines
		public int[] SystemWeaponsPowerStatesMines { get; set; } = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		public double SystemWeaponChargeSpeedMines { get; set; } = 1.0 / 5; // Can deploy lots of mines fast, if so desired

		public double SystemWeaponMinesExplosionInnerDamage { get; set; } = 0.55; // Huge boom.
		public double SystemWeaponMinesExplosionInnerRadius { get; set; } = 12.0;
		public double SystemWeaponMinesExplosionOuterRadius { get; set; } = 18.0;
		public double SystemWeaponMinesDetectionRange { get; set; } = 6.0;
		public double SystemWeaponMinesRadius { get; set; } = 0.4;
		public double SystemWeaponMinesMass { get; set; } = 0.25;
		public double SystemWeaponMinesHealth { get; set; } = 0.01; // One-shot by anything
		public double SystemWeaponMinesStartVelocity { get; set; } = 5.0;
		public double SystemWeaponMinesAcceleration { get; set; } = 0.5;
		public double SystemWeaponMinesMaxVelocity { get; set; } = 0.0;
		public double SystemWeaponMinesHeatIntensity { get; set; } = 0.0; // No heat

		// Probes
		public int[] SystemWeaponsPowerStatesProbes { get; set; } = { 0, 1, 2, 3 };
		public double SystemWeaponChargeSpeedProbes { get; set; } = 1.0 / 8;

		public double SystemWeaponProbesExplosionInnerDamage { get; set; } = 0.16; // Does some damage upon being destroyed
		public double SystemWeaponProbesExplosionInnerRadius { get; set; } = 6.0;
		public double SystemWeaponProbesExplosionOuterRadius { get; set; } = 12.0;
		public double SystemWeaponProbesExplodeDetectionRange { get; set; } = -1.0; // No auto-boom
		public double SystemWeaponProbesRadius { get; set; } = 0.4; // Not sure if mine... or just a probe.
		public double SystemWeaponProbesMass { get; set; } = 0.25;
		public double SystemWeaponProbesHealth { get; set; } = 0.01;
		public double SystemWeaponProbesStartVelocity { get; set; } = 5.0;
		public double SystemWeaponProbesAcceleration { get; set; } = 0.5; // No remote controls
		public double SystemWeaponProbesMaxVelocity { get; set; } = 0.0;
		public double SystemWeaponProbesHeatIntensity { get; set; } = 0.0; // No heat

		public bool SystemWeaponProbesRequiredPowerToSee { get; set; } = false;

		// Other systems

		// Shields
		public int[] SystemShieldsPowerStates { get; set; } = { 0, 1, 2, 3, 4, 5, 6 };
		public double SystemShieldsMaxAbsorption { get; set; } = 0.3;
		public double SystemShieldsRegenerationAbsorptionPerPowerPerGameSecond { get; set; } = 0.005;
		public double SystemShieldsArcSize { get; set; } = 120.0 / 180.0 * Math.PI;
		public double SystemShieldsArcSizeOnemanned { get; set; } = 120.0 / 180.0 * Math.PI; // Anything over 6 radians is a full circle, right? For this game it is.

		// Stealth
		public int[] SystemStealthPowerStates { get; set; } = { 0, 1, 2, 3, 4 };
		public double SystemStealthDuration { get; set; } = 10.0;
		public double SystemStealthChargeBaseSpeed { get; set; } = 1.0 / 10;

		// Auto repair
		public int[] SystemAutoRepairPowerStates { get; set; } = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
		public double SystemAutoRepairRepairRate { get; set; } = 0.001;

		// Engines
		public double SystemEnginesAccelerationPerUnitPower { get; set; } = 1.0;
		public double SystemEnginesHeatPerAccelerationUnit { get; set; } = 0.1;
		public double SystemEnginesMaxVelocity { get; set; } = 12.0; // Slower than both missiles and plasma

		public static ShipConsts GetStationConsts()
		{
			var c = new ShipConsts()
			{
				IsStation = true,

				ShipRadius = 3,
				ShipMass = 200,
				ShipStartingHealth = 6,

				ShipExplosionInnerDamage = 0.55,
				ShipExplosionInnerRadius = 12.0,
				ShipExplosionOuterRadius = 24.0,

				SystemAutoRepairRepairRate = 0.0,

				SystemWeaponLaserAutoFireRange = 25.0,
				SystemWeaponChargeSpeedLaser = 1.0 / 8,
				SystemWeaponLaserInitialPower = 4,

				SystemWeaponMinesStartVelocity = 8.0,
				SystemWeaponMinesAcceleration = 0.5,

				SystemWeaponProbesAcceleration = 0.1,
				SystemWeaponProbesStartVelocity = 12.0,

				SystemWeaponPlasmaFireCount = 41,
				SystemWeaponPlasmaFireSpread = 20 / 180.0 * Math.PI,
				SystemWeaponChargeSpeedPlasma = 1.0 / 15,

				SystemShieldsArcSize = 0.00001,
				SystemShieldsMaxAbsorption = 0.0,
				SystemShieldsPowerStates = new int[] { 0, 1 },
				SystemAutoRepairPowerStates = new int[] { 0, 1 },
				SystemEnginesAccelerationPerUnitPower = 0.1,
				SystemEnginesMaxVelocity = 2,
				SystemStealthPowerStates = new int[] { 0, 1 },
				SystemStealthChargeBaseSpeed = 0.0,
			};

			c.ShipMaxHealth = c.ShipStartingHealth;

			return c;
		}

		public static ShipConsts GetRaceShipConsts()
		{
			const double visibleRadius = 10000;

			return new ShipConsts()
			{
				AllowWeapons = false,
				SystemEnginesMaxVelocity = 100.0,
				SensorRadiusRadar = visibleRadius,
				SensorRadiusRadarSmall = visibleRadius,
				SensorRadiusCameras = visibleRadius,
			};
		}
	}

	public class AsteroidType
	{
		public double Density { get; set; } = 0.007;
		public double RadiusMin { get; set; } = 4.0;
		public double RadiusMax { get; set; } = 15.0;
		public double VelocityScale { get; set; } = 1.0;
		public int RandomSeed { get; set; } = 0;
		public bool IsImmovable { get; set; } = false;
		public double OutOfRingPow { get; set; } = 0.1;
		public double NormalizedRingScale { get; set; } = 1.0;
	}

	public class GameStrings
	{
		public string[] RoleNames { get; set; } =
		{
			"Cyborg", // special, controls the entire ship
			"Navigation officer",
			"Weapons master",
			"Missile operator",
			"The one with motors",
			"The one with guns",
		};

		public string[] FactionNames { get; set; } =
		{
			"Hippo Federation",
			"Recursive Empire",
			"Cyborgs",
		};

		public string OnDeadChat { get; set; } = "(The dead cannot speak.)";
		public string NotPartOfShip { get; set; } = "(You are not part of a ship.)";

		public string GameEnded { get; set; } = "Game ended, length: {0}";
		public string AttackersWon { get; set; } = "Attackers have won!";
		public string DefendersWon { get; set; } = "Defenders have won!";
		public string GameEndRemainingStationHealthSum { get; set; } = "Remaining station health sum: {0} of {1}";

		public string TeamStats { get; set; } = "Team stats:";
		public string ShipStats { get; set; } = "Ship stats:";

		public string StatsKD { get; set; } = "Kills {0} Teamkills: {1} Deaths: {2} StationKills: {3} {4}";

		public string ShipDestroyed { get; set; } = "{0} has been destroyed.";
		public string StationDestroyed { get; set; } = "{0} has been destroyed, {1} remain.";

		public string ShipKilled { get; set; } = "{0} has been destroyed using {1} by {2}.";
		public string StationKilled { get; set; } = "{0} has been destroyed using {1} by {2}, {3} remain.";

		public string EntDescAsteroid { get; set; } = "Asteroid";
		public string EntDescDebris { get; set; } = "Debris";
		public string EntDescPlasma { get; set; } = "Plasma";

		public string GenericMineDestroyed { get; set; } = "Your {0} has been destroyed.";
		public string GenericMineTriggered { get; set; } = "Your {0} has been triggered.";
		public string GenericMineDescription { get; set; } = "{0} of {1}";
		public string GenericMineInactive { get; set; } = "(inactive)";

		public string ShipRaceFinished { get; set; } = "The ship {0} finished at place {1}!";

		public string EntNameRaceFinish { get; set; } = "Race Finish";

		public string EntNameMine { get; set; } = "Mine";
		public string EntNameMissile { get; set; } = "Missile";
		public string EntNameProbe { get; set; } = "Probe";

		public string ShipDamageIgnoredVictim { get; set; } = "Ignored {0} damage from {1} (spawn protection timer)";
		public string ShipDamageIgnoredDealer { get; set; } = "Target ignored {0} damage from {1} (spawn protection timer)";

		public string ShipDamageAbsorbedVictim { get; set; } = "Taken {0} damage from {1} ({2} of {3} absorbed by shields)";
		public string ShipDamageAbsorbedDealer { get; set; } = "Target received {0} damage from {1} ({2} of {3} absorbed by shields)";

		public string ShipDamageNoShieldsVictim { get; set; } = "Taken {0} damage from {1} (shields missed)";
		public string ShipDamageNoShieldsDealer { get; set; } = "Target received {0} damage from {1} (shields missed)";

		public string ShipDamageSameTeam { get; set; } = "(SAME TEAM)";
		public string ShipDamageSelf { get; set; } = "(SELF DAMAGE)";

		public string ShipNoteInvisible { get; set; } = "(invisible)";
		public string ShipDescription { get; set; } = "{0} of {1}";

		public string Enemys { get; set; } = "Enemy";
		public string Allied { get; set; } = "Allied";
		public string Yours { get; set; } = "Your";

		public string TimeRemaining { get; set; } = "{0} second remain.";

		public string StationDamageTakenDealer { get; set; } = "Target station received {0} damage from {1}";
		public string StationHealthDecreased { get; set; } = "{0} is below {1} health!"; // Includes nice description
		public string StationHealthDesc { get; set; } = "HP: {0}";
		public string StationDescription { get; set; } = "Station {0} of {1}";

		public static GameStrings CzechStrings()
		{
			return new GameStrings()
			{
				RoleNames = new string[]
				{
					"Kyborg",
					"Navigátor",
					"Zbrojní důstojník",
					"Operátor raket",
					"Ten s motorama",
					"Ten se zbraněma",
				},
				FactionNames = new string[]
				{
					"Hroší Federace",
					"Rekurzivní Impérium",
					"Kyborgové",
				},

				OnDeadChat = "(Mrtví nemluví.)",
				NotPartOfShip = "(Nejsi na lodi.)",

				GameEnded = "Hra skončila, délka: {0}",
				AttackersWon = "Útočníci vyhráli!",
				DefendersWon = "Obránci vyhráli!",
				GameEndRemainingStationHealthSum = "Zbývající životy stanic: {0} z {1}",

				TeamStats = "Statistiky týmů:",
				ShipStats = "Statistiky lodí:",

				StatsKD = "Kills {0} TeamKills: {1} Deaths: {2} StationKills: {3} {4}",

				ShipDestroyed = "{0} byla zničena.",
				StationDestroyed = "{0} byla zničena, zbývají {1}.",

				ShipKilled = "{0} byla zničena pomocí {1} lodí {2}.",
				StationKilled = "{0} byla zničena {1} lodí {2}, zbývají {3}.",

				EntDescAsteroid = "Asteroid",
				EntDescDebris = "Prach",
				EntDescPlasma = "Plasma",

				GenericMineDestroyed = "Tvoje {0} byla zničena.",
				GenericMineTriggered = "Tvoje {0} byla aktivována a vybuchla.",
				GenericMineDescription = "{0} týmu {1}",
				GenericMineInactive = "(neaktivní)",

				EntNameMine = "Mina",
				EntNameMissile = "Raketa",
				EntNameProbe = "Sonda",

				ShipDamageIgnoredVictim = "Ignorováno {0} poškození od {1} (pospawnová nesmrtelnost)",
				ShipDamageIgnoredDealer = "Cíl ignoroval {0} poškození od {1} (pospawnová nesmrtelnost)",

				ShipDamageAbsorbedVictim = "Vaše loď byla zraněna o {0} poškození od {1} ({2} z {3} absorbováno štíty)",
				ShipDamageAbsorbedDealer = "Cíl byl zraněn o {0} poškození od {1} ({2} z {3} absorbováno štíty)",

				ShipDamageNoShieldsVictim = "Vaše loď byla zraněna o {0} poškození od {1} (štíty minuty)",
				ShipDamageNoShieldsDealer = "Cíl byl zraněn o {0} poškození od {1} (štíty minuty)",

				ShipDamageSameTeam = "(STEJNÝ TÝM)",
				ShipDamageSelf = "(POŠKOZENÍ SOBĚ)",

				ShipNoteInvisible = "(neviditelná)",
				ShipDescription = "{0} týmu {1}",

				Enemys = "Nepřátelská",
				Allied = "Spojenecká",
				Yours = "Vaše",

				TimeRemaining = "Zbývá {0} sekund.",

				StationDamageTakenDealer = "Cílová stanice byla zraněna o {0} poškození od {1}",
				StationHealthDecreased = "Zdraví {0} je nyní pod {1}!",
				StationHealthDesc = "HP: {0}",
				StationDescription = "Stanice {0} týmu {1}",
			};
		}
	}
}
