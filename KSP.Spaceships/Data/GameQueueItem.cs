#nullable enable

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	public record GameQueueItem
	{
		public DisconnectedEvent? DisconnectionEvent { get; init; } = null;
		public GameTickEvent? TickEvent { get; init; } = null;
		public ClientRequestEvent? ClientRequestEvent { get; init; } = null;
		public ExitMainLoopEvent? ExitMainLoopEvent { get; init; } = null;
		public Action? OnCompleted { get; init; } = null;
	}

	public record DisconnectedEvent
	{
		public string ConnectionId { get; init; } = "";
	}

	public record GameTickEvent
	{
	}

	public record ExitMainLoopEvent
	{
	}

	public record ClientRequestEvent
	{
		public ClientRequest ClientRequest { get; init; } = new ClientRequest();
		public string ConnectionId { get; init; } = "";
	}
}
