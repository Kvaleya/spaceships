#nullable enable

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	//
	// Root
	//

	// Note that while most classes used in the state are immutalbe,
	// it also contains data obtained from the client, which
	// has some normal mutable C# arrays.
	// Consider them immutable as well.
	public record GameState
	{
		/// <summary>
		/// True when the game is being simulated, false in the ship assignment stage.
		/// </summary>
		public bool IsGameRunning { get; init; } = false;

		/// <summary>
		/// In real time seconds.
		/// </summary>
		public double CurrentGameElapsed { get; init; } = 0;

		public GameType GameType { get; init; } = GameType.Inactive;

		public GameStats Stats { get; init; } = new GameStats();

		public PlayerManagementData PlayerManagementData { get; init; } = new PlayerManagementData();
		public ConnectionManagementData ConnectionManagementData { get; init; } = new ConnectionManagementData();

		public int GivePlayerId { get; init; } = 0;
		public int GiveEntityId { get; init; } = 0;
		public int GiveShipId { get; init; } = 0;

		/// <summary>
		/// Ship description includes ship name and role assignment.
		/// Contains all assignable ships.
		/// </summary>
		public ImmutableDictionary<int, ShipPropertyDescription> MapShipIdToShipDesc { get; init; } = ImmutableDictionary<int, ShipPropertyDescription>.Empty;

		/// <summary>
		/// Ship state includes system and power settings and ship entity id.
		/// Contains all ships participating in a given game, even dead ships.
		/// </summary>
		public ImmutableDictionary<int, ShipStateData> MapShipIdToShipState { get; init; } = ImmutableDictionary<int, ShipStateData>.Empty;

		public ImmutableDictionary<int, Entity> MapEntityIdToEntity { get; init; } = ImmutableDictionary<int, Entity>.Empty;

		public ImmutableArray<int> StationEntityIds { get; init; } = ImmutableArray<int>.Empty;
		public ImmutableArray<int> CachedStationShipIds { get; init; } = ImmutableArray<int>.Empty;

		public ImmutableArray<EntityMessage> EntMessages { get; init; } = ImmutableArray<EntityMessage>.Empty;

		public Vector2i WorldSize { get; init; } = new Vector2i(16, 16);

		public int DefendingFactionId { get; init; }

		public DateTimeOffset LastTickTime { get; init; } = DateTimeOffset.UtcNow;

		public ImmutableArray<Vector2d> FactionSpawns { get; init; } = ImmutableArray<Vector2d>.Empty;

		public bool IsReplaying { get; init; } = false;
	}

	//
	// General game stuff
	//

	public record PlayerManagementData
	{
		public ImmutableDictionary<int, string> MapPlayerIdToUserId { get; init; } = ImmutableDictionary<int, string>.Empty;
		public ImmutableDictionary<string, PlayerData> MapUserIdToPlayer { get; init; } = ImmutableDictionary<string, PlayerData>.Empty;
	}

	public record ConnectionManagementData
	{
		public ImmutableDictionary<string, string> MapConnectionIdToUserIds { get; init; } = ImmutableDictionary<string, string>.Empty;
		public ImmutableDictionary<string, ImmutableArray<string>> MapUserIdToConnectionIds { get; init; } = ImmutableDictionary<string, ImmutableArray<string>>.Empty;
	}

	public record PlayerData
	{
		public string Name { get; init; } = "";
		public string UserId { get; init; } = "";
		public int Id { get; init; } = -1;
		public int ShipId { get; init; } = -1;
		public int RoleId { get; init; } = -1;
		public bool IsPrivileged { get; init; } = false;
		public bool IsShadowBanned { get; init; } = false;

		public PlayerDescription GetDescription(GameState state, bool privileged)
		{
			bool isConnected = state.ConnectionManagementData.MapUserIdToConnectionIds.ContainsKey(UserId)
				&& state.ConnectionManagementData.MapUserIdToConnectionIds[UserId].Length > 0;

			var name = this.Name;

			if(privileged)
			{
				name += $" (id: {this.Id})";

				if (this.IsPrivileged)
				{
					name += " (p)";
				}

				if (this.IsShadowBanned)
				{
					name += " (sb)";
				}
			}

			return new PlayerDescription()
			{
				id = this.Id,
				name = name,
				roleId = this.RoleId,
				shipId = this.ShipId,
				isConnected = isConnected,
			};
		}
	}

	public record ShipPropertyDescription
	{
		public string Name { get; init; } = "";
		public int Id { get; init; }

		/// <summary>
		/// Role ids
		/// </summary>
		public ImmutableArray<int> Roles { get; init; } = ImmutableArray<int>.Empty;

		public int FactionId { get; init; }

		/// <summary>
		/// Privileged only ships may use different roles.
		/// </summary>
		public bool onlyAcceptsPrivileged { get; init; } = false;

		public bool isVisible { get; init; } = true;

		public ShipDescription GetDescription()
		{
			return new ShipDescription()
			{
				factionid = this.FactionId,
				id = this.Id,
				name = this.Name,
				onlyAcceptsPrivileged = this.onlyAcceptsPrivileged,
				roles = this.Roles.ToArray(),
			};
		}
	}

	public record GameStats
	{
		public ImmutableDictionary<int, GameStatsShip> MapShipIdToKillCount { get; init; } = ImmutableDictionary<int, GameStatsShip>.Empty;
	}

	public record GameStatsShip
	{
		public int Kills { get; init; } = 0;
		public int StationKills { get; init; } = 0;
		public int TeamKills { get; init; } = 0;
		public int Deaths { get; init; } = 0;
	}

	//
	// Entity simulation stuff
	//

	public record EntitySimulationArgs
	{
		/// <summary>
		/// Game state at the beginning of this tick.
		/// </summary>
		public GameState State { get; init; }

		/// <summary>
		/// Game time elapsed since last tick.
		/// </summary>
		public double DeltaT { get; init; }

		/// <summary>
		/// Grid containing entities after collision simulation, capable of spacial queries.
		/// </summary>
		public CollisionGrid Grid { get; init; }

		/// <summary>
		/// List of collision events that concern this entity.
		/// </summary>
		public List<CollisionEvent> CollisionEvents { get; init; } = new List<CollisionEvent>();

		/// <summary>
		/// Game constants, mostly balancing related, such as damage values.
		/// </summary>
		public GameConstants Constants { get; init; }

		/// <summary>
		/// Messages for inter-entity communication
		/// </summary>
		public List<string> IncomingMessages { get; init; } = new List<string>();
	}

	public enum EntityEventType
	{
		Generic,
		DamageDealt,
		DamageTaken,
		TeamDamageDealt,
		SelfDamageTaken,
	}

	public record EntitySimulationOutput
	{
		/// <summary>
		/// Will be null during damage response phase.
		/// </summary>
		public List<DamageEvent>? DamageEvents { get; init; } = new List<DamageEvent>();
		public Func<Entity, int> SpawnEnt { get; init; }
		public List<(int, string, EntityEventType)> EntityEventsLog { get; init; } = new List<(int, string, EntityEventType)>();
		public List<string> GlobalEventsLog { get; init; } = new List<string>();
		public List<KillEvent> KillEvents { get; init; } = new List<KillEvent>();
		public List<EntityMessage> Messages { get; init; } = new List<EntityMessage>();
	}

	public record EntityDamageArgs
	{
		/// <summary>
		/// Game state at the beginning of this tick.
		/// </summary>
		public GameState State { get; init; }

		/// <summary>
		/// List of damage events that concern this entity.
		/// </summary>
		public IEnumerable<DamageEvent> DamageEvents { get; init; } = new DamageEvent[] { };

		/// <summary>
		/// Grid containing entities after collision simulation, capable of spacial queries.
		/// </summary>
		public CollisionGrid Grid { get; init; }

		/// <summary>
		/// Game constants, mostly balancing related, such as damage values.
		/// </summary>
		public GameConstants Constants { get; init; }
	}

	public abstract record Entity
	{
		/// <summary>
		/// Unique entity identifier
		/// </summary>
		public int Id { get; init; }

		/// <summary>
		/// Absolute world position. Updated automatically using velocity during physics simulation.
		/// </summary>
		public Vector2d Position { get; init; }

		/// <summary>
		/// Entity velocity, in units per second.
		/// </summary>
		public Vector2d Velocity { get; init; }

		/// <summary>
		/// Entity circle radius.
		/// </summary>
		public double Radius { get; init; }

		/// <summary>
		/// Entity mass. Used during physics simulation.
		/// </summary>
		public double Mass { get; init; }

		/// <summary>
		/// Projectiles compute collisions with the final trajectories of non-projectiles object (including bounces).
		/// Projectiles do not collide with other projectiles.
		/// Projectiles are expected to die upon first collision.
		/// Upon collision of projectile with non-projectile, only the projectile receives a collision event.
		/// Their next position is always computed as (position + velocity * deltaT).
		/// Only their first collision during a tick is included into collision events.
		/// </summary>
		public bool IsProjectile { get; init; }

		/// <summary>
		/// EntityIds of entities from which collisions will be ignored (the other entity will ignore such collisions as well).
		/// </summary>
		public ImmutableArray<int> IgnoreCollisionsFrom { get; init; } = ImmutableArray<int>.Empty;

		/// <summary>
		/// Simulates one tick of entity. Returns null if entity is to be destroyed.
		/// </summary>
		/// <param name="args">Input data for simulation.</param>
		/// <param name="damageEvents">List of damage events. The entity may append to this list.</param>
		/// <param name="toSpawn">List of newly spawned entities. The entity may append to this list.</param>
		/// <param name="entityEventsLog">List of entity-local event descriptions for players. Used only by ships.</param>
		/// <param name="globalEventsLog">List of global event descriptions for players. Ship deaths and similar.</param>
		/// <returns>Returns null if the entity is to be destroyed.</returns>
		public virtual Entity? Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			return this;
		}

		/// <summary>
		/// Processes any damage events targeted at this entity. Returns null if entity is to be destroyed.
		/// </summary>
		/// <param name="args">Input data for damage simulation.</param>
		/// <param name="toSpawn">List of newly spawned entities. The entity may append to this list.</param>
		/// <param name="entityEventsLog">List of entity-local event descriptions for players. Used only by ships.</param>
		/// <param name="globalEventsLog">List of global event descriptions for players. Ship deaths and similar.</param>
		/// <param name="killEvents">List of kill events (ship vs ship and ship vs other things).</param>
		/// <returns>Returns null if the entity is to be destroyed.</returns>
		public virtual Entity? ProcessDamageEvents(in EntityDamageArgs args, EntitySimulationOutput simout)
		{
			return this;
		}

		/// <summary>
		/// Called once per tick to update the appearance of this entity on various radars and in spectator view.
		/// May return null if the entity does not wish to generate a signature.
		/// </summary>
		public virtual RadarSignature? GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			return new RadarSignature()
			{
				position = new Vec2()
				{
					x = Position.X,
					y = Position.Y,
				},
				velocity = (this.Velocity * constants.TimeScale).ToVec2(),
				description = string.Empty,
				radius = Radius,
				isVisibleAnywhere = false,
				intensity = 1.0,
				id = Id,
			};
		}
	}

	public record CollisionEvent
	{
		/// <summary>
		/// The current participant.
		/// </summary>
		public int CurrentEntityId { get; init; }

		/// <summary>
		/// The other participant. Can be null if collision is world border.
		/// </summary>
		public int? OtherEntityId { get; init; }

		/// <summary>
		/// Velocity change due to collision computed as (new velocity - old velocity)
		/// </summary>
		public Vector2d VelocityDelta { get; init; }

		/// <summary>
		/// Normalized vector pointing towards collision source. Null when collision is world border.
		/// </summary>
		public Vector2d? CollisionDirection { get; init; }

		/// <summary>
		/// Where the current entity was when the collision ocurred.
		/// </summary>
		public Vector2d CurrentEntityPositionAtCollision { get; init; }

		public Vector2d OtherEntityPositionAtCollision { get; init; }
	}

	public enum DamageType
	{
		Unknown,
		Collision,
		Explosion,
		Laser,
		Plasma,
	}

	public record DamageEvent
	{
		/// <summary>
		/// The current participant.
		/// </summary>
		public int ReceiverEntityId { get; init; }

		/// <summary>
		/// Who deals the damage.
		/// </summary>
		public int DealerEntityId { get; init; }

		/// <summary>
		/// Where is the damage dealer in absolute world coordinates.
		/// </summary>
		public Vector2d Origin { get; init; }

		/// <summary>
		/// How much damage is being dealt to this entity.
		/// </summary>
		public double DamageAmount { get; init; }

		/// <summary>
		/// What type of damage it is.
		/// </summary>
		public DamageType Type { get; init; }
	}

	public record KillEvent
	{
		public int VictimId { get; init; }

		/// <summary>
		/// May be -1.
		/// </summary>
		public int KillerId { get; init; }
		public DamageType Type { get; init; }
	}

	public record EntityMessage
	{
		public int RecipientId { get; init; } = -1;
		public string Message { get; init; } = "";
	}

	//
	// Systems stuff
	//

	/// <summary>
	/// Stores current system settings, power settings and corresponding ship entity ID
	/// </summary>
	public record ShipStateData
	{
		public int EntityId { get; init; } = -1;
		public RequestSystemState Systems { get; init; } = new RequestSystemState();
		public ImmutableArray<(DateTimeOffset, PositionPointer)> Pointers = ImmutableArray<(DateTimeOffset, PositionPointer)>.Empty;
	}
}
