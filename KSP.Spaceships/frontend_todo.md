Sem píšu co objevím.

## High priority

- nějak zviditelnit zdraví lodi v UI
	- číslo ničím nenásobit, neseděly by pak informace o damage co dostávají do chatu
	- velké číslo co mění barvy když je <0.5 nebo <0.25
	- číslo napsané na kruhu lodi v mapě
	- velké průhledné číslo v horní části mapy?
- v assignmnent fázi zobrazovat někde kompletní seznam lidí a kdo je v lodi a kdo ne, a kdo není ani připojený (v API přibylo PlayerDescription.isConnected)

## Medium priority

- zobrazovat všechny floating point čísla hezky, třeba na 2 desetinná místa
- zobrazovat všem všechny systémy, zašednout ty ke kterým nemají přístup
- žlutá čára cílové rychlosti co se zobrazuje u lodi, aby se při ovládání motorů člověk nemusel dívat někam na dolní okraj obrazovky

## Low priority

- "Jsi admin" panel odstranit tlačítko vypnutí hry
	- A ve spectate módu ho posunout někam kde nepřekrývá název hover objektu
- serverresponse posílá defendingFaction id, hodilo by se to někde zobrazit
- serverresponse taky posílá jména rolí a týmů
- víc řádků chatu
- schovat během hry a během spectatora adminbuttons
