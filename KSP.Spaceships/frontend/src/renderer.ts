import * as d3 from "d3";
import type { GameState } from "./protocol/comm";
import type { MainGameState, SystemStateRadar, Vec2 } from "./protobuf/server_comm";

const MARKER_CROSS_SIZE = 1;

function lerp(a: number, b: number, x: number): number {
    return a * (1-x) + b*x
}


function vecLength(v: Vec2): number {
    return Math.sqrt(v.x * v.x + v.y * v.y);
}


const cameraColor = (i: number) => "rgba(32,96,128," + (0.75*i + 0.25) + ")";
const infraredColor = (i: number) => "rgba(255,0,0," + 0.5*i + ")";
const radarColor = (i: number) => "rgba(128,120,110," + (0.9*i + 0.1) + ")";
const probeColor = (i: number) => "rgba(8,80,16," + (0.75*i + 0.25) + ")";

function healthColor(h: number, max: number): string {
  const p = h / max;

  const r = Math.min(1, 1.5-1.5*p) * 200
  const g = Math.min(1, 1.5*p) * 128
  const b = 0

  return `rgb(${r}, ${g}, ${b})`
}


type EntityData = {
    position: Vec2
      velocity: Vec2
    time: number
};
let realPositions: Map<number, EntityData> = new Map()

const FAKE_ID_SHIP = -100;
const FAKE_ID_SENSORS_CAMERA = -1000;
const FAKE_ID_SENSORS_RADAR = -1001;
const FAKE_ID_SENSORS_INFRARED = -1002;
const FAKE_ID_SENSORS_PROBE = -1003;



type RenderContext = {
    ctx: CanvasRenderingContext2D,
    now: number,
    dataAgeSec: number,
    zeroPoint: Vec2
}


/**
 * DO NOT TOUCH THIS, EVEN THE WHITESPACE MAKES IT WORK! LIKE REALLY, GO AWAY!
 * 
 * Works by magic.
 */
function projectPosition(r: RenderContext, entityId: number, realPosition: Vec2, realVelocity: Vec2): Vec2 {
    const expectedReal = {
        x: realPosition.x + realVelocity.x * r.dataAgeSec,
        y: realPosition.y + realVelocity.y * r.dataAgeSec
    }

    const cachedEnt = (realPositions.get(entityId) ?? {position: expectedReal, velocity: realVelocity, time: r.now});
    let deltat = (r.now - cachedEnt.time) / 1000;
    let mpos = Math.min(1, deltat * 2.0);
    let mvel = Math.min(1, deltat * 4.0);
    let visiblePosition = { // lerp(smoothed expected pos, expected pos, mpos)
        x: lerp(cachedEnt.position.x + cachedEnt.velocity.x * deltat, expectedReal.x, mpos),
        y: lerp(cachedEnt.position.y + cachedEnt.velocity.y * deltat, expectedReal.y, mpos)
    }
    let visibleVelocity = { // lerp(smoothed velocity, real velocity, mvel)
        x: lerp(cachedEnt.velocity.x, realVelocity.x, mvel),
        y: lerp(cachedEnt.velocity.y, realVelocity.y, mvel)
    }

    let positionDifference = { // real->visible vector
        x: visiblePosition.x - realPosition.x,
        y: visiblePosition.y - realPosition.y,
    }
    let maxDist = 32;
    let dist = vecLength(positionDifference);
    let speedAdjustedDist = 1.0 * Math.max(vecLength(realVelocity), vecLength(visibleVelocity));
    let speedAdjustedDistBase = 8.0;
    if(dist > maxDist || dist > Math.max(speedAdjustedDistBase, speedAdjustedDist)) {
        // If the smoothed position and real (expected) positions are too far away,
        // teleport the entity to the expected position.
        visiblePosition = expectedReal;
        visibleVelocity = realVelocity;
    }

    realPositions.set(entityId, {position: visiblePosition, velocity: visibleVelocity, time: r.now});
    return visiblePosition;
}

function circle(r: RenderContext, center: Vec2, radius: number, style: string, fill: boolean = true) {
    r.ctx.beginPath()
    r.ctx.arc(center.x - r.zeroPoint.x, center.y - r.zeroPoint.y, radius, 0, 2*Math.PI);
    if (fill) {
        r.ctx.fillStyle = style;
        r.ctx.fill()
    } else {
        r.ctx.lineWidth = 0.2
        r.ctx.strokeStyle = style;
        r.ctx.stroke()
    }
}

function line(r: RenderContext, p1: Vec2, p2: Vec2, width: number, style: string, dashed: boolean = false) {
    r.ctx.strokeStyle = style
    r.ctx!.beginPath()
    r.ctx!.moveTo(p1.x, p1.y)
    if (dashed)
        r.ctx.setLineDash([1,1])
    else
        r.ctx.setLineDash([])

    r.ctx.lineTo(p2.x, p2.y)
    r.ctx.lineWidth = width
    r.ctx.stroke()
}

function text(r: RenderContext, text: string | undefined, pos: Vec2, size: number) {
    if (text != undefined) {
        r.ctx.fillStyle = "lightgray"
        r.ctx.textBaseline = "middle"
        r.ctx.textAlign = "center"
        r.ctx.font = '' + size + 'px sans-serif';
        r.ctx.fillText(text ?? "", pos.x - r.zeroPoint.x, pos.y - r.zeroPoint.y)
    }
}

function sensorSystem(r: RenderContext, fakeEntityId: number, system: SystemStateRadar | undefined, colorFunc: (a: number)=> string) {
    if (system == null) {
        return;
    }

    // range circle
    r.ctx.beginPath()
    const center = projectPosition(r, fakeEntityId, system.origin, system.velocity)
    r.ctx.arc(center.x - r.zeroPoint.x, center.y - r.zeroPoint.y, system.range, 0, 2*Math.PI)
    r.ctx.strokeStyle = ""
    r.ctx.setLineDash([1,1])
    r.ctx.lineWidth = 0.2
    r.ctx.strokeStyle = colorFunc(1)
    r.ctx.stroke()
    
    // detected objects
    for(let sig of system?.signatures ?? []) {
        const projectedPos = projectPosition(r, sig.id, sig.position, sig.velocity)
        circle(r, projectedPos, sig.radius, colorFunc(sig.intensity))
        text(r, sig.description, projectedPos, 1)
    }
}

function createRenderLoop(ctx: CanvasRenderingContext2D): (now:number) => void {
    function renderLoop(now: number) {
        if (gameState == null || gameState.server.mainGameState == null) {
            // this will stop the render loop
            return;
        }

        // handle window resising
        if (doResize) {
            ctx.canvas.width = window.innerWidth
            ctx.canvas.height = window.innerHeight
            doResize = false
            
            handleZoomEvent(ctx, lastZoomEvent)
        }

        // prepare data context
        const state: MainGameState | undefined = gameState.server.mainGameState
        const context = {
            ctx,
            dataAgeSec: (now - gameState.timestamp) / 1000,
            now,
            zeroPoint: projectPosition({ctx, now, dataAgeSec: (now - gameState.timestamp) / 1000, zeroPoint: {x: 0, y: 0}}, FAKE_ID_SHIP, state.shipState.position, state.shipState.velocity)
        }
        const worldSize = gameState.server.worldSize

        // reset image with black background
        ctx.save()
        ctx.resetTransform()
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.restore()

        // draw center lines
        line(context, {x: -1000, y: 0}, {x: 1000, y: 0}, 0.1, 'rgba(255,255,255,0.5)', true)
        line(context, {x: 0, y: -1000}, {x: 0, y: 1000}, 0.1, 'rgba(255,255,255,0.5)', true)

        // world boundary
        line(context, {x: (worldSize.x - context.zeroPoint.x), y: (worldSize.y - context.zeroPoint.y)}, {x: (worldSize.x - context.zeroPoint.x ), y: (-context.zeroPoint.y)}, 0.5, "lightgray")
        line(context, {x: (worldSize.x - context.zeroPoint.x), y: (worldSize.y - context.zeroPoint.y)}, {x: (-context.zeroPoint.x), y: (worldSize.y - context.zeroPoint.y)}, 0.5, "lightgray")
        line(context, {x: (worldSize.x - context.zeroPoint.x), y: (-context.zeroPoint.y)}, {x: (-context.zeroPoint.x), y: (-context.zeroPoint.y)}, 0.5, "lightgray")
        line(context, {x: (-context.zeroPoint.x), y: (worldSize.y - context.zeroPoint.y)}, {x: (-context.zeroPoint.x), y: (-context.zeroPoint.y)}, 0.5, "lightgray")

        // draw myself
        circle(context, context.zeroPoint, state.shipState.shipRadius, healthColor(state.shipState.healthCurrent, state.shipState.healthMax), !state.shipState.isInvisible)
        line(context, {x: 0, y: 0}, state.shipState.velocity, 0.1, 'rgba(255,255,255,0.5)')
        line(context, {x: 0, y: 0}, state.shipState.targetVelocity, 0.1, 'rgba(255,255,0,1)')

        // draw shield arc
        const angle = Math.atan2(state.systems.shields?.arcDirection.x ?? 1, state.systems.shields?.arcDirection.y ?? 0)
        const arc = state.systems.shields?.shieldArcSize ?? 0
        const shieldIntensity = state.systems.shields?.maxAbsorption == 0 ? 0 : (state.systems.shields?.currentAbsorption ?? 0) / (state.systems.shields?.maxAbsorption ?? 1)
        if (state.systems.shields != null) {
            ctx.strokeStyle = "rgba(119, 119, 255, "+ shieldIntensity + ")"
            ctx.lineWidth = 0.2
            ctx.beginPath()
            const startAngle = - angle - arc/2 - Math.PI * 1.5
            const endAngle = - angle + arc/2 - Math.PI * 1.5
            ctx.arc(0, 0, state.shipState.shipRadius * 1.5, startAngle, endAngle)
            ctx.stroke()
        }

        // sensors
        sensorSystem(context, FAKE_ID_SENSORS_RADAR, state.systems.sensorRadar, radarColor);
        sensorSystem(context, FAKE_ID_SENSORS_PROBE, state.systems.sensorProbe, probeColor);
        sensorSystem(context, FAKE_ID_SENSORS_INFRARED, state.systems.sensorInfrared, infraredColor);
        sensorSystem(context, FAKE_ID_SENSORS_CAMERA, state.systems.sensorCameras, cameraColor);

        // hud
        if (state.systems.sensorHud != undefined) {
            for (let sig of state.systems.sensorHud) {
                text(context, sig.description, projectPosition(context, sig.id, sig.position, sig.velocity), 1)
            }
        }

        // communication pointers
        if (state.pointers != undefined) {
            for (let ptr of state.pointers) {
                line(context, {x: (ptr.position.x - context.zeroPoint.x - MARKER_CROSS_SIZE), y: (ptr.position.y - context.zeroPoint.y - MARKER_CROSS_SIZE)}, {x: (ptr.position.x - context.zeroPoint.x + MARKER_CROSS_SIZE), y: (ptr.position.y - context.zeroPoint.y + MARKER_CROSS_SIZE)}, 0.1, "yellow")
                line(context, {x: (ptr.position.x - context.zeroPoint.x + MARKER_CROSS_SIZE), y: (ptr.position.y - context.zeroPoint.y - MARKER_CROSS_SIZE)}, {x: (ptr.position.x - context.zeroPoint.x - MARKER_CROSS_SIZE), y: (ptr.position.y - context.zeroPoint.y + MARKER_CROSS_SIZE)}, 0.1, "yellow")
            }
        }

        // missile target
        if (state.systems.remoteMissile?.active) {
            line(context, {x: (state.systems.remoteMissile?.currentTarget.x - context.zeroPoint.x - MARKER_CROSS_SIZE), y: (state.systems.remoteMissile?.currentTarget.y - context.zeroPoint.y - MARKER_CROSS_SIZE)}, {x: (state.systems.remoteMissile?.currentTarget.x - context.zeroPoint.x + MARKER_CROSS_SIZE), y: (state.systems.remoteMissile?.currentTarget.y - context.zeroPoint.y + MARKER_CROSS_SIZE)}, 0.1, "rgb(255,0,0)")
            line(context, {x: (state.systems.remoteMissile?.currentTarget.x - context.zeroPoint.x + MARKER_CROSS_SIZE), y: (state.systems.remoteMissile?.currentTarget.y - context.zeroPoint.y - MARKER_CROSS_SIZE)}, {x: (state.systems.remoteMissile?.currentTarget.x - context.zeroPoint.x - MARKER_CROSS_SIZE), y: (state.systems.remoteMissile?.currentTarget.y - context.zeroPoint.y + MARKER_CROSS_SIZE)}, 0.1, "rgb(255,0,0)")
        }

        requestAnimationFrame(renderLoop);
    }

    return renderLoop
}


let gameState: GameState | undefined = undefined;

export function updateRenderingState(gs: GameState | undefined) {
    gameState = gs;
}

export function getClickPosition(canvas: HTMLCanvasElement, e: MouseEvent): Vec2 {
    const pt = canvas.getContext("2d")!.getTransform().inverse().transformPoint(new DOMPoint(e.clientX, e.clientY))
    const position = realPositions.get(FAKE_ID_SHIP)?.position ?? {x: 0, y: 0}
    return {x: pt.x + position.x, y: pt.y + position.y}
}

let doResize: boolean = false
let lastZoomEvent: any = undefined

export function requestCanvasResize() {
    doResize = true
}

function handleZoomEvent(ctx: CanvasRenderingContext2D, e: any) {
    ctx.resetTransform()
    ctx.translate(e.transform.x, e.transform.y)
    ctx.scale(e.transform.k, e.transform.k)

    lastZoomEvent = e
}

export function startRendering(canvas: HTMLCanvasElement, container: HTMLElement) {
    // initial resize
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight

    let ctx = canvas.getContext('2d')!;
    if (ctx == null) {
        console.error("failed to initialise graphics");
        alert("Your browser does not support Canvas graphics")
        return;
    }

    // setup zoom and panning
    let zoomer = d3.zoom().clickDistance(20)
    zoomer.on("zoom", (e) => handleZoomEvent(ctx, e));
    const selection = d3.select(canvas) as any
    selection.call(zoomer);
    // initial zoom and transform
    zoomer.translateBy(selection, window.innerWidth/2, window.innerHeight/2)
    zoomer.scaleBy(selection, 10)

    requestAnimationFrame(createRenderLoop(ctx))
}
