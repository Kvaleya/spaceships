export function isEditableElement(e: Element | null | undefined) {
    return !!(e && ((e as HTMLElement).isContentEditable || e.tagName == "INPUT" || e.tagName == "TEXTAREA" || e.tagName == "SELECT"))
}