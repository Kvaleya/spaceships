/// Types are translated from ClientInterface/DataClasses.cs (referenced from git root)


export type SystemStateRequest = {
    weaponLaser?: SystemWeaponFireCommand
    weaponPlasma?: SystemWeaponFireCommand
    weaponMissiles?: SystemWeaponFireCommand
    weaponMines?: SystemWeaponFireCommand
    weaponProbes?: SystemWeaponFireCommand

    remoteMissile?: SystemRemoteControlCommand

    shields?: SystemShieldCommand
    stealthActivate?: boolean
    engines?: SystemEnginesCommand
}

export type SystemStealthState = {
    powerState: SystemPowerState
    chargeState: number
    chargeSpeed: number
    currentStealthRemaining: number
    stealthDuration: number
}


export type PowerState = {
    weaponLaser?: SystemPowerCommand
    weaponPlasma?: SystemPowerCommand
    weaponMissiles?: SystemPowerCommand
    weaponMines?: SystemPowerCommand
    weaponProbes?: SystemPowerCommand

    shields?: SystemPowerCommand
    stealth?: SystemPowerCommand
    autoRepair?: SystemPowerCommand
}

export type ShipControl = {
    pointer?: Vec2
    powerState?: PowerState
    systemState?: SystemStateRequest
}

export type ShipAssignment = {
    targetShipId?: number
    targetRoleId?: number
    requestedShipName?: string
}

export enum GameType {
    Deathmatch,
    TeamDeathmatch,
    Defense,
    Race,
}

export type ClientRequest = {
    messageOrder: number
    userId: string
    chatMessage?: string
    nameChange?: string
    shipAssignment?: ShipAssignment
    shipControl?: ShipControl
    startGame?: GameType
    abortGame?: boolean
    shipEdit?: ShipEdit
};

export type ShipEdit = {
    targetShipId: number
    factionId?: number
    name?: string
    privilegedOnly?: boolean
}

export enum ChatMessageType {
    ChatGeneral,
    ChatTeam,
    EventGlobal,
    EventShip,
    EventDamageTaken,
    EventDamageDealt,
    EventTeamDamageDealt,
    EventSelfDamageTaken,
    Server,
    Broadcast,
}

export type ChatMessage = {
    timestamp: string
    message: string
    type: ChatMessageType
}

export type PositionPointer = {
    position: Vec2
    creatorRoleId: number
}

export type PlayerAndGameState = {
    playerUserId: string
    playerName: string
    playerId: number
    playerIsPrivileged: boolean
    isPlayerDead: boolean
    gameType?: GameType
    isGameActive: boolean
    tickIntervalSeconds: number
    timeScale: number
    roleNames: string[]
    factionNames: string[]
}

export enum SpectateObjectType {
    Unknown,
    Asteroid,
    Ship,
    ShipInvisible,
    Station,
    Mine,
    Probe,
    Missile,
    Plasma,
    Effect,
    RaceFinish,
}

export type SpectatorMapState = {
    objects: Signature[]
}

export type ShipAssignmentState = {
    players: PlayerDescription[]
    ships: ShipDescription[]
    roleNames: string[]
    factionNames: string[]
}

export type PlayerDescription = {
    name: string
    id: number
    shipId: number
    roleId: number
    isConnected: boolean
}

export type ShipDescription = {
    name: string
    id: number
    factionid: number
    roles: number[]
    onlyAcceptsPrivileged: boolean
}

export type Vec2 = {
    x: number
    y: number
}

export enum LockType {
    None = 0,
    Radar = 1,
    Infrared = 2,
}

export enum RadarSignaturePurpose {
    Spectator,
    Camera,
    Infrared,
    Radar
}

export type Signature = {
    position: Vec2
    velocity: Vec2
    radius: number
    id: number
    intensity: number
    description?: string
    isVisibleAnywhere: boolean
	spectatorType: SpectateObjectType
}

export type MainGameState = {
    pointers: PositionPointer[]
    shipState: ShipState
    powerState: ShipReactorPowerState
    systems: SystemState
    timeElapsed : number
    timeMax : number
    deaths : number
    raceFinishPosition: Vec2
    raceFinishRadius: number
    tickIntervalSeconds: number
    timeScale: number
}

export type ShipState = {
    shipRadius: number
    description: ShipDescription
    players: PlayerDescription[]
    healthCurrent: number
    healthMax: number
    position: Vec2
    velocity: Vec2
    isInvisible: boolean
    targetVelocity: Vec2
}

export type ShipReactorPowerState = {
    powerTotal: number
    powerAvailable: number
}

export type SystemState = {
    sensorCameras?: SystemStateRadar
    sensorRadar?: SystemStateRadar
    sensorInfrared?: SystemStateRadar
    sensorProbe?: SystemStateRadar

    sensorHud: Signature[]

    weaponLaser?: SystemGenericChargingState
    weaponPlasma?: SystemGenericChargingState
    weaponMissiles?: SystemGenericChargingState
    weaponMines?: SystemGenericChargingState
    weaponProbes?: SystemGenericChargingState

    remoteMissile?: SystemRemoteState

    shields?: SystemShieldState
    stealth?: SystemGenericChargingState
    engines?: SystemEnginesState
    autoRepair?: SystemAutoRepairState
}

export type ServerResponse = {
    //messageOrder: number
    broadcast?: ChatMessage
    playerGameState: PlayerAndGameState
    spectatorMap?: SpectatorMapState
    shipAssignmentState?: ShipAssignmentState
    mainGameState?: MainGameState
    worldSize: Vec2
    incomingChat: ChatMessage[]
    defendingFaction: number
}

export type SystemPowerState = {
    currentPowerState: number
    powerStates: number[]
    allowPowerUsageChange: boolean
}

export type SystemPowerCommand = {
    targetPower: number
}

export type SystemStateRadar = {
    origin: Vec2
    velocity: Vec2
    signatures: Signature[]
    range: number
}


export type SystemGenericChargingState = {
    powerState: SystemPowerState  
    chargeState: number
    chargeSpeed: number
}

export type SystemWeaponFireCommand = {
    targetPosition: Vec2
}

export type SystemRemoteControlCommand = {
    targetPosition?: Vec2
    selfDestruct?: boolean
}

export type SystemRemoteState = {
    active: boolean
    currentTarget: Vec2
}

export type SystemShieldState = {
    powerState: SystemPowerState
    arcDirection: Vec2
    maxAbsorption: number
    currentAbsorption: number
    shieldRegenerationSpeed: number
    shieldArcSize: number
}

export type SystemShieldCommand = {
    direction: Vec2
}

export type SystemEnginesState = {
    powerState: SystemPowerState
    currentVelocity: Vec2
    maxAcceleration: number
    maxVelocity: number
}

export type SystemEnginesCommand = {
    desiredVelocity: Vec2
    enabled: boolean
}

export type SystemAutoRepairState = {
    powerState: SystemPowerState
    repairRate: number
}