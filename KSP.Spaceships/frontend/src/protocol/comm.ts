import * as signalR from "@microsoft/signalr";
import { Readable, readable } from "svelte/store";
import * as pako from 'pako'
import { ServerResponse, ClientRequest, ChatMessage, GameType, ShipDescription, Vec2, RequestShipControl } from '../protobuf/server_comm'

export type GameState = {
    chat: ChatMessage[]
    server: ServerResponse
    timestamp: number
    connection?: signalR.HubConnection
}

let gameStateInternal: GameState;

function getInitialGameState(): GameState {
    let res: GameState = JSON.parse(localStorage.getItem("gameState")!) ?? {
        chat: [],
        server: {
            defendingFaction: 0,
            playerGameState: {
                isGameActive: false,
                isPlayerDead: false,
                playerUserId: "null",
                playerId: 0,
                playerName: prompt("Ty jsi tu poprvé, že? Jak se jmenuješ?") ?? "unnamed",
                playerIsPrivileged: false,
                tickIntervalSeconds: 1,
                timeScale: 1,
                roleNames: [],
                factionNames: [],
            },
            incomingChat: [],
            worldSize: {x: 0, y: 0},
        }
    };
    res.connection = undefined
    return res
}

let hasSubscribers: boolean = false;

// load saved game state
gameStateInternal = getInitialGameState()

export const gameState: Readable<GameState> = readable(gameStateInternal, function start(set) {
    hasSubscribers = true;

    // wrap set function so that we also set the internal state
    let lastAnimationRequest: number | null = null
    function update() {
        if (lastAnimationRequest == null) {
            lastAnimationRequest = window.requestAnimationFrame(() => {
                lastAnimationRequest = null
                set(gameStateInternal)
            })
        }
        localStorage.setItem("gameState", JSON.stringify(gameStateInternal))
    }

    // open connection and store reference to it
	let connection = startCommunicating(update);

    // return function closing the connection
	return function stop() {
        connection.stop().then(() => {
            console.log("SignalR connection closed")
        })
	};
});


function sendRequest(request: ClientRequest) {
    //console.debug("Sending", request);
    if (gameStateInternal.connection)
        gameStateInternal.connection!.invoke("OnClientRequest", request) // TODO: protobuf too
    else
        console.log("Not connected!")
}

function updateState(update: () => void, new_state: ServerResponse) {
    let arrivalTime = performance.now()

    // if dead count increases, refresh page
    if ((new_state.mainGameState?.deaths ?? 0) > (gameStateInternal.server.mainGameState?.deaths ?? 0)) {
        location = location;
        console.log("Death count increased, refreshing page")
    }

    // store new chat message
    new_state.incomingChat.reverse()
    gameStateInternal.chat = new_state.incomingChat.concat(gameStateInternal.chat).slice(0, 15)

    // if it is a chat message or name change message or broadcast, copy old state
    if (new_state.broadcast || new_state.incomingChat.length != 0 || new_state.playerGameState!.playerName != gameStateInternal.server.playerGameState!.playerName) {
        new_state.shipAssignmentState = new_state.shipAssignmentState ?? gameStateInternal.server.shipAssignmentState
        new_state.spectatorMap = new_state.spectatorMap ?? gameStateInternal.server.spectatorMap
        new_state.mainGameState = new_state.mainGameState ?? gameStateInternal.server.mainGameState
        arrivalTime = gameStateInternal.timestamp
    }

    // store everything
    gameStateInternal.server = new_state
    gameStateInternal.timestamp = arrivalTime
    
    // update Svelte store
    update();
}

function startCommunicating(update: () => void): signalR.HubConnection {
    const hubConnection = new signalR.HubConnectionBuilder()
        .withUrl("/commonHub?userid=" + gameStateInternal.server.playerGameState!.playerUserId)
        .configureLogging(signalR.LogLevel.Information)
        .build();

    // Starts the SignalR connection
    hubConnection.start().then(a => {
        console.log("SignalR connection started, logging in");

        gameStateInternal.connection = hubConnection;
        update();

        // initial login request
        let request = ClientRequest.fromPartial({
            messageOrder: 0,
            userId: gameStateInternal.server.playerGameState!.playerUserId,
            nameChange: gameStateInternal.server.playerGameState!.playerName,
        })
        sendRequest(request);
    }).catch((reason) => {
        console.log("Connection failed", reason)

        // try again after a timeout
        setTimeout(()=>{
            startCommunicating(update)
        }, 2000)
    });

    hubConnection.on("OnServerResponse", function (state: ServerResponse | string) {
        if (typeof state == "string") {
            function _base64ToArrayBuffer(base64: string) {
                var binary_string = atob(base64);
                var len = binary_string.length;
                var bytes = new Uint8Array(len);
                for (var i = 0; i < len; i++) {
                    bytes[i] = binary_string.charCodeAt(i);
                }
                return bytes;
            }
            const decompressed = pako.inflate(_base64ToArrayBuffer(state))
            state = ServerResponse.decode(decompressed);// JSON.parse(new TextDecoder().decode(decompressed)) as ServerResponse
        }
        //console.debug("new state", state);
        updateState(update, state);
    });

    hubConnection.onclose((error) => {
        gameStateInternal.connection = undefined;
        update();

        startCommunicating(update);
    });

    return hubConnection;
}

function createRequest(): any {
    return {
        messageOrder: 0,
        userId: gameStateInternal.server.playerGameState!.playerUserId
    }
}

export function sendChatMessage(msg: string) {
    let request = createRequest()
    request.chatMessage = msg;
    sendRequest(request)
}

export function startGame(type: GameType) {
    let request = createRequest();
    request.startGame = type;
    sendRequest(request)
}

export function changeName(name: string) {
    let request = createRequest();
    request.nameChange = name;
    sendRequest(request)
}

export function assignShip(ship?: ShipDescription, role?: number) {
    let request = createRequest();
    if (ship == undefined || role == undefined) {
        request.shipAssignment = {
            targetRoleId: -1,
            targetShipId: -1,
        }
    } else {
        request.shipAssignment = {
            targetRoleId: role,
            targetShipId: ship.id
        }
    }
    sendRequest(request)
}

export function endGame() {
    let request = createRequest();
    request.abortGame = true;
    sendRequest(request);
}

export function selectPowerLevel(key: "weaponMines" | "weaponPlasma" | "weaponMissiles" | "weaponProbes" | "weaponLaser" | "stealth" | "autoRepair" | "shields", newpower: number) {
    let request = createRequest();
    request.shipControl = {
        powerState: {
            [key]: {
                targetPower: newpower
            }
        }
    }
    sendRequest(request)
}

export function sendPointer(position: Vec2) {
    let request = createRequest()
    request.shipControl = {
        pointer: position
    }
    sendRequest(request)
}

export function setVelocity(enabled: boolean, desiredVelocity: Vec2) {
    let request = createRequest()
    request.shipControl = RequestShipControl.fromPartial({
        systemState: {
            engines: {
                desiredVelocity,
                enabled
            }
        }
    })
    sendRequest(request)

}

export function fireCommand(key: "weaponMines" | "weaponPlasma" | "weaponMissiles" | "weaponProbes" | "weaponLaser", target: Vec2) {
    let request = createRequest()
    request.shipControl = RequestShipControl.fromPartial({
        systemState: {
            [key]: {
                targetPosition: target
            }
        }
    })
    sendRequest(request)
}

export function activateStealth() {
    let request = createRequest();
    request.shipControl = {
        systemState: {
            stealthActivate: true
        }
    }
    sendRequest(request)
}

export function setShieldDirection(dir: Vec2) {
    let request = createRequest();
    request.shipControl = RequestShipControl.fromPartial({
        systemState: {
            shields: {
                direction: dir
            }
        }
    })
    sendRequest(request);
}

export function remoteMissileControl(kaboom: boolean, target: Vec2) {
    let request = createRequest()
    request.shipControl = RequestShipControl.fromPartial({
        systemState: {
            remoteMissile: {
                selfDestruct: kaboom,
                targetPosition: target
            }
        }
    })
    sendRequest(request)
}
