/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "";

export enum ChatMessageType {
  ChatGeneral = 0,
  ChatTeam = 1,
  EventGlobal = 2,
  EventShip = 3,
  EventDamageTaken = 4,
  EventDamageDealt = 5,
  EventTeamDamageDealt = 6,
  EventSelfDamageTaken = 7,
  Server = 8,
  Broadcast = 9,
}

export enum GameType {
  Inactive = 0,
  Deathmatch = 1,
  TeamDeathmatch = 2,
  Defense = 3,
  Race = 4,
}

export enum SpectateObjectType {
  Unknown = 0,
  Asteroid = 1,
  Ship = 2,
  ShipInvisible = 3,
  Station = 4,
  Mine = 5,
  Probe = 6,
  Missile = 7,
  Plasma = 8,
  Effect = 9,
  RaceFinish = 10,
}

export interface ChatMessage {
  message: string;
  timestamp?: DateTime;
  type: ChatMessageType;
}

export interface ClientRequest {
  abortGame: boolean;
  chatMessage: string;
  messageOrder: number;
  nameChange: string;
  shipAssignment?: RequestShipAssignment;
  shipControl?: RequestShipControl;
  shipEdit?: RequestShipEdit;
  startGame: GameType;
  userId: string;
}

export interface DateTime {
  UnixMilliseconds: number;
}

export interface MainGameState {
  deaths: number;
  pointers: PositionPointer[];
  powerState?: ShipReactorPowerState;
  raceFinishPosition?: Vec2;
  raceFinishRadius: number;
  shipState?: ShipState;
  systems?: SystemState;
  tickIntervalSeconds: number;
  timeElapsed: number;
  timeMax: number;
  timeScale: number;
}

export interface PlayerAndGameState {
  factionNames: string[];
  gameType: GameType;
  isPlayerDead: boolean;
  playerId: number;
  playerIsPrivileged: boolean;
  playerName: string;
  playerUserId: string;
  roleNames: string[];
  tickIntervalSeconds: number;
  timeScale: number;
}

export interface PlayerDescription {
  id: number;
  isConnected: boolean;
  name: string;
  roleId: number;
  shipId: number;
}

export interface PositionPointer {
  creatorRoleId: number;
  position?: Vec2;
}

export interface RadarSignature {
  description: string;
  id: number;
  intensity: number;
  isVisibleAnywhere: boolean;
  position?: Vec2;
  radius: number;
  spectatorType: SpectateObjectType;
  velocity?: Vec2;
}

export interface RequestPowerState {
  autoRepair?: SystemPowerCommand;
  shields?: SystemPowerCommand;
  stealth?: SystemPowerCommand;
  weaponLaser?: SystemPowerCommand;
  weaponMines?: SystemPowerCommand;
  weaponMissiles?: SystemPowerCommand;
  weaponPlasma?: SystemPowerCommand;
  weaponProbes?: SystemPowerCommand;
}

export interface RequestShipAssignment {
  requestedShipName: string;
  targetRoleId: number;
  targetShipId: number;
}

export interface RequestShipControl {
  pointer?: Vec2;
  powerState?: RequestPowerState;
  systemState?: RequestSystemState;
}

export interface RequestShipEdit {
  factionId: number;
  name: string;
  privilegedOnly: boolean;
  targetShipId: number;
}

export interface RequestSystemState {
  engines?: SystemEnginesCommand;
  remoteMissile?: SystemRemoteControlCommand;
  shields?: SystemShieldCommand;
  stealthActivate: boolean;
  weaponLaser?: SystemWeaponFireCommand;
  weaponMines?: SystemWeaponFireCommand;
  weaponMissiles?: SystemWeaponFireCommand;
  weaponPlasma?: SystemWeaponFireCommand;
  weaponProbes?: SystemWeaponFireCommand;
}

export interface ResponseShipAssignmentState {
  factionNames: string[];
  players: PlayerDescription[];
  roleNames: string[];
  ships: ShipDescription[];
}

export interface ServerResponse {
  broadcast?: ChatMessage;
  defendingFaction: number;
  incomingChat: ChatMessage[];
  mainGameState?: MainGameState;
  playerGameState?: PlayerAndGameState;
  shipAssignmentState?: ResponseShipAssignmentState;
  spectatorMap?: SpectatorMapState;
  worldSize?: Vec2;
}

export interface ShipDescription {
  factionid: number;
  id: number;
  name: string;
  onlyAcceptsPrivileged: boolean;
  roles: number[];
}

export interface ShipReactorPowerState {
  powerAvailable: number;
  powerTotal: number;
}

export interface ShipState {
  description?: ShipDescription;
  healthCurrent: number;
  healthMax: number;
  isInvisible: boolean;
  players: PlayerDescription[];
  position?: Vec2;
  shipRadius: number;
  targetVelocity?: Vec2;
  velocity?: Vec2;
}

export interface SpectatorMapState {
  objects: RadarSignature[];
}

export interface SystemAutoRepairState {
  powerState?: SystemPowerState;
  repairRate: number;
}

export interface SystemEnginesCommand {
  desiredVelocity?: Vec2;
  enabled: boolean;
}

export interface SystemEnginesState {
  currentVelocity?: Vec2;
  maxAcceleration: number;
  maxVelocity: number;
  powerState?: SystemPowerState;
}

export interface SystemGenericChargingState {
  chargeSpeed: number;
  chargeState: number;
  powerState?: SystemPowerState;
}

export interface SystemPowerCommand {
  targetPower: number;
}

export interface SystemPowerState {
  allowPowerUsageChange: boolean;
  currentPowerState: number;
  powerStates: number[];
}

export interface SystemRemoteControlCommand {
  selfDestruct: boolean;
  targetPosition?: Vec2;
}

export interface SystemRemoteState {
  active: boolean;
  currentTarget?: Vec2;
}

export interface SystemShieldCommand {
  direction?: Vec2;
}

export interface SystemShieldState {
  arcDirection?: Vec2;
  currentAbsorption: number;
  maxAbsorption: number;
  powerState?: SystemPowerState;
  shieldArcSize: number;
  shieldRegenerationSpeed: number;
}

export interface SystemState {
  autoRepair?: SystemAutoRepairState;
  engines?: SystemEnginesState;
  remoteMissile?: SystemRemoteState;
  sensorCameras?: SystemStateRadar;
  sensorHud: RadarSignature[];
  sensorInfrared?: SystemStateRadar;
  sensorProbe?: SystemStateRadar;
  sensorRadar?: SystemStateRadar;
  shields?: SystemShieldState;
  stealth?: SystemStealthState;
  weaponLaser?: SystemGenericChargingState;
  weaponMines?: SystemGenericChargingState;
  weaponMissiles?: SystemGenericChargingState;
  weaponPlasma?: SystemGenericChargingState;
  weaponProbes?: SystemGenericChargingState;
}

export interface SystemStateRadar {
  origin?: Vec2;
  range: number;
  signatures: RadarSignature[];
  velocity?: Vec2;
}

export interface SystemStealthState {
  chargeSpeed: number;
  chargeState: number;
  currentStealthRemaining: number;
  powerState?: SystemPowerState;
  stealthDuration: number;
}

export interface SystemWeaponFireCommand {
  targetPosition?: Vec2;
}

export interface Vec2 {
  x: number;
  y: number;
}

const baseChatMessage: object = { message: "", type: 0 };

export const ChatMessage = {
  encode(
    message: ChatMessage,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.message !== "") {
      writer.uint32(10).string(message.message);
    }
    if (message.timestamp !== undefined) {
      DateTime.encode(message.timestamp, writer.uint32(18).fork()).ldelim();
    }
    if (message.type !== 0) {
      writer.uint32(24).int32(message.type);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ChatMessage {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseChatMessage } as ChatMessage;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.message = reader.string();
          break;
        case 2:
          message.timestamp = DateTime.decode(reader, reader.uint32());
          break;
        case 3:
          message.type = reader.int32() as any;
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<ChatMessage>): ChatMessage {
    const message = { ...baseChatMessage } as ChatMessage;
    if (object.message !== undefined && object.message !== null) {
      message.message = object.message;
    }
    if (object.timestamp !== undefined && object.timestamp !== null) {
      message.timestamp = DateTime.fromPartial(object.timestamp);
    }
    if (object.type !== undefined && object.type !== null) {
      message.type = object.type;
    }
    return message;
  },
};

const baseClientRequest: object = {
  abortGame: false,
  chatMessage: "",
  messageOrder: 0,
  nameChange: "",
  startGame: 0,
  userId: "",
};

export const ClientRequest = {
  encode(
    message: ClientRequest,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.abortGame === true) {
      writer.uint32(8).bool(message.abortGame);
    }
    if (message.chatMessage !== "") {
      writer.uint32(18).string(message.chatMessage);
    }
    if (message.messageOrder !== 0) {
      writer.uint32(24).int32(message.messageOrder);
    }
    if (message.nameChange !== "") {
      writer.uint32(34).string(message.nameChange);
    }
    if (message.shipAssignment !== undefined) {
      RequestShipAssignment.encode(
        message.shipAssignment,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.shipControl !== undefined) {
      RequestShipControl.encode(
        message.shipControl,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.shipEdit !== undefined) {
      RequestShipEdit.encode(
        message.shipEdit,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.startGame !== 0) {
      writer.uint32(64).int32(message.startGame);
    }
    if (message.userId !== "") {
      writer.uint32(74).string(message.userId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ClientRequest {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseClientRequest } as ClientRequest;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.abortGame = reader.bool();
          break;
        case 2:
          message.chatMessage = reader.string();
          break;
        case 3:
          message.messageOrder = reader.int32();
          break;
        case 4:
          message.nameChange = reader.string();
          break;
        case 5:
          message.shipAssignment = RequestShipAssignment.decode(
            reader,
            reader.uint32()
          );
          break;
        case 6:
          message.shipControl = RequestShipControl.decode(
            reader,
            reader.uint32()
          );
          break;
        case 7:
          message.shipEdit = RequestShipEdit.decode(reader, reader.uint32());
          break;
        case 8:
          message.startGame = reader.int32() as any;
          break;
        case 9:
          message.userId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<ClientRequest>): ClientRequest {
    const message = { ...baseClientRequest } as ClientRequest;
    if (object.abortGame !== undefined && object.abortGame !== null) {
      message.abortGame = object.abortGame;
    }
    if (object.chatMessage !== undefined && object.chatMessage !== null) {
      message.chatMessage = object.chatMessage;
    }
    if (object.messageOrder !== undefined && object.messageOrder !== null) {
      message.messageOrder = object.messageOrder;
    }
    if (object.nameChange !== undefined && object.nameChange !== null) {
      message.nameChange = object.nameChange;
    }
    if (object.shipAssignment !== undefined && object.shipAssignment !== null) {
      message.shipAssignment = RequestShipAssignment.fromPartial(
        object.shipAssignment
      );
    }
    if (object.shipControl !== undefined && object.shipControl !== null) {
      message.shipControl = RequestShipControl.fromPartial(object.shipControl);
    }
    if (object.shipEdit !== undefined && object.shipEdit !== null) {
      message.shipEdit = RequestShipEdit.fromPartial(object.shipEdit);
    }
    if (object.startGame !== undefined && object.startGame !== null) {
      message.startGame = object.startGame;
    }
    if (object.userId !== undefined && object.userId !== null) {
      message.userId = object.userId;
    }
    return message;
  },
};

const baseDateTime: object = { UnixMilliseconds: 0 };

export const DateTime = {
  encode(
    message: DateTime,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.UnixMilliseconds !== 0) {
      writer.uint32(9).double(message.UnixMilliseconds);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): DateTime {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseDateTime } as DateTime;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.UnixMilliseconds = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<DateTime>): DateTime {
    const message = { ...baseDateTime } as DateTime;
    if (
      object.UnixMilliseconds !== undefined &&
      object.UnixMilliseconds !== null
    ) {
      message.UnixMilliseconds = object.UnixMilliseconds;
    }
    return message;
  },
};

const baseMainGameState: object = {
  deaths: 0,
  raceFinishRadius: 0,
  tickIntervalSeconds: 0,
  timeElapsed: 0,
  timeMax: 0,
  timeScale: 0,
};

export const MainGameState = {
  encode(
    message: MainGameState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.deaths !== 0) {
      writer.uint32(8).int32(message.deaths);
    }
    for (const v of message.pointers) {
      PositionPointer.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    if (message.powerState !== undefined) {
      ShipReactorPowerState.encode(
        message.powerState,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.raceFinishPosition !== undefined) {
      Vec2.encode(
        message.raceFinishPosition,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.raceFinishRadius !== 0) {
      writer.uint32(41).double(message.raceFinishRadius);
    }
    if (message.shipState !== undefined) {
      ShipState.encode(message.shipState, writer.uint32(50).fork()).ldelim();
    }
    if (message.systems !== undefined) {
      SystemState.encode(message.systems, writer.uint32(58).fork()).ldelim();
    }
    if (message.tickIntervalSeconds !== 0) {
      writer.uint32(65).double(message.tickIntervalSeconds);
    }
    if (message.timeElapsed !== 0) {
      writer.uint32(73).double(message.timeElapsed);
    }
    if (message.timeMax !== 0) {
      writer.uint32(81).double(message.timeMax);
    }
    if (message.timeScale !== 0) {
      writer.uint32(89).double(message.timeScale);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MainGameState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseMainGameState } as MainGameState;
    message.pointers = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.deaths = reader.int32();
          break;
        case 2:
          message.pointers.push(
            PositionPointer.decode(reader, reader.uint32())
          );
          break;
        case 3:
          message.powerState = ShipReactorPowerState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 4:
          message.raceFinishPosition = Vec2.decode(reader, reader.uint32());
          break;
        case 5:
          message.raceFinishRadius = reader.double();
          break;
        case 6:
          message.shipState = ShipState.decode(reader, reader.uint32());
          break;
        case 7:
          message.systems = SystemState.decode(reader, reader.uint32());
          break;
        case 8:
          message.tickIntervalSeconds = reader.double();
          break;
        case 9:
          message.timeElapsed = reader.double();
          break;
        case 10:
          message.timeMax = reader.double();
          break;
        case 11:
          message.timeScale = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<MainGameState>): MainGameState {
    const message = { ...baseMainGameState } as MainGameState;
    message.pointers = [];
    if (object.deaths !== undefined && object.deaths !== null) {
      message.deaths = object.deaths;
    }
    if (object.pointers !== undefined && object.pointers !== null) {
      for (const e of object.pointers) {
        message.pointers.push(PositionPointer.fromPartial(e));
      }
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = ShipReactorPowerState.fromPartial(object.powerState);
    }
    if (
      object.raceFinishPosition !== undefined &&
      object.raceFinishPosition !== null
    ) {
      message.raceFinishPosition = Vec2.fromPartial(object.raceFinishPosition);
    }
    if (
      object.raceFinishRadius !== undefined &&
      object.raceFinishRadius !== null
    ) {
      message.raceFinishRadius = object.raceFinishRadius;
    }
    if (object.shipState !== undefined && object.shipState !== null) {
      message.shipState = ShipState.fromPartial(object.shipState);
    }
    if (object.systems !== undefined && object.systems !== null) {
      message.systems = SystemState.fromPartial(object.systems);
    }
    if (
      object.tickIntervalSeconds !== undefined &&
      object.tickIntervalSeconds !== null
    ) {
      message.tickIntervalSeconds = object.tickIntervalSeconds;
    }
    if (object.timeElapsed !== undefined && object.timeElapsed !== null) {
      message.timeElapsed = object.timeElapsed;
    }
    if (object.timeMax !== undefined && object.timeMax !== null) {
      message.timeMax = object.timeMax;
    }
    if (object.timeScale !== undefined && object.timeScale !== null) {
      message.timeScale = object.timeScale;
    }
    return message;
  },
};

const basePlayerAndGameState: object = {
  factionNames: "",
  gameType: 0,
  isPlayerDead: false,
  playerId: 0,
  playerIsPrivileged: false,
  playerName: "",
  playerUserId: "",
  roleNames: "",
  tickIntervalSeconds: 0,
  timeScale: 0,
};

export const PlayerAndGameState = {
  encode(
    message: PlayerAndGameState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.factionNames) {
      writer.uint32(10).string(v!);
    }
    if (message.gameType !== 0) {
      writer.uint32(16).int32(message.gameType);
    }
    if (message.isPlayerDead === true) {
      writer.uint32(24).bool(message.isPlayerDead);
    }
    if (message.playerId !== 0) {
      writer.uint32(32).int32(message.playerId);
    }
    if (message.playerIsPrivileged === true) {
      writer.uint32(40).bool(message.playerIsPrivileged);
    }
    if (message.playerName !== "") {
      writer.uint32(50).string(message.playerName);
    }
    if (message.playerUserId !== "") {
      writer.uint32(58).string(message.playerUserId);
    }
    for (const v of message.roleNames) {
      writer.uint32(66).string(v!);
    }
    if (message.tickIntervalSeconds !== 0) {
      writer.uint32(73).double(message.tickIntervalSeconds);
    }
    if (message.timeScale !== 0) {
      writer.uint32(81).double(message.timeScale);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PlayerAndGameState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePlayerAndGameState } as PlayerAndGameState;
    message.factionNames = [];
    message.roleNames = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.factionNames.push(reader.string());
          break;
        case 2:
          message.gameType = reader.int32() as any;
          break;
        case 3:
          message.isPlayerDead = reader.bool();
          break;
        case 4:
          message.playerId = reader.int32();
          break;
        case 5:
          message.playerIsPrivileged = reader.bool();
          break;
        case 6:
          message.playerName = reader.string();
          break;
        case 7:
          message.playerUserId = reader.string();
          break;
        case 8:
          message.roleNames.push(reader.string());
          break;
        case 9:
          message.tickIntervalSeconds = reader.double();
          break;
        case 10:
          message.timeScale = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<PlayerAndGameState>): PlayerAndGameState {
    const message = { ...basePlayerAndGameState } as PlayerAndGameState;
    message.factionNames = [];
    message.roleNames = [];
    if (object.factionNames !== undefined && object.factionNames !== null) {
      for (const e of object.factionNames) {
        message.factionNames.push(e);
      }
    }
    if (object.gameType !== undefined && object.gameType !== null) {
      message.gameType = object.gameType;
    }
    if (object.isPlayerDead !== undefined && object.isPlayerDead !== null) {
      message.isPlayerDead = object.isPlayerDead;
    }
    if (object.playerId !== undefined && object.playerId !== null) {
      message.playerId = object.playerId;
    }
    if (
      object.playerIsPrivileged !== undefined &&
      object.playerIsPrivileged !== null
    ) {
      message.playerIsPrivileged = object.playerIsPrivileged;
    }
    if (object.playerName !== undefined && object.playerName !== null) {
      message.playerName = object.playerName;
    }
    if (object.playerUserId !== undefined && object.playerUserId !== null) {
      message.playerUserId = object.playerUserId;
    }
    if (object.roleNames !== undefined && object.roleNames !== null) {
      for (const e of object.roleNames) {
        message.roleNames.push(e);
      }
    }
    if (
      object.tickIntervalSeconds !== undefined &&
      object.tickIntervalSeconds !== null
    ) {
      message.tickIntervalSeconds = object.tickIntervalSeconds;
    }
    if (object.timeScale !== undefined && object.timeScale !== null) {
      message.timeScale = object.timeScale;
    }
    return message;
  },
};

const basePlayerDescription: object = {
  id: 0,
  isConnected: false,
  name: "",
  roleId: 0,
  shipId: 0,
};

export const PlayerDescription = {
  encode(
    message: PlayerDescription,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.id !== 0) {
      writer.uint32(8).int32(message.id);
    }
    if (message.isConnected === true) {
      writer.uint32(16).bool(message.isConnected);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.roleId !== 0) {
      writer.uint32(32).int32(message.roleId);
    }
    if (message.shipId !== 0) {
      writer.uint32(40).int32(message.shipId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PlayerDescription {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePlayerDescription } as PlayerDescription;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.int32();
          break;
        case 2:
          message.isConnected = reader.bool();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.roleId = reader.int32();
          break;
        case 5:
          message.shipId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<PlayerDescription>): PlayerDescription {
    const message = { ...basePlayerDescription } as PlayerDescription;
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    }
    if (object.isConnected !== undefined && object.isConnected !== null) {
      message.isConnected = object.isConnected;
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    if (object.roleId !== undefined && object.roleId !== null) {
      message.roleId = object.roleId;
    }
    if (object.shipId !== undefined && object.shipId !== null) {
      message.shipId = object.shipId;
    }
    return message;
  },
};

const basePositionPointer: object = { creatorRoleId: 0 };

export const PositionPointer = {
  encode(
    message: PositionPointer,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.creatorRoleId !== 0) {
      writer.uint32(8).int32(message.creatorRoleId);
    }
    if (message.position !== undefined) {
      Vec2.encode(message.position, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PositionPointer {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...basePositionPointer } as PositionPointer;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.creatorRoleId = reader.int32();
          break;
        case 2:
          message.position = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<PositionPointer>): PositionPointer {
    const message = { ...basePositionPointer } as PositionPointer;
    if (object.creatorRoleId !== undefined && object.creatorRoleId !== null) {
      message.creatorRoleId = object.creatorRoleId;
    }
    if (object.position !== undefined && object.position !== null) {
      message.position = Vec2.fromPartial(object.position);
    }
    return message;
  },
};

const baseRadarSignature: object = {
  description: "",
  id: 0,
  intensity: 0,
  isVisibleAnywhere: false,
  radius: 0,
  spectatorType: 0,
};

export const RadarSignature = {
  encode(
    message: RadarSignature,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.description !== "") {
      writer.uint32(10).string(message.description);
    }
    if (message.id !== 0) {
      writer.uint32(16).int32(message.id);
    }
    if (message.intensity !== 0) {
      writer.uint32(25).double(message.intensity);
    }
    if (message.isVisibleAnywhere === true) {
      writer.uint32(32).bool(message.isVisibleAnywhere);
    }
    if (message.position !== undefined) {
      Vec2.encode(message.position, writer.uint32(42).fork()).ldelim();
    }
    if (message.radius !== 0) {
      writer.uint32(49).double(message.radius);
    }
    if (message.spectatorType !== 0) {
      writer.uint32(56).int32(message.spectatorType);
    }
    if (message.velocity !== undefined) {
      Vec2.encode(message.velocity, writer.uint32(66).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RadarSignature {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRadarSignature } as RadarSignature;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.description = reader.string();
          break;
        case 2:
          message.id = reader.int32();
          break;
        case 3:
          message.intensity = reader.double();
          break;
        case 4:
          message.isVisibleAnywhere = reader.bool();
          break;
        case 5:
          message.position = Vec2.decode(reader, reader.uint32());
          break;
        case 6:
          message.radius = reader.double();
          break;
        case 7:
          message.spectatorType = reader.int32() as any;
          break;
        case 8:
          message.velocity = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<RadarSignature>): RadarSignature {
    const message = { ...baseRadarSignature } as RadarSignature;
    if (object.description !== undefined && object.description !== null) {
      message.description = object.description;
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    }
    if (object.intensity !== undefined && object.intensity !== null) {
      message.intensity = object.intensity;
    }
    if (
      object.isVisibleAnywhere !== undefined &&
      object.isVisibleAnywhere !== null
    ) {
      message.isVisibleAnywhere = object.isVisibleAnywhere;
    }
    if (object.position !== undefined && object.position !== null) {
      message.position = Vec2.fromPartial(object.position);
    }
    if (object.radius !== undefined && object.radius !== null) {
      message.radius = object.radius;
    }
    if (object.spectatorType !== undefined && object.spectatorType !== null) {
      message.spectatorType = object.spectatorType;
    }
    if (object.velocity !== undefined && object.velocity !== null) {
      message.velocity = Vec2.fromPartial(object.velocity);
    }
    return message;
  },
};

const baseRequestPowerState: object = {};

export const RequestPowerState = {
  encode(
    message: RequestPowerState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.autoRepair !== undefined) {
      SystemPowerCommand.encode(
        message.autoRepair,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.shields !== undefined) {
      SystemPowerCommand.encode(
        message.shields,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.stealth !== undefined) {
      SystemPowerCommand.encode(
        message.stealth,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.weaponLaser !== undefined) {
      SystemPowerCommand.encode(
        message.weaponLaser,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.weaponMines !== undefined) {
      SystemPowerCommand.encode(
        message.weaponMines,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.weaponMissiles !== undefined) {
      SystemPowerCommand.encode(
        message.weaponMissiles,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.weaponPlasma !== undefined) {
      SystemPowerCommand.encode(
        message.weaponPlasma,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.weaponProbes !== undefined) {
      SystemPowerCommand.encode(
        message.weaponProbes,
        writer.uint32(66).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RequestPowerState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRequestPowerState } as RequestPowerState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.autoRepair = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 2:
          message.shields = SystemPowerCommand.decode(reader, reader.uint32());
          break;
        case 3:
          message.stealth = SystemPowerCommand.decode(reader, reader.uint32());
          break;
        case 4:
          message.weaponLaser = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 5:
          message.weaponMines = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 6:
          message.weaponMissiles = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 7:
          message.weaponPlasma = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 8:
          message.weaponProbes = SystemPowerCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<RequestPowerState>): RequestPowerState {
    const message = { ...baseRequestPowerState } as RequestPowerState;
    if (object.autoRepair !== undefined && object.autoRepair !== null) {
      message.autoRepair = SystemPowerCommand.fromPartial(object.autoRepair);
    }
    if (object.shields !== undefined && object.shields !== null) {
      message.shields = SystemPowerCommand.fromPartial(object.shields);
    }
    if (object.stealth !== undefined && object.stealth !== null) {
      message.stealth = SystemPowerCommand.fromPartial(object.stealth);
    }
    if (object.weaponLaser !== undefined && object.weaponLaser !== null) {
      message.weaponLaser = SystemPowerCommand.fromPartial(object.weaponLaser);
    }
    if (object.weaponMines !== undefined && object.weaponMines !== null) {
      message.weaponMines = SystemPowerCommand.fromPartial(object.weaponMines);
    }
    if (object.weaponMissiles !== undefined && object.weaponMissiles !== null) {
      message.weaponMissiles = SystemPowerCommand.fromPartial(
        object.weaponMissiles
      );
    }
    if (object.weaponPlasma !== undefined && object.weaponPlasma !== null) {
      message.weaponPlasma = SystemPowerCommand.fromPartial(
        object.weaponPlasma
      );
    }
    if (object.weaponProbes !== undefined && object.weaponProbes !== null) {
      message.weaponProbes = SystemPowerCommand.fromPartial(
        object.weaponProbes
      );
    }
    return message;
  },
};

const baseRequestShipAssignment: object = {
  requestedShipName: "",
  targetRoleId: 0,
  targetShipId: 0,
};

export const RequestShipAssignment = {
  encode(
    message: RequestShipAssignment,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.requestedShipName !== "") {
      writer.uint32(10).string(message.requestedShipName);
    }
    if (message.targetRoleId !== 0) {
      writer.uint32(16).int32(message.targetRoleId);
    }
    if (message.targetShipId !== 0) {
      writer.uint32(24).int32(message.targetShipId);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): RequestShipAssignment {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRequestShipAssignment } as RequestShipAssignment;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.requestedShipName = reader.string();
          break;
        case 2:
          message.targetRoleId = reader.int32();
          break;
        case 3:
          message.targetShipId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<RequestShipAssignment>
  ): RequestShipAssignment {
    const message = { ...baseRequestShipAssignment } as RequestShipAssignment;
    if (
      object.requestedShipName !== undefined &&
      object.requestedShipName !== null
    ) {
      message.requestedShipName = object.requestedShipName;
    }
    if (object.targetRoleId !== undefined && object.targetRoleId !== null) {
      message.targetRoleId = object.targetRoleId;
    }
    if (object.targetShipId !== undefined && object.targetShipId !== null) {
      message.targetShipId = object.targetShipId;
    }
    return message;
  },
};

const baseRequestShipControl: object = {};

export const RequestShipControl = {
  encode(
    message: RequestShipControl,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.pointer !== undefined) {
      Vec2.encode(message.pointer, writer.uint32(10).fork()).ldelim();
    }
    if (message.powerState !== undefined) {
      RequestPowerState.encode(
        message.powerState,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.systemState !== undefined) {
      RequestSystemState.encode(
        message.systemState,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RequestShipControl {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRequestShipControl } as RequestShipControl;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.pointer = Vec2.decode(reader, reader.uint32());
          break;
        case 2:
          message.powerState = RequestPowerState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 3:
          message.systemState = RequestSystemState.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<RequestShipControl>): RequestShipControl {
    const message = { ...baseRequestShipControl } as RequestShipControl;
    if (object.pointer !== undefined && object.pointer !== null) {
      message.pointer = Vec2.fromPartial(object.pointer);
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = RequestPowerState.fromPartial(object.powerState);
    }
    if (object.systemState !== undefined && object.systemState !== null) {
      message.systemState = RequestSystemState.fromPartial(object.systemState);
    }
    return message;
  },
};

const baseRequestShipEdit: object = {
  factionId: 0,
  name: "",
  privilegedOnly: false,
  targetShipId: 0,
};

export const RequestShipEdit = {
  encode(
    message: RequestShipEdit,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.factionId !== 0) {
      writer.uint32(8).int32(message.factionId);
    }
    if (message.name !== "") {
      writer.uint32(18).string(message.name);
    }
    if (message.privilegedOnly === true) {
      writer.uint32(24).bool(message.privilegedOnly);
    }
    if (message.targetShipId !== 0) {
      writer.uint32(32).int32(message.targetShipId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RequestShipEdit {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRequestShipEdit } as RequestShipEdit;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.factionId = reader.int32();
          break;
        case 2:
          message.name = reader.string();
          break;
        case 3:
          message.privilegedOnly = reader.bool();
          break;
        case 4:
          message.targetShipId = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<RequestShipEdit>): RequestShipEdit {
    const message = { ...baseRequestShipEdit } as RequestShipEdit;
    if (object.factionId !== undefined && object.factionId !== null) {
      message.factionId = object.factionId;
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    if (object.privilegedOnly !== undefined && object.privilegedOnly !== null) {
      message.privilegedOnly = object.privilegedOnly;
    }
    if (object.targetShipId !== undefined && object.targetShipId !== null) {
      message.targetShipId = object.targetShipId;
    }
    return message;
  },
};

const baseRequestSystemState: object = { stealthActivate: false };

export const RequestSystemState = {
  encode(
    message: RequestSystemState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.engines !== undefined) {
      SystemEnginesCommand.encode(
        message.engines,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.remoteMissile !== undefined) {
      SystemRemoteControlCommand.encode(
        message.remoteMissile,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.shields !== undefined) {
      SystemShieldCommand.encode(
        message.shields,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.stealthActivate === true) {
      writer.uint32(32).bool(message.stealthActivate);
    }
    if (message.weaponLaser !== undefined) {
      SystemWeaponFireCommand.encode(
        message.weaponLaser,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.weaponMines !== undefined) {
      SystemWeaponFireCommand.encode(
        message.weaponMines,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.weaponMissiles !== undefined) {
      SystemWeaponFireCommand.encode(
        message.weaponMissiles,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.weaponPlasma !== undefined) {
      SystemWeaponFireCommand.encode(
        message.weaponPlasma,
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.weaponProbes !== undefined) {
      SystemWeaponFireCommand.encode(
        message.weaponProbes,
        writer.uint32(74).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): RequestSystemState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseRequestSystemState } as RequestSystemState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.engines = SystemEnginesCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 2:
          message.remoteMissile = SystemRemoteControlCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 3:
          message.shields = SystemShieldCommand.decode(reader, reader.uint32());
          break;
        case 4:
          message.stealthActivate = reader.bool();
          break;
        case 5:
          message.weaponLaser = SystemWeaponFireCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 6:
          message.weaponMines = SystemWeaponFireCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 7:
          message.weaponMissiles = SystemWeaponFireCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 8:
          message.weaponPlasma = SystemWeaponFireCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        case 9:
          message.weaponProbes = SystemWeaponFireCommand.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<RequestSystemState>): RequestSystemState {
    const message = { ...baseRequestSystemState } as RequestSystemState;
    if (object.engines !== undefined && object.engines !== null) {
      message.engines = SystemEnginesCommand.fromPartial(object.engines);
    }
    if (object.remoteMissile !== undefined && object.remoteMissile !== null) {
      message.remoteMissile = SystemRemoteControlCommand.fromPartial(
        object.remoteMissile
      );
    }
    if (object.shields !== undefined && object.shields !== null) {
      message.shields = SystemShieldCommand.fromPartial(object.shields);
    }
    if (
      object.stealthActivate !== undefined &&
      object.stealthActivate !== null
    ) {
      message.stealthActivate = object.stealthActivate;
    }
    if (object.weaponLaser !== undefined && object.weaponLaser !== null) {
      message.weaponLaser = SystemWeaponFireCommand.fromPartial(
        object.weaponLaser
      );
    }
    if (object.weaponMines !== undefined && object.weaponMines !== null) {
      message.weaponMines = SystemWeaponFireCommand.fromPartial(
        object.weaponMines
      );
    }
    if (object.weaponMissiles !== undefined && object.weaponMissiles !== null) {
      message.weaponMissiles = SystemWeaponFireCommand.fromPartial(
        object.weaponMissiles
      );
    }
    if (object.weaponPlasma !== undefined && object.weaponPlasma !== null) {
      message.weaponPlasma = SystemWeaponFireCommand.fromPartial(
        object.weaponPlasma
      );
    }
    if (object.weaponProbes !== undefined && object.weaponProbes !== null) {
      message.weaponProbes = SystemWeaponFireCommand.fromPartial(
        object.weaponProbes
      );
    }
    return message;
  },
};

const baseResponseShipAssignmentState: object = {
  factionNames: "",
  roleNames: "",
};

export const ResponseShipAssignmentState = {
  encode(
    message: ResponseShipAssignmentState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.factionNames) {
      writer.uint32(10).string(v!);
    }
    for (const v of message.players) {
      PlayerDescription.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.roleNames) {
      writer.uint32(26).string(v!);
    }
    for (const v of message.ships) {
      ShipDescription.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ResponseShipAssignmentState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseResponseShipAssignmentState,
    } as ResponseShipAssignmentState;
    message.factionNames = [];
    message.players = [];
    message.roleNames = [];
    message.ships = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.factionNames.push(reader.string());
          break;
        case 2:
          message.players.push(
            PlayerDescription.decode(reader, reader.uint32())
          );
          break;
        case 3:
          message.roleNames.push(reader.string());
          break;
        case 4:
          message.ships.push(ShipDescription.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<ResponseShipAssignmentState>
  ): ResponseShipAssignmentState {
    const message = {
      ...baseResponseShipAssignmentState,
    } as ResponseShipAssignmentState;
    message.factionNames = [];
    message.players = [];
    message.roleNames = [];
    message.ships = [];
    if (object.factionNames !== undefined && object.factionNames !== null) {
      for (const e of object.factionNames) {
        message.factionNames.push(e);
      }
    }
    if (object.players !== undefined && object.players !== null) {
      for (const e of object.players) {
        message.players.push(PlayerDescription.fromPartial(e));
      }
    }
    if (object.roleNames !== undefined && object.roleNames !== null) {
      for (const e of object.roleNames) {
        message.roleNames.push(e);
      }
    }
    if (object.ships !== undefined && object.ships !== null) {
      for (const e of object.ships) {
        message.ships.push(ShipDescription.fromPartial(e));
      }
    }
    return message;
  },
};

const baseServerResponse: object = { defendingFaction: 0 };

export const ServerResponse = {
  encode(
    message: ServerResponse,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.broadcast !== undefined) {
      ChatMessage.encode(message.broadcast, writer.uint32(10).fork()).ldelim();
    }
    if (message.defendingFaction !== 0) {
      writer.uint32(16).int32(message.defendingFaction);
    }
    for (const v of message.incomingChat) {
      ChatMessage.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.mainGameState !== undefined) {
      MainGameState.encode(
        message.mainGameState,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.playerGameState !== undefined) {
      PlayerAndGameState.encode(
        message.playerGameState,
        writer.uint32(42).fork()
      ).ldelim();
    }
    if (message.shipAssignmentState !== undefined) {
      ResponseShipAssignmentState.encode(
        message.shipAssignmentState,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.spectatorMap !== undefined) {
      SpectatorMapState.encode(
        message.spectatorMap,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.worldSize !== undefined) {
      Vec2.encode(message.worldSize, writer.uint32(66).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ServerResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseServerResponse } as ServerResponse;
    message.incomingChat = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.broadcast = ChatMessage.decode(reader, reader.uint32());
          break;
        case 2:
          message.defendingFaction = reader.int32();
          break;
        case 3:
          message.incomingChat.push(
            ChatMessage.decode(reader, reader.uint32())
          );
          break;
        case 4:
          message.mainGameState = MainGameState.decode(reader, reader.uint32());
          break;
        case 5:
          message.playerGameState = PlayerAndGameState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 6:
          message.shipAssignmentState = ResponseShipAssignmentState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 7:
          message.spectatorMap = SpectatorMapState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 8:
          message.worldSize = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<ServerResponse>): ServerResponse {
    const message = { ...baseServerResponse } as ServerResponse;
    message.incomingChat = [];
    if (object.broadcast !== undefined && object.broadcast !== null) {
      message.broadcast = ChatMessage.fromPartial(object.broadcast);
    }
    if (
      object.defendingFaction !== undefined &&
      object.defendingFaction !== null
    ) {
      message.defendingFaction = object.defendingFaction;
    }
    if (object.incomingChat !== undefined && object.incomingChat !== null) {
      for (const e of object.incomingChat) {
        message.incomingChat.push(ChatMessage.fromPartial(e));
      }
    }
    if (object.mainGameState !== undefined && object.mainGameState !== null) {
      message.mainGameState = MainGameState.fromPartial(object.mainGameState);
    }
    if (
      object.playerGameState !== undefined &&
      object.playerGameState !== null
    ) {
      message.playerGameState = PlayerAndGameState.fromPartial(
        object.playerGameState
      );
    }
    if (
      object.shipAssignmentState !== undefined &&
      object.shipAssignmentState !== null
    ) {
      message.shipAssignmentState = ResponseShipAssignmentState.fromPartial(
        object.shipAssignmentState
      );
    }
    if (object.spectatorMap !== undefined && object.spectatorMap !== null) {
      message.spectatorMap = SpectatorMapState.fromPartial(object.spectatorMap);
    }
    if (object.worldSize !== undefined && object.worldSize !== null) {
      message.worldSize = Vec2.fromPartial(object.worldSize);
    }
    return message;
  },
};

const baseShipDescription: object = {
  factionid: 0,
  id: 0,
  name: "",
  onlyAcceptsPrivileged: false,
  roles: 0,
};

export const ShipDescription = {
  encode(
    message: ShipDescription,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.factionid !== 0) {
      writer.uint32(8).int32(message.factionid);
    }
    if (message.id !== 0) {
      writer.uint32(16).int32(message.id);
    }
    if (message.name !== "") {
      writer.uint32(26).string(message.name);
    }
    if (message.onlyAcceptsPrivileged === true) {
      writer.uint32(32).bool(message.onlyAcceptsPrivileged);
    }
    writer.uint32(42).fork();
    for (const v of message.roles) {
      writer.int32(v);
    }
    writer.ldelim();
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ShipDescription {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseShipDescription } as ShipDescription;
    message.roles = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.factionid = reader.int32();
          break;
        case 2:
          message.id = reader.int32();
          break;
        case 3:
          message.name = reader.string();
          break;
        case 4:
          message.onlyAcceptsPrivileged = reader.bool();
          break;
        case 5:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.roles.push(reader.int32());
            }
          } else {
            message.roles.push(reader.int32());
          }
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<ShipDescription>): ShipDescription {
    const message = { ...baseShipDescription } as ShipDescription;
    message.roles = [];
    if (object.factionid !== undefined && object.factionid !== null) {
      message.factionid = object.factionid;
    }
    if (object.id !== undefined && object.id !== null) {
      message.id = object.id;
    }
    if (object.name !== undefined && object.name !== null) {
      message.name = object.name;
    }
    if (
      object.onlyAcceptsPrivileged !== undefined &&
      object.onlyAcceptsPrivileged !== null
    ) {
      message.onlyAcceptsPrivileged = object.onlyAcceptsPrivileged;
    }
    if (object.roles !== undefined && object.roles !== null) {
      for (const e of object.roles) {
        message.roles.push(e);
      }
    }
    return message;
  },
};

const baseShipReactorPowerState: object = { powerAvailable: 0, powerTotal: 0 };

export const ShipReactorPowerState = {
  encode(
    message: ShipReactorPowerState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.powerAvailable !== 0) {
      writer.uint32(8).int32(message.powerAvailable);
    }
    if (message.powerTotal !== 0) {
      writer.uint32(16).int32(message.powerTotal);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): ShipReactorPowerState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseShipReactorPowerState } as ShipReactorPowerState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.powerAvailable = reader.int32();
          break;
        case 2:
          message.powerTotal = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<ShipReactorPowerState>
  ): ShipReactorPowerState {
    const message = { ...baseShipReactorPowerState } as ShipReactorPowerState;
    if (object.powerAvailable !== undefined && object.powerAvailable !== null) {
      message.powerAvailable = object.powerAvailable;
    }
    if (object.powerTotal !== undefined && object.powerTotal !== null) {
      message.powerTotal = object.powerTotal;
    }
    return message;
  },
};

const baseShipState: object = {
  healthCurrent: 0,
  healthMax: 0,
  isInvisible: false,
  shipRadius: 0,
};

export const ShipState = {
  encode(
    message: ShipState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.description !== undefined) {
      ShipDescription.encode(
        message.description,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.healthCurrent !== 0) {
      writer.uint32(17).double(message.healthCurrent);
    }
    if (message.healthMax !== 0) {
      writer.uint32(25).double(message.healthMax);
    }
    if (message.isInvisible === true) {
      writer.uint32(32).bool(message.isInvisible);
    }
    for (const v of message.players) {
      PlayerDescription.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    if (message.position !== undefined) {
      Vec2.encode(message.position, writer.uint32(50).fork()).ldelim();
    }
    if (message.shipRadius !== 0) {
      writer.uint32(57).double(message.shipRadius);
    }
    if (message.targetVelocity !== undefined) {
      Vec2.encode(message.targetVelocity, writer.uint32(66).fork()).ldelim();
    }
    if (message.velocity !== undefined) {
      Vec2.encode(message.velocity, writer.uint32(74).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ShipState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseShipState } as ShipState;
    message.players = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.description = ShipDescription.decode(reader, reader.uint32());
          break;
        case 2:
          message.healthCurrent = reader.double();
          break;
        case 3:
          message.healthMax = reader.double();
          break;
        case 4:
          message.isInvisible = reader.bool();
          break;
        case 5:
          message.players.push(
            PlayerDescription.decode(reader, reader.uint32())
          );
          break;
        case 6:
          message.position = Vec2.decode(reader, reader.uint32());
          break;
        case 7:
          message.shipRadius = reader.double();
          break;
        case 8:
          message.targetVelocity = Vec2.decode(reader, reader.uint32());
          break;
        case 9:
          message.velocity = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<ShipState>): ShipState {
    const message = { ...baseShipState } as ShipState;
    message.players = [];
    if (object.description !== undefined && object.description !== null) {
      message.description = ShipDescription.fromPartial(object.description);
    }
    if (object.healthCurrent !== undefined && object.healthCurrent !== null) {
      message.healthCurrent = object.healthCurrent;
    }
    if (object.healthMax !== undefined && object.healthMax !== null) {
      message.healthMax = object.healthMax;
    }
    if (object.isInvisible !== undefined && object.isInvisible !== null) {
      message.isInvisible = object.isInvisible;
    }
    if (object.players !== undefined && object.players !== null) {
      for (const e of object.players) {
        message.players.push(PlayerDescription.fromPartial(e));
      }
    }
    if (object.position !== undefined && object.position !== null) {
      message.position = Vec2.fromPartial(object.position);
    }
    if (object.shipRadius !== undefined && object.shipRadius !== null) {
      message.shipRadius = object.shipRadius;
    }
    if (object.targetVelocity !== undefined && object.targetVelocity !== null) {
      message.targetVelocity = Vec2.fromPartial(object.targetVelocity);
    }
    if (object.velocity !== undefined && object.velocity !== null) {
      message.velocity = Vec2.fromPartial(object.velocity);
    }
    return message;
  },
};

const baseSpectatorMapState: object = {};

export const SpectatorMapState = {
  encode(
    message: SpectatorMapState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    for (const v of message.objects) {
      RadarSignature.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SpectatorMapState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSpectatorMapState } as SpectatorMapState;
    message.objects = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.objects.push(RadarSignature.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SpectatorMapState>): SpectatorMapState {
    const message = { ...baseSpectatorMapState } as SpectatorMapState;
    message.objects = [];
    if (object.objects !== undefined && object.objects !== null) {
      for (const e of object.objects) {
        message.objects.push(RadarSignature.fromPartial(e));
      }
    }
    return message;
  },
};

const baseSystemAutoRepairState: object = { repairRate: 0 };

export const SystemAutoRepairState = {
  encode(
    message: SystemAutoRepairState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.powerState !== undefined) {
      SystemPowerState.encode(
        message.powerState,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.repairRate !== 0) {
      writer.uint32(17).double(message.repairRate);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SystemAutoRepairState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemAutoRepairState } as SystemAutoRepairState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.powerState = SystemPowerState.decode(reader, reader.uint32());
          break;
        case 2:
          message.repairRate = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<SystemAutoRepairState>
  ): SystemAutoRepairState {
    const message = { ...baseSystemAutoRepairState } as SystemAutoRepairState;
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = SystemPowerState.fromPartial(object.powerState);
    }
    if (object.repairRate !== undefined && object.repairRate !== null) {
      message.repairRate = object.repairRate;
    }
    return message;
  },
};

const baseSystemEnginesCommand: object = { enabled: false };

export const SystemEnginesCommand = {
  encode(
    message: SystemEnginesCommand,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.desiredVelocity !== undefined) {
      Vec2.encode(message.desiredVelocity, writer.uint32(10).fork()).ldelim();
    }
    if (message.enabled === true) {
      writer.uint32(16).bool(message.enabled);
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SystemEnginesCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemEnginesCommand } as SystemEnginesCommand;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.desiredVelocity = Vec2.decode(reader, reader.uint32());
          break;
        case 2:
          message.enabled = reader.bool();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemEnginesCommand>): SystemEnginesCommand {
    const message = { ...baseSystemEnginesCommand } as SystemEnginesCommand;
    if (
      object.desiredVelocity !== undefined &&
      object.desiredVelocity !== null
    ) {
      message.desiredVelocity = Vec2.fromPartial(object.desiredVelocity);
    }
    if (object.enabled !== undefined && object.enabled !== null) {
      message.enabled = object.enabled;
    }
    return message;
  },
};

const baseSystemEnginesState: object = { maxAcceleration: 0, maxVelocity: 0 };

export const SystemEnginesState = {
  encode(
    message: SystemEnginesState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.currentVelocity !== undefined) {
      Vec2.encode(message.currentVelocity, writer.uint32(10).fork()).ldelim();
    }
    if (message.maxAcceleration !== 0) {
      writer.uint32(17).double(message.maxAcceleration);
    }
    if (message.maxVelocity !== 0) {
      writer.uint32(25).double(message.maxVelocity);
    }
    if (message.powerState !== undefined) {
      SystemPowerState.encode(
        message.powerState,
        writer.uint32(34).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemEnginesState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemEnginesState } as SystemEnginesState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.currentVelocity = Vec2.decode(reader, reader.uint32());
          break;
        case 2:
          message.maxAcceleration = reader.double();
          break;
        case 3:
          message.maxVelocity = reader.double();
          break;
        case 4:
          message.powerState = SystemPowerState.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemEnginesState>): SystemEnginesState {
    const message = { ...baseSystemEnginesState } as SystemEnginesState;
    if (
      object.currentVelocity !== undefined &&
      object.currentVelocity !== null
    ) {
      message.currentVelocity = Vec2.fromPartial(object.currentVelocity);
    }
    if (
      object.maxAcceleration !== undefined &&
      object.maxAcceleration !== null
    ) {
      message.maxAcceleration = object.maxAcceleration;
    }
    if (object.maxVelocity !== undefined && object.maxVelocity !== null) {
      message.maxVelocity = object.maxVelocity;
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = SystemPowerState.fromPartial(object.powerState);
    }
    return message;
  },
};

const baseSystemGenericChargingState: object = {
  chargeSpeed: 0,
  chargeState: 0,
};

export const SystemGenericChargingState = {
  encode(
    message: SystemGenericChargingState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.chargeSpeed !== 0) {
      writer.uint32(9).double(message.chargeSpeed);
    }
    if (message.chargeState !== 0) {
      writer.uint32(17).double(message.chargeState);
    }
    if (message.powerState !== undefined) {
      SystemPowerState.encode(
        message.powerState,
        writer.uint32(26).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SystemGenericChargingState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSystemGenericChargingState,
    } as SystemGenericChargingState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.chargeSpeed = reader.double();
          break;
        case 2:
          message.chargeState = reader.double();
          break;
        case 3:
          message.powerState = SystemPowerState.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<SystemGenericChargingState>
  ): SystemGenericChargingState {
    const message = {
      ...baseSystemGenericChargingState,
    } as SystemGenericChargingState;
    if (object.chargeSpeed !== undefined && object.chargeSpeed !== null) {
      message.chargeSpeed = object.chargeSpeed;
    }
    if (object.chargeState !== undefined && object.chargeState !== null) {
      message.chargeState = object.chargeState;
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = SystemPowerState.fromPartial(object.powerState);
    }
    return message;
  },
};

const baseSystemPowerCommand: object = { targetPower: 0 };

export const SystemPowerCommand = {
  encode(
    message: SystemPowerCommand,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.targetPower !== 0) {
      writer.uint32(8).int32(message.targetPower);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemPowerCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemPowerCommand } as SystemPowerCommand;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.targetPower = reader.int32();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemPowerCommand>): SystemPowerCommand {
    const message = { ...baseSystemPowerCommand } as SystemPowerCommand;
    if (object.targetPower !== undefined && object.targetPower !== null) {
      message.targetPower = object.targetPower;
    }
    return message;
  },
};

const baseSystemPowerState: object = {
  allowPowerUsageChange: false,
  currentPowerState: 0,
  powerStates: 0,
};

export const SystemPowerState = {
  encode(
    message: SystemPowerState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.allowPowerUsageChange === true) {
      writer.uint32(8).bool(message.allowPowerUsageChange);
    }
    if (message.currentPowerState !== 0) {
      writer.uint32(16).int32(message.currentPowerState);
    }
    writer.uint32(26).fork();
    for (const v of message.powerStates) {
      writer.int32(v);
    }
    writer.ldelim();
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemPowerState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemPowerState } as SystemPowerState;
    message.powerStates = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.allowPowerUsageChange = reader.bool();
          break;
        case 2:
          message.currentPowerState = reader.int32();
          break;
        case 3:
          if ((tag & 7) === 2) {
            const end2 = reader.uint32() + reader.pos;
            while (reader.pos < end2) {
              message.powerStates.push(reader.int32());
            }
          } else {
            message.powerStates.push(reader.int32());
          }
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemPowerState>): SystemPowerState {
    const message = { ...baseSystemPowerState } as SystemPowerState;
    message.powerStates = [];
    if (
      object.allowPowerUsageChange !== undefined &&
      object.allowPowerUsageChange !== null
    ) {
      message.allowPowerUsageChange = object.allowPowerUsageChange;
    }
    if (
      object.currentPowerState !== undefined &&
      object.currentPowerState !== null
    ) {
      message.currentPowerState = object.currentPowerState;
    }
    if (object.powerStates !== undefined && object.powerStates !== null) {
      for (const e of object.powerStates) {
        message.powerStates.push(e);
      }
    }
    return message;
  },
};

const baseSystemRemoteControlCommand: object = { selfDestruct: false };

export const SystemRemoteControlCommand = {
  encode(
    message: SystemRemoteControlCommand,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.selfDestruct === true) {
      writer.uint32(8).bool(message.selfDestruct);
    }
    if (message.targetPosition !== undefined) {
      Vec2.encode(message.targetPosition, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SystemRemoteControlCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSystemRemoteControlCommand,
    } as SystemRemoteControlCommand;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.selfDestruct = reader.bool();
          break;
        case 2:
          message.targetPosition = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<SystemRemoteControlCommand>
  ): SystemRemoteControlCommand {
    const message = {
      ...baseSystemRemoteControlCommand,
    } as SystemRemoteControlCommand;
    if (object.selfDestruct !== undefined && object.selfDestruct !== null) {
      message.selfDestruct = object.selfDestruct;
    }
    if (object.targetPosition !== undefined && object.targetPosition !== null) {
      message.targetPosition = Vec2.fromPartial(object.targetPosition);
    }
    return message;
  },
};

const baseSystemRemoteState: object = { active: false };

export const SystemRemoteState = {
  encode(
    message: SystemRemoteState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.active === true) {
      writer.uint32(8).bool(message.active);
    }
    if (message.currentTarget !== undefined) {
      Vec2.encode(message.currentTarget, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemRemoteState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemRemoteState } as SystemRemoteState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.active = reader.bool();
          break;
        case 2:
          message.currentTarget = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemRemoteState>): SystemRemoteState {
    const message = { ...baseSystemRemoteState } as SystemRemoteState;
    if (object.active !== undefined && object.active !== null) {
      message.active = object.active;
    }
    if (object.currentTarget !== undefined && object.currentTarget !== null) {
      message.currentTarget = Vec2.fromPartial(object.currentTarget);
    }
    return message;
  },
};

const baseSystemShieldCommand: object = {};

export const SystemShieldCommand = {
  encode(
    message: SystemShieldCommand,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.direction !== undefined) {
      Vec2.encode(message.direction, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemShieldCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemShieldCommand } as SystemShieldCommand;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.direction = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemShieldCommand>): SystemShieldCommand {
    const message = { ...baseSystemShieldCommand } as SystemShieldCommand;
    if (object.direction !== undefined && object.direction !== null) {
      message.direction = Vec2.fromPartial(object.direction);
    }
    return message;
  },
};

const baseSystemShieldState: object = {
  currentAbsorption: 0,
  maxAbsorption: 0,
  shieldArcSize: 0,
  shieldRegenerationSpeed: 0,
};

export const SystemShieldState = {
  encode(
    message: SystemShieldState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.arcDirection !== undefined) {
      Vec2.encode(message.arcDirection, writer.uint32(10).fork()).ldelim();
    }
    if (message.currentAbsorption !== 0) {
      writer.uint32(17).double(message.currentAbsorption);
    }
    if (message.maxAbsorption !== 0) {
      writer.uint32(25).double(message.maxAbsorption);
    }
    if (message.powerState !== undefined) {
      SystemPowerState.encode(
        message.powerState,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.shieldArcSize !== 0) {
      writer.uint32(41).double(message.shieldArcSize);
    }
    if (message.shieldRegenerationSpeed !== 0) {
      writer.uint32(49).double(message.shieldRegenerationSpeed);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemShieldState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemShieldState } as SystemShieldState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.arcDirection = Vec2.decode(reader, reader.uint32());
          break;
        case 2:
          message.currentAbsorption = reader.double();
          break;
        case 3:
          message.maxAbsorption = reader.double();
          break;
        case 4:
          message.powerState = SystemPowerState.decode(reader, reader.uint32());
          break;
        case 5:
          message.shieldArcSize = reader.double();
          break;
        case 6:
          message.shieldRegenerationSpeed = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemShieldState>): SystemShieldState {
    const message = { ...baseSystemShieldState } as SystemShieldState;
    if (object.arcDirection !== undefined && object.arcDirection !== null) {
      message.arcDirection = Vec2.fromPartial(object.arcDirection);
    }
    if (
      object.currentAbsorption !== undefined &&
      object.currentAbsorption !== null
    ) {
      message.currentAbsorption = object.currentAbsorption;
    }
    if (object.maxAbsorption !== undefined && object.maxAbsorption !== null) {
      message.maxAbsorption = object.maxAbsorption;
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = SystemPowerState.fromPartial(object.powerState);
    }
    if (object.shieldArcSize !== undefined && object.shieldArcSize !== null) {
      message.shieldArcSize = object.shieldArcSize;
    }
    if (
      object.shieldRegenerationSpeed !== undefined &&
      object.shieldRegenerationSpeed !== null
    ) {
      message.shieldRegenerationSpeed = object.shieldRegenerationSpeed;
    }
    return message;
  },
};

const baseSystemState: object = {};

export const SystemState = {
  encode(
    message: SystemState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.autoRepair !== undefined) {
      SystemAutoRepairState.encode(
        message.autoRepair,
        writer.uint32(10).fork()
      ).ldelim();
    }
    if (message.engines !== undefined) {
      SystemEnginesState.encode(
        message.engines,
        writer.uint32(18).fork()
      ).ldelim();
    }
    if (message.remoteMissile !== undefined) {
      SystemRemoteState.encode(
        message.remoteMissile,
        writer.uint32(26).fork()
      ).ldelim();
    }
    if (message.sensorCameras !== undefined) {
      SystemStateRadar.encode(
        message.sensorCameras,
        writer.uint32(34).fork()
      ).ldelim();
    }
    for (const v of message.sensorHud) {
      RadarSignature.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    if (message.sensorInfrared !== undefined) {
      SystemStateRadar.encode(
        message.sensorInfrared,
        writer.uint32(50).fork()
      ).ldelim();
    }
    if (message.sensorProbe !== undefined) {
      SystemStateRadar.encode(
        message.sensorProbe,
        writer.uint32(58).fork()
      ).ldelim();
    }
    if (message.sensorRadar !== undefined) {
      SystemStateRadar.encode(
        message.sensorRadar,
        writer.uint32(66).fork()
      ).ldelim();
    }
    if (message.shields !== undefined) {
      SystemShieldState.encode(
        message.shields,
        writer.uint32(74).fork()
      ).ldelim();
    }
    if (message.stealth !== undefined) {
      SystemStealthState.encode(
        message.stealth,
        writer.uint32(82).fork()
      ).ldelim();
    }
    if (message.weaponLaser !== undefined) {
      SystemGenericChargingState.encode(
        message.weaponLaser,
        writer.uint32(90).fork()
      ).ldelim();
    }
    if (message.weaponMines !== undefined) {
      SystemGenericChargingState.encode(
        message.weaponMines,
        writer.uint32(98).fork()
      ).ldelim();
    }
    if (message.weaponMissiles !== undefined) {
      SystemGenericChargingState.encode(
        message.weaponMissiles,
        writer.uint32(106).fork()
      ).ldelim();
    }
    if (message.weaponPlasma !== undefined) {
      SystemGenericChargingState.encode(
        message.weaponPlasma,
        writer.uint32(114).fork()
      ).ldelim();
    }
    if (message.weaponProbes !== undefined) {
      SystemGenericChargingState.encode(
        message.weaponProbes,
        writer.uint32(122).fork()
      ).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemState } as SystemState;
    message.sensorHud = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.autoRepair = SystemAutoRepairState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 2:
          message.engines = SystemEnginesState.decode(reader, reader.uint32());
          break;
        case 3:
          message.remoteMissile = SystemRemoteState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 4:
          message.sensorCameras = SystemStateRadar.decode(
            reader,
            reader.uint32()
          );
          break;
        case 5:
          message.sensorHud.push(
            RadarSignature.decode(reader, reader.uint32())
          );
          break;
        case 6:
          message.sensorInfrared = SystemStateRadar.decode(
            reader,
            reader.uint32()
          );
          break;
        case 7:
          message.sensorProbe = SystemStateRadar.decode(
            reader,
            reader.uint32()
          );
          break;
        case 8:
          message.sensorRadar = SystemStateRadar.decode(
            reader,
            reader.uint32()
          );
          break;
        case 9:
          message.shields = SystemShieldState.decode(reader, reader.uint32());
          break;
        case 10:
          message.stealth = SystemStealthState.decode(reader, reader.uint32());
          break;
        case 11:
          message.weaponLaser = SystemGenericChargingState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 12:
          message.weaponMines = SystemGenericChargingState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 13:
          message.weaponMissiles = SystemGenericChargingState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 14:
          message.weaponPlasma = SystemGenericChargingState.decode(
            reader,
            reader.uint32()
          );
          break;
        case 15:
          message.weaponProbes = SystemGenericChargingState.decode(
            reader,
            reader.uint32()
          );
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemState>): SystemState {
    const message = { ...baseSystemState } as SystemState;
    message.sensorHud = [];
    if (object.autoRepair !== undefined && object.autoRepair !== null) {
      message.autoRepair = SystemAutoRepairState.fromPartial(object.autoRepair);
    }
    if (object.engines !== undefined && object.engines !== null) {
      message.engines = SystemEnginesState.fromPartial(object.engines);
    }
    if (object.remoteMissile !== undefined && object.remoteMissile !== null) {
      message.remoteMissile = SystemRemoteState.fromPartial(
        object.remoteMissile
      );
    }
    if (object.sensorCameras !== undefined && object.sensorCameras !== null) {
      message.sensorCameras = SystemStateRadar.fromPartial(
        object.sensorCameras
      );
    }
    if (object.sensorHud !== undefined && object.sensorHud !== null) {
      for (const e of object.sensorHud) {
        message.sensorHud.push(RadarSignature.fromPartial(e));
      }
    }
    if (object.sensorInfrared !== undefined && object.sensorInfrared !== null) {
      message.sensorInfrared = SystemStateRadar.fromPartial(
        object.sensorInfrared
      );
    }
    if (object.sensorProbe !== undefined && object.sensorProbe !== null) {
      message.sensorProbe = SystemStateRadar.fromPartial(object.sensorProbe);
    }
    if (object.sensorRadar !== undefined && object.sensorRadar !== null) {
      message.sensorRadar = SystemStateRadar.fromPartial(object.sensorRadar);
    }
    if (object.shields !== undefined && object.shields !== null) {
      message.shields = SystemShieldState.fromPartial(object.shields);
    }
    if (object.stealth !== undefined && object.stealth !== null) {
      message.stealth = SystemStealthState.fromPartial(object.stealth);
    }
    if (object.weaponLaser !== undefined && object.weaponLaser !== null) {
      message.weaponLaser = SystemGenericChargingState.fromPartial(
        object.weaponLaser
      );
    }
    if (object.weaponMines !== undefined && object.weaponMines !== null) {
      message.weaponMines = SystemGenericChargingState.fromPartial(
        object.weaponMines
      );
    }
    if (object.weaponMissiles !== undefined && object.weaponMissiles !== null) {
      message.weaponMissiles = SystemGenericChargingState.fromPartial(
        object.weaponMissiles
      );
    }
    if (object.weaponPlasma !== undefined && object.weaponPlasma !== null) {
      message.weaponPlasma = SystemGenericChargingState.fromPartial(
        object.weaponPlasma
      );
    }
    if (object.weaponProbes !== undefined && object.weaponProbes !== null) {
      message.weaponProbes = SystemGenericChargingState.fromPartial(
        object.weaponProbes
      );
    }
    return message;
  },
};

const baseSystemStateRadar: object = { range: 0 };

export const SystemStateRadar = {
  encode(
    message: SystemStateRadar,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.origin !== undefined) {
      Vec2.encode(message.origin, writer.uint32(10).fork()).ldelim();
    }
    if (message.range !== 0) {
      writer.uint32(17).double(message.range);
    }
    for (const v of message.signatures) {
      RadarSignature.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.velocity !== undefined) {
      Vec2.encode(message.velocity, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemStateRadar {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemStateRadar } as SystemStateRadar;
    message.signatures = [];
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.origin = Vec2.decode(reader, reader.uint32());
          break;
        case 2:
          message.range = reader.double();
          break;
        case 3:
          message.signatures.push(
            RadarSignature.decode(reader, reader.uint32())
          );
          break;
        case 4:
          message.velocity = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemStateRadar>): SystemStateRadar {
    const message = { ...baseSystemStateRadar } as SystemStateRadar;
    message.signatures = [];
    if (object.origin !== undefined && object.origin !== null) {
      message.origin = Vec2.fromPartial(object.origin);
    }
    if (object.range !== undefined && object.range !== null) {
      message.range = object.range;
    }
    if (object.signatures !== undefined && object.signatures !== null) {
      for (const e of object.signatures) {
        message.signatures.push(RadarSignature.fromPartial(e));
      }
    }
    if (object.velocity !== undefined && object.velocity !== null) {
      message.velocity = Vec2.fromPartial(object.velocity);
    }
    return message;
  },
};

const baseSystemStealthState: object = {
  chargeSpeed: 0,
  chargeState: 0,
  currentStealthRemaining: 0,
  stealthDuration: 0,
};

export const SystemStealthState = {
  encode(
    message: SystemStealthState,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.chargeSpeed !== 0) {
      writer.uint32(9).double(message.chargeSpeed);
    }
    if (message.chargeState !== 0) {
      writer.uint32(17).double(message.chargeState);
    }
    if (message.currentStealthRemaining !== 0) {
      writer.uint32(25).double(message.currentStealthRemaining);
    }
    if (message.powerState !== undefined) {
      SystemPowerState.encode(
        message.powerState,
        writer.uint32(34).fork()
      ).ldelim();
    }
    if (message.stealthDuration !== 0) {
      writer.uint32(41).double(message.stealthDuration);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): SystemStealthState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseSystemStealthState } as SystemStealthState;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.chargeSpeed = reader.double();
          break;
        case 2:
          message.chargeState = reader.double();
          break;
        case 3:
          message.currentStealthRemaining = reader.double();
          break;
        case 4:
          message.powerState = SystemPowerState.decode(reader, reader.uint32());
          break;
        case 5:
          message.stealthDuration = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<SystemStealthState>): SystemStealthState {
    const message = { ...baseSystemStealthState } as SystemStealthState;
    if (object.chargeSpeed !== undefined && object.chargeSpeed !== null) {
      message.chargeSpeed = object.chargeSpeed;
    }
    if (object.chargeState !== undefined && object.chargeState !== null) {
      message.chargeState = object.chargeState;
    }
    if (
      object.currentStealthRemaining !== undefined &&
      object.currentStealthRemaining !== null
    ) {
      message.currentStealthRemaining = object.currentStealthRemaining;
    }
    if (object.powerState !== undefined && object.powerState !== null) {
      message.powerState = SystemPowerState.fromPartial(object.powerState);
    }
    if (
      object.stealthDuration !== undefined &&
      object.stealthDuration !== null
    ) {
      message.stealthDuration = object.stealthDuration;
    }
    return message;
  },
};

const baseSystemWeaponFireCommand: object = {};

export const SystemWeaponFireCommand = {
  encode(
    message: SystemWeaponFireCommand,
    writer: _m0.Writer = _m0.Writer.create()
  ): _m0.Writer {
    if (message.targetPosition !== undefined) {
      Vec2.encode(message.targetPosition, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(
    input: _m0.Reader | Uint8Array,
    length?: number
  ): SystemWeaponFireCommand {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = {
      ...baseSystemWeaponFireCommand,
    } as SystemWeaponFireCommand;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.targetPosition = Vec2.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(
    object: DeepPartial<SystemWeaponFireCommand>
  ): SystemWeaponFireCommand {
    const message = {
      ...baseSystemWeaponFireCommand,
    } as SystemWeaponFireCommand;
    if (object.targetPosition !== undefined && object.targetPosition !== null) {
      message.targetPosition = Vec2.fromPartial(object.targetPosition);
    }
    return message;
  },
};

const baseVec2: object = { x: 0, y: 0 };

export const Vec2 = {
  encode(message: Vec2, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.x !== 0) {
      writer.uint32(9).double(message.x);
    }
    if (message.y !== 0) {
      writer.uint32(17).double(message.y);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Vec2 {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = { ...baseVec2 } as Vec2;
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.x = reader.double();
          break;
        case 2:
          message.y = reader.double();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromPartial(object: DeepPartial<Vec2>): Vec2 {
    const message = { ...baseVec2 } as Vec2;
    if (object.x !== undefined && object.x !== null) {
      message.x = object.x;
    }
    if (object.y !== undefined && object.y !== null) {
      message.y = object.y;
    }
    return message;
  },
};

type Builtin =
  | Date
  | Function
  | Uint8Array
  | string
  | number
  | boolean
  | undefined;
export type DeepPartial<T> = T extends Builtin
  ? T
  : T extends Array<infer U>
  ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U>
  ? ReadonlyArray<DeepPartial<U>>
  : T extends { $case: string }
  ? { [K in keyof Omit<T, "$case">]?: DeepPartial<T[K]> } & {
      $case: T["$case"];
    }
  : T extends {}
  ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}
