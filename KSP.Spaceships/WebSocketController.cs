﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.WebSockets;
using System.Threading;
using System;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	[Route("api/")]
	[ApiController]
	public class WebSocketController : ControllerBase
	{
		private readonly ILogger<WebSocketController> _logger;
		private readonly ConnectionManager _connectionManager;

		public WebSocketController(ILogger<WebSocketController> logger, ConnectionManager connectionManager)
		{
			_logger = logger;
			_connectionManager = connectionManager;
		}

		[Route("ws")]
		public async Task Get()
		{
			await HandleWebsocket(WebsocketDataPreference.Json);
		}

		[Route("ws_gzip")]
		public async Task GetGzip()
		{
			await HandleWebsocket(WebsocketDataPreference.GzipJson);
		}

		[Route("ws_hybrid")]
		public async Task GetHybrid()
		{
			await HandleWebsocket(WebsocketDataPreference.GzipJsonHybrid);
		}
		
		[Route("ws_protobuf_hybrid")]
		public async Task GetProtobufHybrid()
		{
			await HandleWebsocket(WebsocketDataPreference.ProtobufHybrid);
		}

		async Task HandleWebsocket(WebsocketDataPreference dataPreference)
		{
			if (HttpContext.WebSockets.IsWebSocketRequest)
			{
				using var webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();
				await _connectionManager.OnWebsocketCreate(webSocket, dataPreference);
			}
			else
			{
				HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
			}
			_logger.LogInformation("WS controller exiting.");
		}
	}
}