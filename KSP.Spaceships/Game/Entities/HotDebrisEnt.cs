using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	public record HotDebrisEnt : Entity
	{
		public double HeatIntensity { get; init; }
		public double IntensityLoss { get; init; }

		public override Entity Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			// Die upon impact
			if(args.CollisionEvents.Count > 0)
			{
				return null;
			}

			double newintensity = HeatIntensity - IntensityLoss * args.DeltaT;

			// Die upon zero intensity
			if (newintensity <= 0)
				return null;

			return this with
			{
				HeatIntensity = newintensity,
			};
		}

		public override RadarSignature GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			sig = sig with
			{
				intensity = HeatIntensity,
			};

			if(purpose == RadarSignaturePurpose.Camera)
			{
				sig = sig with
				{
					description = "",
				};
			}

			if (purpose == RadarSignaturePurpose.Spectator)
			{
				sig = sig with
				{
					description = constants.Strings.EntDescDebris,
					spectatorType = SpectateObjectType.Effect,
				};
			}

			return sig;
		}
	}
}
