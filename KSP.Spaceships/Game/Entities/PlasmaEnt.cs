using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	public record PlasmaEnt : Entity
	{
		public double Damage { get; init; }

		public int OwningShipEntityId { get; init; }

		public int ImpactDebrisCount { get; init; }

		public double HeatIntensity { get; init; }

		public override Entity Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			bool anycollision = false;

			foreach(var c in args.CollisionEvents)
			{
				anycollision = true;

				if (c.OtherEntityId == null)
					continue;

				var origin = this.Position;

				if(c != null && c.CollisionDirection != null && c.OtherEntityId != null && args.State.MapEntityIdToEntity.ContainsKey(c.OtherEntityId.Value))
				{
					var ent = args.State.MapEntityIdToEntity[c.OtherEntityId.Value];
					origin = args.State.MapEntityIdToEntity[c.OtherEntityId.Value].Position - c.CollisionDirection.Value;
					Simulation.MakeImpactDebris(c.OtherEntityPositionAtCollision, ent.Velocity, ent.Id, c.CurrentEntityPositionAtCollision, this.Id, ImpactDebrisCount,
						args.Constants, this.Velocity.Normalized(), simout.SpawnEnt);
				}

				simout.DamageEvents.Add(new DamageEvent()
				{
					ReceiverEntityId = c.OtherEntityId.Value,
					DealerEntityId = OwningShipEntityId,
					DamageAmount = Damage,
					Origin = origin,
					Type = DamageType.Plasma,
				});
			}

			// Die upon impact
			if(anycollision)
			{
				return null;
			}

			return this;
		}

		public override RadarSignature GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			if(purpose == RadarSignaturePurpose.Spectator || purpose == RadarSignaturePurpose.Camera)
			{
				sig = sig with
				{
					//description = constants.Strings.EntDescPlasma,
					description = string.Empty,
				};
			}

			if (purpose == RadarSignaturePurpose.Spectator)
			{
				sig = sig with
				{
					spectatorType = SpectateObjectType.Plasma,
				};
			}

			return sig;
		}
	}
}
