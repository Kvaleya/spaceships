﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public record RaceFinishEnt : Entity
	{
		public double DetectionRange { get; init; }

		public string Name { get; init; }

		public int FinishedCount { get; init; } = 0;

		public override Entity Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			var returnEnt = this;

			var constants = args.Constants;

			int finished = this.FinishedCount;

			// Explode if near enemy
			if (DetectionRange > 0)
			{
				foreach (var proxy in args.Grid.GetEntitesInsideRange(this.Position - new Vector2d(DetectionRange), this.Position + new Vector2d(DetectionRange)))
				{
					if (proxy.OldEntity.Id == this.Id)
						continue;

					if ((proxy.Position0 - Position).Length - proxy.Radius - Radius > DetectionRange)
						continue;

					var ent = proxy.OldEntity;

					if (ent is ShipEnt ship && ship.Health > 0)
					{
						simout.GlobalEventsLog.Add(string.Format(constants.Strings.ShipRaceFinished, ship.GetNiceDescription(args.State, constants), ++finished));
						simout.DamageEvents.Add(new DamageEvent()
						{
							DamageAmount = 1e10,
							DealerEntityId = this.Id,
							Origin = this.Position,
							ReceiverEntityId = ent.Id,
							Type = DamageType.Unknown,
						});
					}
				}
			}

			return returnEnt with
			{
				FinishedCount = finished,
			};
		}

		public override Entity ProcessDamageEvents(in EntityDamageArgs args, EntitySimulationOutput simout)
		{
			return this;
		}

		public override RadarSignature GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			sig = sig with
			{
				description = constants.Strings.EntNameRaceFinish,
				spectatorType = SpectateObjectType.RaceFinish,
			};

			return sig;
		}
	}
}
