using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;
using System.Runtime.CompilerServices;

namespace KSP.Spaceships
{
	enum TargetType
	{
		// Arranged in order of rising priority
		Invalid,
		Friend,
		Neutral,
		Enemy,
	}

	public record ShipEnt : Entity, ITeamEntity, IOwnedEntity
	{
		public const double HealthDeathEpsilon = 1e-6;

		public const string BoundInactivate = "inactivate";

		public int ShipId { get; init; }
		public int FactionId { get; init; }
		public int OwnerEntityId { get { return this.Id; } }

		/// <summary>
		/// Entity id of probe released by this ship, -1 if no probe exists at this time.
		/// </summary>
		public int BoundProbe { get; init; } = -1;

		/// <summary>
		/// Entity id of missile released by this ship, -1 if no missile exists at this time.
		/// </summary>
		public int BoundMissile { get; init; } = -1;

		public double Health { get; init; }

		public ShipSystemSensor SystemSensorCamera { get; init; }
		public ShipSystemSensor SystemSensorInfrared { get; init; }
		public ShipSystemSensor SystemSensorRadar { get; init; }
		public ShipSystemSensor SystemSensorRadarSmall { get; init; }
		public ShipSystemSensor SystemSensorProbe { get; init; }

		public ShipSystemPoweredCharing SystemWeaponLaser { get; init; }
		public ShipSystemPoweredCharing SystemWeaponPlasma { get; init; }
		public ShipSystemPoweredCharing SystemWeaponMissiles { get; init; }
		public ShipSystemPoweredCharing SystemWeaponMines { get; init; }
		public ShipSystemPoweredCharing SystemWeaponProbes { get; init; }

		public SystemMissileControlDummy SystemRemoteMissiles { get; init; } = new SystemMissileControlDummy();

		public ShipSystemPoweredShields SystemShields { get; init; }
		public ShipSystemPoweredStealth SystemStealth { get; init; }
		public ShipSystemAutoRepair SystemAutoRepair { get; init; }
		public ShipSystemEngines SystemEngines { get; init; }

		public ShipConsts Constants { get; init; }

		int _lastNonSelfDamagerId { get; init; } = -1;

		string _niceDescriptionCache { get; init; } = "";

		int? _killerEntId { get; init; } = null;

		/// <summary>
		/// In game seconds.
		/// </summary>
		public double ImmortalityTimer { get; init; } = 0;

		/// <summary>
		/// Creates a ship entity.
		/// Needs to know the final entityId of the ship.
		/// </summary>
		public static ShipEnt Create(GameConstants constants, ShipConsts shipConsts, int entityId, int shipId, int factionId, Vector2d position, bool onemanned)
		{
			return new ShipEnt()
			{
				Constants = shipConsts,
				// Generic
				Id = entityId,
				Radius = shipConsts.ShipRadius,
				Mass = shipConsts.ShipMass,
				Position = position,
				FactionId = factionId,

				// Ship specific
				ShipId = shipId,
				Health = shipConsts.ShipStartingHealth,

				ImmortalityTimer = shipConsts.ShipSpawnImmortalityTime * constants.TimeScale,

				// Sensors
				SystemSensorCamera = new ShipSystemSensor()
				{
					BoundEntityId = entityId,
					Purpose = RadarSignaturePurpose.Camera,
					Range = shipConsts.SensorRadiusCameras,
				},
				SystemSensorInfrared = new ShipSystemSensor()
				{
					BoundEntityId = entityId,
					Purpose = RadarSignaturePurpose.Infrared,
					Range = shipConsts.SensorRadiusInfrared,
				},
				SystemSensorRadar = new ShipSystemSensor()
				{
					BoundEntityId = entityId,
					Purpose = RadarSignaturePurpose.Radar,
					Range = shipConsts.SensorRadiusRadar,
				},
				SystemSensorRadarSmall = new ShipSystemSensor()
				{
					BoundEntityId = entityId,
					Purpose = RadarSignaturePurpose.Radar,
					Range = shipConsts.SensorRadiusRadarSmall,
				},
				SystemSensorProbe = new ShipSystemSensor()
				{
					BoundEntityId = -1,
					Purpose = RadarSignaturePurpose.Camera,
					Range = shipConsts.SensorRadiusProbe,
				},

				// Weapons
				SystemWeaponLaser = new ShipSystemPoweredCharing()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemWeaponsPowerStatesLaser),
					ChargeBaseSpeed = shipConsts.SystemWeaponChargeSpeedLaser,
					Power = shipConsts.SystemWeaponLaserInitialPower,
				},
				SystemWeaponPlasma = new ShipSystemPoweredCharing()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemWeaponsPowerStatesPlasma),
					ChargeBaseSpeed = shipConsts.SystemWeaponChargeSpeedPlasma,
				},
				SystemWeaponMissiles = new ShipSystemPoweredCharing()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemWeaponsPowerStatesMissiles),
					ChargeBaseSpeed = shipConsts.SystemWeaponChargeSpeedMissiles,
				},
				SystemWeaponMines = new ShipSystemPoweredCharing()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemWeaponsPowerStatesMines),
					ChargeBaseSpeed = shipConsts.SystemWeaponChargeSpeedMines,
				},
				SystemWeaponProbes = new ShipSystemPoweredCharing()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemWeaponsPowerStatesProbes),
					ChargeBaseSpeed = shipConsts.SystemWeaponChargeSpeedProbes,
				},

				// Other systems
				SystemShields = new ShipSystemPoweredShields()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemShieldsPowerStates),
					MaxAbsorption = shipConsts.SystemShieldsMaxAbsorption,
					RegenerationAbsorptionPerPowerPerGameSecond = shipConsts.SystemShieldsRegenerationAbsorptionPerPowerPerGameSecond,
					ArcSize = onemanned ? shipConsts.SystemShieldsArcSizeOnemanned : shipConsts.SystemShieldsArcSize,
				},
				SystemStealth = new ShipSystemPoweredStealth()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemStealthPowerStates),
					ChargeBaseSpeed = shipConsts.SystemStealthChargeBaseSpeed,
					StealthDuration = shipConsts.SystemStealthDuration,
				},
				SystemAutoRepair = new ShipSystemAutoRepair()
				{
					PowerStates = ImmutableArray.Create<int>(shipConsts.SystemAutoRepairPowerStates),
					RepairRatePerPowerPerSecond = shipConsts.SystemAutoRepairRepairRate,
				},
				SystemEngines = new ShipSystemEngines()
				{
					Power = shipConsts.ShipReactorPowerUnits,
					AccelerationPerUnitPower = shipConsts.SystemEnginesAccelerationPerUnitPower,
				},
			};
		}

		/// <summary>
		/// Computes how much power all systems consume, excluding engines.
		/// </summary>
		public int GetMainPowerUsage()
		{
			int p = 0;

			p += SystemWeaponLaser.Power;
			p += SystemWeaponPlasma.Power;
			p += SystemWeaponMissiles.Power;
			p += SystemWeaponMines.Power;
			p += SystemWeaponProbes.Power;

			p += SystemShields.Power;
			p += SystemStealth.Power;
			p += SystemAutoRepair.Power;

			return p;
		}

		public override Entity Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			return Simulate(this, args, simout);
		}

		// Static to not use old entity version (this) accidentaly
		static ShipEnt Simulate(ShipEnt ent, in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			// Decrease immortality timer
			ent = ent with
			{
				ImmortalityTimer = Math.Max(ent.ImmortalityTimer - args.DeltaT, -0.01),
			};

			// Hack to get nice description anywhere
			ent = ent with { _niceDescriptionCache = ent.GetNiceDescription(args.State, args.Constants), };

			// Death
			if(ent.Health < HealthDeathEpsilon)
			{
				simout.Messages.Add(new EntityMessage()
				{
					RecipientId = ent.BoundMissile,
					Message = BoundInactivate,
				});

				simout.Messages.Add(new EntityMessage()
				{
					RecipientId = ent.BoundProbe,
					Message = BoundInactivate,
				});

				int boomid = ent.Id;

				if (ent._killerEntId.HasValue)
					boomid = ent._killerEntId.Value;

				// Die. Boom.
				Simulation.MakeExplosion(boomid, ent.Position, ent.Constants.ShipExplosionInnerRadius,
					ent.Constants.ShipExplosionOuterRadius, ent.Constants.ShipExplosionInnerDamage,
					args.Grid, args.Constants, simout.DamageEvents, simout.SpawnEnt);
				return null;
			}

			// Update missile
			if(!args.State.MapEntityIdToEntity.ContainsKey(ent.BoundMissile))
			{
				// Invalidate bound missile if old missile no longer exists
				ent = ent with { BoundMissile = -1, };
			}

			// Update probe
			if (!args.State.MapEntityIdToEntity.ContainsKey(ent.BoundProbe))
			{
				// Invalidate bound missile if old missile no longer exists
				ent = ent with { BoundProbe = -1, };
			}

			var controls = args.State.MapShipIdToShipState[ent.ShipId].Systems;

			int newProbeId = -1;

			MineEnt missileEnt = null;

			// Only fire weapons while not in stealth
			if (ent.Constants.AllowWeapons && !ent.SystemStealth.IsInStealth())
			{
				ent = LaserAutoFire(ent, args, simout);

				// Laser fire
				if (controls.weaponLaser != null && ent.SystemWeaponLaser.IsCharged())
				{
					ent = LaserAssistedFire(ent, args, simout, controls.weaponLaser.targetPosition.ToVector2d());
				}

				// Plasma fire
				if (controls.weaponPlasma != null && ent.SystemWeaponPlasma.IsCharged())
				{
					ent = ent with { SystemWeaponPlasma = ent.SystemWeaponPlasma.ClearCharge(), };

					Vector2d baseVelocity = (controls.weaponPlasma.targetPosition.ToVector2d() - ent.Position);

					if (baseVelocity.LengthSquared == 0)
					{
						baseVelocity = Vector2d.UnitY;
					}

					baseVelocity.Normalize();
					baseVelocity *= ent.Constants.SystemWeaponPlasmaFireVelocity;

					//baseVelocity += ent.Velocity;

					for (int i = 0; i < ent.Constants.SystemWeaponPlasmaFireCount; i++)
					{
						Matrix2d rot = Matrix2d.CreateRotation(((i + 0.5) / ent.Constants.SystemWeaponPlasmaFireCount - 0.5) * ent.Constants.SystemWeaponPlasmaFireSpread);
						var vel = rot * baseVelocity;

						simout.SpawnEnt(new PlasmaEnt()
						{
							Damage = ent.Constants.SystemWeaponPlasmaFireDamage,
							HeatIntensity = ent.Constants.SystemWeaponPlasmaFireHeat,
							IgnoreCollisionsFrom = ImmutableArray.Create<int>(ent.Id),
							IsProjectile = true,
							Mass = 0.01,
							OwningShipEntityId = ent.Id,
							Radius = ent.Constants.SystemWeaponPlasmaProjectileRadius,
							Position = ent.Position,
							Velocity = vel,
							ImpactDebrisCount = ent.Constants.SystemWeaponPlasmaImpactDebrisCount,
						});
					}
				}

				// Mine fire
				if (controls.weaponMines != null && ent.SystemWeaponMines.IsCharged())
				{
					ent = ent with { SystemWeaponMines = ent.SystemWeaponMines.ClearCharge(), };

					Vector2d dir = (controls.weaponMines.targetPosition.ToVector2d() - ent.Position);

					if (dir.LengthSquared == 0)
					{
						dir = Vector2d.UnitY;
					}

					dir.Normalize();

					Vector2d pos = ent.Position;
					pos += dir * (ent.Radius + ent.Constants.SystemWeaponMinesRadius + 0.01);
					Vector2d vel = ent.Velocity + dir * ent.Constants.SystemWeaponMinesStartVelocity;

					simout.SpawnEnt(new MineEnt()
					{
						Name = args.Constants.Strings.EntNameMine,
						SpecType = SpectateObjectType.Mine,
						Acceleration = ent.Constants.SystemWeaponMinesAcceleration,
						MaxVelocity = ent.Constants.SystemWeaponMinesMaxVelocity,
						DetectionRange = ent.Constants.SystemWeaponMinesDetectionRange,
						FactionId = ent.FactionId,
						OwnerEntityId = ent.Id,
						Health = ent.Constants.SystemWeaponMinesHealth,
						ExplosionInnerDamage = ent.Constants.SystemWeaponMinesExplosionInnerDamage,
						ExplosionInnerRadius = ent.Constants.SystemWeaponMinesExplosionInnerRadius,
						ExplosionOuterRadius = ent.Constants.SystemWeaponMinesExplosionOuterRadius,
						IsProjectile = false,
						Mass = ent.Constants.SystemWeaponMinesMass,
						Radius = ent.Constants.SystemWeaponMinesRadius,
						Position = pos,
						Velocity = vel,
						HeatIntensity = ent.Constants.SystemWeaponMinesHeatIntensity,
					});
				}

				// Probes: fire new probe
				if (controls.weaponProbes != null && ent.SystemWeaponProbes.IsCharged())
				{
					if (ent.BoundProbe >= 0)
					{
						simout.Messages.Add(new EntityMessage()
						{
							RecipientId = ent.BoundProbe,
							Message = ShipEnt.BoundInactivate,
						});
					}

					Vector2d target = controls.weaponProbes.targetPosition.ToVector2d();

					ent = ent with
					{
						SystemWeaponProbes = ent.SystemWeaponProbes.ClearCharge(),
					};

					Vector2d dir = (target - ent.Position);

					if (dir.LengthSquared == 0)
					{
						dir = Vector2d.UnitY;
					}

					dir.Normalize();

					Vector2d pos = ent.Position;
					pos += dir * (ent.Radius + ent.Constants.SystemWeaponProbesRadius + 0.01);
					Vector2d vel = ent.Velocity + dir * ent.Constants.SystemWeaponProbesStartVelocity;

					newProbeId = simout.SpawnEnt(new MineEnt()
					{
						Name = args.Constants.Strings.EntNameProbe,
						SpecType = SpectateObjectType.Probe,
						HeatIntensity = ent.Constants.SystemWeaponProbesHeatIntensity,
						Acceleration = ent.Constants.SystemWeaponProbesAcceleration,
						MaxVelocity = ent.Constants.SystemWeaponProbesMaxVelocity,
						NavigationFunc = (c, e) => { return (c.weaponProbes != null && e.SystemWeaponProbes.Power > 0) ? c.weaponProbes.targetPosition.ToVector2d() : null; },
						DetectionRange = ent.Constants.SystemWeaponProbesExplodeDetectionRange,
						FactionId = ent.FactionId,
						OwnerEntityId = ent.Id,
						Health = ent.Constants.SystemWeaponProbesHealth,
						ExplosionInnerDamage = ent.Constants.SystemWeaponProbesExplosionInnerDamage,
						ExplosionInnerRadius = ent.Constants.SystemWeaponProbesExplosionInnerRadius,
						ExplosionOuterRadius = ent.Constants.SystemWeaponProbesExplosionOuterRadius,
						IsProjectile = false,
						Mass = ent.Constants.SystemWeaponProbesMass,
						Radius = ent.Constants.SystemWeaponProbesRadius,
						Position = pos,
						Velocity = vel,
						TargetPosition = target,
					});

					ent = ent with { BoundProbe = newProbeId };
				}

				// Missile: fire new missile
				if (controls.weaponMissiles != null && ent.SystemWeaponMissiles.IsCharged())
				{
					if(ent.BoundMissile >= 0)
					{
						simout.Messages.Add(new EntityMessage()
						{
							RecipientId = ent.BoundMissile,
							Message = ShipEnt.BoundInactivate,
						});
					}

					Vector2d target = controls.weaponMissiles.targetPosition.ToVector2d();

					ent = ent with { SystemWeaponMissiles = ent.SystemWeaponMissiles.ClearCharge(), };

					Vector2d dir = (target - ent.Position);

					if (dir.LengthSquared == 0)
					{
						dir = Vector2d.UnitY;
					}

					dir.Normalize();

					Vector2d pos = ent.Position;
					pos += dir * (ent.Radius + ent.Constants.SystemWeaponMissilesRadius + 0.01);
					Vector2d vel = ent.Velocity + dir * ent.Constants.SystemWeaponMissilesStartVelocity;

					bool requiresPowerToControl = ent.Constants.SystemWeaponMissilesRequiredPowerToControl;

					missileEnt = new MineEnt()
					{
						Name = args.Constants.Strings.EntNameMissile,
						SpecType = SpectateObjectType.Missile,
						HeatIntensity = ent.Constants.SystemWeaponMissilesHeatIntensity,
						Acceleration = ent.Constants.SystemWeaponMissilesAcceleration,
						MaxVelocity = ent.Constants.SystemWeaponMissilesMaxVelocity,
						NavigationFunc = (c, e) =>
						{
							if (c.remoteMissile == null)
								return null;
							if (c.remoteMissile.targetPosition == null)
								return null;
							if (e.SystemWeaponMissiles.Power <= 0 && requiresPowerToControl)
								return null;
							return c.remoteMissile.targetPosition.ToVector2d();
						},
						TriggerFunc = (c) =>
						{
							if (c.remoteMissile == null)
								return false;
							if (c.remoteMissile.selfDestruct == null)
								return false;
							return c.remoteMissile.selfDestruct.Value;
						},
						DetectionRange = ent.Constants.SystemWeaponMissilesExplodeDetectionRange,
						FactionId = ent.FactionId,
						OwnerEntityId = ent.Id,
						Health = ent.Constants.SystemWeaponMissilesHealth,
						ExplosionInnerDamage = ent.Constants.SystemWeaponMissilesExplosionInnerDamage,
						ExplosionInnerRadius = ent.Constants.SystemWeaponMissilesExplosionInnerRadius,
						ExplosionOuterRadius = ent.Constants.SystemWeaponMissilesExplosionOuterRadius,
						IsProjectile = false,
						Mass = ent.Constants.SystemWeaponMissilesMass,
						Radius = ent.Constants.SystemWeaponMissilesRadius,
						Position = pos,
						Velocity = vel,
						TargetPosition = target,
					};

					var missileId = simout.SpawnEnt(missileEnt);

					ent = ent with { BoundMissile = missileId, };
				}
			}

			if(newProbeId >= 0)
			{
				ent = ent with { SystemSensorProbe = ent.SystemSensorProbe.Bind(newProbeId) };
			}

			if(missileEnt == null && args.State.MapEntityIdToEntity.ContainsKey(ent.BoundMissile))
			{
				missileEnt = (MineEnt)args.State.MapEntityIdToEntity[ent.BoundMissile];
			}

			ent = ent with
			{
				SystemRemoteMissiles = new SystemMissileControlDummy()
				{
					State = new SystemRemoteState()
					{
						active = ent.BoundMissile >= 0,
						currentTarget = (missileEnt == null) ? new Vec2() : missileEnt.TargetPosition.ToVec2(),
					},
				},
			};

			ent = ent with { SystemWeaponLaser = ent.SystemWeaponLaser.Tick(args.DeltaT) };
			ent = ent with { SystemWeaponPlasma = ent.SystemWeaponPlasma.Tick(args.DeltaT) };
			ent = ent with { SystemWeaponMissiles = ent.SystemWeaponMissiles.Tick(args.DeltaT) };
			ent = ent with { SystemWeaponMines = ent.SystemWeaponMines.Tick(args.DeltaT) };
			ent = ent with { SystemWeaponProbes = ent.SystemWeaponProbes.Tick(args.DeltaT) };

			ent = ent with { SystemStealth = ent.SystemStealth.Tick(args.DeltaT) };

			if (controls.stealthActivate != null && controls.stealthActivate.Value)
			{
				// Stealth charged check is in the activate call
				ent = ent with { SystemStealth = ent.SystemStealth.Activate() };
			}

			if(controls.engines != null)
			{
				ent = ent.SystemEngines.Tick(ent, args.DeltaT, args.Constants, controls.engines.desiredVelocity.ToVector2d(), controls.engines.enabled);
			}
			else
			{
				ent = ent.SystemEngines.Tick(ent, args.DeltaT, args.Constants);
			}
			
			ent = ent.SystemAutoRepair.Tick(ent, args.DeltaT, args.Constants);

			// Shield controls
			{
				ent = ent with
				{
					SystemShields = ent.SystemShields.Tick(args.DeltaT, controls.shields != null ? controls.shields.direction.ToVector2d() : null),
				};
			}

			Random rnd = new Random((int)(args.State.CurrentGameElapsed * 100));

			// Engine particles
			if(ent.SystemEngines.LastDelta.LengthSquared > 0)
			{
				var rndvel = new Vector2d(rnd.NextDouble(), rnd.NextDouble());
				rndvel = rndvel * 2.0 - Vector2d.One;
				rndvel *= args.Constants.ShipFartRandomVelocity;

				simout.SpawnEnt(new HotDebrisEnt()
				{
					HeatIntensity = args.Constants.ShipFartIntensity,
					IntensityLoss = args.Constants.ShipFartDecay,
					IsProjectile = true,
					Mass = 0.01,
					Radius = args.Constants.ShipFartRadius,
					Position = ent.Position - ent.SystemEngines.LastDelta.Normalized() * (0.1 + args.Constants.ShipFartRadius + ent.Radius),
					Velocity = ent.Velocity + rndvel + -ent.SystemEngines.LastDelta * args.Constants.ShipFartVelocityScale,
				});
			}

			return ent;
		}

		static ShipEnt LaserAutoFire(ShipEnt ent, EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			if (!ent.SystemWeaponLaser.IsCharged() || ent.Constants.SystemWeaponLaserAutoFireRange <= 0)
				return ent;

			foreach (var e in args.Grid.GetEntitesInsideRange(ent.Position - new Vector2d(ent.Constants.SystemWeaponLaserAutoFireRange), ent.Position + new Vector2d(ent.Constants.SystemWeaponLaserAutoFireRange)))
			{
				double dist = (e.Position0 - ent.Position).Length - ent.Radius - e.Radius;
				if (dist > ent.Constants.SystemWeaponLaserAutoFireRange)
					continue;

				if (GetLaserTargetType(ent, args, e) == TargetType.Enemy)
				{
					ent = LaserFire(ent, e.Position0, args, simout);
					break;
				}
			}

			return ent;
		}

		static ShipEnt LaserAssistedFire(ShipEnt ent, EntitySimulationArgs args, EntitySimulationOutput simout, Vector2d target)
		{
			if (!ent.SystemWeaponLaser.IsCharged())
				return ent;

			bool hasLineOfSight(Vector2d targetPos, int targetId)
			{
				var collisionEnt = args.Grid.RayCollision(ent.Position, targetPos, out var collisionPoint, ent.Id);
				if (collisionEnt == null || collisionEnt.OldEntity.Id != targetId)
					return false;
				return true;
			}

			var targetdir = target - ent.Position;
			targetdir.Normalize();

			(Vector2d, double, TargetType) best = (target, -1, TargetType.Invalid);

			// First check whether the actual target position is a valid enemy
			var originalHit = args.Grid.RayCollision(ent.Position, target, out var _, ent.Id);

			if (originalHit != null)
			{
				if (!originalHit.IsImmovable)
				{
					// Just take the original hit and proclaim it the target, unless it is immovable
					best = (target, 2, TargetType.Enemy);
				}

				//var type = GetLaserTargetType(ent, args, originalHit);

				//// The player has hit an enemy without aim assist, use that
				//if (type == TargetType.Enemy)
				//{
				//	best = (target, 2, TargetType.Enemy);
				//}
			}

			Random r = new Random(-42);

			foreach (var e in args.Grid.GetEntitesInsideRange(
				target - new Vector2d(ent.Constants.SystemWeaponLaserAimAssistRange),
				target + new Vector2d(ent.Constants.SystemWeaponLaserAimAssistRange)))
			{
				if (best.Item2 > 1)
					break;

				double dist = (e.Position0 - target).Length - e.Radius;
				if (dist > ent.Constants.SystemWeaponLaserAimAssistRange)
					continue;

				var type = GetLaserTargetType(ent, args, e);

				if (type == TargetType.Invalid)
					continue;

				if ((int)best.Item3 > (int)type)
					continue; // Enemy always has priority

				// Try several random points in the target ent, find those we have line of sight to
				for (int i = 0; i < 20; i++)
				{
					Vector2d attempt = new Vector2d(r.NextDouble(), r.NextDouble()) * 2 - Vector2d.One;
					if (attempt.LengthSquared > 1)
						attempt.Normalize();
					attempt = attempt * e.Radius + e.Position0;

					var dir = attempt - ent.Position;
					dir.Normalize();

					var dot = Vector2d.Dot(dir, targetdir);

					if (dot < Math.Cos(30 / 180.0 * Math.PI))
					{
						// Ignore targets that are out of the 30deg cone around the intended firing direction
						continue;
					}

					if (!hasLineOfSight(attempt, e.OldEntity.Id))
						continue; // No line of sight for this spot

					if (dot > best.Item2 || ((int)best.Item3 < (int)type))
					{
						best = (attempt, dot, type);
					}
				}
			}

			ent = LaserFire(ent, best.Item1, args, simout);

			return ent;
		}

		static TargetType GetLaserTargetType(ShipEnt ent, EntitySimulationArgs args, CollisionGrid.EntityProxy proxy)
		{
			var collisionEnt = args.Grid.RayCollision(ent.Position, proxy.Position0, out var collisionPoint, ent.Id);

			// Ignoring immovable entities is a hack to avoid always hitting big asteroids
			if (proxy.IsImmovable)
				return TargetType.Invalid;

			int ownerEntId = -1;

			if (proxy.OldEntity is IOwnedEntity)
			{
				ownerEntId = ((IOwnedEntity)proxy.OldEntity).OwnerEntityId;
			}

			if (!args.State.MapEntityIdToEntity.ContainsKey(ownerEntId))
				return TargetType.Neutral;

			var other = args.State.MapEntityIdToEntity[ownerEntId];

			if (!(other is ShipEnt))
				return TargetType.Neutral;

			var ship = (ShipEnt)other;

			bool enemy = false;

			if (Simulation.IsGameTeamBased(args.State.GameType))
			{
				enemy = ship.FactionId != ent.FactionId;
			}
			else
			{
				enemy = ship.Id != ent.Id;
			}

			if (!enemy)
				return TargetType.Friend;

			return TargetType.Enemy;
		}

		static ShipEnt LaserFire(ShipEnt ent, Vector2d targetPos, EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			ent = ent with { SystemWeaponLaser = ent.SystemWeaponLaser.ClearCharge(), };

			Vector2d direction = (targetPos - ent.Position);

			if (direction.LengthSquared == 0)
			{
				direction = Vector2d.UnitY;
			}

			direction.Normalize();
			direction *= (args.State.WorldSize.X + args.State.WorldSize.Y) * 2.0;

			var dest = ent.Position + direction;

			var collisionEnt = args.Grid.RayCollision(ent.Position, dest, out var collisionPoint, ent.Id);

			if (collisionEnt != null)
			{
				simout.DamageEvents.Add(new DamageEvent()
				{
					DamageAmount = ent.Constants.SystemWeaponLaserDamage,
					DealerEntityId = ent.Id,
					ReceiverEntityId = collisionEnt.OldEntity.Id,
					Origin = ent.Position,
					Type = DamageType.Laser,
				});
				dest = collisionPoint;
			}

			// Generate cosmetic debris
			Random r = new Random(129);
			double dist = ent.Constants.SystemWeaponLaserDebrisSpacing * 0.5;
			Vector2d dir = (dest - ent.Position);
			double maxdist = dir.Length;
			dir.Normalize();
			while (dist < maxdist)
			{
				Vector2d pos = ent.Position + dist * dir;

				if (pos.X < 0 || pos.Y < 0 || pos.X >= args.State.WorldSize.X || pos.Y >= args.State.WorldSize.Y)
					break;

				Vector2d vel = new Vector2d(r.NextDouble(), r.NextDouble()) * 2.0 - Vector2d.One;
				vel.Normalize();
				vel *= ent.Constants.SystemWeaponLaserDebrisVelocity;
				vel += ent.Constants.SystemWeaponLaserDebrisDirectionalVelocity * dir;

				simout.SpawnEnt(new HotDebrisEnt()
				{
					HeatIntensity = ent.Constants.SystemWeaponLaserDebrisIntensity,
					IntensityLoss = ent.Constants.SystemWeaponLaserDebrisIntensityLoss,
					IsProjectile = true,
					Mass = 0.01,
					Radius = ent.Constants.SystemWeaponLaserDebrisRadius,
					Position = pos,
					Velocity = vel,
				});

				dist += ent.Constants.SystemWeaponLaserDebrisSpacing;
			}

			if(collisionEnt != null)
			{
				Simulation.MakeImpactDebris(collisionEnt.Position0, collisionEnt.Velocity, collisionEnt.OldEntity.Id, collisionPoint,
					collisionEnt.OldEntity.Id + (int)args.State.CurrentGameElapsed,
					ent.Constants.SystemWeaponLaserImpactDebrisCount, args.Constants, dir, simout.SpawnEnt);
			}

			return ent;
		}

		public ShipEnt UpdateSensors(in EntitySimulationArgs args)
		{
			var ent = this;

			ent = ent with { SystemSensorCamera = ent.SystemSensorCamera.Tick(args.State, args.Constants, args.Grid) };
			ent = ent with { SystemSensorInfrared = ent.SystemSensorInfrared.Tick(args.State, args.Constants, args.Grid) };
			ent = ent with { SystemSensorRadar = ent.SystemSensorRadar.Tick(args.State, args.Constants, args.Grid) };
			ent = ent with { SystemSensorRadarSmall = ent.SystemSensorRadarSmall.Tick(args.State, args.Constants, args.Grid) };
			ent = ent with { SystemSensorProbe = ent.SystemSensorProbe.Tick(args.State, args.Constants, args.Grid,
				(ent.SystemWeaponProbes.Power > 0 || !Constants.SystemWeaponProbesRequiredPowerToSee)) };

			return ent;
		}

		public override Entity ProcessDamageEvents(in EntityDamageArgs args, EntitySimulationOutput simout)
		{
			double hp = this.Health;
			double oldhp = hp;

			var shield = this.SystemShields;

			int lastDealer = this._lastNonSelfDamagerId;

			foreach(var e in args.DamageEvents)
			{
				bool isFromShip = false;
				if(args.State.MapEntityIdToEntity.ContainsKey(e.DealerEntityId) && args.State.MapEntityIdToEntity[e.DealerEntityId] is ShipEnt)
				{
					isFromShip = true;
				}


				if (ImmortalityTimer > 0)
				{
					simout.EntityEventsLog.Add((this.Id, string.Format(args.Constants.Strings.ShipDamageIgnoredVictim, e.DamageAmount, e.Type), EntityEventType.DamageTaken));
					simout.EntityEventsLog.Add((e.DealerEntityId, string.Format(args.Constants.Strings.ShipDamageIgnoredDealer, e.DamageAmount, e.Type), EntityEventType.DamageDealt));

					continue;
				}

				double damageBeforeShields = e.DamageAmount;
				double damageAfterShields = damageBeforeShields;
				shield = shield.Absorb(ref damageAfterShields, (e.Origin - this.Position), out var hitShields);
				hp -= damageAfterShields;

				if(e.DealerEntityId != this.Id)
				{
					lastDealer = e.DealerEntityId;
				}

				if (hp < HealthDeathEpsilon)
				{
					// Ship has died
					if(lastDealer < 0)
					{
						lastDealer = e.DealerEntityId;
					}

					var dealer = args.State.MapEntityIdToEntity[lastDealer];

					if (Constants.IsStation)
					{
						if (dealer is ShipEnt)
						{
							simout.KillEvents.Add(new KillEvent()
							{
								VictimId = this.Id,
								KillerId = dealer.Id,
								Type = e.Type,
							});
						}
						else
						{
							simout.KillEvents.Add(new KillEvent()
							{
								VictimId = this.Id,
								KillerId = -1,
								Type = e.Type,
							});
						}
					}
					else
					{
						if (dealer is ShipEnt)
						{
							simout.KillEvents.Add(new KillEvent()
							{
								VictimId = this.Id,
								KillerId = dealer.Id,
								Type = e.Type,
							});
						}
						else
						{
							simout.KillEvents.Add(new KillEvent()
							{
								VictimId = this.Id,
								KillerId = -1,
								Type = e.Type,
							});
						}
					}

					// Die in the next tick in order to explode
					return this with
					{
						Health = -1,
						_killerEntId = dealer.Id,
					};
				}

				EntityEventType typeVictim = EntityEventType.DamageTaken;
				EntityEventType typeDealer = EntityEventType.DamageDealt;

				string messageVictim = null;
				string messageDealer = null;

				if (hitShields)
				{
					messageVictim = args.Constants.Strings.ShipDamageAbsorbedVictim;
					messageDealer = args.Constants.Strings.ShipDamageAbsorbedDealer;
				}
				else
				{
					messageVictim = args.Constants.Strings.ShipDamageNoShieldsVictim;
					messageDealer = args.Constants.Strings.ShipDamageNoShieldsDealer;
				}

				if (this.Id == e.DealerEntityId)
				{
					// Self damage
					messageVictim = args.Constants.Strings.ShipDamageSelf + " " + messageVictim;
					typeVictim = EntityEventType.SelfDamageTaken;
					messageDealer = null;
				}

				if(isFromShip && Simulation.IsGameTeamBased(args.State.GameType))
				{
					var ship = (ShipEnt)args.State.MapEntityIdToEntity[e.DealerEntityId];

					if(ship.FactionId == this.FactionId)
					{
						// Team damage
						messageDealer = args.Constants.Strings.ShipDamageSameTeam + " " + messageDealer;
						typeDealer = EntityEventType.TeamDamageDealt;
					}
				}

				if (messageVictim != null)
				{
					simout.EntityEventsLog.Add((this.Id,
						string.Format(messageVictim,
							damageAfterShields.ToString("0.000"), e.Type, (damageBeforeShields - damageAfterShields).ToString("0.000"),
							damageBeforeShields.ToString("0.000")),
						typeVictim));
				}

				if (messageDealer != null)
				{
					simout.EntityEventsLog.Add((e.DealerEntityId,
					   string.Format(messageDealer,
						   damageAfterShields.ToString("0.000"), e.Type, (damageBeforeShields - damageAfterShields).ToString("0.000"),
						   damageBeforeShields.ToString("0.000")),
					   typeDealer));
				}
			}

			if(Constants.IsStation)
			{
				var consts = args.Constants;

				void announceLowHp(double threshold)
				{
					if (oldhp >= threshold && hp < threshold)
					{
						simout.GlobalEventsLog.Add(string.Format(consts.Strings.StationHealthDecreased, _niceDescriptionCache, threshold));
					}
				}

				announceLowHp(64.0);
				announceLowHp(32.0);
				announceLowHp(16.0);
				announceLowHp(8.0);
				announceLowHp(4.0);
				announceLowHp(2.0);
				announceLowHp(1.0);
				announceLowHp(0.50);
				announceLowHp(0.25);
			}
			
			return this with
			{
				Health = hp,
				SystemShields = shield,
				_lastNonSelfDamagerId = lastDealer,
			};
		}

		public override RadarSignature? GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			if(Constants.IsStation)
			{
				if (purpose == RadarSignaturePurpose.Spectator || purpose == RadarSignaturePurpose.Camera)
				{
					string desc = _niceDescriptionCache;

					desc += " " + string.Format(constants.Strings.StationHealthDesc, Health.ToString("0.00"));

					sig = sig with
					{
						description = desc,
					};
				}

				if (purpose == RadarSignaturePurpose.Spectator)
				{
					sig = sig with
					{
						spectatorType = SpectateObjectType.Station,
					};
				}

				return sig;
			}

			if(purpose == RadarSignaturePurpose.Spectator)
			{
				string desc = _niceDescriptionCache;

				sig = sig with
				{
					spectatorType = SpectateObjectType.Ship,
				};

				if (SystemStealth.IsInStealth())
				{
					desc += " " + constants.Strings.ShipNoteInvisible;
					sig = sig with
					{
						spectatorType = SpectateObjectType.ShipInvisible,
					};
				}

				sig = sig with
				{
					description = desc,
				};
			}
			else
			{
				if (SystemStealth.IsInStealth())
				{
					return null;
				}
			}

			if (purpose == RadarSignaturePurpose.Camera)
			{
				var desc = Simulation.GetFriendlyEnemyYours(state, constants, askingShipId, this.OwnerEntityId, this.FactionId);

				desc += " " + state.MapShipIdToShipDesc[ShipId].Name;

				sig = sig with
				{
					description = desc,
				};
			}

			return sig;
		}

		public string GetNiceDescription(GameState state, GameConstants constants)
		{
			var shipdesc = state.MapShipIdToShipDesc[ShipId];

			if(this.Constants.IsStation)
			{
				return string.Format(constants.Strings.StationDescription, shipdesc.Name, constants.Strings.FactionNames[shipdesc.FactionId]);
			}

			var crew = Simulation.GetShipCrew(state, ShipId);

			string nice = string.Format(constants.Strings.ShipDescription, shipdesc.Name, constants.Strings.FactionNames[shipdesc.FactionId]) + " (";

			for(int i = 0; i < crew.Count; i++)
			{
				nice += crew[i].Name;

				if (i < crew.Count - 1)
					nice += ", ";
			}
			nice += ")";

			return nice;
		}
	}
}
