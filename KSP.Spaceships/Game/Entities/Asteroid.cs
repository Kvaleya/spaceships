using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public record Asteroid : Entity
	{
		public double Health { get; init; } = double.PositiveInfinity;

		public override Entity ProcessDamageEvents(in EntityDamageArgs args, EntitySimulationOutput simout)
		{
			var ent = this;

			if (double.IsInfinity(ent.Health))
				return ent;

			double hp = ent.Health;

			foreach(var d in args.DamageEvents)
			{
				hp -= d.DamageAmount;
			}

			if(hp > 0)
			{
				return ent with
				{
					Health = hp,
				};
			}

			// Asteroid was destroyed. Spawn some smaller new ones.
			Random r = new Random(ent.Id);

			int childCount = r.Next(2, 6);

			double area = Radius * Radius * Math.PI;

			for(int i = 0; i < childCount; i++)
			{
				double childArea = area / childCount;
				childArea *= 0.8;
				childArea *= (r.NextDouble() * 0.5 + 0.5);

				double childRadius = Math.Sqrt(childArea / Math.PI);

				if (childRadius < 0.4)
					continue; // Don't spawn too small asteroids

				double childMass = Math.Pow(childRadius, args.Constants.WorldAsteroidMassFromRadiusPow);

				Vector2d pos = new Vector2d(r.NextDouble(), r.NextDouble()) * 2.0 - Vector2d.One;
				pos.Normalize();
				pos *= Radius - childRadius;

				pos += Position;

				Vector2d vel = new Vector2d(r.NextDouble(), r.NextDouble()) * 2.0 - Vector2d.One;
				vel *= 0.4;
				vel += Velocity;

				simout.SpawnEnt(new Asteroid()
				{
					Mass = childMass,
					Radius = childRadius,
					Health = childMass * args.Constants.WorldAsteroidHealthFromMassScale,
					Position = pos,
					Velocity = vel,
				});
			}

			return null;
		}

		public override RadarSignature GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			if(purpose == RadarSignaturePurpose.Spectator)
			{
				sig = sig with
				{
					description = constants.Strings.EntDescAsteroid,
					spectatorType = SpectateObjectType.Asteroid,
				};
			}

			return sig;
		}
	}
}
