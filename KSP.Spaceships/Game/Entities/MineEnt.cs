using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public record MineEnt : Entity, ITeamEntity, IOwnedEntity
	{
		public int FactionId { get; init; }

		public int OwnerEntityId { get; init; }

		public double Health { get; init; }

		public double DetectionRange { get; init; }

		int? _killerEntId { get; init; } = null;

		public string Name { get; init; }

		public double ExplosionInnerDamage { get; init; } = 0.501;
		public double ExplosionInnerRadius { get; init; } = 4.0;
		public double ExplosionOuterRadius { get; init; } = 8.0;

		public double Acceleration { get; init; } = 0.0;
		public double MaxVelocity { get; init; } = 0.0;

		public double HeatIntensity { get; init; } = 0.0;

		public Func<RequestSystemState, ShipEnt, Vector2d?> NavigationFunc { get; init; } = null;
		public Func<RequestSystemState, bool> TriggerFunc { get; init; } = null;

		public Vector2d TargetPosition { get; init; } = Vector2d.Zero;

		public SpectateObjectType SpecType { get; init; } = SpectateObjectType.Unknown;

		string _niceDescriptionCache { get; init; } = "";

		bool _active { get; init; } = true;

		public override Entity Simulate(in EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			var returnEnt = this;

			// Process messages
			foreach(var msg in args.IncomingMessages)
			{
				if(msg == ShipEnt.BoundInactivate)
				{
					returnEnt = returnEnt with
					{
						_active = false,
					};

					returnEnt = returnEnt with
					{
						_niceDescriptionCache = GetNiceDescription(args.Constants),
					};
				}
			}

			// Try to accelerate towards target (missile mode)
			while(returnEnt._active) // While loop to get early-out with break
			{
				if (NavigationFunc == null)
					break;

				if (Acceleration <= 0)
					break;

				if (!args.State.MapEntityIdToEntity.ContainsKey(OwnerEntityId))
					break;

				var shipent = (ShipEnt)args.State.MapEntityIdToEntity[OwnerEntityId];

				if (!args.State.MapShipIdToShipState.ContainsKey(shipent.ShipId))
					break;

				var controls = args.State.MapShipIdToShipState[shipent.ShipId].Systems;

				if (controls == null)
					break;

				if (TriggerFunc != null && TriggerFunc(controls))
				{
					Explode(args, simout);
					return null;
				}

				var target = NavigationFunc(controls, shipent);

				if (!target.HasValue)
					break;

				returnEnt = returnEnt with
				{
					TargetPosition = target.Value,
				};

				break;
			}

			// Explode if dead
			if (Health < ShipEnt.HealthDeathEpsilon)
			{
				simout.EntityEventsLog.Add((OwnerEntityId, string.Format(args.Constants.Strings.GenericMineDestroyed, Name), EntityEventType.Generic));
				Explode(args, simout);
				return null;
			}

			var constants = args.Constants;

			void onTrigger()
			{
				simout.EntityEventsLog.Add((OwnerEntityId, string.Format(constants.Strings.GenericMineTriggered, Name), EntityEventType.Generic));
			}

			// Explode if near enemy
			if(DetectionRange > 0)
			{
				foreach (var proxy in args.Grid.GetEntitesInsideRange(this.Position - new Vector2d(DetectionRange), this.Position + new Vector2d(DetectionRange)))
				{
					if (proxy.OldEntity.Id == this.Id || proxy.OldEntity.Id == OwnerEntityId)
						continue;

					if ((proxy.Position0 - Position).Length - proxy.Radius - Radius > DetectionRange)
						continue;

					var ent = proxy.OldEntity;

					if(ent is ShipEnt)
					{
						var ship = (ShipEnt)ent;

						if (ship.SystemStealth.IsInStealth())
							continue; // NO BOOM WHEN INVISIBLE

						if (ship.ImmortalityTimer > 0)
							continue; // No boom for newly spawned ships
					}

					if (Simulation.IsGameTeamBased(args.State.GameType))
					{
						if ((ent is ITeamEntity))
						{
							int faction = ((ITeamEntity)ent).FactionId;

							if (faction != this.FactionId)
							{
								onTrigger();
								Explode(args, simout);
								return null;
							}
						}
					}
					else
					{
						if ((ent is IOwnedEntity))
						{
							int owner = ((IOwnedEntity)ent).OwnerEntityId;

							if (owner != this.OwnerEntityId)
							{
								onTrigger();
								Explode(args, simout);
								return null;
							}
						}
					}
				}
			}

			returnEnt = returnEnt with
			{
				_niceDescriptionCache = GetNiceDescription(args.Constants),
			};

			if(Acceleration > 0 && returnEnt._active)
			{
				var targetvel = MaxVelocity * (TargetPosition - returnEnt.Position).Normalized();
				var delta = targetvel - returnEnt.Velocity;

				if(delta.Length > Acceleration * args.DeltaT)
				{
					delta = delta.Normalized() * Acceleration * args.DeltaT;
				}

				returnEnt = returnEnt with
				{
					Velocity = returnEnt.Velocity + delta,
				};

				Random rnd = new Random((int)(args.State.CurrentGameElapsed * 100));

				if (delta.LengthSquared > 0)
				{
					var rndvel = new Vector2d(rnd.NextDouble(), rnd.NextDouble());
					rndvel = rndvel * 2.0 - Vector2d.One;
					rndvel *= args.Constants.ShipFartRandomVelocity;

					simout.SpawnEnt(new HotDebrisEnt()
					{
						HeatIntensity = args.Constants.ShipFartIntensity,
						IntensityLoss = args.Constants.ShipFartDecay,
						IsProjectile = true,
						Mass = 0.01,
						Radius = args.Constants.ShipFartRadius,
						Position = this.Position - delta.Normalized() * (0.1 + args.Constants.ShipFartRadius + this.Radius),
						Velocity = this.Velocity + rndvel + -delta * args.Constants.ShipFartVelocityScale,
					});
				}
			}

			return returnEnt;
		}

		public override Entity ProcessDamageEvents(in EntityDamageArgs args, EntitySimulationOutput simout)
		{
			double hp = this.Health;

			var ent = this;

			foreach(var e in args.DamageEvents)
			{
				var oldhp = hp;
				hp -= e.DamageAmount;
				// Don't die at this point, we want to explode
				if(oldhp >= ShipEnt.HealthDeathEpsilon && hp < ShipEnt.HealthDeathEpsilon)
				{
					ent = ent with
					{
						_killerEntId = e.DealerEntityId,
					};
				}
			}

			return ent with
			{
				Health = hp,
			};
		}

		void Explode(EntitySimulationArgs args, EntitySimulationOutput simout)
		{
			int shipId = OwnerEntityId;

			if(_killerEntId.HasValue)
			{
				shipId = _killerEntId.Value;
			}

			if (ExplosionInnerDamage < 0)
				return; // No explosion - but do the explosion effect for damage=0
			Simulation.MakeExplosion(shipId, Position, ExplosionInnerRadius, ExplosionOuterRadius, ExplosionInnerDamage, args.Grid, args.Constants, simout.DamageEvents, simout.SpawnEnt);
		}

		public override RadarSignature GetSignature(GameState state, GameConstants constants, RadarSignaturePurpose purpose, int? askingShipId = null)
		{
			var sig = base.GetSignature(state, constants, purpose, askingShipId);

			if(purpose == RadarSignaturePurpose.Spectator)
			{
				string desc = _niceDescriptionCache;

				sig = sig with
				{
					description = desc,
				};
			}

			if(purpose == RadarSignaturePurpose.Camera)
			{
				var desc = Simulation.GetFriendlyEnemyYours(state, constants, askingShipId, this.OwnerEntityId, this.FactionId);

				string shipname = "?";

				try
				{
					shipname = state.MapShipIdToShipDesc[((ShipEnt)state.MapEntityIdToEntity[this.OwnerEntityId]).ShipId].Name;
				}
				catch
				{
					// Hope this will not happen.
				}

				desc += $" {this.Name} ({shipname})";

				sig = sig with
				{
					description = desc,
				};
			}

			if (purpose == RadarSignaturePurpose.Spectator)
			{
				sig = sig with { spectatorType = SpecType, };
			}

			return sig;
		}

		public string GetNiceDescription(GameConstants constants)
		{
			string nice = string.Format(constants.Strings.GenericMineDescription, Name, constants.Strings.FactionNames[FactionId]);
			if(!_active)
			{
				nice += " " + constants.Strings.GenericMineInactive;
			}
			return nice;
		}
	}
}
