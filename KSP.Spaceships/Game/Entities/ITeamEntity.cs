using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	public interface ITeamEntity
	{
		/// <summary>
		/// Returns faction id of the entity.
		/// Returns -1 if the entity is not part of any faction at the moment.
		/// </summary>
		public int FactionId { get; }
	}
}
