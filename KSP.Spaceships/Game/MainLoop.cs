using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public class MainLoop
	{
		Thread _loopThread;
		Simulation _sim;
		BlockingCollection<GameQueueItem> _queue = new BlockingCollection<GameQueueItem>();
		ILogger<MainLoop> _logger;

		public MainLoop(ILogger<MainLoop> logger, Simulation sim, ConnectionManager cm)
		{
			_logger = logger;
			_sim = sim;
			cm.SetMainLoop(this);
			Start();
		}

		public void Start()
		{
			if (_loopThread != null)
				throw new Exception("Main loop is already started!");

			_logger.LogInformation("Starting main loop");
			_loopThread = new Thread(() => this.Loop());
			_loopThread.Start();
		}

		void Loop()
		{
			while (true)
			{
				var item = _queue.Take();
				CustomMetrics.QueueLength.Dec();

				if (item.ExitMainLoopEvent != null)
				{
					_logger.LogInformation("Exiting main loop");
					return;
				}

				_sim.Process(item);
				item.OnCompleted?.Invoke();
			}
		}

		public void Enqueue(GameQueueItem item)
		{
			if(item == null)
			{
				throw new ArgumentNullException(nameof(item), "Queue items cannot be null!");
			}

			CustomMetrics.RequestCount.Inc();
			CustomMetrics.QueueLength.Inc();

			_queue.Add(item);
		}
	}
}
