using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.IO.Compression;
using System.Text.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	static class AsteroidSpawner
	{
		/// <summary>
		/// Spawns random asteroids.
		/// </summary>
		/// <param name="state">Input game state.</param>
		/// <param name="asteroidCircleRadius">In percentage, 1.0 = asteroids in inscribed circle in the world square</param>
		/// <returns>Game state with asteroids spawned.</returns>
		public static GameState SpawnAsteroids(GameState state, GameConstants constants, double ringscale = 1.0, bool excludeCorners = false)
		{
			List<Entity> ents = new List<Entity>();
			double area = state.WorldSize.X * state.WorldSize.Y * (constants.WorldAsteroidRingRadiusScale * constants.WorldAsteroidRingRadiusScale * Math.PI * 0.25);

			var exclusionCorners = new Vector3d[]
			{
				new Vector3d(0, 0, 16), // x, y, radius
				new Vector3d(state.WorldSize.X, state.WorldSize.Y, 16),
			};

			foreach (var type in constants.WorldAsteroidTypes)
			{
				ents.Clear();

				Random random = new Random(type.RandomSeed + constants.WorldAsteroidSeedOffset);
				int count = (int)Math.Floor(area * type.Density);

				for (int i = 0; i < count; i++)
				{
					Vector2d pos = new Vector2d(random.NextDouble(), random.NextDouble());
					double radius = type.RadiusMin + (type.RadiusMax - type.RadiusMin) * random.NextDouble();
					Vector2d velocity = new Vector2d(random.NextDouble(), random.NextDouble()) * 2 - Vector2d.One;

					pos = pos * 2.0 - Vector2d.One;
					pos *= type.NormalizedRingScale;

					double len = pos.Length;
					if(len > 1)
					{
						len = Math.Pow(len, type.OutOfRingPow);
						pos = pos.Normalized() * len;
					}

					pos *= constants.WorldAsteroidRingRadiusScale * ringscale;
					pos = pos * 0.5 + new Vector2d(0.5);

					pos = pos * new Vector2d(state.WorldSize.X, state.WorldSize.Y);

					// Reject asteroids that intersect existing ents (even asteroid in previous type)
					bool reject = false;
					foreach (var ent in state.MapEntityIdToEntity.Values)
					{
						if ((ent.Position - pos).Length < radius + ent.Radius)
						{
							reject = true;
							break;
						}
					}

					foreach(var corner in exclusionCorners)
					{
						if((pos - corner.Xy).Length < corner.Z + radius)
						{
							reject = true;
						}
					}

					if ((pos.X - radius < 0 || pos.X + radius > state.WorldSize.X ||
						pos.Y - radius < 0 || pos.Y + radius > state.WorldSize.Y)
						&& !type.IsImmovable)
						continue; // Reject asteroids that touch world border

					if (reject)
						continue;

					
					if (velocity.LengthSquared > 1)
						velocity.Normalize();

					velocity /= radius;
					velocity *= type.VelocityScale;

					double mass = Math.Pow(radius, constants.WorldAsteroidMassFromRadiusPow);

					ents.Add(new Asteroid()
					{
						Position = pos,
						Velocity = velocity,
						Mass = type.IsImmovable ? -1 : mass,
						Radius = radius,
						Health = type.IsImmovable ? double.PositiveInfinity : (constants.WorldAsteroidHealthFromMassScale * mass),
					});
				}

				state = Simulation.SpawnEntities(state, ents.ToArray());
			}

			return state;
		}
	}
}
