using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.IO.Compression;
using System.Text.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	class ZipWriter : IDisposable
	{
		public const string EntryContent = "content";

		FileStream _stream;
		ZipArchive _zip;
		StreamWriter _writer;

		public ZipWriter(string filename)
		{
			_stream = new FileStream(filename, FileMode.Create, FileAccess.Write);
			_zip = new ZipArchive(_stream, ZipArchiveMode.Create);
			var entry = _zip.CreateEntry(EntryContent, CompressionLevel.Optimal);
			_writer = new StreamWriter(entry.Open());
		}

		public void WriteLine(string line)
		{
			_writer.WriteLine(line);
			_writer.Flush();
		}

		public void Dispose()
		{
			_writer.Dispose();
			_zip.Dispose();
			_stream.Dispose();
		}
	}

	public class ZipReader
	{
		FileStream _stream;
		ZipArchive _zip;
		StreamReader _reader;

		public ZipReader(string filename)
		{
			_stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
			_zip = new ZipArchive(_stream, ZipArchiveMode.Read);
			var entry = _zip.GetEntry(ZipWriter.EntryContent);
			_reader = new StreamReader(entry.Open());
		}

		public string ReadLine()
		{
			return _reader.ReadLine();
		}

		public bool EndOfStream => _reader.EndOfStream;

		public void Dispose()
		{
			_reader.Dispose();
			_zip.Dispose();
			_stream.Dispose();
		}
	}

	public class Recorder : IDisposable
	{
		ILogger<Recorder> _logger;

		ZipWriter _writer;

		public Recorder(ILogger<Recorder> logger)
		{
			_logger = logger;
		}

		public static string GetRealPath(string file)
		{
			return "../" + file;
		}

		public void StartRecording()
		{
			string filename = $"record_{DateTimeOffset.UtcNow.Year.ToString("D4")}_{DateTimeOffset.UtcNow.Month.ToString("D2")}_{DateTimeOffset.UtcNow.Day.ToString("D2")}-_{DateTimeOffset.UtcNow.Hour.ToString("D2")}_{DateTimeOffset.UtcNow.Minute.ToString("D2")}_{DateTimeOffset.UtcNow.Second.ToString("D2")}.game";

			var path = GetRealPath(filename);

			_writer?.Dispose();

			_writer = new ZipWriter(path);

			_logger.LogInformation($"Recording started, file: {filename}");
		}

		public void Record(ServerResponse response)
		{
			return; // disable

			if (_writer == null)
				return;

			try
			{
				var json = JsonSerializer.SerializeToUtf8Bytes(response);
				var base64 = System.Convert.ToBase64String(json);
				_writer.WriteLine(base64);
			}
			catch(Exception e)
			{
				_logger.LogError(e, "Recorder encountered an exception.");
				_writer.Dispose();
				_writer = null;
			}
		}

		public void StopRecording()
		{
			_writer?.Dispose();
			_writer = null;
			_logger.LogInformation($"Recording stopped.");
		}

		public void Dispose()
		{
			_writer?.Dispose();
			_writer = null;
		}

		// https://stackoverflow.com/questions/11743160/how-do-i-encode-and-decode-a-base64-string
		public static string Base64Decode(string base64EncodedData)
		{
			var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
			return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
		}
	}

	public class Replayer : IDisposable
	{
		ILogger<Replayer> _logger;

		ZipReader _reader;

		public Replayer(ILogger<Replayer> logger)
		{
			_logger = logger;
		}

		public bool StartReplay(string filename)
		{
			var path = Recorder.GetRealPath(filename);

			if (!File.Exists(path))
			{
				_logger.LogWarning($"Replay failed: file does not exist. ({filename})");
				return false;
			}

			_reader?.Dispose();

			_reader = new ZipReader(path);

			_logger.LogInformation($"Replay started, file: {filename}");

			return true;
		}

		public ServerResponse GetFrame()
		{
			if(_reader == null)
			{
				return null;
			}

			if (_reader.EndOfStream)
			{
				_reader?.Dispose();
				_reader = null;
				_logger.LogInformation("Replay stopped (reached end of file)");
				return null;
			}

			var base64 = _reader.ReadLine();
			var json = Recorder.Base64Decode(base64);
			var obj = JsonSerializer.Deserialize<ServerResponse>(json);
			return obj;
		}

		public void Dispose()
		{
			_reader?.Dispose();
			_reader = null;
		}
	}
}
