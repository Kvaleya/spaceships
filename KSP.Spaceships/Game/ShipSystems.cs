using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public abstract record ShipSystem
	{
		public abstract object GetClientState(GameConstants constants);
	}

	public abstract record ShipSystemPowered : ShipSystem
	{
		/// <summary>
		/// Current power level (not power state index!)
		/// </summary>
		public int Power { get; init; } = 0;

		/// <summary>
		/// Valid power levels, ordered from smallest (zero) to largest.
		/// </summary>
		public ImmutableArray<int> PowerStates { get; init; }

		public double PowerRatio()
		{
			return Power / (double)PowerStates[PowerStates.Length - 1];
		}
	}

	public record ShipSystemPoweredCharing : ShipSystemPowered
	{
		public double CurrentCharge { get; init; } = 0;
		public double ChargeBaseSpeed { get; init; } = 1;

		/// <summary>
		/// Charge speed is scaled by current power level.
		/// </summary>
		public double CurrentChargeSpeed()
		{
			return ChargeBaseSpeed * PowerRatio();
		}

		public bool IsCharged()
		{
			return CurrentCharge >= 1;
		}

		/// <summary>
		/// Sets current charge to zero.
		/// </summary>
		public ShipSystemPoweredCharing ClearCharge()
		{
			return this with
			{
				CurrentCharge = 0,
			};
		}

		/// <summary>
		/// Simulates one charging system tick.
		/// If the system is not powered, it instantly loses all charge.
		/// </summary>
		/// <param name="deltaT">Game time delta since last tick.</param>
		public ShipSystemPoweredCharing Tick(double deltaT)
		{
			if(Power == 0)
			{
				return this.ClearCharge();
			}

			double newcharge = CurrentCharge;

			newcharge += CurrentChargeSpeed() * deltaT;
			newcharge = Math.Min(newcharge, 1);

			return this with
			{
				CurrentCharge = newcharge,
			};
		}

		public override object GetClientState(GameConstants constants)
		{
			return new SystemGenericChargingState()
			{
				chargeSpeed = CurrentChargeSpeed() * constants.TimeScale,
				chargeState = CurrentCharge,
				powerState = new SystemPowerState()
				{
					currentPowerState = Power,
					powerStates = PowerStates.ToArray(),
					allowPowerUsageChange = true,
				},
			};
		}
	}

	public record SystemMissileControlDummy : ShipSystem
	{
		public SystemRemoteState State { get; init; } = new SystemRemoteState();

		public override object GetClientState(GameConstants constants)
		{
			return State;
		}
	}

	// Partly a copy of ShipSystemCharing
	public record ShipSystemPoweredStealth : ShipSystemPowered
	{
		public double CurrentStealthRemaining { get; init; }

		public double StealthDuration { get; init; }

		public double CurrentCharge { get; init; }
		public double ChargeBaseSpeed { get; init; }

		/// <summary>
		/// Charge speed is scaled by current power level.
		/// </summary>
		public double CurrentChargeSpeed()
		{
			return ChargeBaseSpeed * PowerRatio();
		}

		public bool IsCharged()
		{
			return CurrentCharge >= 1;
		}

		public bool IsInStealth()
		{
			return CurrentStealthRemaining > 0;
		}

		/// <summary>
		/// Sets current charge to zero, activates stealth.
		/// </summary>
		public ShipSystemPoweredStealth Activate()
		{
			if (!IsCharged() || IsInStealth())
				return this; // Cannot activate stealth

			return this with
			{
				CurrentCharge = 0,
				CurrentStealthRemaining = StealthDuration,
			};
		}

		/// <summary>
		/// Simulates one charging system tick.
		/// If the system is not powered, it instantly loses all charge.
		/// </summary>
		/// <param name="deltaT">Game time delta since last tick.</param>
		public ShipSystemPoweredStealth Tick(double deltaT)
		{
			var sys = this;

			double newcharge = CurrentCharge;

			newcharge += CurrentChargeSpeed() * deltaT;
			newcharge = Math.Min(newcharge, 1);

			double newtimer = CurrentStealthRemaining;

			newtimer -= deltaT;
			newtimer = Math.Max(newtimer, 0);

			sys = sys with
			{
				CurrentCharge = newcharge,
				CurrentStealthRemaining = newtimer,
			};

			if (Power == 0 || IsInStealth())
			{
				sys = sys with
				{
					CurrentCharge = 0,
				};
			}

			return sys;
		}

		public override object GetClientState(GameConstants constants)
		{
			return new SystemStealthState()
			{
				chargeSpeed = CurrentChargeSpeed() * constants.TimeScale,
				chargeState = CurrentCharge,
				powerState = new SystemPowerState()
				{
					currentPowerState = Power,
					powerStates = PowerStates.ToArray(),
					allowPowerUsageChange = !IsInStealth(),
				},
				currentStealthRemaining = CurrentStealthRemaining / constants.TimeScale,
				stealthDuration = StealthDuration / constants.TimeScale,
			};
		}
	}

	public record ShipSystemPoweredShields : ShipSystemPowered
	{
		public Vector2d Direction { get; init; } = Vector2d.UnitX;

		public double Absorption { get; init; } = 0;
		public double MaxAbsorption { get; init; } = 0;
		public double RegenerationAbsorptionPerPowerPerGameSecond { get; init; } = 0;
		public double ArcSize { get; init; }

		/// <summary>
		/// Modifies damage to damage after shield absorption, returns new shield state.
		/// </summary>
		/// <param name="damage">Incoming damage value, will be modified.</param>
		/// <param name="damageDir">Incoming damage direction, vector pointing from ship to damage source.</param>
		/// <param name="hitShields">True if the damage direction intersected the shield arc.</param>
		public ShipSystemPoweredShields Absorb(ref double damage, Vector2d damageDir, out bool hitShields)
		{
			hitShields = false;
			if (Absorption == 0 || damageDir.LengthSquared == 0)
				return this; // No absorption

			damageDir.Normalize();

			if(Vector2d.Dot(damageDir, Direction) >= Math.Cos(ArcSize / 2.0) || ArcSize >= 6)
			{
				damage = Math.Max(damage, 0);
				double absorped = Math.Min(damage, Absorption);
				damage -= absorped; // Modify damage
				double a = Absorption - absorped;

				hitShields = true;

				return this with
				{
					Absorption = a,
				};
			}
			else
			{
				// The damage direction does not intersect the shield arc
				return this;
			}
		}

		public ShipSystemPoweredShields Tick(double deltaT, Vector2d? newdir)
		{
			var absorption = Absorption;
			absorption += Power * deltaT * RegenerationAbsorptionPerPowerPerGameSecond;
			absorption = Math.Min(absorption, MaxAbsorption);

			Vector2d dir = Direction;

			if(newdir != null && newdir.Value.LengthSquared > 0)
			{
				dir = newdir.Value.Normalized();
			}

			return this with
			{
				Absorption = absorption,
				Direction = dir,
			};
		}

		public override object GetClientState(GameConstants constants)
		{
			return new SystemShieldState()
			{
				powerState = new SystemPowerState()
				{
					currentPowerState = Power,
					powerStates = PowerStates.ToArray(),
					allowPowerUsageChange = true,
				},
				arcDirection = Direction.ToVec2(),
				currentAbsorption = Absorption,
				maxAbsorption = MaxAbsorption,
				shieldArcSize = ArcSize,
				shieldRegenerationSpeed = RegenerationAbsorptionPerPowerPerGameSecond * Power * constants.TimeScale,
			};
		}
	}

	public record ShipSystemEngines : ShipSystem
	{
		/// <summary>
		/// Filled in by Simulation.cs during ship control request handling.
		/// </summary>
		public int Power { get; init; }

		public double AccelerationPerUnitPower { get; init; }

		public Vector2d TargetVelocity { get; init; }

		public Vector2d LastDelta { get; init; }

		public double HeatIntensity { get; init; }

		bool _enabled { get; init; } = true;

		Vector2d _cachedVelocity { get; init; } = Vector2d.Zero;
		double _cachedMaxVelocity { get; init; }

		public ShipEnt Tick(ShipEnt ent, double deltaT, GameConstants constants, Vector2d? targetVelocity = null, bool? enabled = null)
		{
			var target = TargetVelocity;

			if (targetVelocity != null)
				target = targetVelocity.Value;
			if(target.Length > ent.Constants.SystemEnginesMaxVelocity)
			{
				target = target.Normalized() * ent.Constants.SystemEnginesMaxVelocity;
			}

			Vector2d toTarget = (target - ent.Velocity);
			double len = toTarget.Length;

			var delta = Vector2d.Zero;

			if (len > 0.00001)
			{
				// Avoid nans when velocity matches target velocity
				delta = toTarget.Normalized() * Math.Min(len, Power * AccelerationPerUnitPower * deltaT);
			}

			bool enginesEnabled = _enabled;

			if (enabled.HasValue)
				enginesEnabled = enabled.Value;

			if (!enginesEnabled)
				delta = Vector2d.Zero;

			var newvel = ent.Velocity + delta;

			return ent with
			{
				Velocity = newvel,
				SystemEngines = this with
				{
					TargetVelocity = target,
					HeatIntensity = Math.Min(delta.Length * ent.Constants.SystemEnginesHeatPerAccelerationUnit, 1),
					LastDelta = delta,
					_cachedVelocity = newvel,
					_cachedMaxVelocity = ent.Constants.SystemEnginesMaxVelocity,
					_enabled = enginesEnabled,
				},
			};
		}

		public override object GetClientState(GameConstants constants)
		{
			return new SystemEnginesState()
			{
				powerState = new SystemPowerState()
				{
					currentPowerState = Power,
					allowPowerUsageChange = false,
					powerStates = { },
				},
				currentVelocity = _cachedVelocity.ToVec2(),
				maxAcceleration = AccelerationPerUnitPower * Power * constants.TimeScale,
				maxVelocity = _cachedMaxVelocity,
			};
		}
	}

	public record ShipSystemSensor : ShipSystem
	{
		const double InvalidOrigin = -10000000;

		public double Range { get; init; }

		/// <summary>
		/// View will be centered on this entity. Bound to ship entity most of the time, bound to probe entity for probes.
		/// </summary>
		public int BoundEntityId { get; init; } = -1;

		/// <summary>
		/// What type of sensor this is.
		/// </summary>
		public RadarSignaturePurpose Purpose { get; init; } = RadarSignaturePurpose.Camera;

		public SystemStateRadar CachedSystemState { get; init; } = new SystemStateRadar();

		public ShipSystemSensor Bind(int bind)
		{
			int boundId = BoundEntityId;

			if (bind >= 0)
				boundId = bind;

			return this with
			{
				BoundEntityId = boundId,
			};
		}

		public ShipSystemSensor Tick(GameState state, GameConstants constants, CollisionGrid grid, bool sendData = true)
		{
			if (!state.MapEntityIdToEntity.ContainsKey(BoundEntityId))
			{
				return this with
				{
					BoundEntityId = -1,
					CachedSystemState = new SystemStateRadar()
					{
						origin = new Vec2()
						{
							x = InvalidOrigin,
							y = InvalidOrigin,
						},
						range = Range,
						signatures = { },
					},
				};
			}

			var boundEnt = state.MapEntityIdToEntity[BoundEntityId];

			int? askingShipId = null;

			if(boundEnt is IOwnedEntity)
			{
				var entid = ((IOwnedEntity)boundEnt).OwnerEntityId;
				var eee = state.MapEntityIdToEntity[entid];

				if(eee is ShipEnt)
				{
					var ship = (ShipEnt)eee;
					askingShipId = ship.ShipId;
				}
			}

			const double epsilon = 1.0;

			List<RadarSignature> signatures = new List<RadarSignature>();

			if(sendData)
			{
				foreach (var e in grid.GetEntitesInsideRange(boundEnt.Position - new Vector2d(Range), boundEnt.Position + new Vector2d(Range)))
				{
					if ((e.Position0 - boundEnt.Position).Length > Range + epsilon + e.Radius)
						continue; // Entity is out of range

					if (!state.MapEntityIdToEntity.ContainsKey(e.OldEntity.Id))
						continue; // Ent has died

					var realEnt = state.MapEntityIdToEntity[e.OldEntity.Id];

					if (realEnt.Id == BoundEntityId)
						continue; // Don't send signature of self.

					var sig = realEnt.GetSignature(state, constants, Purpose, askingShipId);

					if (sig == null)
						continue;

					if (sig != null)
						signatures.Add(sig);
				}
			}

			return this with
			{
				CachedSystemState = new SystemStateRadar()
				{
					origin = boundEnt.Position.ToVec2(),
					velocity = (boundEnt.Velocity * constants.TimeScale).ToVec2(),
					range = Range,
					signatures = signatures.ToArray(),
				},
			};
		}

		public override object GetClientState(GameConstants constants)
		{
			return CachedSystemState;
		}
	}

	public record ShipSystemAutoRepair : ShipSystemPowered
	{
		/// <summary>
		/// How much a single power unit spent by this system repairs the ship per second.
		/// </summary>
		public double RepairRatePerPowerPerSecond { get; init; }

		public ShipEnt Tick(ShipEnt ship, double deltaT, GameConstants constants)
		{
			var hp = ship.Health + RepairRatePerPowerPerSecond * deltaT * Power;
			if(hp > ship.Constants.ShipMaxHealth)
			{
				hp = ship.Constants.ShipMaxHealth;
			}

			return ship with { Health = hp, };
		}

		public override object GetClientState(GameConstants constants)
		{
			return new SystemAutoRepairState()
			{
				powerState = new SystemPowerState()
				{
					currentPowerState = Power,
					powerStates = PowerStates.ToArray(),
					allowPowerUsageChange = true,
				},
				repairRate = RepairRatePerPowerPerSecond * Power * constants.TimeScale,
			};
		}
	}
}
