using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	public class CollisionGrid
	{
		public class EntityProxy
		{
			public readonly Entity OldEntity;

			public readonly double Mass;
			public readonly double Radius;

			/// <summary>
			/// Current position
			/// </summary>
			public Vector2d Position0;

			/// <summary>
			/// Previous position (before movement)
			/// </summary>
			public Vector2d Position1;

			/// <summary>
			/// Previous-previous position
			/// </summary>
			public Vector2d Position2;

			public double T0;
			public double T1;
			public double T2;

			public Vector2d Velocity;

			public bool IsImmovable => Mass < 0;

			public EntityProxy(Entity e)
			{
				OldEntity = e;
				Position0 = e.Position;
				Position1 = Position0;
				Position2 = Position0;
				T0 = 0;
				T1 = 0;
				T2 = 0;
				Velocity = e.Velocity;
				Radius = e.Radius;
				Mass = e.Mass;
			}

			/// <summary>
			/// Updates current position and position history
			/// </summary>
			/// <param name="pos">New position</param>
			/// <param name="t">Time when this new position occured.</param>
			public void PushPosition(Vector2d pos, double t)
			{
				Position2 = Position1;
				Position1 = Position0;
				Position0 = pos;

				T2 = T1;
				T1 = T0;
				T0 = t;
			}

			public override string ToString()
			{
				return $"Type: {OldEntity.GetType().Name} Id: {OldEntity.Id} Pos: {Position0} Vel: {Velocity} Mass: {Mass} Radius: {Radius}";
			}
		}

		public const int WorldCellSize = 16;

		public readonly Vector2i WorldSize;
		readonly Vector2i _cellsCount;

		List<EntityProxy> _ents;
		List<EntityProxy>[] _cells;

		public CollisionGrid(Vector2i worldSize)
		{
			int getcells(int i)
			{
				return (i + WorldCellSize - 1) / WorldCellSize;
			}

			_cellsCount = new Vector2i(getcells(worldSize.X), getcells(worldSize.Y));
			WorldSize = worldSize;
			_cells = new List<EntityProxy>[_cellsCount.X * _cellsCount.Y];

			for(int i = 0; i < _cells.Length; i++)
			{
				_cells[i] = new List<EntityProxy>();
			}
		}

		/// <summary>
		/// Hacky solution to get up-to-date acceleration structure for sensor spacial queries
		/// </summary>
		public static CollisionGrid WithEntityPositions(Vector2i worldSize, IEnumerable<Entity> entities)
		{
			var grid = new CollisionGrid(worldSize);

			foreach(var e in entities)
			{
				const double epsilon = 0.1;

				Vector2d start = e.Position;
				Vector2d min = start - new Vector2d(e.Radius + epsilon);
				Vector2d max = start + new Vector2d(e.Radius + epsilon);

				var proxy = new EntityProxy(e);

				foreach (var cell in grid.IterateCells(min, max))
				{
					cell.Add(proxy);
				}
			}

			return grid;
		}

		/// <summary>
		/// Ignores projectiles
		/// </summary>
		public EntityProxy RayCollision(Vector2d origin, Vector2d destination, out Vector2d collision, params int[] ignoredEnts)
		{
			double nearestDist = double.PositiveInfinity;
			EntityProxy nearestEnt = null;
			collision = destination;

			foreach (var e in _ents)
			{
				if (e.OldEntity.IsProjectile || ignoredEnts.Contains(e.OldEntity.Id))
					continue;

				double t = TwoBallsCollision(origin, destination, 0.0, e.Position0, e.Position0, e.Radius);

				if (t.IsInfOrNaN())
					continue;
				if(t < nearestDist)
				{
					nearestDist = t;
					nearestEnt = e;
					collision = Vector2d.Lerp(origin, destination, t);
				}
			}

			return nearestEnt;
		}

		/// <summary>
		/// Gets a grid cell. Returns null if outside of grid.
		/// </summary>
		/// <param name="x">Grid cell x coordinate</param>
		/// <param name="y">Grid cell x coordinate</param>
		List<EntityProxy> GetCell(int x, int y)
		{
			if (x < 0 || y < 0 || x >= _cellsCount.X || y >= _cellsCount.Y)
				return null;
			return _cells[x + y * _cellsCount.X];
		}

		/// <summary>
		/// Returns cells that intersect a supplied AABB
		/// </summary>
		/// <param name="corner1">AABB corner</param>
		/// <param name="corner2">AABB corner</param>
		IEnumerable<List<EntityProxy>> IterateCells(Vector2d corner1, Vector2d corner2)
		{
			var min = Vector2d.ComponentMin(corner1, corner2);
			var max = Vector2d.ComponentMax(corner1, corner2);

			int toCell(double d)
			{
				return (int)Math.Floor(d / WorldCellSize);
			}

			Vector2i imin = new Vector2i(toCell(min.X), toCell(min.Y));
			Vector2i imax = new Vector2i(toCell(max.X), toCell(max.Y));
			imin = Vector2i.Clamp(imin, Vector2i.Zero, _cellsCount - new Vector2i(1));
			imax = Vector2i.Clamp(imax, Vector2i.Zero, _cellsCount - new Vector2i(1));

			for(int y = imin.Y; y <= imax.Y; y++)
			{
				for(int x = imin.X; x <= imax.X; x++)
				{
					var cell = GetCell(x, y);
					if (cell != null)
						yield return cell;
				}
			}
		}

		/// <summary>
		/// Returns entities that may intersect a supplied AABB
		/// </summary>
		/// <param name="min">AABB min</param>
		/// <param name="max">AABB max</param>
		public HashSet<EntityProxy> GetEntitesInsideRange(Vector2d min, Vector2d max)
		{
			HashSet<EntityProxy> unique = new HashSet<EntityProxy>();

			foreach(var cell in IterateCells(min, max))
			{
				foreach(var e in cell)
				{
					if(!unique.Contains(e))
						unique.Add(e);
				}
			}

			return unique;
		}

		HashSet<(EntityProxy, EntityProxy)> GetPotencialCollisionsBouncy()
		{
			HashSet<(EntityProxy, EntityProxy)> potencialCollisions = new HashSet<(EntityProxy, EntityProxy)>();

			// Get potencial collision pairs
			foreach (var cell in _cells)
			{
				// Iterate over pairs in cell
				for (int i = 0; i < cell.Count - 1; i++)
				{
					var e1 = cell[i];

					if (e1.OldEntity.IsProjectile)
						continue; // Outside projectile phase, skip collisions including any projectiles

					for (int j = i + 1; j < cell.Count; j++)
					{
						var e2 = cell[j];

						if (e2.OldEntity.IsProjectile)
							continue; // Outside projectile phase, skip collisions including any projectiles

						if (e1.OldEntity.IgnoreCollisionsFrom.Contains(e2.OldEntity.Id) ||
							e2.OldEntity.IgnoreCollisionsFrom.Contains(e1.OldEntity.Id))
							continue;

						if (e1.IsImmovable && e2.IsImmovable)
							continue;

						var pair = (e1, e2);

						// Hack to ensure we dont get duplicate but different pairs like (A,B) and (B,A)
						if (e1.GetHashCode() > e2.GetHashCode())
						{
							pair = (e2, e1);
						}

						// But the first ent is always the immovable one
						if(e2.IsImmovable)
						{
							pair = (e2, e1);
						}

						potencialCollisions.Add(pair);
					}
				}
			}

			return potencialCollisions;
		}

		HashSet<(EntityProxy, EntityProxy)> GetPotencialCollisionsProjectiles(List<EntityProxy> ents, double deltaT)
		{
			HashSet<(EntityProxy, EntityProxy)> potencialCollisions = new HashSet<(EntityProxy, EntityProxy)>();

			foreach(var e1 in ents)
			{
				if (!e1.OldEntity.IsProjectile)
					continue;

				const double epsilon = 0.1;

				Vector2d start = e1.Position0;
				Vector2d end = start + e1.Velocity * deltaT;
				Vector2d min = Vector2d.ComponentMin(start, end) - new Vector2d(e1.Radius + epsilon);
				Vector2d max = Vector2d.ComponentMax(start, end) + new Vector2d(e1.Radius + epsilon);

				foreach (var cell in IterateCells(min, max))
				{
					foreach(var e2 in cell)
					{
						if (e1.OldEntity.IgnoreCollisionsFrom.Contains(e2.OldEntity.Id) ||
							e2.OldEntity.IgnoreCollisionsFrom.Contains(e1.OldEntity.Id))
							continue;

						potencialCollisions.Add((e1, e2));
					}
				}
			}

			return potencialCollisions;
		}

		void SolveBouncyCollisions(in HashSet<(EntityProxy, EntityProxy)> potencialCollisions, in List<EntityProxy> entProxies, List<CollisionEvent> collisionEvents, double deltaT)
		{
			// We only apply the results of the nearest collision to entity
			HashSet<EntityProxy> hadCollisions = new HashSet<EntityProxy>();
			List<CollisionResult> collisions = new List<CollisionResult>();

			// Solve actual collisions
			foreach ((var e1, var e2) in potencialCollisions)
			{
				Debug.Assert(!e1.OldEntity.IsProjectile && !e2.OldEntity.IsProjectile);

				// If one ent is immovable, it's always e1

				Vector2d tickVelocity1 = e1.Velocity * deltaT;
				Vector2d tickVelocity2 = e2.Velocity * deltaT;

				// t is in range 0..1 where 0 is old position 1 is new position
				double t = TwoBallsCollision(
						e1.Position0, e1.Position0 + tickVelocity1, e1.Radius,
						e2.Position0, e2.Position0 + tickVelocity2, e2.Radius);

				if (t.IsInfOrNaN())
					continue; // No collision

				// https://stackoverflow.com/questions/345838/ball-to-ball-collision-detection-and-handling
				Vector2d collision1 = e1.Position0 + tickVelocity1 * t;
				Vector2d collision2 = e2.Position0 + tickVelocity2 * t;

				Vector2d dir1to2 = (collision2 - collision1);
				if(dir1to2.LengthSquared < 1e-9)
				{
					dir1to2 = Vector2d.UnitX;
				}
				dir1to2.Normalize();

				const double pushAwayEpsilon = 0.02;

				if (!e1.IsImmovable && !e2.IsImmovable)
				{
					double u1 = Vector2d.Dot(e1.Velocity, dir1to2);
					double u2 = Vector2d.Dot(e2.Velocity, dir1to2);

					double v1 = (u1 * (e1.Mass - e2.Mass) + 2.0 * e2.Mass * u2) / (e1.Mass + e2.Mass);
					double v2 = (u2 * (e2.Mass - e1.Mass) + 2.0 * e1.Mass * u1) / (e1.Mass + e2.Mass);

					Vector2d newvel1 = e1.Velocity - u1 * dir1to2 + v1 * dir1to2;
					Vector2d newvel2 = e2.Velocity - u2 * dir1to2 + v2 * dir1to2;

					// Push/pull colliders apart
					Vector2d minDistanceFrom1to2 = dir1to2 * (e1.Radius + e2.Radius + pushAwayEpsilon - (collision2 - collision1).Length);
					double im1 = 1.0 / e1.Mass;
					double im2 = 1.0 / e2.Mass;

					Vector2d newpos1 = collision1;
					Vector2d newpos2 = collision2;
					newpos1 += -minDistanceFrom1to2 * (im1 / (im1 + im2));
					newpos2 += minDistanceFrom1to2 * (im2 / (im1 + im2));
					newpos1 += newvel1 * (1.0 - t) * deltaT;
					newpos2 += newvel2 * (1.0 - t) * deltaT;

					collisions.Add(new CollisionResult()
					{
						T = t,
						Ent1 = e1,
						Ent2 = e2,
						Pos1 = newpos1,
						Pos2 = newpos2,
						Vel1 = newvel1,
						Vel2 = newvel2,
						PosAtCollision1 = collision1,
						PosAtCollision2 = collision2,
					});
				}
				else
				{
					// e1 is immovable
					var pos2 = collision2;
					pos2 += dir1to2 * Math.Max(0, (e1.Radius + e2.Radius + pushAwayEpsilon - (collision2 - collision1).Length));
					var posBeforeMove = pos2;
					var vel2 = e2.Velocity - 2.0 * Vector2d.Dot(e2.Velocity, dir1to2) * dir1to2;
					pos2 += vel2 * (1.0 - t) * deltaT;

					var range = GetEntitesInsideRange(pos2 - new Vector2d(e2.Radius), pos2 + new Vector2d(e2.Radius)).ToList();

					foreach (var proxy in range)
					{
						if(proxy.IsImmovable && (proxy.Position0 - pos2).Length < proxy.Radius + e2.Radius)
						{
							// Move e2 back to its collision pos
							pos2 = posBeforeMove;
						}
					}

					collisions.Add(new CollisionResult()
					{
						T = t,
						Ent1 = e1,
						Ent2 = e2,
						Pos1 = e1.Position0,
						Pos2 = pos2,
						Vel1 = e1.Velocity,
						Vel2 = vel2,
						PosAtCollision1 = collision1,
						PosAtCollision2 = collision2,
					});
				}
			}

			// Sort collisions by time
			var sortedCollisions = collisions.OrderBy(x => x.T).ToList();

			// Apply collisions
			foreach (var c in sortedCollisions)
			{
				if (hadCollisions.Contains(c.Ent1) || hadCollisions.Contains(c.Ent2))
					continue;

				if(!c.Ent1.IsImmovable)
				{
					hadCollisions.Add(c.Ent1);
					c.Ent1.PushPosition(c.Pos1, c.T);
					c.Ent1.Velocity = c.Vel1;
				}

				if (!c.Ent2.IsImmovable)
				{
					hadCollisions.Add(c.Ent2);
					c.Ent2.PushPosition(c.Pos2, c.T);
					c.Ent2.Velocity = c.Vel2;
				}

				// Generate collision events
				collisionEvents.Add(new CollisionEvent()
				{
					CurrentEntityId = c.Ent1.OldEntity.Id,
					OtherEntityId = c.Ent2.OldEntity.Id,
					VelocityDelta = c.Vel1 - c.Ent1.OldEntity.Velocity,
					CollisionDirection = (c.PosAtCollision2 - c.PosAtCollision1).Normalized(),
					CurrentEntityPositionAtCollision = c.PosAtCollision1,
					OtherEntityPositionAtCollision = c.PosAtCollision2,
				});
				collisionEvents.Add(new CollisionEvent()
				{
					CurrentEntityId = c.Ent2.OldEntity.Id,
					OtherEntityId = c.Ent1.OldEntity.Id,
					VelocityDelta = c.Vel2 - c.Ent2.OldEntity.Velocity,
					CollisionDirection = (c.PosAtCollision1 - c.PosAtCollision2).Normalized(),
					CurrentEntityPositionAtCollision = c.PosAtCollision2,
					OtherEntityPositionAtCollision = c.PosAtCollision1,
				});
			}

			// Apply the rest of movement, excluding projectiles
			foreach (var e in entProxies)
			{
				if (!hadCollisions.Contains(e) && !e.OldEntity.IsProjectile)
				{
					e.PushPosition(e.Position0 + e.Velocity * deltaT, 1);
				}
			}
		}

		void SolveProjectileCollisions(in HashSet<(EntityProxy, EntityProxy)> potencialCollisions, in List<EntityProxy> entProxies, List<CollisionEvent> collisionEvents, double deltaT)
		{
			// We only want the results of the nearest collision
			List<CollisionResult> collisions = new List<CollisionResult>();

			// Solve actual collisions
			foreach ((var e1, var e2) in potencialCollisions)
			{
				Debug.Assert(e1.OldEntity.IsProjectile && !e2.OldEntity.IsProjectile);

				// Projectile positions from oldest to newest
				Vector2d proj2 = e1.Position0;
				Vector2d proj1 = e1.Position0 + e1.OldEntity.Velocity * e2.T1 * deltaT;
				Vector2d proj0 = e1.Position0 + e1.OldEntity.Velocity * e2.T0 * deltaT;

				// First part of trajectory pos2->pos1
				double t0 = TwoBallsCollision(
						proj2, proj1, e1.Radius,
						e2.Position2, e2.Position1, e2.Radius) * e2.T1;
				// Second part of trajectory pos1->pos0
				double t1 = TwoBallsCollision(
						proj1, proj0, e1.Radius,
						e2.Position1, e2.Position0, e2.Radius) * (1 - e2.T1) + e2.T1;

				if (t0.IsInfOrNaN())
					t0 = 2;
				if (t1.IsInfOrNaN())
					t1 = 2;

				// Actual time of collision
				double t = Math.Min(t0, t1);

				if (t > 1)
					continue; // No collision

				Vector2d collision1 = e1.Position0 + e1.Velocity * t * deltaT;

				Vector2d collision2;
				if(t == t0)
				{
					collision2 = Vector2d.Lerp(e2.Position2, e2.Position1, t0);
				}
				else
				{
					collision2 = Vector2d.Lerp(e2.Position1, e2.Position0, t1);
				}

				collisions.Add(new CollisionResult()
				{
					T = t,
					Ent1 = e1,
					Ent2 = e2,
					PosAtCollision1 = collision1,
					PosAtCollision2 = collision2,
				});
			}

			// Sort collisions by time
			var sortedCollisions = collisions.OrderBy(x => x.T).ToList();

			HashSet<EntityProxy> hadCollision = new HashSet<EntityProxy>();

			// Apply collisions
			foreach (var c in sortedCollisions)
			{
				if (hadCollision.Contains(c.Ent1))
					continue; // Only process the first collision per projectile

				// Generate collision event for projectile only
				collisionEvents.Add(new CollisionEvent()
				{
					CurrentEntityId = c.Ent1.OldEntity.Id,
					OtherEntityId = c.Ent2.OldEntity.Id,
					VelocityDelta = Vector2d.Zero,
					CollisionDirection = (c.PosAtCollision2 - c.PosAtCollision1).Normalized(),
					CurrentEntityPositionAtCollision = c.PosAtCollision1,
					OtherEntityPositionAtCollision = c.PosAtCollision2,
				});
			}

			// Apply movement to projectiles regardless of collisions
			foreach (var e in entProxies)
			{
				if (e.OldEntity.IsProjectile)
				{
					e.PushPosition(e.Position0 + e.Velocity * deltaT, 1);
				}
			}
		}

		void MoveEntsIntoWorld(in List<EntityProxy> entProxies, List<CollisionEvent> collisionEvents)
		{
			foreach (var e in entProxies)
			{
				var oldvel = e.Velocity;

				if (e.IsImmovable)
					continue; // Ignore moveToWorld for immovable entities

				bool wasOutside = false;

				// Check entity outside world
				foreach (var plane in EnumerateBorderPlanes())
				{
					const double epsilon = 0.01;

					double dist = Vector2d.Dot(plane.Xy, e.Position0) + plane.Z - e.Radius;

					if (dist >= 0)
						continue;

					wasOutside = true;

					// Move entity to border
					// Note: we do not care about position history at this point
					e.Position0 += plane.Xy * (-dist + epsilon);

					// Reflect velocity
					double ndoti = Vector2d.Dot(plane.Xy, e.Velocity);
					if (ndoti < 0)
					{
						e.Velocity = e.Velocity - 2.0 * ndoti * plane.Xy;
					}
				}

				if(wasOutside)
				{
					// Generate collision event
					collisionEvents.Add(new CollisionEvent()
					{
						CurrentEntityId = e.OldEntity.Id,
						OtherEntityId = null,
						VelocityDelta = e.Velocity - oldvel,
						CollisionDirection = null,
					});
				}
			}
		}

		/// <summary>
		/// Constructs a grid for quick spacial entity queries.
		/// Solves entity movement including collisions and special handling for projectiles.
		/// Returns a map of entityId->entity with updated entities after collisions.
		/// </summary>
		/// <param name="entities">Input set of entities.</param>
		/// <param name="collisionEvents">List into which collision events are appended.</param>
		/// <returns>Map of entityId->entity with updated entities after collisions.</returns>
		public Dictionary<int, Entity> SolveCollisions(IEnumerable<Entity> entities, List<CollisionEvent> collisionEvents, double deltaT)
		{
			// Projectiles dies on collision with anything, doesnt affect the collider
			// Projectiles don't collide with themselves
			// 1. simulate all collidable objects
			// 2. store their trajectory (two line segments)
			// 3. simulate plasma shots hitting the trajectories accurately

			// Clear grid
			ClearCells();

			// Get ent proxies
			_ents = new List<EntityProxy>();
			foreach (var e in entities)
			{
				_ents.Add(new EntityProxy(e));
			}

			// Fill grid with base potencial trajectories (pos...pos+vel)
			foreach (var e in _ents)
			{
				const double epsilon = 0.1;

				if (e.OldEntity.IsProjectile)
					continue; // Skip projectiles

				Vector2d start = e.Position0;
				Vector2d end = start + e.Velocity * deltaT;
				Vector2d min = Vector2d.ComponentMin(start, end) - new Vector2d(e.Radius + epsilon);
				Vector2d max = Vector2d.ComponentMax(start, end) + new Vector2d(e.Radius + epsilon);

				foreach (var cell in IterateCells(min, max))
				{
					cell.Add(e);
				}
			}

			// Get potencial collision pairs for non-projectiles
			var potencialCollisionsBouncy = GetPotencialCollisionsBouncy();

			SolveBouncyCollisions(potencialCollisionsBouncy, _ents, collisionEvents, deltaT);

			// Fill grid with final trajectories (for proper projectile collisions later)
			foreach (var e in _ents)
			{
				if (e.OldEntity.IsProjectile)
					continue; // Skip projectiles, their grid placement is valid

				const double epsilon = 0.1;

				// Note that the last two entity positions are enough to update the grid
				Vector2d min = Vector2d.ComponentMin(e.Position0, e.Position1) - new Vector2d(e.Radius + epsilon);
				Vector2d max = Vector2d.ComponentMax(e.Position0, e.Position1) + new Vector2d(e.Radius + epsilon);

				foreach (var cell in IterateCells(min, max))
				{
					if(!cell.Contains(e))
						cell.Add(e); // O(n), but there are other quadratic loops here anyway, and the list should be small
				}
			}

			var potencialCollisionsProjectiles = GetPotencialCollisionsProjectiles(_ents, deltaT);

			SolveProjectileCollisions(potencialCollisionsProjectiles, _ents, collisionEvents, deltaT);

			// Populate grid with projectiles
			foreach (var e1 in _ents)
			{
				if (!e1.OldEntity.IsProjectile)
					continue;

				const double epsilon = 0.1;

				Vector2d start = e1.Position0;
				Vector2d end = start + e1.Velocity * deltaT;
				Vector2d min = Vector2d.ComponentMin(start, end) - new Vector2d(e1.Radius + epsilon);
				Vector2d max = Vector2d.ComponentMax(start, end) + new Vector2d(e1.Radius + epsilon);

				foreach (var cell in IterateCells(min, max))
				{
					cell.Add(e1);
				}
			}

			// Ensure all entities are in world
			MoveEntsIntoWorld(_ents, collisionEvents);

			// Generate map of updated entities
			Dictionary<int, Entity> mapents = new Dictionary<int, Entity>();
			foreach(var e in _ents)
			{
				mapents.Add(e.OldEntity.Id, e.OldEntity with
				{
					Position = e.Position0,
					Velocity = e.Velocity,
				});
			}

			return mapents;
		}

		void ClearCells()
		{
			for (int i = 0; i < _cells.Length; i++)
			{
				_cells[i].Clear();
			}
		}

		IEnumerable<Vector3d> EnumerateBorderPlanes()
		{
			yield return new Vector3d(1, 0, 0);
			yield return new Vector3d(0, 1, 0);
			yield return new Vector3d(-1, 0, this.WorldSize.X);
			yield return new Vector3d(0, -1, this.WorldSize.Y);
		}

		/// <summary>
		/// Returns the first point on line1 when it touches line2, where "lines" are "2D capsules" of a given radius.
		/// </summary>
		/// <param name="line1a">Start of line1</param>
		/// <param name="line1b">End of line1</param>
		/// <param name="line1r">Radius of line1</param>
		/// <param name="line1a">Start of line2</param>
		/// <param name="line1b">End of line2</param>
		/// <param name="line1r">Radius of line2</param>
		/// <returns>Number between 0..1 describing the first intersection, where 0 is line1 start and 1 is line1 end, and positive infinity if no collision is found.</returns>
		static double TwoBallsCollision(
			Vector2d line1a, Vector2d line1b, double line1r,
			Vector2d line2a, Vector2d line2b, double line2r)
		{
			Vector2d edge1 = line1b - line1a;
			Vector2d edge2 = line2b - line2a;
			Vector2d dir2 = edge2.Normalized();

			if (edge2.LengthSquared < 0.00001)
			{
				dir2 = Vector2d.UnitX;
			}

			// f(t) = length(lerp(l1a, l1b, t) - lerp(l2a, l2b, t)) - radius1 - radius2
			// f(t) = lengthsquared(lerp(l1a, l1b, t) - lerp(l2a, l2b, t)) - (radius1 + radius2)^2
			// f(t) = ((l1a.x * (1 - t) + l1b.x * t) - (l2a.x * (1 - t) + l2b.x * t))^2 + ((l1a.y * (1 - t) + l1b.y * t) - (l2a.y * (1 - t) + l2b.y * t))^2 - (radius1 + radius2)^2
			// ((l1a.x - l1a.x * t + l1b.x * t) - (l2a.x - l2a.x * t + l2b.x * t))^2 = ((l1a.x - l2a.x) + t * (l1b.x + l2a.x - l1a.x - l2b.x)) ^ 2

			// f(t) = (xconst + t * xlinear)^2 + (yconst + t * ylinear)^2 - (radius1 + radius2)^2
			double xconst = line1a.X - line2a.X;
			double xlinear = line1b.X + line2a.X - line1a.X - line2b.X;
			double yconst = line1a.Y - line2a.Y;
			double ylinear = line1b.Y + line2a.Y - line1a.Y - line2b.Y;

			// 0 = at^2 + bt + c
			double a = xlinear * xlinear + ylinear * ylinear;
			double b = 2.0 * xlinear * xconst + 2.0 * ylinear * yconst;
			double c = xconst * xconst + yconst * yconst - (line1r + line2r) * (line1r + line2r);

			var preRoot = b * b - 4 * a * c;

			if (preRoot <= 0)
				return double.PositiveInfinity;

			double root1 = (-b + Math.Sqrt(preRoot)) / (2.0 * a);
			double root2 = (-b - Math.Sqrt(preRoot)) / (2.0 * a);

			if(root1 > root2)
			{
				(root1, root2) = (root2, root1);
			}
			// now root1 <= root2

			if (root1 > 1 || root2 <= 0)
				return double.PositiveInfinity;

			return Math.Max(root1, 0);
		}

		class CollisionResult
		{
			public double T;
			public EntityProxy Ent1;
			public EntityProxy Ent2;
			public Vector2d Pos1; // Includes trajectory after collision
			public Vector2d Pos2;
			public Vector2d Vel1;
			public Vector2d Vel2;

			public Vector2d PosAtCollision1;
			public Vector2d PosAtCollision2;
		}
	}
}
