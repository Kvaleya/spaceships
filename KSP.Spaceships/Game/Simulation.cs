#nullable enable

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.Json;
using Microsoft.AspNetCore.SignalR;
using OpenTK.Mathematics;
using System.Diagnostics;
using Prometheus;
using System.Diagnostics.Eventing.Reader;

// TODO: fix game replays

namespace KSP.Spaceships
{
	/// <summary>
	/// All game logic goes here (minus entities and collisions)
	/// </summary>
	public class Simulation
	{
		const bool PrintGameEvents = true;
		const bool PrintTickTimes = false;

		const bool AllowResponseThrottling = true; // Set true for any game with API bots
		const bool RecoverFromExceptions = true; // TODO: this should be true

		const string DefaultPlayerName = "unnamed";

		private readonly ConnectionManager _connectionManager;
		private readonly ILogger<Simulation> _logger;
		private readonly Recorder _recorder;
		private readonly Replayer _replayer;

		private Random _simulationRandom = new Random();

		const int RandomStringBytes = 32;

		// TODO: if we want to enable replay of incoming game queue items, we need to generate new userids in a deterministic way
		private RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();

		GameState _mainState = new GameState();
		GameConstants _gameConstants { get; init; } = new GameConstants();

		int _counterRequests = 0;
		int _counterResponses = 0;

		public Simulation(ILogger<Simulation> logger, ConnectionManager connectionManager, GameConstants gameConstants,
			Recorder recorder, Replayer replayer)
		{
			_logger = logger;
			_connectionManager = connectionManager;
			_gameConstants = gameConstants;
			_recorder = recorder;
			_replayer = replayer;
			InicializeState();
		}

		// May run in parallel
		void SendToConnection(string connectionId, ServerResponse payload, bool isTickUpdate)
		{
			bool forbidWebsockets = AllowResponseThrottling && _mainState.IsGameRunning && !isTickUpdate;

			using (CustomMetrics.MessageSerializationTime.NewTimer())
			{
				Interlocked.Increment(ref _counterResponses);
				_connectionManager.Send(connectionId, payload, allowWebsockets: !forbidWebsockets);
					// gzip.Write(serializedBytes, 0, serializedBytes.Length);
				// var protobufLen = ms.Length;
				// ms = new System.IO.MemoryStream();
				// using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
				// {
				// 	var serializedBytes = JsonSerializer.SerializeToUtf8Bytes(payload);
				// 	gzip.Write(serializedBytes, 0, serializedBytes.Length);
				// }
				// Console.WriteLine($"Sending message {ms.Length} json.gz, {protobufLen} proto");
				// Console.WriteLine(JsonSerializer.Serialize(payload, new JsonSerializerOptions { WriteIndented = true }));
			}
		}

		// May run in parallel
		void SendToUser(string userId, ServerResponse payload, bool isTickUpdate = false)
		{
			foreach(var connectionId in _mainState.ConnectionManagementData.MapUserIdToConnectionIds[userId])
			{
				SendToConnection(connectionId, payload, isTickUpdate);
			}
		}

		public void Process(GameQueueItem item)
		{
			void process()
			{
				_counterRequests++;
				if (item.DisconnectionEvent != null)
				{
					ProcessDisconnect(item.DisconnectionEvent);
				}

				if (item.ClientRequestEvent != null)
				{
					ProcessClientRequestEvent(item.ClientRequestEvent);
				}

				if (item.TickEvent != null)
				{
					ProcessTickFork();
				}
			}

			var oldstate = _mainState;

			if(RecoverFromExceptions)
			{
				try
				{
					process();
				}
				catch (Exception e)
				{
					_logger.LogError(e, "Encountered an exception while processing game queue, reverting to previous state.");
					_mainState = oldstate;
				}
			}
			else
			{
				process();
			}
		}

		void InicializeState()
		{
			_mainState = new GameState();
			_mainState = CreateTestShips(_mainState, _gameConstants);
		}

		void ProcessDisconnect(DisconnectedEvent disconnection)
		{
			var cm = _mainState.ConnectionManagementData;

			if (!cm.MapConnectionIdToUserIds.ContainsKey(disconnection.ConnectionId))
			{
				return; // We did not know about this connection!?
			}

			var userid = cm.MapConnectionIdToUserIds[disconnection.ConnectionId];

			_mainState = _mainState with
			{
				ConnectionManagementData = cm with
				{
					MapConnectionIdToUserIds = cm.MapConnectionIdToUserIds.Remove(disconnection.ConnectionId),
					MapUserIdToConnectionIds = cm.MapUserIdToConnectionIds.SetItem(userid, cm.MapUserIdToConnectionIds[userid].Remove(disconnection.ConnectionId)),
				}
			};

			if(!_mainState.IsGameRunning)
			{
				SendUpdateShipAssignment();
			}
		}

		void ProcessClientRequestEvent(ClientRequestEvent eventRequest)
		{
			var request = eventRequest.ClientRequest;

			// Verify userid and do connection management
			string realUserId = ManagePlayerConnection(request.userId, eventRequest.ConnectionId);

			var player = _mainState.PlayerManagementData.MapUserIdToPlayer[realUserId];

			if (player.IsShadowBanned)
			{
				if (_simulationRandom.NextDouble() > _gameConstants.ShadowBanRequestProb)
				{
					return;
				}
			}

			bool throttleResponses = _mainState.IsGameRunning;
			bool userRecievedRespose = false;

			// Process name change
			if(request.nameChange != null)
			{
				// Each of the ProcessSomething methods returns true if it sent something to the current user.
				userRecievedRespose |= ProcessNameChange(realUserId, request.nameChange);
			}

			// Process ship assignment
			if(request.shipAssignment != null)
			{
				userRecievedRespose |= ProcessShipAssignment(realUserId, request.shipAssignment);
			}

			// Process ship edit
			if(request.shipEdit != null)
			{
				userRecievedRespose |= ProcessShipEdit(request.shipEdit);
			}

			// Process chat message
			if (!string.IsNullOrEmpty(request.chatMessage))
			{
				userRecievedRespose |= ProcessChatMessage(realUserId, request.chatMessage);
			}

			// Game start
			if (IsUserPrivileged(_mainState, realUserId) && request.startGame != GameType.Inactive && !_mainState.IsGameRunning)
			{
				// Only start the game if:
				// - the player has the right to do so
				// - the player wants to do so
				// - the game is not already running
				_mainState = InitializeGame(_mainState, _gameConstants, request.startGame, out var totalParticipants);

				_logger.LogInformation($"Game started, gamemode: {_mainState.GameType}, total participants: {totalParticipants}");

				_recorder.StartRecording();

				// Notify players
				foreach (var p in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					if (HasPlayerAccessToAShip(_mainState, p.UserId))
					{
						SendToUser(p.UserId, CreateResponse(_mainState, _gameConstants, p.UserId, chat: new ChatMessage[] {new ChatMessage()
					{
						timestamp = DateTimeOffset.UtcNow,
						message = $"Game started ({_mainState.GameType}).",
						type = ChatMessageType.Server,
					}}));
					}
					else
					{
						SendToUser(p.UserId, CreateResponse(_mainState, _gameConstants, p.UserId, chat: new ChatMessage[] {new ChatMessage()
					{
						timestamp = DateTimeOffset.UtcNow,
						message = $"Game started ({_mainState.GameType}). You are not participating.",
						type = ChatMessageType.Server,
					}}));
					}
				}

				userRecievedRespose = true;
			}

			// Process ship control
			if(request.shipControl != null)
			{
				userRecievedRespose |= ProcessShipControl(realUserId, request.shipControl) & !throttleResponses;
			}

			// Process game abort
			if(request.abortGame != null && request.abortGame.Value &&
				_mainState.IsGameRunning && IsUserPrivileged(_mainState, realUserId))
			{
				FinishGame();
				SendUpdateShipAssignment();
				userRecievedRespose = true;
			}

			if(!userRecievedRespose && !throttleResponses)
			{
				// Always at least confirm userid

				if(_mainState.IsGameRunning)
				{
					SendToUser(realUserId, CreateResponse(_mainState, _gameConstants, realUserId));
				}
				else
				{
					// Update everyone
					SendUpdateShipAssignment();
				}
			}
		}

		void ProcessTickFork()
		{
			if(_mainState.IsReplaying)
			{
				ProcessReplayFrame();
			}
			else
			{
				ProcessTick();
			}
		}

		void ProcessReplayFrame()
		{
			var frame = _replayer.GetFrame();

			if(frame == null)
			{
				_mainState = _mainState with
				{
					IsReplaying = false,
				};
				SendUpdateShipAssignment();
				return;
			}

			// Send spectatormap to everyone
			foreach (var userid in _mainState.PlayerManagementData.MapUserIdToPlayer.Keys)
			{
				if (!IsUserIdConnected(_mainState, userid))
					continue; // No need to update disconnected players

				var realResponse = CreateResponse(_mainState, _gameConstants, userid);

				var response = frame with
				{
					playerGameState = frame.playerGameState with
					{
						playerUserId = realResponse.playerGameState.playerUserId,
						playerName = realResponse.playerGameState.playerName,
						playerId = realResponse.playerGameState.playerId,
						playerIsPrivileged = realResponse.playerGameState.playerIsPrivileged,
						isPlayerDead = true,
					},
					mainGameState = null,
				};
				SendToUser(userid, response);
			}
		}

		static async Task AfterTickSimulated(
			GameState state,
			DateTimeOffset now,
			List<string> globalLogs,
			Recorder recorder,
			GameConstants gameConstants,
			string? broadcast,
			Dictionary<int, List<(string, EntityEventType)>> entLogs,
			ILogger logger,
			Action<string, ServerResponse> sendToUser)
		{
			await Task.Yield();

			var timer = CustomMetrics.AfterTickTime.NewTimer();
			CustomMetrics.Game_Entities.Set(state.PlayerManagementData.MapPlayerIdToUserId.Count);
			CustomMetrics.Game_Entities.Set(state.MapEntityIdToEntity.Count);


			SpectatorMapState spectatorMap = GetSpectatorMap(state, gameConstants);

			var hud = GenHudSignatures(state, gameConstants);

			var recordTask = Task.CompletedTask;
			// Make fake response for recording
			if (state.PlayerManagementData.MapUserIdToPlayer.Count > 0)
			{
				recordTask = Task.Run(() =>
				{
					var timer = CustomMetrics.RecorderSaveTime.NewTimer();
					List<ChatMessage> messages = new List<ChatMessage>();
					if (globalLogs != null)
					{
						foreach (var l in globalLogs)
						{
							messages.Add(new ChatMessage()
							{
								message = l,
								timestamp = now,
								type = ChatMessageType.EventGlobal,
							});
						}
					}

					var response = CreateResponse(state, gameConstants, state.PlayerManagementData.MapUserIdToPlayer.Keys.First(),
						maingameState: true,
						chat: messages,
						hudSignatures: hud,
						spectatorMap: spectatorMap,
						broadcast: broadcast);

					// Delete useless stuff
					response = response with
					{
						mainGameState = null,
					};

					recorder.Record(response);
					timer.ObserveDuration();
				});
			}

			var userSendTasks = new List<Task>();

			// Send to players
			foreach (var userid in state.PlayerManagementData.MapUserIdToPlayer.Keys)
			{
				if (!IsUserIdConnected(state, userid))
					continue; // No need to update disconnected players

				userSendTasks.Add(Task.Run(() => {

					List<(string, EntityEventType)>? shipLog = null;

					var player = state.PlayerManagementData.MapUserIdToPlayer[userid];

					// Shadowban tick dropping
					if (player.IsShadowBanned)
					{
						var r = new Random(unchecked((int)(state.LastTickTime.Ticks ^ player.Id)));
						if (r.NextDouble() > gameConstants.ShadowBanTickProb)
						{
							return;
						}
					}

					if (HasPlayerAccessToAShip(state, userid) && player.ShipId >= 0)
					{
						var ship = state.MapShipIdToShipState[player.ShipId];

						if (entLogs.ContainsKey(ship.EntityId))
						{
							shipLog = entLogs[ship.EntityId];
						}
					}

					SpectatorMapState? localSpectatorMap = null;

					if (IsUserDead(state, userid)
						|| (!HasPlayerAccessToAShip(state, userid) && IsUserPrivileged(state, userid)))
					{
						localSpectatorMap = spectatorMap;
					}

					List<ChatMessage> messages = new List<ChatMessage>();

					if (globalLogs != null)
					{
						foreach (var l in globalLogs)
						{
							messages.Add(new ChatMessage()
							{
								message = l,
								timestamp = now,
								type = ChatMessageType.EventGlobal,
							});
						}
					}

					if (shipLog != null)
					{
						foreach (var l in shipLog)
						{
							var type = ChatMessageType.EventShip;

							if(l.Item2 == EntityEventType.Generic)
							{
								type = ChatMessageType.EventShip;
							}
							if (l.Item2 == EntityEventType.DamageTaken)
							{
								type = ChatMessageType.EventDamageTaken;
							}
							if (l.Item2 == EntityEventType.DamageDealt)
							{
								type = ChatMessageType.EventDamageDealt;
							}
							if (l.Item2 == EntityEventType.TeamDamageDealt)
							{
								type = ChatMessageType.EventTeamDamageDealt;
							}
							if (l.Item2 == EntityEventType.SelfDamageTaken)
							{
								type = ChatMessageType.EventSelfDamageTaken;
							}

							messages.Add(new ChatMessage()
							{
								message = l.Item1,
								timestamp = now,
								type = type,
							});
						}
					}

					var response = CreateResponse(state, gameConstants, userid,
						maingameState: true,
						chat: messages,
						hudSignatures: hud,
						spectatorMap: localSpectatorMap,
						broadcast: broadcast);
					sendToUser(userid, response);
				}));
			}

			await recordTask;
			await Task.WhenAll(userSendTasks);
			var runtime = timer.ObserveDuration();
			if(PrintTickTimes)
			{
				logger.LogInformation("After tick stuff took {Elapsed} ms", runtime.TotalMilliseconds);
			}
			
		}

		Task afterTickTask = Task.CompletedTask;

		void ProcessTick()
		{
			if (!_mainState.IsGameRunning)
				return;

			var start = DateTimeOffset.UtcNow;

			var simulationTimer = CustomMetrics.SimulationTime.NewTimer();
			_mainState = SimulateGameTick(_mainState, out var entLogs, out var globalLogs, out var gameEnds, out var broadcast);
			simulationTimer.ObserveDuration();

			var now = DateTimeOffset.UtcNow;

			var sinceLastTick = now - _mainState.LastTickTime;

			_mainState = _mainState with
			{
				LastTickTime = now,
			};

			if (!afterTickTask.IsCompleted)
			{
				_logger.LogWarning("Previous AfterTickSimulated method did not finish running.");
			}
			if (afterTickTask.IsFaulted)
			{
				_logger.LogError("Error occurred in AfterTickSimulated: {err}", afterTickTask.Exception);
			}

			void SendTickToUser(string userId, ServerResponse response)
			{
				SendToUser(userId, response, isTickUpdate: true);
			}

			afterTickTask = AfterTickSimulated(_mainState, now, globalLogs, _recorder, _gameConstants, broadcast, entLogs, _logger, SendTickToUser);

			var span = DateTimeOffset.UtcNow - start;

			if(PrintTickTimes)
			{
				_logger.LogInformation($"Tick time: {span.TotalMilliseconds.ToString("0.00")} ms, since last: {sinceLastTick.TotalMilliseconds.ToString("0.00")} ms,"
					+ $" Game time: {_mainState.CurrentGameElapsed.ToString("0.0")}"
					+ $" Since last tick: requests: {_counterRequests} responses: {_counterResponses}");
			}
			if(PrintGameEvents)
			{
				foreach(var s in globalLogs)
				{
					_logger.LogInformation(s);
				}
			}

			_counterRequests = 0;
			_counterResponses = 0;

			if (gameEnds)
			{
				afterTickTask.Wait();
				FinishGame();
			}
		}

		GameState SimulateGameTick(in GameState oldState, out Dictionary<int, List<(string, EntityEventType)>> entLogs, out List<string> globalLogs, out bool endGame,
			out string? broadcast)
		{
			// Incrementally construct new state into this var
			var localState = oldState;

			// Update pointers
			{
				var now = DateTimeOffset.UtcNow;

				localState = localState with
				{
					MapShipIdToShipState = localState.MapShipIdToShipState.EagerSelect(s =>
					{
						return s with
						{
							Pointers = s.Pointers.EagerWhere(x => (now - x.Item1).TotalSeconds < _gameConstants.PointerLifetime),
						};
					}),
				};
			}

			// Inicialize entity grid
			var grid = new CollisionGrid(oldState.WorldSize);

			// Solve collisions
			var collisionEvents = new List<CollisionEvent>();
			var entsAfterCollisions = grid.SolveCollisions(oldState.MapEntityIdToEntity.Values, collisionEvents, _gameConstants.GetTimeDeltaPerTick()) ?? new Dictionary<int, Entity>();

			// Sort collision events
			Dictionary<int, List<CollisionEvent>> mapEntityIdToCollisions = new Dictionary<int, List<CollisionEvent>>();

			foreach (var id in entsAfterCollisions.Keys)
			{
				mapEntityIdToCollisions[id] = new List<CollisionEvent>();
			}

			foreach (var c in collisionEvents)
			{
				mapEntityIdToCollisions[c.CurrentEntityId].Add(c);
			}

			var toKill = new HashSet<Entity>();

			//
			// Entity simulation
			//

			// For creating new ents
			var entBuilder = ImmutableDictionary.CreateBuilder<int, Entity>();
			int entid = localState.GiveEntityId;

			int spawnEnt(Entity e)
			{
				e = e with { Id = entid++, };
				entBuilder.Add(e.Id, e);
				return e.Id;
			}

			EntitySimulationOutput simout = new EntitySimulationOutput()
			{
				SpawnEnt = spawnEnt,
			};
			globalLogs = simout.GlobalEventsLog;

			var entsAfterSimulation = new Dictionary<int, Entity>();

			// Create a fake state that has up to date entity positions
			var entBuilderFake = ImmutableDictionary.CreateBuilder<int, Entity>();
			entBuilderFake.AddRange(entsAfterCollisions);

			var entsimState = oldState with
			{
				MapEntityIdToEntity = entBuilderFake.ToImmutable(),
			};

			var simulationArgs = new EntitySimulationArgs()
			{
				Constants = _gameConstants,
				DeltaT = _gameConstants.GetTimeDeltaPerTick(),
				Grid = grid,
				State = entsimState,
			};

			// Sort ent messages
			var entMessages = new Dictionary<int, List<string>>();

			foreach(var m in localState.EntMessages)
			{
				if (!entMessages.ContainsKey(m.RecipientId))
					entMessages.Add(m.RecipientId, new List<string>());
				entMessages[m.RecipientId].Add(m.Message);
			}

			// Find all ships
			List<int> shipEntIds = new List<int>();

			foreach (var ent in entsAfterCollisions.Values)
			{
				var simArgs = simulationArgs with
				{
					CollisionEvents = mapEntityIdToCollisions[ent.Id],
					IncomingMessages = entMessages.ContainsKey(ent.Id) ? entMessages[ent.Id] : simulationArgs.IncomingMessages,
				};

				if(ent is ShipEnt)
				{
					shipEntIds.Add(ent.Id);
				}

				var newent = ent.Simulate(simArgs, simout);

				if (newent != null)
				{
					entsAfterSimulation.Add(newent.Id, newent);
				}
				else
				{
					toKill.Add(ent);
				}
			}

			// Sort damage events
			Dictionary<int, List<DamageEvent>> mapEntityIdToDamage = new Dictionary<int, List<DamageEvent>>();

			foreach (var id in entsAfterSimulation.Keys)
			{
				mapEntityIdToDamage[id] = new List<DamageEvent>();
			}

			if (simout.DamageEvents == null)
				throw new NullReferenceException("This should never happen.");

			foreach (var d in simout.DamageEvents)
			{
				if (mapEntityIdToDamage.ContainsKey(d.ReceiverEntityId))
				{
					// The entity may be dead already
					mapEntityIdToDamage[d.ReceiverEntityId].Add(d);
				}
			}

			// Process damage events
			var entsAfterDamage = new Dictionary<int, Entity>();

			var damageArgs = new EntityDamageArgs()
			{
				Constants = _gameConstants,
				State = oldState,
				Grid = grid,
			};

			simout = simout with
			{
				DamageEvents = null,
			};

			foreach (var ent in entsAfterSimulation.Values)
			{
				var newent = ent.ProcessDamageEvents(damageArgs with { DamageEvents = mapEntityIdToDamage[ent.Id] }, simout);

				if (newent != null)
				{
					entsAfterDamage.Add(newent.Id, newent);
				}
				else
				{
					toKill.Add(ent);
				}
			}

			// Generate ent logs
			entLogs = new Dictionary<int, List<(string, EntityEventType)>>();
			foreach((var id, var msg, var type) in simout.EntityEventsLog)
			{
				if (!entLogs.ContainsKey(id))
					entLogs.Add(id, new List<(string, EntityEventType)>());
				entLogs[id].Add((msg, type));
			}

			// Record ent messages
			localState = localState with
			{
				EntMessages = ImmutableArray.Create(simout.Messages.ToArray()),
			};

			broadcast = null;
			const int warningTime = 60;

			if (_gameConstants.MaxGameTime - localState.CurrentGameElapsed > warningTime
				&& _gameConstants.MaxGameTime - (localState.CurrentGameElapsed + _gameConstants.TickIntervalSeconds) <= warningTime)
			{
				broadcast = string.Format(_gameConstants.Strings.TimeRemaining, warningTime);
			}

			// Add to elapsed time
			localState = localState with
			{
				CurrentGameElapsed = localState.CurrentGameElapsed + _gameConstants.TickIntervalSeconds,
			};

			var stateWithVictims = localState;

			endGame = false;

			// Check game time
			if(localState.CurrentGameElapsed > _gameConstants.MaxGameTime)
			{
				endGame = true;
			}

			// Create new ent dictionary
			entBuilder.AddRange(entsAfterDamage);

			// Create new state
			localState = localState with
			{
				GiveEntityId = entid,
			};

			localState = localState with
			{
				MapEntityIdToEntity = entBuilder.ToImmutable(),
			};

			foreach(var killed in toKill)
			{
				if(killed is ShipEnt)
				{
					var ship = (ShipEnt)killed;

					if (GetRespawnsAllowed(localState) && !ship.Constants.IsStation)
					{
						localState = localState with
						{
							// Recycle entity id
							MapEntityIdToEntity = localState.MapEntityIdToEntity.Add(ship.Id, RespawnShip(localState, _gameConstants, ship.ShipId, ship.Id, ship.FactionId)),
						};
					}
					else
					{
						localState = localState with
						{
							MapShipIdToShipState = localState.MapShipIdToShipState.SetItem(ship.ShipId, localState.MapShipIdToShipState[ship.ShipId] with { EntityId = -1, })
						};
					}
				}
			}

			var sensorSimulationArgs = simulationArgs with
			{
				State = localState,
				Grid = CollisionGrid.WithEntityPositions(localState.WorldSize, localState.MapEntityIdToEntity.Values),
			};

			// Update ship sensors
			foreach (var shipEntId in shipEntIds)
			{
				if (!localState.MapEntityIdToEntity.ContainsKey(shipEntId))
					continue;

				var updated = ((ShipEnt)localState.MapEntityIdToEntity[shipEntId]).UpdateSensors(sensorSimulationArgs);

				localState = localState with
				{
					MapEntityIdToEntity = localState.MapEntityIdToEntity.SetItem(shipEntId, updated),
				};
			}

			// Clear system controls
			localState = localState with
			{
				MapShipIdToShipState = localState.MapShipIdToShipState.EagerSelect((data) =>
				{
					return data with
					{
						Systems = new RequestSystemState(),
					};
				}),
			};

			// Process kill events
			localState = ProcessKillEvents(localState, stateWithVictims, _gameConstants, simout.KillEvents, globalLogs, out var killBroadcast);

			if (killBroadcast != null)
				broadcast = killBroadcast;

			// Check game end by killing stations
			if (localState.GameType == GameType.Defense)
			{
				if (GetLivingStationCount(localState) <= 0)
					endGame = true;
			}

			return localState;
		}

		bool ProcessShipControl(string userid, RequestShipControl request)
		{
			if (!HasPlayerAccessToAShip(_mainState, userid))
				return false;

			PlayerData player = _mainState.PlayerManagementData.MapUserIdToPlayer[userid];
			var ship = _mainState.MapShipIdToShipState[player.ShipId];
			var shipcrew = GetShipCrew(_mainState, player.ShipId);

			bool sent = false;

			if(request.pointer != null)
			{
				var now = DateTimeOffset.UtcNow;

				_mainState = _mainState with
				{
					MapShipIdToShipState = _mainState.MapShipIdToShipState.SetItem(player.ShipId, ship with
					{
						Pointers = ship.Pointers.Add((now, new PositionPointer()
						{
							creatorRoleId = player.RoleId,
							position = request.pointer,
						})),
					}),
				};

				// Update everyone
				foreach (var shipmate in shipcrew)
				{
					SendToUser(shipmate.UserId, CreateResponse(_mainState, _gameConstants, shipmate.UserId, maingameState: true));
				}

				sent = true;
			}

			if(request.powerState != null)
			{
				T trySetPower<T>(string key, SystemPowerCommand? command, T system, ref int available)
					where T : ShipSystemPowered
				{
					if (command == null)
						return system; // Player doesn't want to set this system
					if (!_gameConstants.RoleSystems[player.RoleId].Contains(key))
						return system; // Player's role doesn't have access
					if (!system.PowerStates.Contains(command.targetPower))
						return system; // System doesn't support target power state
					int pow = available;
					pow += system.Power;
					pow -= command.targetPower;

					if (pow < 0)
						return system; // Not enough power available

					// Success!
					available = pow;
					return system with
					{
						Power = command.targetPower,
					};
				}

				ShipEnt shipent = (ShipEnt)_mainState.MapEntityIdToEntity[_mainState.MapShipIdToShipState[player.ShipId].EntityId];

				int powerAvailable = shipent.Constants.ShipReactorPowerUnits - shipent.GetMainPowerUsage();

				var ps = request.powerState;

				var newent = shipent with
				{
					SystemWeaponLaser = trySetPower(nameof(ShipEnt.SystemWeaponLaser), ps.weaponLaser, shipent.SystemWeaponLaser, ref powerAvailable),
					SystemWeaponPlasma = trySetPower(nameof(ShipEnt.SystemWeaponPlasma), ps.weaponPlasma, shipent.SystemWeaponPlasma, ref powerAvailable),
					SystemWeaponMissiles = trySetPower(nameof(ShipEnt.SystemWeaponMissiles), ps.weaponMissiles, shipent.SystemWeaponMissiles, ref powerAvailable),
					SystemWeaponMines = trySetPower(nameof(ShipEnt.SystemWeaponMines), ps.weaponMines, shipent.SystemWeaponMines, ref powerAvailable),
					SystemWeaponProbes = trySetPower(nameof(ShipEnt.SystemWeaponProbes), ps.weaponProbes, shipent.SystemWeaponProbes, ref powerAvailable),

					SystemShields = trySetPower(nameof(ShipEnt.SystemShields), ps.shields, shipent.SystemShields, ref powerAvailable),
					SystemStealth = trySetPower(nameof(ShipEnt.SystemStealth), ps.stealth, shipent.SystemStealth, ref powerAvailable),
					SystemAutoRepair = trySetPower(nameof(ShipEnt.SystemAutoRepair), ps.autoRepair, shipent.SystemAutoRepair, ref powerAvailable),

					SystemEngines = shipent.SystemEngines with
					{
						Power = powerAvailable,
					},
				};

				_mainState = _mainState with
				{
					MapEntityIdToEntity = _mainState.MapEntityIdToEntity.SetItem(shipent.Id, newent),
				};

				// Update everyone
				foreach (var shipmate in shipcrew)
				{
					SendToUser(shipmate.UserId, CreateResponse(_mainState, _gameConstants, shipmate.UserId, maingameState: true));
				}

				sent = true;
			}

			if(request.systemState != null)
			{
				T trySetSystem<T>(string key, T value, T oldvalue)
				{
					if (value == null)
						return oldvalue; // Player doesn't want to set this system
					if (!_gameConstants.RoleSystems[player.RoleId].Contains(key))
						return oldvalue; // Player's role doesn't have access
					return value;
				}

				var oldsystems = _mainState.MapShipIdToShipState[player.ShipId].Systems;

				_mainState = _mainState with
				{
					MapShipIdToShipState = _mainState.MapShipIdToShipState.SetItem(player.ShipId, _mainState.MapShipIdToShipState[player.ShipId] with
					{
						Systems = new RequestSystemState()
						{
							weaponLaser = trySetSystem(nameof(ShipEnt.SystemWeaponLaser), request.systemState.weaponLaser, oldsystems.weaponLaser),
							weaponPlasma = trySetSystem(nameof(ShipEnt.SystemWeaponPlasma), request.systemState.weaponPlasma, oldsystems.weaponPlasma),
							weaponMissiles = trySetSystem(nameof(ShipEnt.SystemWeaponMissiles), request.systemState.weaponMissiles, oldsystems.weaponMissiles),
							weaponMines = trySetSystem(nameof(ShipEnt.SystemWeaponMines), request.systemState.weaponMines, oldsystems.weaponMines),
							weaponProbes = trySetSystem(nameof(ShipEnt.SystemWeaponProbes), request.systemState.weaponProbes, oldsystems.weaponProbes),

							remoteMissile = trySetSystem(nameof(ShipEnt.SystemRemoteMissiles), request.systemState.remoteMissile, oldsystems.remoteMissile),

							shields = trySetSystem(nameof(ShipEnt.SystemShields), request.systemState.shields, oldsystems.shields),
							stealthActivate = trySetSystem(nameof(ShipEnt.SystemStealth), request.systemState.stealthActivate, oldsystems.stealthActivate),
							engines = trySetSystem(nameof(ShipEnt.SystemEngines), request.systemState.engines, oldsystems.engines),
						},
					}),
				};

				// Send ship state to current user only. Otherwise an empty response would be sent, making the screen blank until the next tick.
				SendToUser(player.UserId, CreateResponse(_mainState, _gameConstants, player.UserId, maingameState: true));

				sent = true;
			}

			return sent;
		}

		bool ProcessNameChange(string userid, string newname)
		{
			if (string.IsNullOrEmpty(newname))
				return false;

			if(newname.Length > _gameConstants.MaxNameChars)
			{
				newname = newname.Substring(0, _gameConstants.MaxNameChars);
			}

			_mainState = _mainState with
			{
				PlayerManagementData = _mainState.PlayerManagementData with
				{
					MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(userid, _mainState.PlayerManagementData.MapUserIdToPlayer[userid] with
					{
						Name = newname,
					}),
				}
			};

			if(_mainState.IsGameRunning)
			{
				SendToUser(userid, CreateResponse(_mainState, _gameConstants, userid, maingameState: true));
				return true;
			}
			else
			{
				// Ship assignment phase - update everyone immediately
				SendUpdateShipAssignment();
				return true;
			}

			return false;
		}

		bool ProcessShipAssignment(string userid, RequestShipAssignment assignment)
		{
			if(assignment.requestedShipName != null)
			{
				var player = _mainState.PlayerManagementData.MapUserIdToPlayer[userid];

				bool hasCorrectShip = false;

				if (_mainState.MapShipIdToShipDesc.ContainsKey(player.ShipId))
				{
					var current = _mainState.MapShipIdToShipDesc[player.ShipId];

					if(current.Name == assignment.requestedShipName && current.Roles.Length == 1)
					{
						hasCorrectShip = true;
					}
				}

				if(hasCorrectShip)
				{
					return false;
				}

				if(!hasCorrectShip)
				{
					_mainState = CreateShip(_mainState, assignment.requestedShipName, 0, true, false, out var ship);
					assignment = new RequestShipAssignment()
					{
						targetRoleId = ship.Roles[0],
						targetShipId = ship.Id,
					};
				}
			}

			AssignRole(userid, assignment);

			// Send role update to everyone
			SendUpdateShipAssignment();

			return true;
		}

		bool ProcessShipEdit(RequestShipEdit edit)
		{
			if (!_mainState.MapShipIdToShipDesc.ContainsKey(edit.targetShipId))
				return false;

			var desc = _mainState.MapShipIdToShipDesc[edit.targetShipId];

			if(edit.factionId != null && edit.factionId.Value >= 0 && edit.factionId.Value < _gameConstants.Strings.FactionNames.Length)
			{
				desc = desc with
				{
					FactionId = edit.factionId.Value,
				};
			}

			if(!string.IsNullOrEmpty(edit.name))
			{
				if(NewShipNameValid(_mainState, edit.name))
				{
					desc = desc with
					{
						Name = edit.name,
					};
				}
			}

			if (edit.privilegedOnly != null)
			{
				desc = desc with
				{
					onlyAcceptsPrivileged = edit.privilegedOnly.Value,
				};
			}

			_mainState = _mainState with
			{
				MapShipIdToShipDesc = _mainState.MapShipIdToShipDesc.SetItem(edit.targetShipId, desc),
			};

			// Send role update to everyone
			SendUpdateShipAssignment();

			return true;
		}

		void SendUpdateShipAssignment()
		{
			foreach (var p in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
			{
				SendToUser(p.UserId, CreateResponse(_mainState, _gameConstants, p.UserId, shipAssignment: true));
			}
		}

		void AssignRole(string userid, RequestShipAssignment assignment)
		{
			// Clear assignment
			var newplayer = _mainState.PlayerManagementData.MapUserIdToPlayer[userid];

			if(newplayer.IsShadowBanned)
			{
				return;
			}

			// If this condition holds, the user just wants to clear its current role.
			if (assignment.targetRoleId == null ||
				assignment.targetRoleId < 0 ||
				assignment.targetShipId == null ||
				assignment.targetShipId < 0)
			{
				newplayer = newplayer with
				{
					RoleId = -1,
					ShipId = -1,
				};
				_mainState = _mainState with
				{
					PlayerManagementData = _mainState.PlayerManagementData with
					{
						MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(userid, newplayer),
					}
				};
			}

			// Check whether the target ship exists
			if (!_mainState.MapShipIdToShipDesc.ContainsKey(assignment.targetShipId.Value))
			{
				return;
			}

			var shipdesc = _mainState.MapShipIdToShipDesc[assignment.targetShipId.Value];

			// Check whether the target ship allows this role.
			if (!shipdesc.Roles.Contains(assignment.targetRoleId.Value))
			{
				return;
			}

			if(!_gameConstants.AllowAnyoneToUsePrivilegedShips && shipdesc.onlyAcceptsPrivileged && !newplayer.IsPrivileged)
			{
				// Player can't access this ship
				return;
			}

			// Check whether anyone already occupies the role
			foreach (var p in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
			{
				if (p.ShipId == assignment.targetShipId.Value && p.RoleId == assignment.targetRoleId.Value)
					return;
			}

			// All passed, set the role)
			_mainState = _mainState with
			{
				PlayerManagementData = _mainState.PlayerManagementData with
				{
					MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(userid, newplayer with
					{
						RoleId = assignment.targetRoleId.Value,
						ShipId = assignment.targetShipId.Value,
					}),
				}
			};
		}

		bool ProcessChatMessage(string userid, string chatMessage)
		{
			if(chatMessage.Length > 0 && chatMessage[0] == '/')
			{
				return ProcessCommand(userid, chatMessage);
			}

			var currentPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer[userid];

			if (_mainState.IsGameRunning && IsUserDead(_mainState, userid))
			{
				// The dead cannot speak.
				SendToUser(userid, CreateResponse(_mainState, _gameConstants, userid, chat: new ChatMessage[] {new ChatMessage()
				{
					message = _gameConstants.Strings.OnDeadChat,
					timestamp = DateTimeOffset.UtcNow,
					type = ChatMessageType.Server,
				}}));

				return true;
			}

			string msg = $"[{currentPlayer.Name}]: ";

			int faction = -1;

			if(_mainState.IsGameRunning)
			{
				// Also check MapShipIdToShipState, because that will only contain spawned ships
				if (!_mainState.MapShipIdToShipDesc.ContainsKey(currentPlayer.ShipId) || !_mainState.MapShipIdToShipState.ContainsKey(currentPlayer.ShipId))
				{
					_logger.LogInformation($"Player {currentPlayer.Name} tried to send chat, but is not assigned a valid ship. (userid: {userid})");

					SendToUser(userid, CreateResponse(_mainState, _gameConstants, userid, false, shipAssignment: false, chat: new ChatMessage[] {new ChatMessage()
					{
						message = _gameConstants.Strings.NotPartOfShip,
						timestamp = DateTimeOffset.UtcNow,
						type = ChatMessageType.Server,
					}
					}));

					return true;
				}

				var ship = _mainState.MapShipIdToShipDesc[currentPlayer.ShipId];

				msg = $"[{ship.Name}]" + msg;

				if(IsGameTeamBased(_mainState.GameType))
				{
					faction = ship.FactionId;
					msg = $"[{_gameConstants.Strings.FactionNames[faction]}]" + msg;
				}
			}

			if(chatMessage.Length > _gameConstants.MaxChatChars)
			{
				chatMessage = chatMessage.Substring(0, _gameConstants.MaxChatChars);
			}
			msg += chatMessage;

			ChatMessageType type = (_mainState.IsGameRunning && IsGameTeamBased(_mainState.GameType)) ? ChatMessageType.ChatTeam : ChatMessageType.ChatGeneral;

			// Send to relevant players
			foreach (var p in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
			{
				if(faction >= 0)
				{
					var ship = _mainState.MapShipIdToShipDesc[p.ShipId];

					if (ship.FactionId != faction)
						continue; // Only send to the same faction
				}

				SendToUser(p.UserId, CreateResponse(_mainState, _gameConstants, p.UserId, maingameState: false, chat: new ChatMessage[] {new ChatMessage()
					{
						message = msg,
						timestamp = DateTimeOffset.UtcNow,
						type = type,
					}
				}));
			}

			return true;
		}

		/// <summary>
		/// Assumes that command string starts with '/'
		/// </summary>
		bool ProcessCommand(string userid, string command)
		{
			var player = _mainState.PlayerManagementData.MapUserIdToPlayer[userid];

			void serverTell(string message)
			{
				SendToUser(userid, CreateResponse(_mainState, _gameConstants, userid, chat: new ChatMessage[] {new ChatMessage()
				{
					message = message,
					timestamp = DateTimeOffset.UtcNow,
					type = ChatMessageType.Server,
				}}));
			}

			var split = command.Split();

			// Set privilege command
			if(split[0] == "/privilege" || split[0] == "/p")
			{
				bool success = false;

				if(string.IsNullOrEmpty(_gameConstants.PrivilegePassword))
				{
					success = true;
				}

				if(split.Length >= 2)
				{
					if (split[1] == _gameConstants.PrivilegePassword)
					{
						success = true;
					}
				}

				if(success)
				{
					if (player.IsPrivileged)
					{
						serverTell("You are already privileged.");
						return true;
					}
					else
					{
						_mainState = _mainState with
						{
							PlayerManagementData = _mainState.PlayerManagementData with
							{
								MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(userid, player with
								{
									IsPrivileged = true,
								}),
							},
						};

						serverTell("Privileged rights assigned.");
						return true;
					}
				}
			}

			// Privileged-only commands
			if(player.IsPrivileged)
			{
				// Stops current game
				if (split.Length >= 1 && split[0] == "/stopgame")
				{
					if(_mainState.IsGameRunning)
					{
						serverTell("Stopping game.");
						FinishGame();
					}
					else
					{
						serverTell("Game is not running.");
					}
					return true;
				}

				// Starts a replay
				if (split.Length >= 1 && split[0] == "/replay")
				{
					if (_mainState.IsGameRunning)
					{
						serverTell("Cannot start a replay during a game.");
					}
					else
					{
						if(split.Length >= 2)
						{
							var file = split[1];
							if(!_replayer.StartReplay(file))
							{
								serverTell("Replay not started, wrong file?");
							}
							_mainState = _mainState with
							{
								IsReplaying = true,
							};
						}
						serverTell("Replay started.");
					}
					return true;
				}

				// Stops a replay
				if (split.Length >= 1 && split[0] == "/stopreplay")
				{
					if (!_mainState.IsReplaying)
					{
						serverTell("Replay isn't running.");
					}
					else
					{
						_mainState = _mainState with
						{
							IsReplaying = false,
						};
						serverTell("Replay stopped.");
						SendUpdateShipAssignment();
					}
					return true;
				}

				// Sets the defending faction
				// Sets ship's faction
				if (split.Length >= 1 && split[0] == "/setdefending")
				{
					serverTell($"Current defenders are: {_gameConstants.Strings.FactionNames[_mainState.DefendingFactionId]} (id {_mainState.DefendingFactionId})");

					try
					{
						int faction = int.Parse(split[1]);
						if (_mainState.IsGameRunning)
						{
							serverTell("Cannot set defending faction during a game.");
						}
						else
						{
							if (faction < 0 || faction >= _gameConstants.Strings.FactionNames.Length)
							{
								serverTell($"Faction id {faction} does not exist.");
							}
							else
							{
								var olddef = _mainState.DefendingFactionId;

								_mainState = _mainState with
								{
									DefendingFactionId = faction,
								};

								serverTell($"Set defending faction from {_gameConstants.Strings.FactionNames[olddef]} to {_gameConstants.Strings.FactionNames[faction]}.");

								if (!_mainState.IsGameRunning)
								{
									SendUpdateShipAssignment();
								}
							}
						}
					}
					catch (Exception e)
					{
						serverTell("Error executing command. Usage: /setdefending factionId");
					}
					return true;
				}

				// Kills the player's ship
				if (split.Length >= 1 && split[0] == "/suicide")
				{
					if (_mainState.IsGameRunning && HasPlayerAccessToAShip(_mainState, userid))
					{
						var ship = (ShipEnt)_mainState.MapEntityIdToEntity[_mainState.MapShipIdToShipState[player.ShipId].EntityId];

						_mainState = _mainState with
						{
							MapEntityIdToEntity = _mainState.MapEntityIdToEntity.SetItem(ship.Id, ship with
							{
								Health = -1,
							}),
						};

						serverTell("Killing your ship.");
					}
					else
					{
						serverTell("You are not in a ship.");
					}
					return true;
				}

				// Clears everyone's privilege
				if (split.Length >= 1 && split[0] == "/clearprivilege")
				{
					var builder = ImmutableDictionary.CreateBuilder<string, PlayerData>();

					foreach((var key, var p) in _mainState.PlayerManagementData.MapUserIdToPlayer)
					{
						builder.Add(key, p with
						{
							IsPrivileged = false,
						});
					}
					_mainState = _mainState with
					{
						PlayerManagementData = _mainState.PlayerManagementData with
						{
							MapUserIdToPlayer = builder.ToImmutableDictionary(),
						},
					};

					if(!_mainState.IsGameRunning)
					{
						SendUpdateShipAssignment();
					}

					serverTell("Privileges cleared.");
					return true;
				}

				// Kick offline people from ships
				if (split.Length >= 1 && split[0] == "/kickoffline")
				{
					var builder = ImmutableDictionary.CreateBuilder<string, PlayerData>();

					foreach ((var key, var p) in _mainState.PlayerManagementData.MapUserIdToPlayer)
					{
						bool connected = _mainState.ConnectionManagementData.MapUserIdToConnectionIds.ContainsKey(p.UserId)
							&& _mainState.ConnectionManagementData.MapUserIdToConnectionIds[p.UserId].Length > 0;

						builder.Add(key, p with
						{
							ShipId = connected ? p.ShipId : -1,
							RoleId = connected ? p.RoleId : -1,
						});
					}
					_mainState = _mainState with
					{
						PlayerManagementData = _mainState.PlayerManagementData with
						{
							MapUserIdToPlayer = builder.ToImmutableDictionary(),
						},
					};

					if (!_mainState.IsGameRunning)
					{
						SendUpdateShipAssignment();
					}

					serverTell("Kick offlines.");
					return true;
				}

				// Only intended for exploring seeds
				// RESTARTS THE GAME
				if (split.Length >= 1 && split[0] == "/seed")
				{
					if (_mainState.IsGameRunning)
					{
						if(split.Length >= 2 && int.TryParse(split[1], out int seed))
						{
							var type = _mainState.GameType;
							FinishGame();
							_gameConstants.WorldAsteroidTypes[0].RandomSeed = seed;
							_mainState = InitializeGame(_mainState, _gameConstants, type, out _);

							serverTell($"Seed is now {seed}.");
						}
						else
						{
							serverTell("Usage: /seed integer");
						}
					}
					else
					{
						serverTell("Game is not running.");
					}
					return true;
				}
				// RESTARTS THE GAME
				if (split.Length >= 1 && split[0] == "/density")
				{
					if (_mainState.IsGameRunning)
					{
						if (split.Length >= 2 && double.TryParse(split[1], out double dens))
						{
							var type = _mainState.GameType;
							FinishGame();
							_gameConstants.WorldAsteroidTypes[0].Density = dens * 1e-4;
							_mainState = InitializeGame(_mainState, _gameConstants, type, out _);

							serverTell($"Density is now {dens} * 10^-4.");
						}
						else
						{
							serverTell("Usage: /density double");
						}
					}
					else
					{
						serverTell("Game is not running.");
					}
					return true;
				}

				// Shadowbans a player
				if (split.Length >= 1 && split[0] == "/shadowban")
				{
					if (split.Length >= 2 && int.TryParse(split[1], out int pid))
					{
						try
						{
							bool ban = true;

							if(split.Length >= 3 && split[2] == "0")
							{
								ban = false;
							}

							if (_mainState.PlayerManagementData.MapPlayerIdToUserId.ContainsKey(pid))
							{
								var p = _mainState.PlayerManagementData.MapUserIdToPlayer[_mainState.PlayerManagementData.MapPlayerIdToUserId[pid]];

								_mainState = _mainState with
								{
									PlayerManagementData = _mainState.PlayerManagementData with
									{
										MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(p.UserId, p with
										{
											IsShadowBanned = ban,
										}),
									},
								};

								if(ban)
								{
									serverTell($"{p.Name} ({p.Id}) will have a bad time.");
								}
								else
								{
									serverTell($"{p.Name} ({p.Id}) will no longer have a bad time.");
								}
							}
						}
						catch
						{
							serverTell("Usage: /shadowban playerId [0 if you want to unban]");
						}
					}
					else
					{
						serverTell("Usage: /shadowban playerId [0 if you want to unban]");
					}
					return true;
				}

				// Broadcasts an important message to everyone
				if (split.Length >= 1 && split[0] == "/orgsay")
				{
					string message = "";

					for(int i = 1; i < split.Length; i++)
					{
						message += split[i];

						if (i < split.Length - 1)
							message += " ";
					}

					if(!string.IsNullOrEmpty(message))
					{
						serverTell("Message sent.");

						foreach (var p in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
						{
							SendToUser(p.UserId, CreateResponse(_mainState, _gameConstants, p.UserId, broadcast: message));
						}
					}
					else
					{
						serverTell("Message cannot be empty.");
					}

					return true;
				}

				// Set game constant
				if (split.Length >= 1 && (split[0] == "/gameconst" || split[0] == "/gc"))
				{
					if (split.Length >= 2)
					{
						string? val = null;

						if (split.Length >= 3)
						{
							val = split[2];
						}

						TrySetGameConst(_gameConstants, split[1], val, out var old, out var setnew);

						if(old == null)
						{
							serverTell("Invalid constant name");
							return true;
						}

						if (setnew)
						{
							serverTell($"Game constant {split[1]} set from {old} to {split[2]}");
							return true;
						}
						else
						{
							if(val == null)
							{
								serverTell($"Game constant {split[1]} is {old}.");
								return true;
							}
							else
							{
								{
									serverTell($"Game constant {split[1]} is {old}. Error assigning new value.");
									return true;
								}
							}
						}
					}
					else
					{
						serverTell("Usage: /gameconst name [newValue]");
					}
					return true;
				}

				//// Resets all game constant
				//if (split.Length >= 1 && split[0] == "/resetconst")
				//{
				//	_gameConstants = new GameConstants();

				//	serverTell("Game constants are now set to their initial state.");
				//	return true;
				//}

				// Creates a new ship
				if (split.Length >= 1 && split[0] == "/spawnship")
				{
					try
					{
						string name = split[1];
						int faction = int.Parse(split[2]);
						int rolecount = int.Parse(split[3]);

						if(FindShipByName(_mainState, name, false) != null)
						{
							throw new Exception("This shipname already exists.");
						}

						if (rolecount != 1 && rolecount != 3)
							throw new Exception("Invalid rolecount."); // Will be caught few lines down...

						int privileged = -1;
						if(split.Length <= 4 || !int.TryParse(split[4], out privileged))
						{
							privileged = -1;
						}

						bool onemanned = rolecount == 1;

						bool privilegedOnly = privileged > 0;

						if (privileged < 0)
						{
							privilegedOnly = onemanned;
						}

						_mainState = CreateShip(_mainState, name, faction, onemanned, privilegedOnly, out _);

						serverTell("Ship created.");
						if (!_mainState.IsGameRunning)
						{
							SendUpdateShipAssignment();
						}
					}
					catch(Exception e)
					{
						serverTell($"Error executing command: {e.Message} Usage: /spawnship name factionId roleCount privilegedOnly, eg. /spawnship BidnaUcastnikolod 0 3 0, privileged parameter is optional, default is 0 for three roles, 1 for one role");
					}
					return true;
				}

				// Sets ship's faction
				if (split.Length >= 1 && split[0] == "/setshipfaction")
				{
					try
					{
						string name = split[1];
						int faction = int.Parse(split[2]);

						var ship = FindShipByName(_mainState, name, true);

						if (ship == null)
						{
							serverTell("No ship of that name found.");
						}
						else
						{
							if (faction < 0 || faction >= _gameConstants.Strings.FactionNames.Length)
							{
								serverTell($"Faction id {faction} does not exist.");
							}
							else
							{
								_mainState = _mainState with
								{
									MapShipIdToShipDesc = _mainState.MapShipIdToShipDesc.SetItem(ship.Id, ship with
									{
										FactionId = faction,
									}),
								};

								serverTell($"Ship faction set from {_gameConstants.Strings.FactionNames[ship.FactionId]} to {_gameConstants.Strings.FactionNames[faction]}.");
								if (!_mainState.IsGameRunning)
								{
									SendUpdateShipAssignment();
								}
							}
						}
					}
					catch (Exception e)
					{
						serverTell($"Error executing command: {e.Message} Usage: /setshipfaction shipName newFactionId");
					}
					return true;
				}

				// Sets ship's name
				if (split.Length >= 1 && split[0] == "/setshipname")
				{
					try
					{
						string name = split[1];
						string newname = split[2];

						var ship = FindShipByName(_mainState, name, true);

						if (ship == null)
						{
							serverTell("No ship of that name found.");
						}
						else
						{
							if (!NewShipNameValid(_mainState, newname))
							{
								serverTell("New ship name is not valid.");
							}
							else
							{
								_mainState = _mainState with
								{
									MapShipIdToShipDesc = _mainState.MapShipIdToShipDesc.SetItem(ship.Id, ship with
									{
										Name = newname,
									}),
								};

								serverTell($"Ship name set from {name} to {newname}.");
								if (!_mainState.IsGameRunning)
								{
									SendUpdateShipAssignment();
								}
							}
						}
					}
					catch (Exception e)
					{
						serverTell("Error parsing command. Usage: /setshipname oldName newName");
					}
					return true;
				}

				// Kicks ship's crew
				if (split.Length >= 1 && split[0] == "/kickship")
				{
					try
					{
						string name = split[1];

						var ship = FindShipByName(_mainState, name, true);

						if (ship == null)
						{
							serverTell("No ship of that name found.");
						}
						else
						{
							var crew = GetShipCrew(_mainState, ship.Id);

							var newstate = _mainState;

							foreach (var p in crew)
							{
								newstate = newstate with
								{
									PlayerManagementData = newstate.PlayerManagementData with
									{
										MapUserIdToPlayer = newstate.PlayerManagementData.MapUserIdToPlayer.SetItem(p.UserId, p with
										{
											RoleId = -1,
											ShipId = -1,
										}),
									},
								};
							}

							_mainState = newstate;

							serverTell($"Kicked the crew of {ship.Name} from the ship.");

							if (!_mainState.IsGameRunning)
							{
								SendUpdateShipAssignment();
							}
						}
					}
					catch (Exception e)
					{
						serverTell("Error parsing command. Usage: /kickship shipName");
					}
					return true;
				}

				// Take control of ship
				if (split.Length >= 1 && split[0] == "/control")
				{
					try
					{
						string name = split[1];

						var ship = FindShipByName(_mainState, name, true);

						if (ship == null)
						{
							serverTell("No ship of that name found.");
						}
						else
						{
							const int role = 0;

							var oldplayers = _mainState.PlayerManagementData.MapUserIdToPlayer.Values.Where(x => x.ShipId == ship.Id && x.RoleId == role);

							// Kick old players
							foreach(var o in oldplayers)
							{
								_mainState = _mainState with
								{
									PlayerManagementData = _mainState.PlayerManagementData with
									{
										MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(o.UserId, o with
										{
											ShipId = -1,
											RoleId = -1,
										}),
									}
								};
							}

							_mainState = _mainState with
							{
								PlayerManagementData = _mainState.PlayerManagementData with
								{
									MapUserIdToPlayer = _mainState.PlayerManagementData.MapUserIdToPlayer.SetItem(userid, player with
									{
										ShipId = ship.Id,
										RoleId = role,
									}),
								}
							};

							serverTell("Enjoy your new ship.");
						}
					}
					catch (Exception e)
					{
						serverTell("Error parsing command. Usage: /setshipname oldName newName");
					}
					return true;
				}

				// TODO: doesn't work!
				// Resets game state
				//if (split.Length >= 1 && split[0] == "/resetstate")
				//{
				//	serverTell("Resetting state...");
				//	InicializeState();
				//}
			}

			serverTell("Unknown command.");
			return true;
		}

		//
		// Non-static helpers
		//

		/// <summary>
		/// Checks whether the claimed userId exists.
		/// If not, generates a new userId and a new player.
		/// Pairs the provided connectionId with this userId.
		/// <returns>A userId, equal to claimedUserId if it is valid.</returns>
		/// </summary>
		string ManagePlayerConnection(string? claimedUserId, string connectionId)
		{
			if (!string.IsNullOrEmpty(claimedUserId))
			{
				// Verify userid
				// Note: we are verifying using userid -> playerdata map
				if (_mainState.PlayerManagementData.MapUserIdToPlayer.ContainsKey(claimedUserId))
				{
					// Verified, try to pair connectionId

					var map = _mainState.ConnectionManagementData.MapUserIdToConnectionIds;
					if (!map[claimedUserId].Contains(connectionId))
					{
						// New connectionId, store it
						_mainState = _mainState with
						{
							ConnectionManagementData = _mainState.ConnectionManagementData with
							{
								MapConnectionIdToUserIds = _mainState.ConnectionManagementData.MapConnectionIdToUserIds.Add(connectionId, claimedUserId),
								MapUserIdToConnectionIds = map.SetItem(claimedUserId, map[claimedUserId].Add(connectionId)),
							}
						};
					}

					// Exit
					return claimedUserId;
				}
			}

			// Verification failed, generate a new userId

			var newUserId = GetNewUserId();

			// Allow users to keep whatever userid they want, as long as it is long enough
			if(!string.IsNullOrEmpty(claimedUserId) && claimedUserId.Length == newUserId.Length)
			{
				newUserId = claimedUserId;
			}

			int playerGiveId = _mainState.GivePlayerId;

			var player = new PlayerData()
			{
				Id = playerGiveId++,
				IsPrivileged = _gameConstants.EveryoneIsPrivileged,
				Name = DefaultPlayerName,
				UserId = newUserId,
			};

			var pm = _mainState.PlayerManagementData;
			var cm = _mainState.ConnectionManagementData;

			if (cm.MapConnectionIdToUserIds.ContainsKey(connectionId))
			{
				_logger.LogWarning($"An established connection has sent a request with invalid userid, even though it already has one assigne ({cm.MapConnectionIdToUserIds[connectionId]})d. (second request send before recieving the first's response?)");
				// We just return the existing userid the connection has assigned.
				return cm.MapConnectionIdToUserIds[connectionId];
			}

			var a1 = pm.MapPlayerIdToUserId.Add(player.Id, newUserId);
			var a2 = pm.MapUserIdToPlayer.Add(newUserId, player);
			var a3 = cm.MapConnectionIdToUserIds.Add(connectionId, newUserId);
			var a4 = cm.MapUserIdToConnectionIds.Add(newUserId, ImmutableArray.Create(connectionId));

			_mainState = _mainState with
			{
				PlayerManagementData = _mainState.PlayerManagementData with
				{
					MapPlayerIdToUserId = a1,
					MapUserIdToPlayer = a2,
				},
				ConnectionManagementData = _mainState.ConnectionManagementData with
				{
					MapConnectionIdToUserIds = a3,
					MapUserIdToConnectionIds = a4,
				},
				GivePlayerId = playerGiveId,
			};

			return newUserId;
		}

		/// <summary>
		/// Generates a new random userid
		/// </summary>
		string GetNewUserId()
		{
			string FromBytes(byte[] bytes)
			{
				var sb = new System.Text.StringBuilder();

				for (int i = 0; i < bytes.Length; i++)
				{
					// Only generate nice a..z strings that can go into url parameter
					sb.Append((char)('a' + (bytes[i] % ('z' - 'a' + 1))));
				}

				return sb.ToString();
			}

			string userid;
			do
			{
				// Loop for paranoid programmers
				byte[] randomBytes = new byte[RandomStringBytes];
				_rng.GetBytes(randomBytes);

				userid = FromBytes(randomBytes);
			}
			while (_mainState.PlayerManagementData.MapUserIdToPlayer.ContainsKey(userid));

			return userid;
		}

		//
		// Helper methods
		//

		static bool GetRespawnsAllowed(GameState state)
		{
			return state.GameType != GameType.Race;
		}

		/// <summary>
		/// Returns old value.
		/// </summary>
		static void TrySetGameConst(GameConstants constants, string name, string? value, out string? oldValue, out bool setNew)
		{
			oldValue = null;
			setNew = false;

			System.Reflection.PropertyInfo? prop = typeof(GameConstants).GetProperties().SingleOrDefault(x => x.Name.ToLowerInvariant() == name.ToLowerInvariant());

			object obj = constants;

			if (prop == null)
			{
				var split = name.Split('.');

				if (split.Length < 2)
					return;

				prop = typeof(ShipConsts).GetProperties().SingleOrDefault(x => x.Name.ToLowerInvariant() == split[1].ToLowerInvariant());

				if (prop == null)
					return;

				if (split[0].ToLowerInvariant() == "ship")
				{
					obj = constants.Ship;
				}

				if (split[0].ToLowerInvariant() == "station")
				{
					obj = constants.Station;
				}
			}

			oldValue = prop.GetValue(obj)?.ToString();

			if(value != null)
			{
				if(prop.PropertyType == typeof(bool))
				{
					bool target = true;

					if(value == "False" || value == "false" || value == "f" || value == "0")
					{
						target = false;
					}

					prop.SetValue(obj, target);
					setNew = true;
					return;
				}

				if (prop.PropertyType == typeof(int))
				{
					if(int.TryParse(value, out var i))
					{
						prop.SetValue(obj, i);
						setNew = true;
						return;
					}
				}

				if (prop.PropertyType == typeof(double))
				{
					if (double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out var d))
					{
						prop.SetValue(obj, d);
						setNew = true;
						return;
					}
				}
			}
		}

		static ShipPropertyDescription? FindShipByName(GameState state, string name, bool prefix = true)
		{
			var prefixes = state.MapShipIdToShipDesc.Values.Where(x => x.Name.ToLowerInvariant().StartsWith(name.ToLowerInvariant())).ToList();
			var exact = state.MapShipIdToShipDesc.Values.Where(x => x.Name.ToLowerInvariant() == name.ToLowerInvariant()).ToList();

			if (prefix)
			{
				if (prefixes.Count != 1)
				{
					if (exact.Count != 1)
					{
						return null;
					}

					return exact[0];
				}

				return prefixes[0];
			}
			else
			{
				if (exact.Count != 1)
				{
					return null;
				}
				return exact[0];
			}
		}

		static bool NewShipNameValid(GameState state, string newname)
		{
			return !(string.IsNullOrEmpty(newname) || newname.Length > 50 || newname.Contains(' ') || FindShipByName(state, newname, false) != null);
		}

		static ShipEnt RespawnShip(GameState state, GameConstants constants, int shipId, int entId, int factionId)
		{
			var ship = state.MapShipIdToShipDesc[shipId];

			var ent = ShipEnt.Create(constants, constants.Ship, entId, shipId, factionId, GetSpawnPos(state, constants, factionId, ship.Id, 7), ship.Roles.Length == 1);
			return ent;
		}

		static int GetLivingStationCount(GameState state)
		{
			return state.StationEntityIds.Where(s =>
			{
				if (!state.MapEntityIdToEntity.ContainsKey(s))
					return false;
				var station = (ShipEnt)state.MapEntityIdToEntity[s];

				return station.Health > 0;

			}).Count();
		}

		static string GetEndGameMessage(GameState state, GameConstants constants)
		{
			string msg = string.Format(constants.Strings.GameEnded, state.CurrentGameElapsed);

			if(state.GameType == GameType.Defense)
			{
				if(GetLivingStationCount(state) == 0)
				{
					msg += "\n" + constants.Strings.AttackersWon;
				}
				else
				{
					msg += "\n" + constants.Strings.DefendersWon;

					double stationHealthSum = 0;
					foreach(var id in state.StationEntityIds)
					{
						if (!state.MapEntityIdToEntity.ContainsKey(id))
							continue;
						var st = (ShipEnt)state.MapEntityIdToEntity[id];
						stationHealthSum += st.Health;
					}

					msg += "\n" + string.Format(constants.Strings.GameEndRemainingStationHealthSum, stationHealthSum.ToString("0.00"), (constants.Station.ShipMaxHealth* constants.DefenseStationCount).ToString("0.00"));
				}
			}

			if(IsGameTeamBased(state.GameType))
			{
				Dictionary<int, GameStatsShip> factionKills = new Dictionary<int, GameStatsShip>();

				foreach (var stat in state.Stats.MapShipIdToKillCount)
				{
					var ship = state.MapShipIdToShipDesc[stat.Key];

					if (!factionKills.ContainsKey(ship.FactionId))
						factionKills.Add(ship.FactionId, new GameStatsShip());
					var s = factionKills[ship.FactionId];
					factionKills[ship.FactionId] = s with
					{
						Kills = s.Kills + stat.Value.Kills,
						Deaths = s.Deaths + stat.Value.Deaths,
						TeamKills = s.TeamKills + stat.Value.TeamKills,
						StationKills = s.StationKills + stat.Value.StationKills,
					};
				}

				msg += "\n" + constants.Strings.TeamStats;

				foreach (var stat in factionKills)
				{
					msg += "\n" + string.Format(constants.Strings.StatsKD, stat.Value.Kills.ToString("D3"),
						stat.Value.TeamKills.ToString("D3"), stat.Value.Deaths.ToString("D3"),
						stat.Value.StationKills.ToString("D3"), constants.Strings.FactionNames[stat.Key]);
				}
			}

			msg += "\n" + constants.Strings.ShipStats;

			foreach (var stat in state.Stats.MapShipIdToKillCount.OrderByDescending(x => x.Value.Kills))
			{
				if (!state.MapShipIdToShipDesc.ContainsKey(stat.Key))
					continue;

				var ship = state.MapShipIdToShipDesc[stat.Key];

				if (!state.MapShipIdToShipState.ContainsKey(ship.Id))
					continue;

				int eid = state.MapShipIdToShipState[ship.Id].EntityId;

				if (!state.MapEntityIdToEntity.ContainsKey(eid))
					continue;

				var ent = (ShipEnt)state.MapEntityIdToEntity[eid];

				msg += "\n" + string.Format(constants.Strings.StatsKD, stat.Value.Kills.ToString("D3"),
						stat.Value.TeamKills.ToString("D3"), stat.Value.Deaths.ToString("D3"),
						stat.Value.StationKills.ToString("D3"), ent.GetNiceDescription(state, constants));
			}

			return msg;
		}

		/// <summary>
		/// Updates stats and announces kills, decides if the game ends. Does not handle ship deaths in any way.
		/// </summary>
		static GameState ProcessKillEvents(GameState state, GameState stateWithVictims, GameConstants constants, List<KillEvent> killEvents, List<string> globalLog, out string? globalAnnounce)
		{
			globalAnnounce = null;

			var stats = state.Stats.MapShipIdToKillCount;

			foreach(var kill in killEvents)
			{
				var victim = stateWithVictims.MapEntityIdToEntity[kill.VictimId];

				// Always record ship death
				if (victim is ShipEnt)
				{
					var v = (ShipEnt)victim;

					if(!v.Constants.IsStation)
					{
						stats = stats.SetItem(v.ShipId, stats[v.ShipId] with
						{
							Deaths = stats[v.ShipId].Deaths + 1,
						});
					}
				}

				// Announce generic ship or station death.
				if (!stateWithVictims.MapEntityIdToEntity.ContainsKey(kill.KillerId))
				{
					if(victim is ShipEnt)
					{
						var v = (ShipEnt)victim;

						if(v.Constants.IsStation)
						{
							globalLog.Add(string.Format(constants.Strings.StationDestroyed, v.GetNiceDescription(stateWithVictims, constants), GetLivingStationCount(state)));
						}
						else
						{
							globalLog.Add(string.Format(constants.Strings.ShipDestroyed, v.GetNiceDescription(stateWithVictims, constants)));
						}
					}

					continue;
				}
				
				var killer = stateWithVictims.MapEntityIdToEntity[kill.KillerId];

				// Ship kills ship
				if(victim is ShipEnt && killer is ShipEnt)
				{
					var v = (ShipEnt)victim;
					var k = (ShipEnt)killer;

					if(!v.Constants.IsStation)
					{
						globalLog.Add(string.Format(constants.Strings.ShipKilled, v.GetNiceDescription(stateWithVictims, constants), kill.Type, k.GetNiceDescription(stateWithVictims, constants)));

						bool isteamkill = (IsGameTeamBased(stateWithVictims.GameType) && v.FactionId == k.FactionId);
						isteamkill = isteamkill || v.ShipId == k.ShipId;

						if (isteamkill)
						{
							stats = stats.SetItem(k.ShipId, stats[k.ShipId] with
							{
								TeamKills = stats[k.ShipId].TeamKills + 1,
							});
						}
						else
						{
							stats = stats.SetItem(k.ShipId, stats[k.ShipId] with
							{
								Kills = stats[k.ShipId].Kills + 1,
							});
						}
					}
				}

				// Ship kills station
				if (victim is ShipEnt && killer is ShipEnt)
				{
					var v = (ShipEnt)victim;
					var k = (ShipEnt)killer;

					if(v.Constants.IsStation)
					{
						var msg = string.Format(constants.Strings.StationKilled, v.GetNiceDescription(stateWithVictims, constants),
							kill.Type, k.GetNiceDescription(stateWithVictims, constants), GetLivingStationCount(state));
						globalLog.Add(msg);

						globalAnnounce = msg;

						stats = stats.SetItem(k.ShipId, stats[k.ShipId] with
						{
							StationKills = stats[k.ShipId].StationKills + 1,
						});

						continue;
					}
				}
			}

			return state with
			{
				Stats = state.Stats with
				{
					MapShipIdToKillCount = stats,
				},
			};
		}

		static Dictionary<string, List<RadarSignature>> GenHudSignatures(GameState state, GameConstants constants)
		{
			var allSignatures = new List<RadarSignature>();

			foreach(var e in state.MapEntityIdToEntity.Values)
			{
				var sig = e.GetSignature(state, constants, RadarSignaturePurpose.Spectator, null);

				if (sig != null)
					allSignatures.Add(sig);
			}

			var dict = new Dictionary<string, List<RadarSignature>>();

			if (!state.IsGameRunning)
				return dict;

			if(IsGameTeamBased(state.GameType))
			{
				Dictionary<int, List<RadarSignature>> factionSignatures = new Dictionary<int, List<RadarSignature>>();

				foreach (var signature in allSignatures)
				{
					if (!state.MapEntityIdToEntity.ContainsKey(signature.id))
						continue;

					var ent = state.MapEntityIdToEntity[signature.id];

					if (ent is ITeamEntity)
					{
						var teament = (ITeamEntity)ent;

						if (!factionSignatures.ContainsKey(teament.FactionId))
							factionSignatures.Add(teament.FactionId, new List<RadarSignature>());
						factionSignatures[teament.FactionId].Add(signature);
					}
				}

				foreach(var player in state.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					if (!HasPlayerAccessToAShip(state, player.UserId))
						continue;

					var shipentid = state.MapShipIdToShipState[player.ShipId].EntityId;

					int faction = state.MapShipIdToShipDesc[player.ShipId].FactionId;

					List<RadarSignature> list = new List<RadarSignature>();
					if (factionSignatures.ContainsKey(faction))
						list = factionSignatures[faction].ToList(); // Clone
					// Exclude the player's ship
					list = list.Where(x => x.id != shipentid).ToList();

					// Will also include probes and missiles and mines of every freindly ship, including the players
					dict.Add(player.UserId, list);
				}
			}
			else
			{
				Dictionary<int, List<RadarSignature>> ownedSignatures = new Dictionary<int, List<RadarSignature>>();

				foreach (var signature in allSignatures)
				{
					if (!state.MapEntityIdToEntity.ContainsKey(signature.id))
						continue;

					var ent = state.MapEntityIdToEntity[signature.id];

					if (ent is IOwnedEntity)
					{
						var owned = (IOwnedEntity)ent;

						if (!ownedSignatures.ContainsKey(owned.OwnerEntityId))
							ownedSignatures.Add(owned.OwnerEntityId, new List<RadarSignature>());
						ownedSignatures[owned.OwnerEntityId].Add(signature);
					}
				}

				foreach (var player in state.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					if (!HasPlayerAccessToAShip(state, player.UserId))
						continue;

					var ship = (ShipEnt)state.MapEntityIdToEntity[state.MapShipIdToShipState[player.ShipId].EntityId];

					List<RadarSignature> signatures = new List<RadarSignature>();

					if(ownedSignatures.ContainsKey(ship.Id))
					{
						foreach(var s in ownedSignatures[ship.Id])
						{
							if (s.id != ship.Id)
								signatures.Add(s); // Exclude current ship
						}
					}

					// Only includes whatever is owned by this ship.
					dict.Add(player.UserId, signatures);
				}
			}

			// Hack to get the right signatures for stuff

			foreach(var pair in dict)
			{
				var list2 = pair.Value.ToList();

				var list = pair.Value;

				var player = state.PlayerManagementData.MapUserIdToPlayer[pair.Key];

				list.Clear();

				foreach(var s in list2)
				{
					var ent = state.MapEntityIdToEntity[s.id];
					var s2 = ent.GetSignature(state, constants, RadarSignaturePurpose.Camera, player.ShipId);
					list.Add(s2);
				}
			}

			return dict;
		}

		public static void MakeImpactDebris(Vector2d collisionEntPos, Vector2d collisionEntVelocity, int collisionEntId, Vector2d collisionPoint, int seed, int debrisCount, GameConstants constants,
			Vector2d firingDirection, Func<Entity, int> spawnEnt)
		{
			var normal = (collisionPoint - collisionEntPos).Normalized();

			Random r = new Random(seed);

			for(int i = 0; i < debrisCount; i++)
			{
				var velocity = new Vector2d(r.NextDouble(), r.NextDouble()) * 2 - Vector2d.One;
				if (velocity.LengthSquared > 1)
					velocity.Normalize();
				velocity *= constants.ImpactDebrisVelocityRandom;
				velocity += collisionEntVelocity;
				velocity += (-firingDirection.Normalized() * 2 + normal).Normalized() * constants.ImpactDebrisVelocity;

				spawnEnt(new HotDebrisEnt()
				{
					HeatIntensity = constants.ImpactDebrisIntensity,
					IntensityLoss = constants.ImpactDebrisIntensityLoss,
					IsProjectile = true,
					Mass = 0.01,
					Radius = constants.ImpactDebrisRadius,
					Position = collisionPoint,
					Velocity = velocity,
					IgnoreCollisionsFrom = ImmutableArray.Create(collisionEntId),
				});
			}
		}

		public static void MakeExplosion(int owningShipEntId, Vector2d origin, double innerRadius, double outerRadius, double innerDamage,
			in CollisionGrid grid, in GameConstants constants, List<DamageEvent> damageEvents, Func<Entity, int> spawnEnt)
		{
			CustomMetrics.Game_Explosions.Inc();

			if(innerDamage > 0)
			{
				foreach (var e in grid.GetEntitesInsideRange(origin - new Vector2d(outerRadius), origin + new Vector2d(outerRadius)))
				{
					double dist = (e.Position0 - origin).Length;
					if (dist > outerRadius)
						continue;

					double damage = (1.0 - Math.Max(0, dist - innerRadius) / (outerRadius - innerRadius)) * innerDamage;
					damageEvents.Add(new DamageEvent()
					{
						DamageAmount = damage,
						DealerEntityId = owningShipEntId,
						ReceiverEntityId = e.OldEntity.Id,
						Origin = origin,
						Type = DamageType.Explosion,
					});
				}
			}

			int debrisCount = (int)Math.Floor(outerRadius * outerRadius * Math.PI * constants.ExplosionDebrisDensity);

			Random r = new Random(33);

			for(int i = 0; i < debrisCount; i++)
			{
				Vector2d pos = new Vector2d(r.NextDouble(), r.NextDouble()) * 2 - Vector2d.One;
				if (pos.LengthSquared > 1)
					pos.Normalize();
				pos *= outerRadius;
				var vel = pos.Normalized() * constants.ExplosionDebrisVelocity;

				pos += origin;

				spawnEnt(new HotDebrisEnt()
				{
					HeatIntensity = constants.ExplosionDebrisIntensity,
					IntensityLoss = constants.ExplosionDebrisIntensityLoss,
					IsProjectile = true,
					Mass = 0.01,
					Radius = constants.ExplosionDebrisRadius,
					Position = pos,
					Velocity = vel,
				});
			}
		}

		static GameState CreateShip(GameState state, string name, int faction, bool oneman, bool privilegedOnly, out ShipPropertyDescription ship)
		{
			ship = new ShipPropertyDescription()
			{
				Id = state.GiveShipId,
				FactionId = faction,
				Name = name,
				onlyAcceptsPrivileged = privilegedOnly,
				Roles = oneman ? ImmutableArray.Create<int>(0) : ImmutableArray.Create<int>(1, 2, 3),
			};

			return state with
			{
				MapShipIdToShipDesc = state.MapShipIdToShipDesc.Add(state.GiveShipId, ship),
				GiveShipId = state.GiveShipId + 1,
			};
		}

		static GameState CreateTestShips(GameState state, GameConstants constants)
		{
			int giveShipId = state.GiveShipId;
			var shipBuilder = ImmutableDictionary.CreateBuilder<int, ShipPropertyDescription>();

			void spawnShip(ImmutableDictionary<int, ShipPropertyDescription>.Builder b, ShipPropertyDescription desc)
			{
				b.Add(desc.Id, desc);
			}

			// 3-man
			{
				int i = 0;
				foreach (var name in constants.InicialCommonShips)
				{
					spawnShip(shipBuilder, new ShipPropertyDescription()
					{
						Id = giveShipId++,
						FactionId = (i < constants.InicialCommonShips.Length / 2) ? 0 : 1,
						Name = name,
						onlyAcceptsPrivileged = false,
						Roles = ImmutableArray.Create<int>(1, 2, 3),
					});
					i++;
				}
			}

			// 1-man
			{
				int i = 0;
				foreach (var name in constants.InicialPrivilegedShips)
				{
					spawnShip(shipBuilder, new ShipPropertyDescription()
					{
						Id = giveShipId++,
						FactionId = (i < constants.InicialPrivilegedShips.Length / 2) ? 0 : 1,
						Name = name,
						onlyAcceptsPrivileged = true,
						Roles = ImmutableArray.Create<int>(0),
					});
					i++;
				}
			}

			// 2-man
			{
				int i = 0;
				foreach (var name in constants.InicialTwoManShips)
				{
					spawnShip(shipBuilder, new ShipPropertyDescription()
					{
						Id = giveShipId++,
						FactionId = (i < constants.InicialTwoManShips.Length / 2) ? 0 : 1,
						Name = name,
						onlyAcceptsPrivileged = false,
						Roles = ImmutableArray.Create<int>(4, 5),
					});
					i++;
				}
			}

			return state with
			{
				MapShipIdToShipDesc = shipBuilder.ToImmutable(),
				GiveShipId = giveShipId,
			};
		}

		public static List<PlayerData> GetShipCrew(GameState state, int shipid)
		{
			return state.PlayerManagementData.MapUserIdToPlayer.Values.Where(x => x.ShipId == shipid).ToList();
		}

		/// <summary>
		/// True when game is running, player is a valid part of a ship, and the player is not dead.
		/// </summary>
		static bool HasPlayerAccessToAShip(GameState state, string userid)
		{
			if (!state.IsGameRunning)
				return false; // Game is not running
			if (IsUserDead(state, userid))
				return false; // User is dead

			var player = state.PlayerManagementData.MapUserIdToPlayer[userid];

			if (!state.MapShipIdToShipState.ContainsKey(player.ShipId))
				return false; // Player's ship is invalid
			if (!state.MapShipIdToShipDesc.ContainsKey(player.ShipId))
				return false; // Player's ship is not participating in this game
			if (!state.MapShipIdToShipDesc[player.ShipId].Roles.Contains(player.RoleId))
				return false; // Player's role is invalid

			return true;
		}

		public static bool IsGameTeamBased(GameType type)
		{
			return type == GameType.TeamDeathmatch || type == GameType.Defense;
		}

		static bool IsUserDead(GameState state, string userid)
		{
			// Player is dead <=> has ship assigned && ship ent is not in living entity set

			int shipid = state.PlayerManagementData.MapUserIdToPlayer[userid].ShipId;

			if (!state.MapShipIdToShipState.ContainsKey(shipid))
				return false;

			int entid = state.MapShipIdToShipState[shipid].EntityId;

			return !state.MapEntityIdToEntity.ContainsKey(entid);
		}

		static bool IsUserPrivileged(GameState state, string userid)
		{
			return state.PlayerManagementData.MapUserIdToPlayer[userid].IsPrivileged;
		}

		static bool IsUserIdConnected(GameState state, string userid)
		{
			return state.ConnectionManagementData.MapUserIdToConnectionIds[userid].Length > 0;
		}

		static ServerResponse CreateResponse(GameState state, GameConstants constants, string recipientUserId, bool maingameState = false, IEnumerable<ChatMessage>? chat = null,
			Dictionary<string, List<RadarSignature>>? hudSignatures = null,
			SpectatorMapState? spectatorMap = null,
			bool shipAssignment = false,
			string? broadcast = null)
		{
			var player = state.PlayerManagementData.MapUserIdToPlayer[recipientUserId];

			// Base response
			ServerResponse response = new ServerResponse()
			{
				playerGameState = new PlayerAndGameState()
				{
					gameType = (state.IsGameRunning ? state.GameType : GameType.Inactive),
					isPlayerDead = IsUserDead(state, recipientUserId),
					playerId = player.Id,
					playerIsPrivileged = player.IsPrivileged,
					playerUserId = recipientUserId,
					playerName = player.Name,
					tickIntervalSeconds = constants.TickIntervalSeconds,
					timeScale = constants.TimeScale,
					roleNames = constants.Strings.RoleNames.ToArray(),
					factionNames = constants.Strings.FactionNames.ToArray(),
				},
				worldSize = new Vec2()
				{
					x = state.WorldSize.X,
					y = state.WorldSize.Y,
				},
			};

			if((maingameState || hudSignatures != null) && !IsUserDead(state, recipientUserId))
			{
				response = response with
				{
					mainGameState = CreateResponseMainGameState(state, constants, recipientUserId, hudSignatures),
				};
			}

			if(spectatorMap != null)
			{
				if (IsUserDead(state, recipientUserId) || IsUserPrivileged(state, recipientUserId))
				{
					spectatorMap = GetSpectatorMap(state, constants);

					response = response with
					{
						spectatorMap = spectatorMap,
					};
				}
			}

			if(shipAssignment)
			{
				var players = new List<PlayerDescription>();
				var ships = new List<ShipDescription>();

				foreach(var s in state.MapShipIdToShipDesc.Values)
				{
					if (!s.isVisible)
						continue;

					var d = s.GetDescription();

					// Hack to display ship factions during assignment
					d = d with
					{
						name = $"[{constants.Strings.FactionNames[d.factionid]}] " + d.name,
					};

					ships.Add(d);
				}

				foreach(var p in state.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					players.Add(p.GetDescription(state, player.IsPrivileged));
				}

				response = response with
				{
					shipAssignmentState = new ResponseShipAssignmentState()
					{
						factionNames = constants.Strings.FactionNames.ToArray(),
						roleNames = constants.Strings.RoleNames.ToArray(),
						players = players.ToArray(),
						ships = ships.ToArray(),
					}
				};
			}

			if(chat != null)
			{
				response = response with
				{
					incomingChat = chat.ToArray(),
				};
			}

			if(broadcast != null)
			{
				response = response with
				{
					broadcast = new ChatMessage()
					{
						timestamp = DateTimeOffset.UtcNow,
						message = broadcast,
						type = ChatMessageType.Broadcast,
					},
				};
			}

			return response;
		}

		static MainGameState? CreateResponseMainGameState(GameState state, GameConstants constants, string userid,
			Dictionary<string, List<RadarSignature>>? hudSignatures)
		{
			if (!HasPlayerAccessToAShip(state, userid))
				return null;

			MainGameState main = new MainGameState()
			{
				timeScale = constants.TimeScale,
				tickIntervalSeconds = constants.TickIntervalSeconds,
			};

			PlayerData player = state.PlayerManagementData.MapUserIdToPlayer[userid];
			int shipid = player.ShipId;
			ShipPropertyDescription shipdesc = state.MapShipIdToShipDesc[shipid];
			var shipstate = state.MapShipIdToShipState[shipid];
			ShipEnt shipent = (ShipEnt)state.MapEntityIdToEntity[state.MapShipIdToShipState[shipid].EntityId];

			// Fill in death count
			{
				main = main with
				{
					deaths = state.Stats.MapShipIdToKillCount[shipid].Deaths,
				};
			}

			// Fill in pointers
			{
				main = main with
				{
					pointers = shipstate.Pointers.Select(x => x.Item2).ToArray(),
				};
			}

			// Fill in ship state
			{
				main = main with
				{
					shipState = new ShipState()
					{
						shipRadius = shipent.Radius,
						description = shipdesc.GetDescription(),
						players = GetShipCrew(state, shipid).Select(x => x.GetDescription(state, player.IsPrivileged)).ToArray(),
						healthCurrent = shipent.Health,
						healthMax = shipent.Constants.ShipMaxHealth,
						isInvisible = shipent.SystemStealth.IsInStealth(),
						position = shipent.Position.ToVec2(),
						velocity = (shipent.Velocity * constants.TimeScale).ToVec2(),
						targetVelocity = shipent.SystemEngines.TargetVelocity.ToVec2(),
					}
				};
			}

			// Fill in power state
			{
				main = main with
				{
					powerState = new ShipReactorPowerState()
					{
						powerAvailable = shipent.Constants.ShipReactorPowerUnits - shipent.GetMainPowerUsage(),
						powerTotal = shipent.Constants.ShipReactorPowerUnits,
					},
				};
			}

			// Fill in systems state
			{
				T? trySetSystem<T>(string key, ShipSystem system, T? tnull) // Can't just `return null;`?
				{
					if (!constants.RoleSystems[player.RoleId].Contains(key))
						return tnull; // Player's role doesn't have access
					return (T)system.GetClientState(constants);
				}

				main = main with
				{
					systems = new SystemState()
					{
						sensorCameras = trySetSystem<SystemStateRadar>(nameof(ShipEnt.SystemSensorCamera), shipent.SystemSensorCamera, null),
						sensorInfrared = trySetSystem<SystemStateRadar>(nameof(ShipEnt.SystemSensorInfrared), shipent.SystemSensorInfrared, null),
						sensorRadar = trySetSystem<SystemStateRadar>(nameof(ShipEnt.SystemSensorRadar), shipent.SystemSensorRadar, null)
							?? trySetSystem<SystemStateRadar>(nameof(ShipEnt.SystemSensorRadarSmall), shipent.SystemSensorRadarSmall, null),
						sensorProbe = trySetSystem<SystemStateRadar>(nameof(ShipEnt.SystemSensorProbe), shipent.SystemSensorProbe, null),

						weaponLaser = trySetSystem<SystemGenericChargingState>(nameof(ShipEnt.SystemWeaponLaser), shipent.SystemWeaponLaser, null),
						weaponPlasma = trySetSystem<SystemGenericChargingState>(nameof(ShipEnt.SystemWeaponPlasma), shipent.SystemWeaponPlasma, null),
						weaponMissiles = trySetSystem<SystemGenericChargingState>(nameof(ShipEnt.SystemWeaponMissiles), shipent.SystemWeaponMissiles, null),
						weaponMines = trySetSystem<SystemGenericChargingState>(nameof(ShipEnt.SystemWeaponMines), shipent.SystemWeaponMines, null),
						weaponProbes = trySetSystem<SystemGenericChargingState>(nameof(ShipEnt.SystemWeaponProbes), shipent.SystemWeaponProbes, null),

						remoteMissile = trySetSystem<SystemRemoteState>(nameof(ShipEnt.SystemRemoteMissiles), shipent.SystemRemoteMissiles, null),

						shields = trySetSystem<SystemShieldState>(nameof(ShipEnt.SystemShields), shipent.SystemShields, null),
						stealth = trySetSystem<SystemStealthState>(nameof(ShipEnt.SystemStealth), shipent.SystemStealth, null),
						engines = trySetSystem<SystemEnginesState>(nameof(ShipEnt.SystemEngines), shipent.SystemEngines, null),
						autoRepair = trySetSystem<SystemAutoRepairState>(nameof(ShipEnt.SystemAutoRepair), shipent.SystemAutoRepair, null),
					},
				};
			}

			// HUD/IFF
			if(hudSignatures != null && hudSignatures.ContainsKey(userid))
			{
				main = main with
				{
					systems = main.systems with
					{
						sensorHud = hudSignatures[userid].ToArray(),
					},
				};
			}

			// Timers
			main = main with
			{
				timeMax = constants.MaxGameTime,
				timeElapsed = state.CurrentGameElapsed,
			};

			main = main with
			{
				raceFinishPosition = new Vec2()
				{
					x = -1,
					y = -1,
				},
				raceFinishRadius = -1,
			};

			main = OptimizeRadars(constants, main, shipent);

			foreach(var e in state.MapEntityIdToEntity.Values)
			{
				if(e is RaceFinishEnt finish)
				{
					main = main with
					{
						raceFinishPosition = new Vec2()
						{
							x = finish.Position.X,
							y = finish.Position.Y,
						},
						raceFinishRadius = finish.DetectionRange,
					};
				}
			}

			return main;
		}

		static SpectatorMapState GetSpectatorMap(GameState state, GameConstants constants)
		{
			List<RadarSignature> signatures = new List<RadarSignature>();

			foreach(var ent in state.MapEntityIdToEntity.Values)
			{
				var sig = ent.GetSignature(state, constants, RadarSignaturePurpose.Spectator, null);

				if (sig == null)
					continue;

				sig = sig with
				{
					isVisibleAnywhere = true,
				};

				signatures.Add(sig);
			}

			return new SpectatorMapState {
				objects = signatures.ToArray()
			};
		}

		static GameState InitializeGame(GameState state, GameConstants constants, GameType gameType, out int totalParticipants)
		{
			state = state with
			{
				GameType = gameType,
				WorldSize = new Vector2i(constants.WorldSizeXY, constants.WorldSizeXY),
				IsGameRunning = true,
				MapEntityIdToEntity = ImmutableDictionary<int, Entity>.Empty,
				MapShipIdToShipState = ImmutableDictionary<int, ShipStateData>.Empty,
				CurrentGameElapsed = 0,
			};

			Vector2d[] factionSpawns = new Vector2d[constants.Strings.FactionNames.Length];

			totalParticipants = 0;

			// Inicialize ships
			{
				// Find out what factions are present, later store how many ships each has
				// faction id -> number of spawned ships
				var factionSpawned = new Dictionary<int, int>();
				// faction id -> faction order
				var factionOrder = new Dictionary<int, int>();

				// Find out who mans what ship
				var assignments = new Dictionary<int, List<PlayerData>>();
				foreach (var p in state.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					if (p.ShipId < 0 || p.RoleId < 0)
					{
						continue;
					}

					if (!assignments.ContainsKey(p.ShipId))
					{
						assignments.Add(p.ShipId, new List<PlayerData>());
					}

					assignments[p.ShipId].Add(p);
				}

				// Find fully manned ships
				var mannedShips = new List<ShipPropertyDescription>();
				foreach (var ship in state.MapShipIdToShipDesc.Values)
				{
					if (!assignments.ContainsKey(ship.Id) && !constants.AllowUnmannedShips)
						continue; // Unmanned ship

					int localParticipants = 0;

					bool manned = true;

					if(assignments.ContainsKey(ship.Id))
					{
						foreach (var role in ship.Roles)
						{
							if (!assignments[ship.Id].Any(x => x.RoleId == role))
							{
								manned = false;
							}
							else
							{
								localParticipants++;
							}
						}
					}

					if(constants.AllowPartiallyMannedShips)
					{
						manned = true;
					}

					if(!manned)
					{
						// Not fully manned
						continue;
					}

					mannedShips.Add(ship);
					totalParticipants += localParticipants;

					int faction = ship.FactionId;

					if (!factionSpawned.ContainsKey(faction))
					{
						factionSpawned.Add(faction, 0);
						factionOrder.Add(faction, factionOrder.Count);
					}
				}

				// Inicialize stats as well
				var statsBuilder = ImmutableDictionary.CreateBuilder<int, GameStatsShip>();
				var builder = ImmutableDictionary.CreateBuilder<int, ShipStateData>();
				var ents = new Dictionary<int, Entity>();
				int giveEntId = state.GiveEntityId;

				// Inicialize faction spawns
				if (IsGameTeamBased(state.GameType))
				{
					foreach ((int faction, int order) in factionOrder)
					{
						double placementAngle = 0;

						double radius = constants.SpawnRingRadiusScale;

						if (gameType == GameType.Defense)
						{
							// Defense
							if (faction == state.DefendingFactionId)
							{
								// Bottom right
								placementAngle += Math.PI * 0.25;
								radius *= constants.DefenseDefenderSpawnDistanceMultiplier;
							}
							else
							{
								// Top left
								placementAngle += Math.PI * 1.25;
								placementAngle += order * 0.01;
							}
						}
						else
						{
							// Team deathmatch
							placementAngle += order / (double)factionOrder.Count * Math.PI * 2;
						}

						factionSpawns[faction] = new Vector2d(
							state.WorldSize.X * (Math.Sin(placementAngle) * radius * 0.5 + 0.5),
							state.WorldSize.Y * (Math.Cos(placementAngle) * radius * 0.5 + 0.5)
							);
					}
				}

				state = state with
				{
					FactionSpawns = ImmutableArray.Create(factionSpawns),
				};

				// Spawn ships
				for(int i = 0; i < mannedShips.Count; i++)
				{
					var ship = mannedShips[i];

					Vector2d pos = Vector2d.Zero;

					if(IsGameTeamBased(state.GameType))
					{
						pos = GetSpawnPos(state, constants, ship.FactionId, factionSpawned[ship.FactionId]);
						factionSpawned[ship.FactionId]++;
					}
					else
					{
						pos = GetSpawnPos(state, constants, ship.FactionId, i, mannedShips.Count);
					}

					// Spawn ship entity, un-dead its players
					ShipEnt ent = ShipEnt.Create(
						constants: constants,
						shipConsts: gameType == GameType.Race ? constants.RaceShip : constants.Ship,
						entityId: giveEntId++,
						shipId: ship.Id,
						factionId: ship.FactionId,
						position: pos,
						onemanned: ship.Roles.Length == 1
						);
					
					ents.Add(ent.Id, ent);

					builder.Add(ship.Id, new ShipStateData()
					{
						EntityId = ent.Id,
					});

					statsBuilder.Add(ship.Id, new GameStatsShip());
				}

				state = state with
				{
					MapShipIdToShipState = builder.ToImmutable(),
					GiveEntityId = giveEntId,
					MapEntityIdToEntity = state.MapEntityIdToEntity.AddRange(ents),
					Stats = new GameStats()
					{
						MapShipIdToKillCount = statsBuilder.ToImmutable(),
					},
				};
			}

			// Spawn stations
			if(gameType == GameType.Defense)
			{
				// Ensure we have enough ship ids
				while(state.CachedStationShipIds.Length < constants.DefenseStationCount)
				{
					state = state with
					{
						GiveShipId = state.GiveShipId + 1,
						CachedStationShipIds = state.CachedStationShipIds.Add(state.GiveShipId),
						MapShipIdToShipDesc = state.MapShipIdToShipDesc.Add(state.GiveShipId, new ShipPropertyDescription()),
					};
				}

				int giveEntityId = state.GiveEntityId;

				var ents = new Dictionary<int, Entity>();

				// TODO: random seed?
				Random r = new Random(128);

				for(int i = 0; i < constants.DefenseStationCount; i++)
				{
					// Get position in -1..1
					Vector2d pos = Vector2d.Zero;
					pos += Vector2d.One * constants.DefenseStationPlacementBias; // Offset towards 1,1
					pos += new Vector2d(-1, 1) * (i / (double)(constants.DefenseStationCount - 1) * 2 - 1) *
						constants.DefenseStationPlacementDistance * (1.0 - Math.Abs(constants.DefenseStationPlacementBias));

					// Convert to coordinates
					pos = (pos * 0.5 + new Vector2d(0.5)) * state.WorldSize;

					var shipid = state.CachedStationShipIds[i];

					var ent = ShipEnt.Create(
						constants: constants,
						shipConsts: constants.Station,
						entityId: giveEntityId++,
						shipId: shipid,
						factionId: state.DefendingFactionId,
						position: pos,
						onemanned: true
						);

					state = state with
					{
						MapShipIdToShipState = state.MapShipIdToShipState.SetItem(shipid, new ShipStateData()
						{
							EntityId = ent.Id,
						}),
						MapShipIdToShipDesc = state.MapShipIdToShipDesc.SetItem(shipid, new ShipPropertyDescription()
						{
							FactionId = state.DefendingFactionId,
							Id = shipid,
							isVisible = false,
							Name = $"S{i+1}",
							onlyAcceptsPrivileged = true,
							Roles = ImmutableArray.Create(0),
						}),
						Stats = state.Stats with
						{
							MapShipIdToKillCount = state.Stats.MapShipIdToKillCount.SetItem(shipid, new GameStatsShip()),
						}
					};

					ents.Add(ent.Id, ent);
				}

				state = state with
				{
					GiveEntityId = giveEntityId,
					MapEntityIdToEntity = state.MapEntityIdToEntity.AddRange(ents),
					StationEntityIds = ImmutableArray.Create<int>(ents.Keys.ToArray()),
				};
			}

			if(gameType == GameType.Race)
			{
				state = SpawnEntities(state, new RaceFinishEnt()
				{
					DetectionRange = constants.RaceFinishRadius,
					IsProjectile = true,
					Mass = 10e10,
					Radius = 4.00,
					Velocity = Vector2d.Zero,
					Name = constants.Strings.EntNameRaceFinish,
					Position = new Vector2d(state.WorldSize.X, state.WorldSize.Y) - new Vector2d(1, 1).Normalized() * constants.RaceFinishRadius,
				});
			}

			state = AsteroidSpawner.SpawnAsteroids(state, constants, gameType == GameType.Race ? 1.7 : 1.0);

			return state;
		}

		void FinishGame()
		{
			_recorder.StopRecording();

			string message = GetEndGameMessage(_mainState, _gameConstants);

			_logger.LogInformation(message);

			// Clear state
			_mainState = _mainState with
			{
				MapEntityIdToEntity = _mainState.MapEntityIdToEntity.Clear(),
				MapShipIdToShipState = _mainState.MapShipIdToShipState.Clear(),
				IsGameRunning = false,
				Stats = new GameStats(),
				StationEntityIds = ImmutableArray<int>.Empty,
			};

			var now = DateTimeOffset.UtcNow;

			var split = message.Split('\n');
			for(int i = split.Length - 1; i >= 0; i--) // TODO: tohle je pras�rna
			{
				var line = split[i];

				// Notify all players
				foreach (var player in _mainState.PlayerManagementData.MapUserIdToPlayer.Values)
				{
					SendToUser(player.UserId, CreateResponse(_mainState, _gameConstants, player.UserId, chat: new ChatMessage[]
					{
						new ChatMessage()
						{
							message = line,
							timestamp = now,
							type = ChatMessageType.EventGlobal,
						},
					}));
				}
			}

			SendUpdateShipAssignment();
		}

		static Vector2d GetSpawnPos(GameState state, GameConstants constants, int faction, int index, int deathMatchTotalShips = 1)
		{
			if(state.GameType == GameType.Race)
			{
				return new Vector2d(2 + index * 4, 2 + faction * 4);
			}

			if (IsGameTeamBased(state.GameType) && faction >= 0 && faction < state.FactionSpawns.Length)
			{
				double shipAngleStep = constants.SpawnBaseShipSpacing;

				// Ensure that distance between two ships will be at least the value of minspacing
				// Only used when spawning teams
				double circumference = 2 * Math.PI * (constants.WorldSizeXY);
				const double minspacing = 4;
				shipAngleStep = Math.Max(shipAngleStep, (minspacing / circumference) * Math.PI * 2);

				Vector2d spawn = state.FactionSpawns[faction];
				var worldsize = new Vector2d(state.WorldSize.X, state.WorldSize.Y);

				double angle = shipAngleStep * index;

				double s = Math.Sin(angle);
				double c = Math.Cos(angle);

				// Translate so that world center is (0,0)
				spawn = spawn - worldsize * 0.5;
				// Rotate
				spawn = new Matrix2d( c, s,
									-s, c) * spawn;
				// Translate back
				spawn = spawn + worldsize * 0.5;

				return spawn;
			}
			else
			{
				// Offset angle based on current game time to "randomize" spawn locations
				double angle = state.CurrentGameElapsed * 0.5;
				// Offset angle based on ship index (for initial spawns)
				angle += ((index + 0.5) / (double)deathMatchTotalShips) * 2.0 * Math.PI;

				return new Vector2d(
					state.WorldSize.X * (Math.Sin(angle) * constants.SpawnRingRadiusScale * 0.5 + 0.5),
					state.WorldSize.Y * (Math.Cos(angle) * constants.SpawnRingRadiusScale * 0.5 + 0.5)
					);
			}
		}

		/// <summary>
		/// Returns game state with the provided entities ready for simulation
		/// </summary>
		public static GameState SpawnEntities(GameState state, params Entity[] entities)
		{
			List<KeyValuePair<int, Entity>> pairs = new List<KeyValuePair<int, Entity>>();

			var entid = state.GiveEntityId;

			foreach (var e in entities)
			{
				int id = entid++;
				pairs.Add(new KeyValuePair<int, Entity>(id, e with
				{
					Id = id,
				}));
			}

			return state with
			{
				MapEntityIdToEntity = state.MapEntityIdToEntity.AddRange(pairs),
				GiveEntityId = entid,
			};
		}

		public static string GetFriendlyEnemyYours(GameState state, GameConstants constants, int? askingShipId, int ownerId, int thisFactionId)
		{
			int factionId = -1;
			if (askingShipId.HasValue)
			{
				factionId = state.MapShipIdToShipDesc[askingShipId.Value].FactionId;
			}

			int ownerShipId = -56445;

			if(state.MapEntityIdToEntity.ContainsKey(ownerId) && state.MapEntityIdToEntity[ownerId] is ShipEnt)
			{
				var ship = (ShipEnt)state.MapEntityIdToEntity[ownerId];
				ownerShipId = ship.ShipId;
			}

			string desc = "";

			if (askingShipId == ownerShipId)
			{
				desc += constants.Strings.Yours;
			}
			else
			{
				if (Simulation.IsGameTeamBased(state.GameType) && factionId == thisFactionId)
				{
					desc += constants.Strings.Allied;
				}
				else
				{
					desc += constants.Strings.Enemys;
				}
			}

			return desc;
		}

		static MainGameState OptimizeRadars(GameConstants constants, MainGameState main, ShipEnt ent)
		{
			if(!constants.RadarCompaction)
				return main;

			if (main.systems == null)
				return main;

			Dictionary<int, (int, RadarSignature)> uniqueSigs = new Dictionary<int, (int, RadarSignature)>();

			void addSigs(SystemStateRadar? r, int o, bool forceDescriptions = false)
			{
				if (r == null || r.signatures == null)
					return;

				foreach(var s in r.signatures)
				{
					if (s == null)
						continue;

					if (s.id == ent.Id)
						continue;

					if(uniqueSigs.ContainsKey(s.id))
					{
						if(uniqueSigs[s.id].Item1 < o)
						{
							uniqueSigs[s.id] = (o, s);
						}
						else
						{
							if(forceDescriptions && !string.IsNullOrEmpty(s.description))
							{
								uniqueSigs[s.id] = (uniqueSigs[s.id].Item1, uniqueSigs[s.id].Item2 with
								{
									description = s.description,
								});
							}
						}
					}
					else
					{
						uniqueSigs.Add(s.id, (o, s));
					}
				}
			}

			RadarSignature[] getSigs(int order)
			{
				List<RadarSignature> sigs = new List<RadarSignature>();

				foreach((var id, (var o, var s)) in uniqueSigs)
				{
					if(o == order)
					{
						sigs.Add(s);
					}
				}

				return sigs.ToArray();
			}

			addSigs(main.systems.sensorRadar, 1);
			addSigs(main.systems.sensorInfrared, 2);
			addSigs(main.systems.sensorProbe, 3);
			addSigs(main.systems.sensorCameras, 4);

			if(main.systems.sensorHud != null)
			{
				addSigs(new SystemStateRadar()
				{
					signatures = main.systems.sensorHud, // Hacky hacky
				}, 0, forceDescriptions: true); // Force higher priority signatures to inherit this description
				
				main = main with
				{
					systems = main.systems with
					{
						sensorHud = getSigs(0),
					},
				};
			}

			if (main.systems.sensorRadar != null)
			{
				main = main with
				{
					systems = main.systems with
					{
						sensorRadar = main.systems.sensorRadar with
						{
							signatures = getSigs(1),
						},
					},
				};
			}

			if (main.systems.sensorInfrared != null)
			{
				main = main with
				{
					systems = main.systems with
					{
						sensorInfrared = main.systems.sensorInfrared with
						{
							signatures = getSigs(2),
						},
					},
				};
			}

			if (main.systems.sensorProbe != null)
			{
				main = main with
				{
					systems = main.systems with
					{
						sensorProbe = main.systems.sensorProbe with
						{
							signatures = getSigs(3),
						},
					},
				};
			}

			if (main.systems.sensorCameras != null)
			{
				main = main with
				{
					systems = main.systems with
					{
						sensorCameras = main.systems.sensorCameras with
						{
							signatures = getSigs(4),
						},
					},
				};
			}

			return main;
		}
	}
}
