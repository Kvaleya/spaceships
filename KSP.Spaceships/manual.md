---
title: 'BVL: Bitva Vesmírných Lodí'
---

<h1 style="text-align: center">[Ke hře!](/game.html)</h1>

---

V této hře budete ve skupinách po **třech lidech** ovládat vesmírnou loď a soupeřit
s ostatními.
Každý z vaší skupiny bude moci interagovat s jinými **systémy**, pro efektivní
řízení bude tedy nutná dobrá komunikace.

Po **spuštění zápasu** (až po přihlašování do lodí) uvidíte v horní části rozhraní odkaz na **videokonfenci přes Jitsi Meet**. Všichni hráči na dané lodi mají odkaz stejný, použijte jej pro komunikaci s vaší posádkou!

Jedna hra potrvá přibližně 15 minut, budeme hrát vícekrát, dokud nás to bude bavit.

# Cíle a game mody

Cíl hry se liší podle game modu:

- *Deathmatch* - Všichni proti všem. Která loď zničí do uplynutí časového limitu nejvíce ostatních, vyhrává.
- *Team Deathmatch* - Souboj dvou týmů, kde každý se skládá z více lodí. Který tým zničí do časového limitu více lodí nepřítele, vyhrává.
- *Defense* - Soupeří dva týmy: **obránci** a **útočníci**. Obránci brání vesmírné stanice, útočníci je musí do uplynutí časového limitu zničit nebo aspoň co nejvíce poškodit.

# Jak hrát

Ke hře vám stačí webový prohlížeč (doporučujeme na PC, telefony a tablety jsou příliš malé) a výše zmíněný komunikační kanál. Potřebujete **myš** - skutečnou, živou, počítačovou myš. Na jiných věcech to neuklikáte.

Po připojení si prosíme **nastavte jméno** v levém horním rohu stránky.
Nastavte ho tak, aby vás poznali ostatní učastníci a orgové, pomůže to
s organizací hry.

## Přiřazovací fáze

Na začátku je **přiřazovací fáze**, kdy si vyberete na **jaké lodi** a v **jaké roli** budete hrát.
Role se liší tím, jaké systémy budete mít na dané lodi k dispozici.

Do konkrétní role na konkrétní lodi se přihlášíte **kliknutím na danou buňku v tabulce lodí**.

Také máte k dispozici také in-game **chat**. Vaše zprávy v tuto chvíli uvidí všichni.

## Herní fáze

Po zahájení hry se vám zobrazí ovládání lodi a herní mapa.
Konkrétní ovládání a fungování lodi bude podrobně rozebráno v sekci **Systémy**.

Hra se odehrává na 2D ploše omezené na obdélník. Všechny objekty ve hře,
včetně vaší lodi, mají tvar kruhu.

Kromě vaší lodi se ve hře nacházejí také **asteroidy**, ostatní lodě, střely,
miny, sondy a stanice. S velkými asteroidy nejde pohybovat, s ostatními objekty ano.

Co všechno uvidíte na herní mapě závisí na tom, k jakým sensorům máte přístup.
Některé věci ale uvidí na dané lodi všichni, konkrétně pozice spřátelených
objektů, jako jsou vaše střely, další lodě ve vašem týmu a jejich střely.

Dále můžete na mapě vidět **pointery**. **Pointer** vytvoříte na konkrétní pozici
ve světě kliknutím do mapy **pravým tlačítkem myši**, bude se zobrazovat jako "X"
a uvidí ho všichni členové vaší lodi. Používejte je ke koordinaci.

S mapou jde **posouvat** (kliknutím a táhnutím kurzoru) a také jde **zoomovat** (kolečkem myši).

Také máte k dispozici in-game **chat**. Budou se v něm zobrazovat různé herní události,
ať už se týkají jen vaší lodi nebo všech.

Zprávy od hráčů v chatu uvidí v módu Deathmatch **všichni hráči**, v týmových módech
jen **členové vašeho týmu**. Používejte týmový chat pro koordinaci v rámci týmu.

Simulace hry samotné probíhá v diskrétních krocích, a to přibližně 3x za sekundu.

Vaše loď má omezené **zdraví**. Pokud klesne na nulu, umíráte. Instantně se ovšem
respawnujete na jiném místě ve vesmíru, a po respawnu jste omezenou dobu
(řádově sekundy) nesmrtelní.

Výchozí zdraví vaší lodi je **1**, hodnoty poškození různých zbraní jsou
malé desetinné čísla.

# Systémy

Vaše loď má k dispozici reaktor, který vytváří nějaké množství **energie**.
Energii přiřazujete **systémům**. Zvláštně se chovají jen motory, které mají
automaticky přiřazenou všechnu jinak nevyužitou energii z reaktoru.

Změna přiřazení energie k systému se projeví pro ostatní členy lodi okamžitě.

## Sensory

Sensorů má vaše loď několik druhů, konkrétně:

- **Kamery** - Mají malý dosah, ale poskytují detailní informace o objektech. Má k nim přístup jen **Navigátor**.
- **Radar středního dosahu** - Vidí jen obrysy objektů. Mají k němu přístup **Navigátor** a **Zbrojní důstojník**.
- **Radar dalekého dosahu** - Funguje stejně, jen má větší dosah. Má k němu přístup jen **Operátor radaru**.

Tyto sensory nejdou nijak ovládat a ke svému chodu nepotřebují žádnou energii.

Dále se ve hře nacházejí **sondy**. Loď může mít v jeden okamžik nejvýše jednu aktivní sondu.
Sonda se chová stejně jako **kamery**, ale má větší dosah.
Data ze sondy jsou přístupná všech rolím na lodi.

## Zbraně

Zbraňových systémů je několik. Mají společné to, že ke svému použití se musí nejprve **nabít**.
Nabití trvá nějakou dobu a pro nabíjení musí být daný systém napájen. Pokud napájení ztratí,
**instantně se vybije**.

Energie přidělená zbraňovému systému má vliv především na rychlost jeho nabíjení.

Zbraně jdou ovládat také klávesovými zkratkami. Jsou ekvivalentní tlačíku "odjistit" či "zaměřit".

Zbraní je několik druhů:

- **Laser** - Instatní paprsek. Rychlé nabíjení, malý damage. Hotkey **1**.
- **Plasma** - Vystřelí rojnici mnoha rychlých projektilů, každý z nich má malý damage, dohromany ale napáchají paseku. Hotkey **2**.
- **Rakety** - Vypustí z lodi raketu. Rakera se dá dálkově ovládát. Raketa detonuje v extrémní blízkosti nepřítele (a to i neaktivní raketa), případně ji lze detonovat ručně. Hotkey **3**.
- **Remote control** - Ovládání vypuštěné rakety. Kam kliknete, tam letí. Hotkey **4**, pro detonaci funguje klávesa **Q**.
- **Miny** - Vypustí z lodi minu. Mina automaticky exploduje v blízkosti nepřátelského objektu. Hotkey **5**.
- **Sondy** - Vypustí z lodi sondu. Její chování bylo popsáno výše. Sonda nemá žádné bojové schopnosti. Hotkey **6**.

Raket, sond a min lze za hru vypustit neomezené množství. Vypuštění nové rakety existující raketu deaktivuje. To samé platí pro sondy. Naopak vypuštění nové miny nemá na existující miny žádný vliv.

Rakety, miny a ostatní exploze mají nějaký vnitřní rádius, řádově 5 a 10 násobek poloměru lodi, ve kterém dávají plný damage, poté klesá damage lineárně se vzdáleností, od cca 20 násobku poloměru lodi je damage nulový.

## Štíty

Loď disponuje štítem, který dokáže absorbovat příchozí damage. Štít se regeneruje
rychlostí, která závisí na jeho přiřazené energii.

Štít je jediná kruhová výseč, kterou lze namířit nějakým směrem. Příchozí damage
z daného směru absorbuje. Štít má pevně danou úhlovou velikost.

## Neviditelnost

Pokud je tento systém nabit, umožňuje vás se na nějakou dobu zneviditelnit.
Během neviditelnosti se nebudete zobrazovat na nepřátelských sensorech,
ale také **nebudete moct střílet ze zbraní**.

Neviditelnost lze též aktivovat **mezerníkem**.

## Auto repair

Pomalu opravuje vaši loď rychlostí závislou na přiřazené energii.

## Motory

Umožňují vaší lodi pohyb.

V ovládání motorů nastavujete cílovou rychlost vaší lodi. Jak rychle této rychlosti
dosáhnete závisí na dostupné energii pro motory (automaticky využívají všechnu energii, která není využitá jinak).
Dostupná energie tedy ovlivňuje vaše maximální zrychlení.

Též lze zvýšit cílovou rychlost klávesou **W**, snížit klávesou **S**, posunout směr letu proti směru hodin pomocí **A**, po směru pomocí **D**.

# Rozdělení systémů mezi role:

- Navigátor:
	- Kamery
	- Radar (střední)
	- Štíty
	- Motory
- Zbrojní důstojník:
	- Radar (střední)
	- Laser
	- Plasma
	- Miny
	- Sondy (vypouštění)
- Operátor radaru:
	- Infrared
	- Rakety
	- Neviditelnost
	- Auto repair
