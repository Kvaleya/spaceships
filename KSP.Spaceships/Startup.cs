using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Globalization;
using Prometheus;
using Microsoft.AspNetCore.Http;

namespace KSP.Spaceships
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();
			services.AddSignalR();
			services.AddSingleton<Recorder>();
			services.AddSingleton<Replayer>();
			services.AddSingleton<GameConstants>();
			services.AddSingleton<ConnectionManager>();
			services.AddSingleton<Simulation>();
			services.AddSingleton<MainLoop>();
			services.AddHostedService<TickService>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
			CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			// No HTTPS redirects, that's a job of a reverse proxy. Not this app
			// app.UseHttpsRedirection();

			app.UseRouting();

			app.UseAuthorization();
			app.UseDefaultFiles();  // serve /index.html at /
			app.UseStaticFiles();
			var webSocketOptions = new WebSocketOptions
			{
				KeepAliveInterval = TimeSpan.FromMinutes(2),
			};
			app.UseWebSockets(webSocketOptions);
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapHub<CommonHub>("/commonHub");
				endpoints.MapMetrics();
			});
		}
	}
}
