#!/bin/bash

# build frontend
cd frontend
npm install
npm run build
cd ..

# build container
podman build -t registry.havran.vsq.cz/spaceships .

# upload container
podman login registry.havran.vsq.cz
podman push registry.havran.vsq.cz/spaceships