using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Mathematics;

namespace KSP.Spaceships
{
	static class Extensions
	{
		public static double Sanitized(this ref double d)
		{
			if(double.IsNaN(d) || double.IsInfinity(d))
			{
				return 0;
			}
			return d;
		}

		// https://github.com/exyi/coberec/blob/83f4a744af8cc9ec2c3e24d86d25840c41617ed2/src/Coberec.CSharpGenHelpers/FunctionalExtensions.cs#L92
		public static ImmutableArray<U> EagerSelect<T, U>(this ImmutableArray<T> arr, Func<T, U> fn)
		{
			if (arr.IsEmpty) return ImmutableArray<U>.Empty;

			var builder = ImmutableArray.CreateBuilder<U>(initialCapacity: arr.Length);
			for (int i = 0; i < arr.Length; i++)
			{
				builder.Add(fn(arr[i]));
			}
			return builder.MoveToImmutable();
		}

		public static ImmutableDictionary<K,V> EagerSelect<K,V,U>(this ImmutableDictionary<K,U> dict, Func<U, V> fn)
		{
			if (dict.IsEmpty)
			{
				return ImmutableDictionary<K, V>.Empty;
			}

			var builder = ImmutableDictionary.CreateBuilder<K, V>();
			foreach((K key, U val) in dict)
			{
				builder.Add(key, fn(val));
			}
			return builder.ToImmutable();
		}

		public static ImmutableArray<T> EagerWhere<T>(this ImmutableArray<T> arr, Func<T, bool> predicate)
		{
			if (arr.IsEmpty) return ImmutableArray<T>.Empty;

			var builder = ImmutableArray.CreateBuilder<T>();
			for (int i = 0; i < arr.Length; i++)
			{
				if(predicate(arr[i]))
				{
					builder.Add(arr[i]);
				}
			}
			return builder.ToImmutable();
		}

		public static Vec2 ToVec2(this Vector2d v)
		{
			return new Vec2()
			{
				x = v.X,
				y = v.Y,
			};
		}

		public static bool IsInfOrNaN(this double d)
		{
			return double.IsInfinity(d) || double.IsNaN(d);
		}
	}
}
