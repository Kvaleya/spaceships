﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.Json;
using Microsoft.AspNetCore.SignalR;
using OpenTK.Mathematics;
using System.Diagnostics;
using Prometheus;
using System.Diagnostics.Eventing.Reader;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Text;
using System.IO;

namespace KSP.Spaceships
{
	public enum WebsocketDataPreference
	{
		Json,
		GzipJson,
		GzipJsonHybrid,
		ProtobufHybrid,
	}

	class WebsocketWrapper
	{
		WebSocket _ws;
		SemaphoreSlim _mutex;
		ILogger _logger;
		public readonly WebsocketDataPreference DataPreference;

		public WebsocketWrapper(WebSocket ws, ILogger logger, WebsocketDataPreference dataPreference)
		{
			_ws = ws;
			_mutex = new SemaphoreSlim(1, 1);
			_logger = logger;
			DataPreference = dataPreference;
		}

		public async Task<bool> SendAsync(byte[] data, WebSocketMessageType type)
		{
			await _mutex.WaitAsync();
			try
			{
				await _ws.SendAsync(new ArraySegment<byte>(data, 0, data.Length), type, true, CancellationToken.None);
			}
			catch(Exception e)
			{
				if(e is System.Net.WebSockets.WebSocketException && _ws.State != WebSocketState.Open)
				{
					_logger.LogInformation("Websocket closed.");
					return false;
				}

				_logger.LogError(e, "WS: Websocket send failed");
				return false;
			}
			finally
			{
				_mutex.Release();
			}
			return true;
		}
	}

	public class ConnectionManager
	{
		public const string ClientReceiveMethod = "OnServerResponse";

		private readonly IHubContext<CommonHub> _hubContext;
		private readonly ILogger<ConnectionManager> _logger;
		private MainLoop _loop;

		private readonly ConcurrentDictionary<string, WebsocketWrapper> _webSockets = new();
		private int _nextWsId = 0;

		public ConnectionManager(ILogger<ConnectionManager> logger, IHubContext<CommonHub> hubContext)
		{
			_logger = logger;
			_hubContext = hubContext;
		}

		public void SetMainLoop(MainLoop loop)
		{
			_loop = loop;
		}

		private static async Task<ArraySegment<byte>> ReceiveEntireMessage(WebSocket webSocket, CancellationToken ct)
		{
			var buffer = new byte[16384];
			var index = 0;
			var done = false;
			while (!done)
			{
				if (index >= buffer.Length)
				{
					var newBuffer = new byte[buffer.Length * 2];
					Array.Copy(buffer, newBuffer, buffer.Length);
					buffer = newBuffer;
				}
				var result = await webSocket.ReceiveAsync(buffer.AsMemory(startIndex: index), ct);
				index += result.Count;
				done = result.EndOfMessage;
			}
			return new ArraySegment<byte>(buffer, 0, index);
		}

		public async Task OnWebsocketCreate(WebSocket webSocket, WebsocketDataPreference dataPreference)
		{
			int id = Interlocked.Increment(ref _nextWsId);
			string connectionId = $"WEBSOCKET{id}_{dataPreference}";
			var wrapped = new WebsocketWrapper(webSocket, _logger, dataPreference);
			_webSockets.AddOrUpdate(connectionId, wrapped, (k, v) => wrapped);

			while (true)
			{
				if (webSocket.State != WebSocketState.Open)
				{
					_logger.LogInformation($"WS: Websocket closed (state={webSocket.State})");
					break;
				}

				ArraySegment<byte> bytes = null;

				try
				{
					bytes = await ReceiveEntireMessage(webSocket, CancellationToken.None);
				}
				catch(Exception e)
				{
					if (e is System.Net.WebSockets.WebSocketException && webSocket.State != WebSocketState.Open)
					{
						_logger.LogInformation("Websocket closed.");
						break;
					}
					_logger.LogError(e, "WS: Websocket receive failed");
					break;
				}

				ClientRequest data = null;

				if (dataPreference == WebsocketDataPreference.Json ||
					dataPreference == WebsocketDataPreference.GzipJsonHybrid ||
					dataPreference == WebsocketDataPreference.ProtobufHybrid)
				{
					data = ParseRequestJson(bytes);
				}

				if (dataPreference == WebsocketDataPreference.GzipJson)
				{
					data = ParseRequestJsonGzipped(bytes);
				}

				if (data != null)
				{
					ScheduleClientRequest(data, connectionId);
				}

				if (webSocket.CloseStatus.HasValue)
				{
					_logger.LogInformation($"WS: Websocket closed (closeStatus={webSocket.CloseStatus})");
					break;
				}
			}

			OnDisconnect(connectionId);
		}

		ClientRequest? ParseRequestJson(ArraySegment<byte> bytes)
		{
			var textPayload = Encoding.UTF8.GetString(bytes);

			if (string.IsNullOrEmpty(textPayload))
				return null;

			try
			{
				return JsonSerializer.Deserialize<ClientRequest>(textPayload);
			}
			catch (Exception e)
			{
				_logger.LogError(e, "WS: Receive json parse failed");
				return null;
			}
		}
		
		ClientRequest? ParseRequestJsonGzipped(ArraySegment<byte> bytes)
		{
			using(var ms = new MemoryStream(bytes.ToArray()))
			using(var decompressed = new MemoryStream())
			using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Decompress))
			{
				gzip.CopyTo(decompressed);
				return ParseRequestJsonGzipped(decompressed.ToArray());
			}
		}

		void ScheduleClientRequest(ClientRequest request, string connectionId)
		{
			var timer = CustomMetrics.RequestSchedulingDelay.NewTimer();
			_loop.Enqueue(new GameQueueItem()
			{
				ClientRequestEvent = new ClientRequestEvent()
				{
					ClientRequest = request,
					ConnectionId = connectionId,
				},
				OnCompleted = () => timer.ObserveDuration()
			});
		}

		void OnDisconnect(string connectionId)
		{
			_loop.Enqueue(new GameQueueItem()
			{
				DisconnectionEvent = new DisconnectedEvent()
				{
					ConnectionId = connectionId,
				}
			});
			_webSockets.TryRemove(connectionId, out _);
		}

		async Task SendWebsocketAsync(WebsocketWrapper ws, string connectionId, byte[] data, WebSocketMessageType type)
		{
			try
			{
				var result = await ws.SendAsync(data, type);

				if(!result)
				{
					OnDisconnect(connectionId);
				}
			}
			catch (WebSocketException e)
			{
				OnDisconnect(connectionId);
			}
		}

		public void Send(string connectionId, ServerResponse payload, bool allowWebsockets)
		{
			if (_webSockets.TryGetValue(connectionId, out var ws))
			{
				if(!allowWebsockets)
				{
					return;
				}

				if(ws.DataPreference == WebsocketDataPreference.Json)
				{
					SendWebsocketJson(connectionId, payload);
				}
				if (ws.DataPreference == WebsocketDataPreference.GzipJson || ws.DataPreference == WebsocketDataPreference.GzipJsonHybrid)
				{
					SendWebsocketJsonGzipped(connectionId, payload);
				}
				if(ws.DataPreference == WebsocketDataPreference.ProtobufHybrid)
				{
					SendWebsocketProtobuf(connectionId, payload);
				}
			}
			else
			{
				SendSignalRProtobuf(connectionId, payload);
			}
		}

		void SendWebsocketJson(string connectionId, ServerResponse payload)
		{
			var serializedBytes = JsonSerializer.SerializeToUtf8Bytes(payload);

			if (_webSockets.TryGetValue(connectionId, out var ws))
			{
				SendWebsocketAsync(ws, connectionId, serializedBytes, WebSocketMessageType.Text).ContinueWith((t) =>
				{
					OnDisconnect(connectionId); // unreachable?
				}, TaskContinuationOptions.OnlyOnFaulted);
			}
		}

		void SendWebsocketJsonGzipped(string connectionId, ServerResponse payload)
		{
			if (_webSockets.TryGetValue(connectionId, out var ws))
			{
				var serializedBytes = JsonSerializer.SerializeToUtf8Bytes(payload);

				using (var ms = new System.IO.MemoryStream())
				{
					using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress))
					{
						gzip.Write(serializedBytes, 0, serializedBytes.Length);
					}
					var data = ms.ToArray();

					SendWebsocketAsync(ws, connectionId, data, WebSocketMessageType.Binary).ContinueWith((t) =>
					{
						OnDisconnect(connectionId); // unreachable?
					}, TaskContinuationOptions.OnlyOnFaulted);
				}
			}
		}

		void SendWebsocketProtobuf(string connectionId, ServerResponse payload)
		{
			if (_webSockets.TryGetValue(connectionId, out var ws))
			{
				using (var ms = new System.IO.MemoryStream())
				{
					using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
					{
						ProtoBuf.Serializer.Serialize(gzip, payload);
					}

					SendWebsocketAsync(ws, connectionId, ms.ToArray(), WebSocketMessageType.Binary).ContinueWith((t) =>
					{
						OnDisconnect(connectionId); // unreachable?
					}, TaskContinuationOptions.OnlyOnFaulted);
				}
			}
		}

		void SendSignalRProtobuf(string connectionId, ServerResponse payload)
		{
			using (var ms = new System.IO.MemoryStream())
			{
				using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress, true))
				{
					ProtoBuf.Serializer.Serialize(gzip, payload);
				}

				_hubContext.Clients.Client(connectionId).SendAsync(ClientReceiveMethod, ms.ToArray());
			}
		}

		void SendSignalRGzippedJson(string connectionId, ServerResponse payload)
		{
			var serializedBytes = JsonSerializer.SerializeToUtf8Bytes(payload);

			var ms = new System.IO.MemoryStream();
			using (var gzip = new System.IO.Compression.GZipStream(ms, System.IO.Compression.CompressionMode.Compress))
			{
				gzip.Write(serializedBytes, 0, serializedBytes.Length);
			}
			var data = ms.ToArray();
			_hubContext.Clients.Client(connectionId).SendAsync(ClientReceiveMethod, data).ContinueWith((t) =>
			{
				_logger.LogError(t.Exception, "SignalR send failed");
			}, TaskContinuationOptions.OnlyOnFaulted);
		}
	}
}
