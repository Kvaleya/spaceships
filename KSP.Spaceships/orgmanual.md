# Orgomanuál (Nešířit - TOP SNEAKY!!!)

## Admin práva - chcete je!

- ve hře jsou příkazy, zadávají se do chatu, začínají lomítkem `/`
- admin práva získáte pomocí `/privilege lanius` nebo `/p lanius`

## Speciální orgolodě

- pokud máte admin práva, můžete se nalodit na jednu z připravených orgolodí
- orgolodě mají jedinou roli, ve které ovládáte celou loď sami
- také mají všechny speciální frakci *Kyborgové*
- jde pak dělat souboj orgové vs. účastníci
- orgolodě mají **speciální štíty** - jsou kruhové, tedy absorbují poškození ze všech stran a nemusíte je nijak ovládat (kromě přiřazování energie)

## Konkrétní příkazy

- `/stopgame` - zastaví hru
- `/setdefending factionId` - nastaví obránce pro *Defense* gamemode
- `/suicide` - zabije vás. Nepoužívat, protože to zasahuje do hry (vaše loď vybuchne a zraní ostatní).
- `/spawnship name factionId roleCount privilegedOnly` - vytvoří novou loď, roleCount může být jen 1 nebo 3, privilegedOnly je optional, eg. `/spawnship BidnaUcastnikolod 0 3 0`
- `/setshipfaction shipName newFactionId` - nastaví tým lodi
- `/setshipname oldName newName` - nastaví jméno lodi
- `/kickship shipName` - vykopne posádku dané lodi ven z lodi. Funguje i během běžící hry, ale nejde to nijak revertovat. Posádka se poté může stále přihlásit do nějaké lodi, i do té stejné (ledaže běží hra, pak musí čekat).
- `/replay fileName` - spustí spectator replay starší hry. Jen pro účely ladění, při hře nepoužívat.
- `/stopreplay` - vypne právě přehrávaný replay.
- `/gameconst name [newValue]`, `/gc ... ...` - nastaví herní konstantu na novou hodnotu, nebo vrátí její aktuální hodnotu, pokud newValue chybí. Většina konstant se projeví až po spuštění nové hry. Zamýšleno jen pro game-design related ladění. Pro konstanty viz všechny `bool`/`int`/`double` property v `Data/GameConstants.cs`.
- `/orgsay Oslavujte Hroší Bohy!` - pošle všem hráčům důležitou zprávu, kterou nelze ignorovat
- `/control jmenoLodiNeboStanice` - budete ovládat stanici/loď, ale funguje jen na stanice a na orgolodě
- `/kickoffline` - kickne všechny offline lidi z lodí
- `/shadowban playerId [0]` - posere někomu hru, pokud je na konci 0, tak je to unban

*Pozn: příkazy co žerou jméno lodi fungují i s nejmenším unikátním prefixem jména a nejsou case sensitive.*
