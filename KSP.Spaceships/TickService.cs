using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.SignalR;
using System.Diagnostics;
using Prometheus;

namespace KSP.Spaceships
{
	// See https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-5.0&tabs=visual-studio#timed-background-tasks

	/// <summary>
	/// Generates game tick events for the simulation
	/// </summary>
	public class TickService : IHostedService, IDisposable
	{
		private readonly ILogger<TickService> _logger;
		private readonly GameConstants _constants;
		private readonly MainLoop _loop;
		private readonly Stopwatch _clock = new Stopwatch();
		private TimeSpan _lastTick = TimeSpan.Zero;
		bool _running = true;

		public TickService(ILogger<TickService> logger, GameConstants constants, MainLoop loop)
		{
			_logger = logger;
			_constants = constants;
			_loop = loop;
		}

		public Task StartAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Tick Service running.");

			_clock.Start();

			ScheduleNextTick();

			return Task.CompletedTask;
		}

		private async void ScheduleNextTick()
		{
			if (!_running)
				return;

			await Task.Yield();

			var nextTick = _lastTick + TimeSpan.FromSeconds(_constants.TickIntervalSeconds);
			var waitTime = nextTick - _clock.Elapsed;
			// _logger.LogInformation("Tick Wait time is {t}", waitTime);
			
			if (waitTime > TimeSpan.Zero)
				await Task.Delay(waitTime);
			
			if (waitTime < TimeSpan.Zero)
			{
				// we are running late. Just adjust the timing
				_lastTick = _clock.Elapsed;
			}
			else
			{
				// running according to schedule. For more precise timing use the expected time, not the current time
				_lastTick = nextTick;
			}

			var timer = CustomMetrics.TickSchedulingDelay.NewTimer();
			_loop.Enqueue(new GameQueueItem()
			{
				TickEvent = new GameTickEvent(),
				OnCompleted = () => {
					timer.ObserveDuration();
					ScheduleNextTick();
				}
			});
		}

		public Task StopAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Tick Service is stopping.");

			_running = false;

			// Kill main loop
			_loop.Enqueue(new GameQueueItem()
			{
				ExitMainLoopEvent = new ExitMainLoopEvent(),
			});

			return Task.CompletedTask;
		}

		public void Dispose()
		{
			_running = false;
		}
	}
}
