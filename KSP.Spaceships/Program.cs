using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Serilog.Events;
using Prometheus;
using Prometheus.DotNetRuntime;
using ProtoBuf;

namespace KSP.Spaceships
{
	[ProtoContract(Name = "DateTime")]
	public class DateTimeOffsetSurrogate
	{
		[ProtoMember(1)]
		public double UnixMilliseconds { get; set; }

		public static implicit operator DateTimeOffsetSurrogate(DateTimeOffset value)
		{
			return new DateTimeOffsetSurrogate { UnixMilliseconds = value.ToUnixTimeMilliseconds() };
		}

		public static implicit operator DateTimeOffset(DateTimeOffsetSurrogate value)
		{
			return DateTimeOffset.FromUnixTimeMilliseconds((long)value.UnixMilliseconds);
		}
	}
	public class Program
	{
		public static void Main(string[] args)
		{
            ProtoBuf.Meta.RuntimeTypeModel.Default.Add(typeof(DateTimeOffset), false).SetSurrogate(typeof(DateTimeOffsetSurrogate));

			var protoSchema = ProtoBuf.Serializer.GetProto(new ProtoBuf.Meta.SchemaGenerationOptions {
				Types = { typeof(ServerResponse), typeof(ClientRequest) },
				Syntax = ProtoBuf.Meta.ProtoSyntax.Proto3,
				Package = "",
			 });
			System.IO.File.WriteAllText("ClientInterface/server_comm.proto", protoSchema);

			var prometheusServer = new MetricServer(hostname: "localhost", port: 1234);
			DotNetRuntimeStatsBuilder
				.Customize()
				.WithContentionStats(CaptureLevel.Informational)
				.WithJitStats(CaptureLevel.Informational)
				.WithThreadPoolStats(CaptureLevel.Informational)
				.WithGcStats(CaptureLevel.Verbose)
				.WithExceptionStats(CaptureLevel.Errors)
				.StartCollecting();
			prometheusServer.Start();


			Log.Logger = new LoggerConfiguration()
				.MinimumLevel.Override("Microsoft", LogEventLevel.Information)
				.Enrich.FromLogContext()
				.WriteTo.File("../log.txt", rollingInterval: RollingInterval.Day)
				.WriteTo.Console()
				.CreateLogger();
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseSerilog();
					webBuilder.UseStartup<Startup>();
					webBuilder.UseWebRoot("frontend/public");
				});
		}
}
