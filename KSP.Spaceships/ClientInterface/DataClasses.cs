#nullable enable

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KSP.Spaceships
{
	/// <summary>
	/// Universal message from client to server
	/// Note: only one of the following may be not null at the same time:
	/// - chatMessage
	/// - nameChange
	/// - shipAssignment
	/// - shipControl
	/// - startGame
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ClientRequest
	{
		/// <summary>
		/// Incremented for every message, to ensure proper ordering.
		/// TODO: unused. Remove?
		/// </summary>
		public int messageOrder { get; init; } = -1;

		/// <summary>
		/// Identifies the user, needed in every message that does something.
		/// Upon first connection, the client has no userid and shall leave this field null.
		/// The server will respond with a message that contains a new userid the client will use from now on.
		/// </summary>
		public string userId { get; init; } = "";

		/// <summary>
		/// Will be sent to all members of the ship's faction (if the gamemode is team based).
		/// May be null.
		/// </summary>
		public string? chatMessage { get; init; }

		/// <summary>
		/// Used when the client wishes to change their name.
		/// May be null.
		/// </summary>
		public string? nameChange { get; init; }

		/// <summary>
		/// When the user wants to jump ship.
		/// May be null.
		/// </summary>
		public RequestShipAssignment? shipAssignment { get; init; }

		/// <summary>
		/// When the user wants to control their ship and systems.
		/// May be null.
		/// </summary>
		public RequestShipControl? shipControl { get; init; }

		/// <summary>
		/// Only privileged players may initiate the game. Other players will not even have this option in the UI.
		/// </summary>
		public GameType startGame { get; init; } = GameType.Inactive;

		/// <summary>
		/// When true, the game will be aborted. Only the privileged may do so.
		/// </summary>
		public bool? abortGame { get; init; }

		/// <summary>
		/// When not null, edits a ship. Only the privileged may do so.
		/// </summary>
		public RequestShipEdit? shipEdit { get; init; } = null;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RequestShipEdit
	{
		public int targetShipId { get; init; }

		/// <summary>
		/// When not null, sets ships faction id
		/// </summary>
		public int? factionId { get; init; }

		/// <summary>
		/// When not null, sets ships name
		/// </summary>
		public string? name { get; init; }

		/// <summary>
		/// When not null, sets whethter the ship is only accessible to the privileged.
		/// </summary>
		public bool? privilegedOnly { get; init; }
	}

	public enum GameType
	{
		Inactive,
		Deathmatch,
		TeamDeathmatch,
		Defense,
		Race,
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RequestShipAssignment
	{
		/// <summary>
		/// Can be null to clear the assignment
		/// </summary>
		public int? targetShipId { get; init; }
		public int? targetRoleId { get; init; }

		/// <summary>
		/// When not null, a one-manned ship of this name will be created and the user will be assigned to it, unless the player is already in such a ship.
		/// </summary>
		public string? requestedShipName { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RequestShipControl
	{
		/// <summary>
		/// When not null, the player clicked into the map to create a pointer other players can see.
		/// Absolute world coordinates.
		/// </summary>
		public Vec2? pointer { get; init; }

		/// <summary>
		/// May be null.
		/// </summary>
		public RequestPowerState? powerState { get; init; }

		/// <summary>
		/// May be null for requests that only create a pointer / chat message / power command
		/// </summary>
		public RequestSystemState? systemState { get; init; }
	}

	/// <summary>
	/// Always fill in the entire desired state. Every system the player controls must be present in every request.
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RequestPowerState
	{
		// When not null, power will change for the given system (instantly visible, takes gameplay effect the next tick)
		public SystemPowerCommand? weaponLaser { get; init; }
		public SystemPowerCommand? weaponPlasma { get; init; }
		public SystemPowerCommand? weaponMissiles { get; init; }
		public SystemPowerCommand? weaponMines { get; init; }
		public SystemPowerCommand? weaponProbes { get; init; }

		public SystemPowerCommand? shields { get; init; }
		public SystemPowerCommand? stealth { get; init; }
		public SystemPowerCommand? autoRepair { get; init; }
		// engines automatically use any unused power
	}

	/// <summary>
	/// Always fill in the entire desired state.
	/// SystemWeaponFireCommand may be null even for systems the player controls, but the rest should always be filled in for every system the player controls.
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RequestSystemState
	{
		/// <summary>
		/// When not null, the weapon will try to fire.
		/// </summary>
		public SystemWeaponFireCommand? weaponLaser { get; init; } = null;
		public SystemWeaponFireCommand? weaponPlasma { get; init; } = null;
		public SystemWeaponFireCommand? weaponMissiles { get; init; } = null;
		public SystemWeaponFireCommand? weaponMines { get; init; } = null;
		public SystemWeaponFireCommand? weaponProbes { get; init; } = null;

		public SystemRemoteControlCommand? remoteMissile { get; init; } = null;

		public SystemShieldCommand? shields { get; init; } = null;
		public bool? stealthActivate { get; init; } = null;
		public SystemEnginesCommand? engines { get; init; } = null;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/// <summary>
	/// Universal message from server to client
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ServerResponse
	{
		///// <summary>
		///// Incremented for every message, to ensure proper ordering.
		///// </summary>
		//public int messageOrder { get; init; } = -1;

		public ChatMessage? broadcast { get; init; } = null;

		/// <summary>
		/// Info about the current player and game.
		/// Always present.
		/// </summary>
		public PlayerAndGameState playerGameState { get; init; } = new PlayerAndGameState();

		/// <summary>
		/// Spoils the entire game state. Only the dead and the privileged may view.
		/// </summary>
		public SpectatorMapState? spectatorMap { get; init; }

		/// <summary>
		/// Describes the ships in the game, what players are in game, and who is what role on what ship.
		/// </summary>
		public ResponseShipAssignmentState? shipAssignmentState { get; init; }

		/// <summary>
		/// All actual game/ship stuff is here.
		/// Sent when the player is in control of a ship.
		/// </summary>
		public MainGameState? mainGameState { get; init; }

		/// <summary>
		/// New chat messages since last update
		/// </summary>
		public ChatMessage[] incomingChat { get; init; } = { };

		/// <summary>
		/// Always let the players know when they are at the edge of the map.
		/// </summary>
		public Vec2 worldSize { get; init; } = new Vec2();

		/// <summary>
		/// Which faction id is the defenders in Defense game mode.
		/// </summary>
		public int defendingFaction { get; init; } = 0;
	}

	public enum ChatMessageType
	{
		ChatGeneral, // Messages everyone can see
		ChatTeam,    // Team-only chat
		EventGlobal, // Messages describing global events, such as game start or ship deaths
		EventShip,   // Ship-local messages, generic
		EventDamageTaken,   // Ship-local messages, damage taken
		EventDamageDealt,   // Ship-local messages, damage dealt
		EventTeamDamageDealt, // Note that we do not reveal if taken damage was team damage
		EventSelfDamageTaken, // Stop killing yourself!
		Server,      // Server wants to tell the player something
		Broadcast,
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ChatMessage
	{
		/// <summary>
		/// When the server received the message.
		/// </summary>
		public DateTimeOffset timestamp { get; init; }

		public string message { get; init; } = "";

		public ChatMessageType type { get; init; } = ChatMessageType.ChatGeneral;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record PositionPointer
	{
		public Vec2 position { get; init; } = new Vec2();
		public int creatorRoleId { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record PlayerAndGameState
	{
		/// <summary>
		/// If the client has invalid userid, the server generates a new one automatically.
		/// Always present.
		/// </summary>
		public string playerUserId { get; init; } = "";

		/// <summary>
		/// Name of the current player.
		/// </summary>
		public string playerName { get; init; } = "";

		/// <summary>
		/// Numerical identificator of the current player, used to link them with ship assignments etc.
		/// Always present.
		/// </summary>
		public int playerId { get; init; }

		/// <summary>
		/// Whether current player is privileged. Only controls if player sees privileged-only UI options, actual privileged rights will be checked server side.
		/// Always present.
		/// </summary>
		public bool playerIsPrivileged { get; init; } = false;

		/// <summary>
		/// Helper for determining game state (pregame hub / controlling a ship / dead and spectating)
		/// </summary>
		public bool isPlayerDead { get; init; } = false;

		/// <summary>
		/// When null, game is not active. When not null, describes current game mode.
		/// </summary>
		public GameType gameType { get; init; } = GameType.Inactive;

		/// <summary>
		/// How often a game tick happens, in real time seconds.
		/// Game time delta per tick is computed as (tickIntervalSeconds * timeScale).
		/// </summary>
		public double tickIntervalSeconds { get; init; }

		/// <summary>
		/// Scale of game time.
		/// </summary>
		public double timeScale { get; init; }

		public string[] roleNames { get; init; } = { };
		public string[] factionNames { get; init; } = { };
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SpectatorMapState
	{
		/// <summary>
		/// May be empty.
		/// May not be null.
		/// </summary>
		public RadarSignature[] objects { get; init; } = { };

		// TODO?
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ResponseShipAssignmentState
	{
		public PlayerDescription[] players { get; init; } = { };
		public ShipDescription[] ships { get; init; } = { };

		// TODO: remove this when it gets unused
		/// <summary>
		/// Map of role id -> role displayed name
		/// </summary>
		public string[] roleNames { get; init; } = { };
		public string[] factionNames { get; init; } = { };
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record PlayerDescription
	{
		public string name { get; init; } = "";
		public int id { get; init; }
		public int shipId { get; init; }
		public int roleId { get; init; }
		public bool isConnected { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ShipDescription
	{
		public string name { get; init; } = "";
		public int id { get; init; }
		public int[] roles { get; init; } = { };
		public int factionid { get; init; }

		/// <summary>
		/// Privileged only ships may use different roles.
		/// </summary>
		public bool onlyAcceptsPrivileged { get; init; } = false;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record Vec2
	{
		/// <summary>
		/// X is right
		/// </summary>
		public double x
		{
			get
			{
				return _x;
			}
			init
			{
				_x = value.Sanitized();
			}
		}
		private double _x;

		/// <summary>
		/// Y is down
		/// </summary>
		public double y
		{
			get
			{
				return _y;
			}
			init
			{
				_y = value.Sanitized();
			}
		}
		private double _y;

		public OpenTK.Mathematics.Vector2d ToVector2d()
		{
			return new OpenTK.Mathematics.Vector2d(x, y);
		}
	}

	/// <summary>
	/// Used as a parameter given to an entity that generates a signature.
	/// Entity may wish to generate different signature for radar, infrared, etc.
	/// </summary>
	public enum RadarSignaturePurpose
	{
		Spectator,
		Camera,
		Infrared,
		Radar,
	}

	public enum SpectateObjectType
	{
		Unknown,
		Asteroid,
		Ship,
		ShipInvisible,
		Station,
		Mine,
		Probe,
		Missile,
		Plasma,
		Effect,
		RaceFinish,
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record RadarSignature
	{
		/// <summary>
		/// Where it is in absolute world coordinates.
		/// </summary>
		public Vec2 position { get; init; } = new Vec2();

		/// <summary>
		/// How fast is it moving, in unit per real time second.
		/// </summary>
		public Vec2 velocity { get; init; } = new Vec2();

		/// <summary>
		/// How large it appears.
		/// </summary>
		public double radius { get; init; }

		/// <summary>
		/// Unique signature identifier.
		/// Currently only used by spectator map to map markers to objects.
		/// </summary>
		public int id { get; init; } = -1;

		/// <summary>
		/// How hot the object is for infrared. Number in range 0 to 1.
		/// Note: an entity may have zero radius, but nonzero intensity!
		/// Use case for this would be an explosion - high heat intensity, but no radar radius.
		/// </summary>
		public double intensity { get; init; } = 0.0;

		/// <summary>
		/// Optional description for the object. Used for HUD and telescope.
		/// </summary>
		public string description { get; init; } = string.Empty;

		/// <summary>
		/// True for HUD elements that should be displayed even when out of range.
		/// Maybe snap them to the edge of displayed range?
		/// </summary>
		public bool isVisibleAnywhere { get; init; } = false;

		public SpectateObjectType spectatorType { get; init; } = SpectateObjectType.Unknown;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record MainGameState
	{
		/// <summary>
		/// List of all visible pointers
		/// </summary>
		public PositionPointer[] pointers { get; init; } = { };

		/// <summary>
		/// General ship state. Everyone on ship gets this information.
		/// </summary>
		public ShipState shipState { get; init; } = new ShipState();

		/// <summary>
		/// Updated between ticks as players use power.
		/// Always present.
		/// </summary>
		public ShipReactorPowerState powerState { get; init; } = new ShipReactorPowerState();

		/// <summary>
		/// Always present.
		/// </summary>
		public SystemState systems { get; init; } = new SystemState();

		/// <summary>
		/// How much real time elapsed since game start.
		/// </summary>
		public double timeElapsed { get; init; }

		/// <summary>
		/// How long will the game take at most.
		/// </summary>
		public double timeMax { get; init; }

		public int deaths { get; init; }

		public Vec2 raceFinishPosition { get; init; } = new Vec2() { x = -1000, y = -1000 };
		public double raceFinishRadius { get; init; }

		/// <summary>
		/// How often a simulation tick happens, in real time. Value of 0.1 corresponds to 10 Hz simulation tickrate.
		/// </summary>
		public double tickIntervalSeconds { get; init; }

		/// <summary>
		/// How is time delta scaled in the simulation.
		/// Time delta is computed as <see cref="tickIntervalSeconds"/> multiplied by this value.
		/// So a TickIntervalSeconds of 0.1 and TimeScale of 2 would result in
		/// the game simulating 1 ticks per real time seconds,
		/// and each tick using a time delta of 0.2 seconds of game time.
		/// The game would appear to run at twice the normal speed.
		/// </summary>
		public double timeScale { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ShipState
	{
		public double shipRadius { get; init; }

		/// <summary>
		/// What is the ship's name
		/// </summary>
		public ShipDescription description { get; init; } = new ShipDescription();

		/// <summary>
		/// Who is on the ship
		/// </summary>
		public PlayerDescription[] players { get; init; } = { };

		/// <summary>
		/// Zero or negative = dead
		/// </summary>
		public double healthCurrent { get; init; }

		/// <summary>
		/// Maximum ship health
		/// </summary>
		public double healthMax { get; init; }

		/// <summary>
		/// Position in absolute world coordinates.
		/// Included for convenience.
		/// </summary>
		public Vec2 position { get; init; } = new Vec2();

		/// <summary>
		/// How fast is it moving, in unit per real time second.
		/// </summary>
		public Vec2 velocity { get; init; } = new Vec2();

		/// <summary>
		/// True when ship activates stealth (FTL-style)
		/// </summary>
		public bool isInvisible { get; init; } = false;

		public Vec2 targetVelocity { get; init; } = new Vec2();
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record ShipReactorPowerState
	{
		/// <summary>
		/// How much power the ship generates in total.
		/// </summary>
		public int powerTotal { get; init; }

		/// <summary>
		/// Any available power is automatically used by the engines, but can be rerouted to other system instantly.
		/// </summary>
		public int powerAvailable { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemState
	{
		public SystemStateRadar? sensorCameras { get; init; }
		public SystemStateRadar? sensorInfrared { get; init; }
		public SystemStateRadar? sensorRadar { get; init; }

		/// <summary>
		/// To be displayed separately from other sensors.
		/// </summary>
		public SystemStateRadar? sensorProbe { get; init; }

		/// <summary>
		/// List of hud-highlighted things. Can fill the role of IFF. May be empty.
		/// Signatures here are a subset of what is send to spectators.
		/// Signatures sent here may also be sent inside a SystemStateRadar. These will share the same id, but will have different properties.
		/// In that case, display the HUD signature, since its properties will be a superset of whatever sensors may contain.
		/// May not be null.
		/// </summary>
		public RadarSignature[] sensorHud { get; init; } = { };

		public SystemGenericChargingState? weaponLaser { get; init; }
		public SystemGenericChargingState? weaponPlasma { get; init; }
		public SystemGenericChargingState? weaponMissiles { get; init; }
		public SystemGenericChargingState? weaponMines { get; init; }
		public SystemGenericChargingState? weaponProbes { get; init; }

		public SystemRemoteState? remoteMissile { get; init; }

		public SystemShieldState? shields { get; init; }
		public SystemStealthState? stealth { get; init; }
		public SystemEnginesState? engines { get; init; }
		public SystemAutoRepairState? autoRepair { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemPowerState
	{
		/// <summary>
		/// Current power state the system is in. Not a state index.
		/// </summary>
		public int currentPowerState { get; init; }

		/// <summary>
		/// What amouns of power the system may consume. For example:
		/// Laser: 0 or 4
		/// Autorepair: 0, 1, 2, 3, 4, 5, 6
		/// Engines: {}
		/// Active stealth: 4
		/// Inactive stealth: 0, 4
		/// </summary>
		public int[] powerStates { get; init; } = { };

		/// <summary>
		/// Sometimes a system may do something for a period of time, during which it cannot be turned off.
		/// For example cloaking gets turned on by assigning it power, and cannot be turned off until the timer runs out and the ship decloaks.
		/// </summary>
		public bool allowPowerUsageChange { get; init; } = true;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemPowerCommand
	{
		/// <summary>
		/// Desired system power amount. Not a power state index!
		/// </summary>
		public int targetPower { get; init; }
	}

	/// <summary>
	/// Shared for radar and infrared
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemStateRadar
	{
		/// <summary>
		/// Where is the center of the radar's detection range.
		/// Will be different from ship's position for probes.
		/// </summary>
		public Vec2 origin { get; init; } = new Vec2();

		/// <summary>
		/// How fast is it moving, in unit per real time second.
		/// </summary>
		public Vec2 velocity { get; init; } = new Vec2();

		/// <summary>
		/// What the radar sees.
		/// </summary>
		public RadarSignature[] signatures { get; init; } = { };

		/// <summary>
		/// Effective range of the system
		/// </summary>
		public double range { get; init; }
	}

	/// <summary>
	/// Generic FTL-style weapon state. When powered, it charges up. When charged, it can fire. Firing resets charge.
	/// Charge resets when not powered.
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemGenericChargingState
	{
		public SystemPowerState powerState { get; init; } = new SystemPowerState();

		/// <summary>
		/// How charged up the system is, in range 0..1 (1 = 100% charge, ready)
		/// </summary>
		public double chargeState { get; init; }

		/// <summary>
		/// How much charge per second (0.5 means two second chargeup).
		/// Would be nice to display this value to players, so they can have some intuition about how long stuff takes.
		/// </summary>
		public double chargeSpeed { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemRemoteState
	{
		public bool active { get; init; } = false;
		public Vec2 currentTarget { get; init; } = new Vec2();
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemStealthState
	{
		public SystemPowerState powerState { get; init; } = new SystemPowerState();

		/// <summary>
		/// How charged up the system is, in range 0..1 (1 = 100% charge, ready)
		/// </summary>
		public double chargeState { get; init; }

		/// <summary>
		/// How much charge per second (0.5 means two second chargeup).
		/// Would be nice to display this value to players, so they can have some intuition about how long stuff takes.
		/// </summary>
		public double chargeSpeed { get; init; }

		/// <summary>
		/// How many seconds of stealth remain at this moment.
		/// </summary>
		public double currentStealthRemaining { get; init; }

		/// <summary>
		/// How long the ship can remain in stealth state.
		/// </summary>
		public double stealthDuration { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemWeaponFireCommand
	{
		/// <summary>
		/// Where the weapon is to fire, in absolute world coordinates.
		/// Special case for missiles and probes: the missile/probe will fly towards this position.
		/// A missile/probe is fired is the system is charged and there is no preexisting missile and probe.
		/// </summary>
		public Vec2 targetPosition { get; init; } = new Vec2();
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemRemoteControlCommand
	{
		/// <summary>
		/// Where the probe will fly
		/// </summary>
		public Vec2? targetPosition { get; init; } = new Vec2();
		public bool? selfDestruct { get; init; } = null;
	}

	/// <summary>
	/// FTL-like shield. Made up of a single shield arc. The arc can have shield layers. Layers get destroyed by weapons.
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemShieldState
	{
		public SystemPowerState powerState { get; init; } = new SystemPowerState();

		/// <summary>
		/// Direction of the shield arc. (1, 0) is right, (0, 1) is down.
		/// This vector will always be normalized.
		/// </summary>
		public Vec2 arcDirection { get; init; } = new Vec2() { x = 1, y = 0 };

		/// <summary>
		/// How much damage the shield may absorb at most, at the current power level.
		/// </summary>
		public double maxAbsorption { get; init; }

		/// <summary>
		/// How much damage can be absorped by the shield at this time.
		/// </summary>
		public double currentAbsorption { get; init; }

		/// <summary>
		/// How much absorption is regenerated per real time second.
		/// </summary>
		public double shieldRegenerationSpeed { get; init; }

		/// <summary>
		/// How large the single arc is, in radians.
		/// The arc extends to half this size in both directions.
		/// A size of pi means a half-ring shield arc.
		/// </summary>
		public double shieldArcSize { get; init; }
	}

	/// <summary>
	/// Sets the shield arc direction.
	/// </summary>
	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemShieldCommand
	{
		/// <summary>
		/// Desired shield arc direction.
		/// The vector can have any length, it will be normalized server-side.
		/// </summary>
		public Vec2 direction { get; init; } = new Vec2() { x = 1, y = 0 };
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemEnginesState
	{
		/// <summary>
		/// Engines automatically use any available power that is not used by other systems.
		/// Thus they have no valid power states and do not allow power assignment.
		/// This is here only so the player knows how much power the engines had in the last tick.
		/// </summary>
		public SystemPowerState powerState { get; init; } = new SystemPowerState();

		public Vec2 currentVelocity { get; init; } = new Vec2();

		/// <summary>
		/// Engines will not accelerate beyond this velocity.
		/// </summary>
		public double maxVelocity { get; init; }

		/// <summary>
		/// Depends on how much power engines can use.
		/// </summary>
		public double maxAcceleration { get; init; }
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemEnginesCommand
	{
		/// <summary>
		/// Desired velocity, not acceleration. Should make ship control easier.
		/// </summary>
		public Vec2 desiredVelocity { get; init; } = new Vec2();

		/// <summary>
		/// When true, ship will accelerate towards target velocity, when false, ship will not try to modify its velocity at all.
		/// </summary>
		public bool enabled { get; init; } = true;
	}

	[ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
	public record SystemAutoRepairState
	{
		public SystemPowerState powerState { get; init; } = new SystemPowerState();

		/// <summary>
		/// How much health is repaired per second.
		/// </summary>
		public double repairRate { get; init; }
	}
}
