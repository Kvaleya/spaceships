
using System;
using System.Collections.Generic;
using System.Linq;
using Prometheus;
using Prometheus.DotNetRuntime;

namespace KSP.Spaceships
{
	public static class CustomMetrics
	{
		public static readonly Counter RequestCount =
            Metrics.CreateCounter("spaceships_request_count", "Number of requests the simulation is handling");
		public static readonly Gauge QueueLength =
            Metrics.CreateGauge("spaceships_queue_length", "Length of the main loop queue");

		public static readonly Histogram SimulationTime =
			Metrics.CreateHistogram("spaceships_simulation_time", "");
		public static readonly Histogram AfterTickTime =
			Metrics.CreateHistogram("spaceships_aftertick_time", "");
		public static readonly Histogram MessageSerializationTime =
			Metrics.CreateHistogram("spaceships_msg_serialization_time", "");
		public static readonly Histogram RecorderSaveTime =
			Metrics.CreateHistogram("spaceships_recorder_save_time", "");
		public static readonly Histogram TickSchedulingDelay =
			Metrics.CreateHistogram("spaceships_tick_scheduling_delay", "");
		public static readonly Histogram RequestSchedulingDelay =
			Metrics.CreateHistogram("spaceships_request_scheduling_delay", "");

		public static readonly Gauge Game_Players =
			Metrics.CreateGauge("spaceships_game_players", "");
		public static readonly Gauge Game_Entities =
			Metrics.CreateGauge("spaceships_game_entities", "");

		public static readonly Counter Game_Explosions =
			Metrics.CreateCounter("spaceships_game_exposions", "");

    }
}
