*Toto je "design doc" z doby prvotní implementace hry, vylovený z hlubin hackmd. Nemusí reflektovat finální stav hry.*

# Týmové FTL

Distanční Space Alert / týmové kooperační+versus FTL

Tady nahoře je nový zápis z reálně vznikající hry, dole je starý dump.

## Vesmír

- omezená 2D plocha
- obsahuje lodě
- obsahuje asteroidy (překážky)
    - velké i malé (řádově velikosti lodi ;-)

## Loď

- loď může být součástí většího týmu lodí - *faction*
- jedna loď je jeden subtým
- ovládají ji tři lidé
- každý z nich plní na lodi jednu *roli*
    - "motory"
    - "štíty"
    - "zbraně"
- všechny role musí být obsazené
- má omezené zdraví

## Gamemode

- deathmatch
    - všichni proti všem
    - respawnují se lidi?
    - časový limit?
    - last man standing?
- team deathmatch
    - jako předchozí, ale na dva až tři týmy
- assault
    - jeden tým brání vesmírnou stanici, druhý ji chce zničit
- PvE
    - zničte AI lodě
- účastníci vs orgové
    - každý org ovládá sám jednu loď, takže orgové mají efektivní koordinaci mezi loděmi

## Systémy

- senzory
    - kamery
        - vidí kolize jako radar
        - navíc má popisky objektů, konkrétně ví, jestli je něco loď, a jaké má %hp pokud ano
    - IR
        - vidí teplo - motory, rakety, plasmové střely atd.
        - vidí i kolize jako radar
        - střední dosah
    - long range radar
        - vidí kolize - lodě, asteroidy, rakety...
        - velký dosah
    - sondy (data)
        - jako kamery
        - je vystřelena, pak se jen pasivně odráží
    - IFF
        - zvýrazňuje spřátelené lodě
        - je třeba zařadik k jinému systému (long-range radar?)
- zbraně
    - laser
        - jde pálit libovolným směrem
        - instantní, dělá viditelnou tepelnou stopu
        - každá vrstva štítu lodi zmenší jeho sílu na polovinu
            - včetně štítu lodi, co laserem střílí
    - rakety
        - vlastník kliká na místo, kam má letět
        - ignorují štíty
        - žere málo energie na odpálení
        - taky dědí rychlost lodi
        - bouchne v blízkosti nepřítele (mají IFF, zabíjí i nepřátelské sondy)
    - plasma
        - rychlá, ale ne moc
        - umí střílet libovolným směrem
        - vystřelí rojnici
        - zničí vrstvu štítu
            - opět včetně vlastního
    - miny
        - straší na místě
        - mají jen radar signature
        - bouchnou v blízkosti nepřítele (mají IFF, zabíjí i nepřátelské sondy)
        - při vypuštění mají rychlost lodi
- různé
    - *reaktor (pasivní)*
        - dělá konstantní množství energie za tick
        - dá se rozdělovat energie různým rolím
    - motory
        - pohyb lodi
        - žerou málo energie
        - automaticky žerou nevyužitou energii
    - štíty
        - shield arcs
        - 
    - auto repair
        - opravuje loď (pomalu)
    - sondy (vypuštění)
        - umožňuje vystřelit sondu určitým směrem
    - neviditelnost
        - dlouhý cooldown? nebo chargeup?
        - při neviditelnosti loď není na žádném senzoru (kromě spektátora)
        - netriggeruje miny

## Tech stuff

- entita
    - je něco v prostoru
- collision timeline:
    - 0. každá entita má pozici a rychlost (příští pozice = aktuální pozice + rychlost)
    - 1. entity se vloží do akcelerační struktury
    - 2. akcelerační strukura detekuje kolize
    - 3. vyřeší se kolize `Entity.OnCollision`
    - 4. entity se přesunou na nové místo (odražení se řeší automaticky a *nerekurzivně*)
        - když se jedna věc odrazí od druhé, ale pak by se v rámci ticku měla odrazit ještě od něčeho dalšího, tak přesně to se nestane
    - 5. entity se simulují
    - 6. entity co umřely se odstraní
    - Body 1..5 se dělají v `SolveCollisions`
    - stav se rozešle všem hráčům (každému zvlášť, jen ten relevantní)
        - a spektátorům
- druhy entit
    - asteroidy
        - jen fyzika
    - laser
        - jen kosmetická věc
        - ale dá dmg všemu co protne jeho paprsek?
    - plasma
        - reaguje na kolize
        - dává dmg
        - fyzika, ale speciální
    - raketa
        - fyzika, ale speciální
        - vlastní logika pohybu
        - dává dmg
    - mina
        - je jen divná raketa
    - sonda
        - fyzika
        - zničitelná
    - lodě
        - složité...
- timeline simulace:
    - 0. tick
        - máme immutable stav od hráčů
        - máme immutable stav předchozí simulace
    - 1. fyzika
        - 1.1 simuluje se "odrazová" fyzika
            - vygenerují se reálné trajektorie entit
            - ukládají se kolizní eventy
        - 1.2 simuluje se "projektilová" fyzika
            - co nekolidovalo v předchozím kroku koliduje teď
            - ukládají se kolizní eventy
    - 2. simulace
        - 2.1 simulují se obecné entity
            - zpracují kolizní eventy
            - generují dmg eventy
        - 2.2 simulují se lodě
            - zpracují kolizní eventy
            - updatují systémy
                - štíty se updatují dřív než zbraně střílí
            - naspawnují entity
            - vygenerují damage eventy
        - 2.3 započítají se damage eventy
            - umře co umřít má
    - 3. zkompiluje nový stav simulace
        - odešleme všem
    - 4. přijímáme změny hráč stavů dokud není tick
- server data
    - mapa connectionid -> userid
    - mapa userid -> playerid
    - mapa userid -> stav systémů
    - mapa userid -> stav energie
    - popis stavu hry


## Problémy

- jak rozdělit systémy mezi role?
    - playtest
- komunikace lidí v rámci faction?
    - vlastní chat? ano
- consumables: rakety a sondy a miny?
    - whatever
- nastavení všech konstant ve hře (ale až bude hra implementovaná)
    - ano
- technické blbiny
    - používat rychlosti za tick nebo za sekundu?
        - nastavitelná doba mezi ticky?
        - nastavitelný delta-time za tick?
        - sub-ticky pro fyziku
        - nastavuju rychlost za sekundu
    - write-locking když 30 lidí náráz kliká?
        - detekovat deadlocky automaticky?
    - signalr nezaručuje pořadí zpráv
        - řešení: role vždy posílá celé nastavení svých systému s timestampem, pak se přinejhorším někdy projeví trochu starší stav místo mutanta

## Rozdělení systémů na lidi

- seznam všeho: (bold jsou nutné celou hru)
    - input:
        * IR
        * radar
        * IFF + teleskop
        * sonda (data)
    - output zbraň:
        * laser
        * rakety
        * plasma
    - output jiné
        * miny
        * **motory**
        * štíty
        - auto repair
        * sondy (vypuštění)
        * neviditelnost
- constrainty:
    - rakety nemají IR
    - laser nemá ani IR, ani radar
    - štíty nemají laser
    - štíty nemají plasmu
    - každý má nějaký input a nějaký output
    - každý má něco zajímavějšího než charge-up akci
    - každý má co dělat v libovolný okamžik hry
- role:
    - 0:
        - in: IR
        - out: plasma
        - out: miny
        - out: stealth
        - out: auto repair
    - 1:
        - in: radar
        - out: štíty
        - out: rakety
        - out: sonda (vypuštění)
    - 2: (motory jsou třeba pořád)
        - in: sonda (data)
        - in: IFF a teleskop
        - out: laser *nejefektivnější zbraně jsou slepé*
        - out: motory

## Poznámky

- privileged
    - hráč může nebo nemusí být privileged
    - privileged budou vždy jen orgové
    - hráč se stane privileged nějakým zadáním hesla nebo tak něco
    - privileged hráči mohou dělat orgověci
        - koukat neomezeně na mapu celé hry
        - mít k dispozici orgolodě, co jdou ovládat jediným hráčem
- log událostí pro hráče
    - aby věděli, že jim štít nezmizel sám od sebe, ale že to byl zásah

## Do budoucna

- ovládatelné sondy a miny a rakety
- ale jen s line of sight
- ale dají se dropovat repeatery
- dá se budovat "komunikační síť"

## TODO

- práva (pro orgy)
- kickování lidí z lodí (pokud jsou afk a čeká se na ně)
- spectatovat mohou jen mrtvoly a orgové
- manuál

-------------------------------------------------------------------------------

# Starý zápis

## Inspirace

- KSPí Space Alert (dále SA)
    - do jisté míry Space Team, byť naše implementace je zajímavější
- FTL: Faster Than Light
    - toto je ta hlavní inspirace
    - roguelike, kde se prozkoumává galaxie a bojuje s dalšími loďmi
    - taky je hodně o náhodných událostech a sbírání zdrojů, to ale není relevantní pro tuto hru
    - už existuje multiplayerový FTL clone: https://spektor.itch.io/tachyon
- Los Piratos (ŠMFí lodičky)
    - https://github.com/jagotu/LosPiratos
    - pravidla: https://github.com/jagotu/LosPiratos/blob/master/pravidla%20-%20Los%20Piratos%20de%20la%20Casa.pdf
- méně relevantní hry co dělají něco velmi zhruba podobného:
    - Lovers In A Dangerous Spacetime
    - Overcooked (sort of)
    - Keep Talking And Nobody Explodes
- TODO: Art of Game Design:
    - přečíst kapitolu "Game Mechanics Must be in Balance"

## Co a proč

- komunikační hra
- realtime simulace vesmírných lodí
    - různí lidé ovládají různé systémy lodi
    - lodě bojují proti sobě nebo proti botům
- aspoň nějaký kontakt s účastníky (a účastníků navzájem) mimo sous
- vymyslet a nakódit zábavnou hru
    - implementace tohoto je dobrá záminka pro interakci orgů navzájem
- **recyklace**
    - na sous
        - jako čistě digitální hra, ale i to má něco do sebe
        - třeba celotáborovka ve stylu lodiček ze ZŠMF?
        - nebo jednovečerní velká hra
            - která bude nakóděná dopředu muhehe
        - imo to má docela potenciál
    - hraní a modifikace do budoucna
    - využití mimo KSP?
        - MASO?
        - ŠMF?

## Netechnické featury

- není nekonečně složité na implementaci ani hraní
    - to první se stejně nepovede...
- vyžaduje komunikaci v rámci jedné lodi
    - ideálně rovnoměrně mezi lidmi/systémy/místnostmi
- jedna hra trvá rozumně dlouho - ale jak dlouho? 15 min? 30?
    - dá se hrát víc her, třeba s obměnou týmů a stanovišť
- hra škáluje s nepředvídatelným počtem hráčů (10 až 100)
    - více či méně systémů
    - více či méně instancí jednoho systémů
    - vícě či méně instancí lodí
- recyklace
    - nemá smysl něco takového dělat kvůli jednomu večeru
- playtestovat. vše. prosím.

## Technické featury

- jde oživit pokud spadne server (s krátkýma hrama možná není nutné?)
- jde oživit pokud spadne klient - hodně nutné
- rozhraní funguje a je použitelné na:
    - počítači s myší
    - počítači s touchpadem
    - telefonech
- pamatuje si kompletní historii hry?
    - později jde lépe analyzovat playtesty, dívat se na systémy z POV konkrétních lidí
    - jak něco takového implementovat?
    - jak se to má rádo s potenciálním rozšířením hry?
        - persistentní svět?
        - sbírání zdrojů, ničení asteroidů atd?
- hra jde ladit za běhu (měnit hodnoty zdraví/dmg a tak)
- asi by šlo použít Box2D na fyziku (nebo vlastní pokud stačí jednoduchá)

## Popis hry

- originální SA:
    - vesmírná loď má systémy a oddělené místnosti
    - ovládání je na jednom místě
    - grafy a feedback daného systému na jiném
    - manuál jak ho používat ještě na jiném
    - různá místa spolu musí komunikovat
- distanční adaptace
    - vše je webové
    - jedna místnost je jeden počítač/browser (klidně to může být víc lidí v místnosti, whatever)
    - komunikace přes discord, asi text i voicechat
- **žádný obskurní manuál**
    - náplň SA bylo naučit se loď ovládat
    - to ale není scalable na více hraní jedné hry
    - každé kolo by totiž potřebovalo nový obsah
- zajímavější herní cíl/kontext než základní SA
    - cílem není jen přežít (resp. je to těžší)
- komunikaci vynucují hlavně externí situace
    - PvP dvou či více učastníkolodí proti sobě
    - PvE jedné či více učastníkolodí proti org/bot lodím
- několik možných variant velikosti lodi:
    - složitá loď pro velký tým (i 10+)
        - můžou být i bullshit systémy
        - relativní anonymita v týmu je výhodou, viz další varianta
    - jednoduchá loď pro střední tým (~5 lidí)
        - jen ty důležité systémy
        - jeden hráč jich třeba může ovládat víc
        - lidé asi budou mít tendenci tvořit týmy kamarádů jako třeba na šifrovačku
            - nezapojí se noví nebo méně sociální lidé
            - a distanční prvek toto jen zhorší
    - miniloď pro minitým (2-3 lidi)
        - asi se hodí na early playable prototype
        - trochu použitelnější než střední týmy
    - sám si myslím, že pro distanční jednorázovou hru jsou nejlepší obří relativně anonymní týmy
    - pro sous / jen tak na hraní jsou dobré spíš malé a střední týmy
    - asi cheme být v tomto ohledu scalable
- hra se odehrává na 2D ploše
    - střely i lodě se v něm pohybují
    - možný systém gravitace okolo "planet"
    - alternativně lze použít FTL model bez pohybu a bez prostoru, kde "pohyb" jen zvyšuje šanci, že nepřítel mine
- lodě spolu bojují
    - umožňujeme protivníkovi reagovat
    - asi vyjdeme z mechanik FTL (štíty, rakety, lasery)
    - alternativně: zbraně targetují konkrétní místnosti/systémy jako ve FTL
- systémy je třeba opravovat?
- lodě mohou mít různé vybavení
    - jdou randomizovat/customizovat
    - zajímavější pro opakované hraní

## Strategická + resource management vrstva

- sbírání zdrojů, těžení asteriodů, lootování vraků lodí atd.
- jak zapadá do minimálního prototypu? distanční hry? celotáborovky?
- pokud loď ovládá tým, tak i nebojové situace musí vyžadovat nějakou spolupráci
    - jinak se nemalá část týmu nudí

## Systémy

- jeden systém pro jednoho hráče
- alternativně víc systémů pro jednoho hráče
- nálož mechanik/ovládání pro jednoho hráče:
    - zvládnutelně malá, ale zajímavě velká
    - možná by mohla být scalable - pro první hru jednoduchá, pak zajímavější
    - např. jenom zapínat a vypínat štíty pro konkrétní směr by byla nuda
    - ale ovládat celý reaktor ze SA by bylo moc složité
    - **tohle je třeba dobře vymyslet**
- po většinu hry nebudou potřeba všechny systémy naráz -> někteří lidé se mohou nudit
    - ledaže dovolíme přejít do jiné místnosti
- chceme ale stále omezit přístup různých lidí k různým systémům
    - jinak možná přijdeme o komunikaci
    - např. pokud zbraně potřebují víc energie a člověk si může prostě přejít k reaktoru a vzít si ji, tak to udělá a nebude se namáhat s někým mluvit
    - "Pusťte mě k tomu, udělám to sám!"
- jak skloubit přechod jednoho člověka mezi systémy s omezeným přistupem k systémům?
    - kdo ovládá jaký systém by se dalo implementovat virtuální postavičkou co chodí po virtuální lodi jako posádka v FTL

## Konkrétní systémy

### Energie/reaktor

- není zdaleka dost energie pro všechno, jinak by to byla nuda

### Motory/řízení letu

- pohyb na 2D ploše
- úhybné manévry atd.
- pohyb musí být dost omezený/pomalý na to:
    - aby lodě šlo stále nějak rozumně trefovat
    - aby šlo loď vůbec nějak rozumně ovládat

### Zbraně

- copypaste z FTL:
    - laser
        - instantní a nekonečně přesný, ale neprojde žádným štítem
    - rakety
        - pomalé, projdou štítem
        - naváděné?
        - je jich omezený počet?
    - plasma
        - rychlejší než rakety ale stále pomalé, zničí vrstvu štítu
        - pro snadnější míření vystřelí rojnici mnoha projektilů?
- miny
    - straší někde na herní ploše
    - relevantní jen když se lodě pohybují po ploše
    - teleportační alternativa z FTL?
- gauss cannon
    - vystřelí velmi rychlý (a silný?) projektil
    - pošle vlastní loď dozadu
    - nějaký dlouhý buildup/cooldown?

### Radar

- vidí cizí lodě, střely atd.
- mohou to být dva systémy
    - kde jeden vidí ve viditelném světle a druhý v něčem obskurním
    - a vidí různé typy věcí
- konkrétně:
    - IR kamery
        - vidí teplo z motorů a zbraní cizích lodí
        - vidí projektily (co vydávají teplo)
        - nevidí moc dobře kolize
    - radar
        - vidí kolize, lodě, velké projektily
        - je ale aktivní, detekuje a je detekován cizími radary
- stealth
    - ve hře se dá schovávat, ale explicitně na to neupozzorňujeme
    - na "aktivnost" radaru explicitně neupozorňujeme
    - pokud vypnou radar, motory, zbraně, tak nepůjdou vidět pro cizí lodi
    - musí si ale vystačit s IR radarem

### Štíty

- směrové
- nelze mít aktivní všechny naráz

### "Okno"

- vidí kousek zorného pole
- záloha za vypadlý radar

### Podpůrné

- podpora života: když chcípne tak chcípnete taky
- autorepair: opravuje ostatní
- možno rozdělit mezi ostatní místnosti jako "sekundární systémy"

### "Hacking"

- umožňuje dočasně ovládnout kus nepřátelské lodi?
- minihra mezi loďmi, kdo vyhrává, vidí na grafíky nepřílete?

### Další systémy z FTL

- https://ftl.fandom.com/wiki/Systems
- senzory - vidí do vlastní i nepřátelské lodi
- dvěře - relevantní jen pro invaze a kontrolu ohně
- teleportér - relevantní jen pro invaze posádkou
- medbay
- klonování - invaze
- mind control - divnější hacking co ovládá posádku
- pilotování (zde sloučeno s motory)
- drony - alternativa ke zbraním
- O2 - zde podora života
- backup battery - dočasná extra energie
- cloaking - na chvíli na loď nejde útočit, zvýší evasion o hodně, i přes 100% (takže všechny existující střely minou)

## Legenda

- lodě:
    - sousové/kspí:
        - UFRS Backspace (sksp2014)
        - KSP Hipporion (sksp2019)
        - "Lucciola"? (sksp2020)
        - "Alzaloď"? (jksp2019)
- alternativní zalegendění
    - námořní lodě
    - ponorky
    - nějaké divné scifi tanky
- asi by šly vytvořit nějaké plakáty na dřívější propagaci ve stylu mafie
- chceme *nějaký* kontext pro souboje

## Problémy

- zalegendit
- vymyslet tomu dobré jméno
- bude fungovat týmová komunikace mezi pár náhodnými účastníky?
- software bude nekonečně složitý
    - cheme aby byl scalable a aby šli kombinovat různé systémy
- jak skloubit přechod jednoho člověka mezi systémy s omezeným přistupem k systémům?
- pokud by se to hrálo na sousu v jedné místnosti, je triviální si hlědět na monitory
    - snižuje to míru potřebné komunikace
    - vadí to?
    - a budou to opravdu dělat, když potřebují hledět i na vlastní monitor?

## Minimální prototyp

- systémy pohybu, zbraní, štítů, radaru?
- je něco takového vůbec zábava?
    - a na jak dlouho?
- člověk 1:
    - motory
    - radar
- člověk 2:
    - zbraně
    - IR kamery
- člověk 3
    - štíty
    - reaktor?
    - auto repair?

## Balance notes

- rock-paper-scissors
- různé strategie jsou různě těžké (ty silnější jsou náročnější na komunikaci)
- playtestovat s různými lidmi (někým, kdo už hru zná, i s úplnými nováčky)
- pozor na dominantní strategie, nechceme je
- low risk + low reward vs high risk+reward
- prokládat náhodu se skillem (Lens of skill vs chance)
- pro celotáborovou variantu by šlo kombinovat pvp s kooperačními misemi, kde musí dvě nebo víc účastníkolodí spolupracovat
- TODO: Balance  Type  8:  Rewards
